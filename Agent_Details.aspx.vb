﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class DetailsPort_Admin_Agent_Details
    Inherits System.Web.UI.Page

    Private STDom As New SqlTransactionDom
    Private ST As New SqlTransaction
    Dim AgncyName As String
    Dim objSql As New SqlTransactionNew
    Dim objSMSAPI As New SMSAPI.SMS
    Dim dtmodule As New DataTable
    Public Property AgentDt() As DataTable
        Get
            Return DirectCast(Session("AgentDt"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            Session("AgentDt") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Try
            CType(Page.Master.FindControl("lblBC"), Label).Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Profile </a><a class='current' href='#'>Brand Details</a>"
            If Session("UID") = "" Or Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx")
            End If
            'If Session("User_Type").ToString().ToUpper() <> "ADMIN" And Session("User_Type").ToString().ToUpper() <> "ACC" And Session("User_Type").ToString().ToUpper() <> "DI" Then
            '    Response.Redirect("~/Login.aspx")

            'End If

            If Not IsPostBack Then



                Dim msg As String = ""

                DropDownListType.AppendDataBoundItems = True

                DropDownListType.DataSource = GroupTypeMGMT("", "", "MultipleSelect", msg)
                DropDownListType.DataTextField = "GroupType"
                DropDownListType.DataValueField = "GroupType"
                DropDownListType.DataBind()


                If Session("UserType") = "DI" Then
                    tr_AgentType.Visible = False
                    ''tr_GroupType.Visible = False
                    tr_SalesPerson.Visible = False
                    tr_ddlSalesPerson.Visible = False
                    If (Request("DIAG") = 1) Then
                        GridView1.Columns(0).Visible = False
                    Else
                        GridView1.Columns(0).Visible = True
                    End If
                    td_SBS.Visible = True
                    td_ddlSBS.Visible = True
                Else
                    tr_AgentType.Visible = True
                    ''tr_GroupType.Visible = True
                    tr_SalesPerson.Visible = True
                    tr_ddlSalesPerson.Visible = True
                End If
                DropDownListSalesPerson.DataSource = STDom.GetSalesRef().Tables(0)
                DropDownListSalesPerson.DataTextField = "EmailId"
                DropDownListSalesPerson.DataValueField = "EmailId"
                DropDownListSalesPerson.DataBind()
            End If

            'Dim dtmodul As New DataTable

            td_SBS.Visible = True
            tr_SalesPerson.Visible = False

            dtmodule = STDom.GetModuleAccessDetails(Session("UID"), MODULENAME.AGENCYSEARCH.ToString()).Tables(0)
            If (dtmodule.Rows.Count > 0) Then
                If (Convert.ToBoolean(dtmodule.Rows(0)("STATUS").ToString()) = True) Then
                    export.Visible = False
                End If
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try




    End Sub
    Protected Sub grd_P_IntlDiscount_RowEditing(ByVal sender As Object, ByVal e As GridViewEditEventArgs)
        If Session("TypeID").ToString() = "AD2" Then
            'btn_update.Style(HtmlTextWriterStyle.Display) = "none"
            'txt_saleref.Enabled = True
            ddl_type.Enabled = True
            ddl_activation.Enabled = True
            ddl_TicketingActiv.Enabled = True
            txt_pwd.Enabled = True
            lbl_currentPassMsg.Visible = True
            lbl_currentPassword.Visible = True
            txt_AgencyName.Enabled = True
            btn_update.Visible = True
        ElseIf Session("TypeID").ToString() = "AD1" Then
            'txt_saleref.Enabled = True
            ddl_type.Enabled = True
            ddl_activation.Enabled = True
            ddl_TicketingActiv.Enabled = True
            btn_update.Visible = True
            td_pwd.Visible = True
            txt_pwd.Enabled = True
            lbl_currentPassMsg.Visible = True
            lbl_currentPassword.Visible = True
        ElseIf Session("TypeID").ToString() = "AC1" OrElse Session("User_Type").ToString().ToUpper() = "EXEC" Then
            'txt_saleref.Enabled = True
            ddl_type.Enabled = True
            ddl_activation.Enabled = True
            ddl_TicketingActiv.Enabled = True
            btn_update.Visible = True
        ElseIf Session("TypeID").ToString() <> "AD1" OrElse Session("TypeID").ToString() = "AD2" OrElse Session("TypeID").ToString() = "AC1" OrElse Session("User_Type").ToString().ToUpper() = "EXEC" Then
            'txt_saleref.Enabled = True
            ddl_type.Enabled = True
            ddl_activation.Enabled = True
            btn_update.Visible = True
            ddl_TicketingActiv.Enabled = True
        End If
        If Session("TypeID").ToString() = "AD1" OrElse Session("TypeID").ToString() = "AD2" OrElse Session("User_Type").ToString().ToUpper() = "EXEC" Then
            txt_Address.Enabled = True
            txt_City.Enabled = True
            txt_Country.Enabled = True
            txt_State.Enabled = True
            txt_title.Enabled = True
            txt_Fname.Enabled = True
            txt_Lname.Enabled = True
            txt_Fax.Enabled = True
            txt_zip.Enabled = True
            txt_Mobile.Enabled = True
            txt_Email.Enabled = True
            txt_Pan.Enabled = True
            TxtNameOnPancard.Enabled = True

        End If

        If Session("User_Type").ToString().ToUpper() = "SALES" Then
            Sales_DDL.Enabled = False
            ddl_type.Enabled = False
            ddl_activation.Enabled = False
            btn_update.Visible = False
            ddl_TicketingActiv.Enabled = False
        End If








        Dim dtSales As New DataTable
        dtSales = STDom.GetSalesRef().Tables(0)
        Sales_DDL.AppendDataBoundItems = True
        dtSales = STDom.GetSalesRef().Tables(0)
        Sales_DDL.Items.Clear()
        Sales_DDL.Items.Insert(0, "Select Sales Ref.")
        Sales_DDL.DataSource = dtSales
        Sales_DDL.DataTextField = "Name"
        Sales_DDL.DataValueField = "EmailId"
        Sales_DDL.DataBind()

        Dim dttype As New DataTable
        dttype = STDom.GetAgentType().Tables(0)
        If dttype.Rows.Count > 0 Then
            ddl_type.AppendDataBoundItems = True
            ddl_type.Items.Clear()
            ddl_type.Items.Insert(0, "--Select Type--")
            ddl_type.DataSource = dttype
            ddl_type.DataTextField = "GroupType"
            ddl_type.DataValueField = "GroupType"
            ddl_type.DataBind()
        End If
        Dim dt As New DataTable
        Dim txt As Label = TryCast(GridView1.Rows(e.NewEditIndex).FindControl("Label1"), Label)
        Dim name As String = txt.Text

        dt = ST.GetAgencyDetails(name).Tables(0)
        AgentDt = dt
        txt_AgencyName.Text = dt.Rows(0)("Agency_name").ToString
        'AgncyName = Convert.ToString(dt.Rows(0)("Agency_name"))
        td_AgentID.InnerText = dt.Rows(0)("User_Id").ToString

        txt_Address.Text = dt.Rows(0)("Address").ToString
        txt_City.Text = dt.Rows(0)("city").ToString
        txt_Country.Text = dt.Rows(0)("country").ToString
        txt_State.Text = dt.Rows(0)("State").ToString
        'td_CrLimit.InnerText = dt.Rows(0)("Crd_Limit").ToString
        td_CrLimit.InnerText = dt.Rows(0)("Balance").ToString
        td_LTDate.InnerText = dt.Rows(0)("Crd_Trns_Date").ToString
        txt_pwd.Text = dt.Rows(0)("pwd").ToString
        lbl_currentPassword.Text = dt.Rows(0)("pwd").ToString
        txt_title.Text = dt.Rows(0)("Title").ToString
        txt_Fname.Text = dt.Rows(0)("FName").ToString
        txt_Lname.Text = dt.Rows(0)("LName").ToString
        txt_Fax.Text = dt.Rows(0)("Fax_no").ToString
        txt_zip.Text = dt.Rows(0)("zipcode").ToString
        txt_Mobile.Text = dt.Rows(0)("mobile").ToString
        txt_Email.Text = dt.Rows(0)("email").ToString
        txt_Pan.Text = dt.Rows(0)("PanNo").ToString
        TxtNameOnPancard.Text = Convert.ToString(dt.Rows(0)("NamePanCard"))

        OPTLOGIN.Checked = Convert.ToBoolean(dt.Rows(0)("OTPLoginStatus").ToString)
        EXPPASS.Checked = Convert.ToBoolean(dt.Rows(0)("PasswordExpMsg").ToString)


        txt_zip.Text = dt.Rows(0)("zipcode").ToString
        txt_Mobile.Text = dt.Rows(0)("mobile").ToString
        txt_Email.Text = dt.Rows(0)("email").ToString
        txt_Pan.Text = dt.Rows(0)("PanNo").ToString
        'txt_pwd.Text = dt.Rows(0)("Password").ToString
        ddl_type.SelectedValue = dt.Rows(0)("Agent_Type").ToString


        'txt_saleref.Text = dt.Rows(0)("SalesExecID").ToString
        If String.IsNullOrEmpty(Convert.ToString(dt.Rows(0)("SalesExecID"))) Then
            Sales_DDL.SelectedValue = "Select Sales Ref."
        Else
            Sales_DDL.SelectedValue = Convert.ToString(dt.Rows(0)("SalesExecID"))
        End If
        'ddl_activation.SelectedValue = dt.Rows(0)("Activation").ToString
        'ddl_TicketingActiv.SelectedValue = dt.Rows(0)("Online_Tkt").ToString
        'TxtAgentCredit.Text = dt.Rows(0)("Credit").ToString
        'txtNewPassword.Text = dt.Rows(0)("New Password").ToString
        'txtConfirmPassword.Text = dt.Rows(0)("Confirm Password").ToString
        'td_tds.InnerText = dt.Rows(0)("TDS").ToString


        lblAgentLimit.Text = dt.Rows(0)("AgentLimit").ToString
        lblDueAmount.Text = dt.Rows(0)("DueAmount").ToString


        If Session("User_Type").ToString() = "DI" Then
            ddl_activation.Enabled = True
            ddl_TicketingActiv.Enabled = True
            btn_update.Visible = True
            td_pwd.Visible = False
            txt_pwd.Visible = False
            lbl_currentPassMsg.Visible = False
            lbl_currentPassword.Visible = False
        End If

        Dim dtmodule As New DataTable
        dtmodule = STDom.GetModuleAccessDetails(Session("UID"), MODULENAME.AGENCYDETAILS.ToString()).Tables(0)
        If (dtmodule.Rows.Count > 0) Then
            For Each dr As DataRow In dtmodule.Rows
                If (dr("MODULETYPE").ToString().ToUpper() = MODULETYPE.UPDATE.ToString().ToUpper() AndAlso Convert.ToBoolean(dr("STATUS").ToString()) = True) Then
                    btn_update.Visible = False
                    'End If
                ElseIf (dr("MODULETYPE").ToString().ToUpper() = MODULETYPE.PASSWORD.ToString().ToUpper() AndAlso Convert.ToBoolean(dr("STATUS").ToString()) = True) Then
                    td_pwd.Visible = False
                    txt_pwd.Visible = False
                    lbl_currentPassMsg.Visible = False
                    lbl_currentPassword.Visible = False
                ElseIf (dr("MODULETYPE").ToString().ToUpper() = "TYPE" AndAlso Convert.ToBoolean(dr("STATUS").ToString()) = True) Then
                    ddl_type.Enabled = False
                    'ElseIf (dr("MODULETYPE").ToString().ToUpper() = "SALESREF" AndAlso Convert.ToBoolean(dr("STATUS").ToString()) = True) Then
                    '    txt_saleref.Enabled = False
                ElseIf (dr("MODULETYPE").ToString().ToUpper() = "ACTIVATION" AndAlso Convert.ToBoolean(dr("STATUS").ToString()) = True) Then
                    ddl_activation.Enabled = False
                ElseIf (dr("MODULETYPE").ToString().ToUpper() = "TKTACTIVATION" AndAlso Convert.ToBoolean(dr("STATUS").ToString()) = True) Then
                    ddl_TicketingActiv.Enabled = False
                End If
            Next
        End If
        ag_detail.Style.Add("display", "none")
        hidden_fld.Style.Add("display", "block")



        'HdnId.Value = ID
        'BindCredential(name)
    End Sub
    'Private Sub BindCredential(ByVal Id As String)
    '    Dim dt As DataTable = New DataTable()

    '    Try

    '        If Not String.IsNullOrEmpty(Convert.ToString(Id)) Then
    '            dt = GetCredential(Convert.ToInt32(Id))

    '            If dt.Rows.Count > 0 Then
    '                txt_Address.Text = Convert.ToString(dt.Rows(0)("Address"))
    '                txt_AgencyName.Text = Convert.ToString(dt.Rows(0)("BrandName"))
    '                txt_City.Text = Convert.ToString(dt.Rows(0)("City"))
    '                txt_zip.Text = Convert.ToString(dt.Rows(0)("Zip Code"))
    '                txt_Country.Text = Convert.ToString(dt.Rows(0)("Country"))
    '                txt_title.Text = Convert.ToString(dt.Rows(0)("Name"))
    '                txt_Fname.Text = Convert.ToString(dt.Rows(0)("First Name"))
    '                txt_Lname.Text = Convert.ToString(dt.Rows(0)("Last Name"))
    '                txt_Mobile.Text = Convert.ToString(dt.Rows(0)("Mobile"))
    '                txt_Email.Text = Convert.ToString(dt.Rows(0)("Email"))
    '                TxtNameOnPancard.Text = Convert.ToString(dt.Rows(0)("Pan Name"))
    '                txt_Pan.Text = Convert.ToString(dt.Rows(0)("Pan No"))
    '                txt_Fax.Text = Convert.ToString(dt.Rows(0)("Fax No"))

    '                OPTLOGIN.Text = Convert.ToString(dt.Rows(0)("Mobile"))
    '                txt_Email.Text = Convert.ToString(dt.Rows(0)("Email"))
    '                Dim Status As String = Convert.ToString(dt.Rows(0)("Status")).ToLower()
    '                Dim Provider As String = Convert.ToString(dt.Rows(0)("Provider"))

    '                If Provider = "1G" OrElse Provider = "1GINT" Then
    '                    CorporateID.Visible = True
    '                    CarrierAcc.Visible = True
    '                    ResultFrom.Visible = False
    '                    Url.Visible = False
    '                    LoginID.Visible = False
    '                    LoginPassword.Visible = False
    '                Else
    '                    CorporateID.Visible = True
    '                    CarrierAcc.Visible = False
    '                    ResultFrom.Visible = True
    '                    Url.Visible = False
    '                    LoginID.Visible = False
    '                    LoginPassword.Visible = False
    '                End If

    '                If Status = "true" Then
    '                    DdlStatus.SelectedValue = "true"
    '                ElseIf Status = "false" Then
    '                    DdlStatus.SelectedValue = "false"
    '                End If
    '            Else
    '                BtnSubmit.Visible = False
    '            End If
    '        End If

    '    Catch ex As Exception
    '        clsErrorLog.LogInfo(ex)
    '    End Try
    'End Sub




    Protected Sub btn_update_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_update.Click
        Try
            Dim SalesId As String = Sales_DDL.SelectedValue
            If Sales_DDL.SelectedValue = "Select Sales Ref." Then
                SalesId = ""
            End If


            Dim otp As String = "False"
            If (OPTLOGIN.Checked = True) Then
                otp = "True"
            End If

            Dim EXPP As String = "False"
            If (EXPPASS.Checked = True) Then
                EXPP = "True"
            End If





            'updating passsword
            Dim flag As Boolean = False
            If txtNewPassword.Text.Trim <> "" Then
                STDom.UpdateAgentTypeSalesRef(Request("AgentID"), SalesId, ddl_type.SelectedValue, ddl_activation.SelectedValue, ddl_TicketingActiv.SelectedValue, txt_Address.Text.Trim, txt_City.Text.Trim, txt_State.Text.Trim, txt_Country.Text.Trim, txt_zip.Text.Trim, txt_Fname.Text.Trim, txt_Lname.Text.Trim, txt_Mobile.Text.Trim, txt_Email.Text.Trim, txt_Fax.Text.Trim, txt_Pan.Text.Trim, txt_title.Text.Trim, txt_AgencyName.Text.Trim, txtNewPassword.Text.Trim, TxtNameOnPancard.Text.Trim, otp, EXPP)
                flag = True
                SendSms(txt_Fname.Text.Trim, txt_Lname.Text.Trim, td_AgentID.InnerText.ToString(), txt_Mobile.Text.Trim, txtNewPassword.Text.Trim, txt_AgencyName.Text.Trim)
                SendEmail(td_AgentID.InnerText.ToString(), txtNewPassword.Text.Trim, txt_Email.Text.Trim, txt_AgencyName.Text.Trim)
                ScriptManager.RegisterStartupScript(Me, Page.[GetType](), "key", "MyFunc()", True)
                'Else
                '    ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('old password not match record not updated');", True)
            Else
                STDom.UpdateAgentTypeSalesRef(Request("AgentID"), SalesId, ddl_type.SelectedValue, ddl_activation.SelectedValue, ddl_TicketingActiv.SelectedValue, txt_Address.Text.Trim, txt_City.Text.Trim, txt_State.Text.Trim, txt_Country.Text.Trim, txt_zip.Text.Trim, txt_Fname.Text.Trim, txt_Lname.Text.Trim, txt_Mobile.Text.Trim, txt_Email.Text.Trim, txt_Fax.Text.Trim, txt_Pan.Text.Trim, txt_title.Text.Trim, txt_AgencyName.Text.Trim, txt_pwd.Text.Trim, TxtNameOnPancard.Text.Trim, otp, EXPP)
                flag = True
                ScriptManager.RegisterStartupScript(Me, Page.[GetType](), "key", "MyFunc()", True)
            End If
            'end password updation


            'STDom.UpdateAgentTypeSalesRef(Request("AgentID"), txt_saleref.Text.Trim, ddl_type.SelectedValue, ddl_activation.SelectedValue, ddl_TicketingActiv.SelectedValue, txt_Address.Text.Trim, txt_City.Text.Trim, txt_State.Text.Trim, txt_Country.Text.Trim, txt_zip.Text.Trim, txt_Fname.Text.Trim, txt_Lname.Text.Trim, txt_Mobile.Text.Trim, txt_Email.Text.Trim, txt_Fax.Text.Trim, txt_Pan.Text.Trim, txt_title.Text.Trim, txt_AgencyName.Text.Trim, txt_pwd.Text.Trim)

            'previous--
            'STDom.UpdateAgentTypeSalesRef(Request("AgentID"), SalesId, ddl_type.SelectedValue, ddl_activation.SelectedValue, ddl_TicketingActiv.SelectedValue, txt_Address.Text.Trim, txt_City.Text.Trim, txt_State.Text.Trim, txt_Country.Text.Trim, txt_zip.Text.Trim, txt_Fname.Text.Trim, txt_Lname.Text.Trim, txt_Mobile.Text.Trim, txt_Email.Text.Trim, txt_Fax.Text.Trim, txt_Pan.Text.Trim, txt_title.Text.Trim, txt_AgencyName.Text.Trim, txt_pwd.Text.Trim)
            '---
            If flag = True Then
                Dim upXml As String = UpdationXml()
                If upXml <> "<Changes></Changes>" Then
                    InsertAgencyDetailsUpdationLog(Request("AgentID"), Session("UID"), Request.ServerVariables("REMOTE_ADDR"), upXml)
                End If
            End If
            'ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Updated Sucessfully');", True)


        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
           
        End Try
          End Sub




    Private Sub SendEmail(ByVal UserId As String, ByVal UserNewPassword As String, ByVal UserEMail As String, ByVal agency As String)
        Try
            Dim mailDataTable As New DataTable
            'mailDataTable = STDom.GetMailingDetails(MAILING.REGISTRATION_AGENT.ToString().Trim(), "").Tables(0)
            mailDataTable = STDom.GetMailingDetails(MAILING.RESETPWD.ToString().Trim(), "").Tables(0)
            Dim messageBody As String = ""
            messageBody += "<html><head><title></title><meta http-equiv=Content-Type content=text/html; charset=iso-8859-1></head><body>"
            messageBody += "<p><font face=""Verdana, Arial, Helvetica, sans-serif""><strong>Dear " & agency & "</strong></font></p>"
            messageBody += "<p><font face=""Verdana, Arial, Helvetica, sans-serif"">Your password has been changed successfully, your new login credential is-</font></p>"
            messageBody += "<p><font face=""Verdana, Arial, Helvetica, sans-serif"">User Id:</font><font color='blue'>" & UserId & "</font><br/>New Password:<font color='Blue'>" & UserNewPassword & "</font></p>"
            messageBody += "<p><font face=""Verdana, Arial, Helvetica, sans-serif"">We thank you to connect with <strong>" & mailDataTable.Rows(0)("Body").ToString().Trim & ",The Only Online web Access for Travel Agents in India.</strong></font></p>"
            messageBody += "<p><font face=""Verdana, Arial, Helvetica, sans-serif"">If you need immediate assistance or have any questions, concerns or suggestions, please do not hesitate to email us at <a href=""mailto:" & mailDataTable.Rows(0)("Body").ToString().Trim() & """>" & mailDataTable.Rows(0)("Body").ToString().Trim() & "</a>.</font></p>"
            messageBody += "<p><font face=""Verdana, Arial, Helvetica, sans-serif"">Regards,</font></p>"
            messageBody += "<p><font face=""Verdana, Arial, Helvetica, sans-serif""><strong>" & mailDataTable.Rows(0)("REGARDS").ToString() & "</strong><br />"
            messageBody += "</body></html>"

            Try
                If (mailDataTable.Rows.Count > 0) Then
                    Dim status As Boolean = False
                    status = Convert.ToBoolean(mailDataTable.Rows(0)("Status").ToString())
                    If status = True Then
                        Dim effect As Integer = STDom.SendMail(UserEMail, mailDataTable.Rows(0)("MAILFROM").ToString(), mailDataTable.Rows(0)("BCC").ToString(), mailDataTable.Rows(0)("CC").ToString(), mailDataTable.Rows(0)("SMTPCLIENT").ToString(), mailDataTable.Rows(0)("UserId").ToString(), mailDataTable.Rows(0)("Pass").ToString(), messageBody, mailDataTable.Rows(0)("SUBJECT").ToString() + " for fly wid us.", "")
                    End If
                End If
            Catch ex As Exception
                clsErrorLog.LogInfo(ex)
            End Try
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Private Sub SendSms(ByVal userFirstName As String, ByVal userLastName As String, ByVal userId As String, ByVal mobileNumber As String, ByVal newPassword As String, ByVal agencyName As String)
        Try

            Dim status As String = ""
            Dim smsText As String = ""
            Try
                Dim fullName As String = userFirstName + " " + userLastName
                Dim smsDatatable As DataTable
                smsDatatable = ST.SmsCredential("RESETPWD").Tables(0)
                'smsBody = "Dear " + fullName + ",Your password has been changed successfully.Your login credential is-User Id:" + userId + "& Password is:" + newPassword + " Thanks."
                If (smsDatatable.Rows.Count > 0) AndAlso smsDatatable.Rows(0)("Status") = True Then
                    status = objSMSAPI.SendSmsForResetPassword(fullName, userId, newPassword, mobileNumber, smsText, smsDatatable)
                    objSql.SmsLogDetails(userId, mobileNumber, smsText, status)
                End If
            Catch ex As Exception
            End Try
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub

    Public Function UpdationXml() As String

        Dim Changesxml As String = "<Changes>"


        If txt_AgencyName.Text <> AgentDt.Rows(0)("Agency_name").ToString Then
            Changesxml = Changesxml & "<Agency_name><New>" & txt_AgencyName.Text & "</New><Old>" & AgentDt.Rows(0)("Agency_name").ToString & "</Old></Agency_name>"

        End If




        If td_AgentID.InnerText <> AgentDt.Rows(0)("User_Id").ToString Then
            Changesxml = Changesxml & "<User_Id><New>" & td_AgentID.InnerText & "</New><Old>" & AgentDt.Rows(0)("User_Id").ToString & "</Old></User_Id>"

        End If


        If txt_Address.Text <> AgentDt.Rows(0)("Address").ToString Then
            Changesxml = Changesxml & "<Addrmess><New>" & txt_Address.Text & "</New><Old>" & AgentDt.Rows(0)("Address").ToString & "</Old></Address>"

        End If

        If txt_City.Text <> AgentDt.Rows(0)("city").ToString Then
            Changesxml = Changesxml & "<City><New>" & txt_City.Text & "</New><Old>" & AgentDt.Rows(0)("city").ToString & "</Old></City>"

        End If

        If txt_Country.Text <> AgentDt.Rows(0)("country").ToString Then
            Changesxml = Changesxml & "<Country><New>" & txt_Country.Text & "</New><Old>" & AgentDt.Rows(0)("country").ToString & "</Old></Country>"

        End If

        If txt_State.Text <> AgentDt.Rows(0)("State").ToString Then
            Changesxml = Changesxml & "<State><New>" & txt_State.Text & "</New><Old>" & AgentDt.Rows(0)("State").ToString & "</Old></State>"

        End If

        'If td_CrLimit.InnerText <> AgentDt.Rows(0)("Crd_Limit").ToString Then
        '    Changesxml = Changesxml & "<Crd_Limit><New>" & td_CrLimit.InnerText & "</New><Old>" & AgentDt.Rows(0)("Crd_Limit").ToString & "</Old></Crd_Limit>"
        'End If

        'If td_LTDate.InnerText <> Convert.ToString(AgentDt.Rows(0)("Crd_Trns_Date")) Then
        '    Changesxml = Changesxml & "<Crd_Trns_Date><New>" & td_LTDate.InnerText & "</New><Old>" & Convert.ToString(AgentDt.Rows(0)("Crd_Trns_Date")) & "</Old></Crd_Trns_Date>"
        'End If

        'If txt_pwd.Text <> AgentDt.Rows(0)("PWD").ToString Then
        '    Changesxml = Changesxml & "<Pwd><New>" & txt_pwd.Text & "</New><Old>" & AgentDt.Rows(0)("PWD").ToString & "</Old></Pwd>"
        'End If

        If txtNewPassword.Text <> "" And txtNewPassword.Text <> AgentDt.Rows(0)("PWD").ToString Then
            Changesxml = Changesxml & "<Pwd><New>" & txtNewPassword.Text.Trim & "</New><Old>" & AgentDt.Rows(0)("PWD").ToString & "</Old></Pwd>"
        End If


        If txt_title.Text <> AgentDt.Rows(0)("Title").ToString Then
            Changesxml = Changesxml & "<Title><New>" & txt_title.Text & "</New><Old>" & AgentDt.Rows(0)("Title").ToString & "</Old></Title>"
        End If


        If txt_Fname.Text <> AgentDt.Rows(0)("FName").ToString Then
            Changesxml = Changesxml & "<FName><New>" & txt_Fname.Text & "</New><Old>" & AgentDt.Rows(0)("FName").ToString & "</Old></FName>"

        End If

        If txt_Lname.Text <> AgentDt.Rows(0)("LName").ToString Then
            Changesxml = Changesxml & "<LName><New>" & txt_Lname.Text & "</New><Old>" & AgentDt.Rows(0)("LName").ToString & "</Old></LName>"

        End If

        'If txt_Fax.Text <> AgentDt.Rows(0)("Fax_no").ToString Then
        '    Changesxml = Changesxml & "<Fax_no><New>" & txt_Fax.Text & "</New><Old>" & AgentDt.Rows(0)("Fax_no").ToString & "</Old></Fax_no>"

        'End If

        If txt_zip.Text <> AgentDt.Rows(0)("zipcode").ToString Then
            Changesxml = Changesxml & "<Zipcode><New>" & txt_zip.Text & "</New><Old>" & AgentDt.Rows(0)("zipcode").ToString & "</Old></Zipcode>"

        End If

        If txt_Mobile.Text <> AgentDt.Rows(0)("mobile").ToString Then
            Changesxml = Changesxml & "<Mobile><New>" & txt_Mobile.Text & "</New><Old>" & AgentDt.Rows(0)("mobile").ToString & "</Old></Mobile>"

        End If

        If txt_Email.Text <> AgentDt.Rows(0)("email").ToString Then
            Changesxml = Changesxml & "<Email><New>" & txt_Email.Text & "</New><Old>" & AgentDt.Rows(0)("email").ToString & "</Old></Email>"

        End If

        If txt_Pan.Text <> AgentDt.Rows(0)("PanNo").ToString Then
            Changesxml = Changesxml & "<PanNo><New>" & txt_Pan.Text & "</New><Old>" & AgentDt.Rows(0)("PanNo").ToString & "</Old></PanNo>"

        End If
        If TxtNameOnPancard.Text <> AgentDt.Rows(0)("NamePanCard").ToString Then
            Changesxml = Changesxml & "<NamePanCard><New>" & TxtNameOnPancard.Text & "</New><Old>" & AgentDt.Rows(0)("NamePanCard").ToString & "</Old></NamePanCard>"

        End If

        If td_tds.InnerText <> AgentDt.Rows(0)("TDS").ToString Then
            Changesxml = Changesxml & "<TDS><New>" & td_tds.InnerText & "</New><Old>" & AgentDt.Rows(0)("TDS").ToString & "</Old></TDS>"

        End If

        Dim SalesId As String = Sales_DDL.SelectedValue
        If Sales_DDL.SelectedValue = "Select Sales Ref." Then
            SalesId = ""
        End If
        If Sales_DDL.SelectedValue <> AgentDt.Rows(0)("SalesExecID").ToString Then
            Changesxml = Changesxml & "<SalesExecID><New>" & SalesId & "</New><Old>" & AgentDt.Rows(0)("SalesExecID").ToString & "</Old></SalesExecID>"

        End If

        If ddl_type.SelectedValue <> AgentDt.Rows(0)("Agent_Type").ToString Then
            Changesxml = Changesxml & "<Agent_Type><New>" & ddl_type.SelectedValue & "</New><Old>" & AgentDt.Rows(0)("Agent_Type").ToString & "</Old></Agent_Type>"

        End If

        If ddl_activation.SelectedValue <> AgentDt.Rows(0)("Agent_Status").ToString Then
            Changesxml = Changesxml & "<Agent_Status><New>" & ddl_activation.SelectedValue & "</New><Old>" & AgentDt.Rows(0)("Agent_Status").ToString & "</Old></Agent_Status>"

        End If

        If ddl_TicketingActiv.SelectedValue <> AgentDt.Rows(0)("Online_Tkt").ToString Then
            Changesxml = Changesxml & "<Online_Tkt><New>" & ddl_TicketingActiv.SelectedValue & "</New><Old>" & AgentDt.Rows(0)("Online_Tkt").ToString & "</Old></Online_Tkt>"

        End If
        Changesxml = Changesxml & "</Changes>"
        UpdationXml = Changesxml

    End Function

    Public Function InsertAgencyDetailsUpdationLog(ByVal agencyId As String, ByVal UpdatedBy As String, ByVal ip As String, ByVal updatedFieldLog As String) As Integer

        Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString.ToString())
        Dim i As Integer = 0
        Try
            Dim cmd As New SqlCommand("Sp_Insert_Agency_Details_Updation_Log", con)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AgencyID", agencyId)
            cmd.Parameters.AddWithValue("@UpdatedBy", UpdatedBy)
            cmd.Parameters.AddWithValue("@IP", ip)
            cmd.Parameters.AddWithValue("@UpdatedFieldLog", updatedFieldLog)
            con.Open()
            cmd.ExecuteNonQuery()
            i = 1
            con.Close()
        Catch ex As SqlException
            'throw ex;
            ' ex.ToString();

        Finally
        End Try
        Return i
    End Function






    Public Function GroupTypeMGMT(ByVal type As String, ByVal desc As String, ByVal cmdType As String, ByRef msg As String) As DataTable
        Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
        Dim dt As New DataTable()
        Try

            con.Open()

            Dim cmd As New SqlCommand()

            cmd.CommandText = "usp_agentTypeMGMT"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 200).Value = type
            cmd.Parameters.Add("@desc", SqlDbType.VarChar, 500).Value = desc
            cmd.Parameters.Add("@cmdType", SqlDbType.VarChar, 50).Value = cmdType
            cmd.Parameters.Add("@msg", SqlDbType.VarChar, 500)
            cmd.Parameters("@msg").Direction = ParameterDirection.Output

            cmd.Connection = con
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            msg = cmd.Parameters("@msg").Value.ToString().Trim()



            con.Close()


        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
            con.Close()

        End Try
        Return dt
    End Function
    Protected Sub export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles export.Click
        Try
            Dim dtag As New DataSet

            Dim type As String = If(DropDownListType.SelectedValue = "Select", "", DropDownListType.SelectedValue)
            Dim saleId As String = If(DropDownListSalesPerson.SelectedValue = "Select", "", DropDownListSalesPerson.SelectedValue)
            Dim agencyId As String = If(Request("hidtxtAgencyName") = "Agency Name or ID", "", Request("hidtxtAgencyName").Trim)

            Dim FromDate As String
            Dim ToDate As String
            If [String].IsNullOrEmpty(Request("From")) Then
                FromDate = ""
            Else

                FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "/" + Strings.Left((Request("From")).Split(" ")(0), 2) + "/" + Strings.Right((Request("From")).Split(" ")(0), 4)
                FromDate = FromDate + " " + "12:00:00 AM"
            End If
            If [String].IsNullOrEmpty(Request("To")) Then
                ToDate = ""
            Else
                ToDate = Mid((Request("To")).Split(" ")(0), 4, 2) & "/" & Left((Request("To")).Split(" ")(0), 2) & "/" & Right((Request("To")).Split(" ")(0), 4)
                ToDate = ToDate & " " & "11:59:59 PM"
            End If
            Dim DiSearch As String = ""
            If (ddl_stock.SelectedValue = "ALL") Then
                DiSearch = "ALL"
            ElseIf (ddl_stock.SelectedValue = "STAG") Then
                DiSearch = "STAG"
            End If
            dtag = STDom.GetAgencyDetailsDynamic(agencyId, type, saleId, FromDate, ToDate, Session("UID"), Session("UserType"), DiSearch) 'ST.GetAgencyDetails(Request("hidtxtAgencyName")).Tables(0)

            dtag.Tables(0).Columns.Remove(dtag.Tables(0).Columns("Sec_Qes"))
            dtag.Tables(0).Columns.Remove(dtag.Tables(0).Columns("Sec_Ans"))
            dtag.Tables(0).Columns.Remove(dtag.Tables(0).Columns("User_Id"))
            dtag.Tables(0).Columns.Remove(dtag.Tables(0).Columns("ag_logo"))
            dtag.Tables(0).Columns.Remove(dtag.Tables(0).Columns("ExmptTdsLimit"))
            dtag.Tables(0).Columns.Remove(dtag.Tables(0).Columns("Name"))
            dtag.Tables(0).Columns.Remove(dtag.Tables(0).Columns("Crd_Limit"))
            'dtag.Tables(0).Columns.Remove(dtag.Tables(0).Columns("TicketID"))
            'dtag.Tables(0).Columns.Remove(dtag.Tables(0).Columns("AirLine"))




            STDom.ExportData(dtag)
        Catch ex As Exception

        End Try

    End Sub
    Public Sub Export_rep(ByVal fileName As String, ByVal gv As GridView)
        Try
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
            HttpContext.Current.Response.ContentType = "application/ms-excel"

            Using sw As New IO.StringWriter()
                Using htw As New HtmlTextWriter(sw)
                    ' Create a form to contain the grid 
                    Dim table As New Table()

                    ' add the header row to the table 
                    If gv.HeaderRow IsNot Nothing Then
                        PrepareControlForExport(gv.HeaderRow)
                        table.Rows.Add(gv.HeaderRow)
                    End If

                    ' add each of the data rows to the table 
                    For Each row As GridViewRow In gv.Rows
                        PrepareControlForExport(row)
                        table.Rows.Add(row)
                    Next

                    ' add the footer row to the table 
                    If gv.FooterRow IsNot Nothing Then
                        PrepareControlForExport(gv.FooterRow)
                        table.Rows.Add(gv.FooterRow)
                    End If

                    ' render the table into the htmlwriter 
                    table.RenderControl(htw)

                    ' render the htmlwriter into the response 
                    HttpContext.Current.Response.Write(sw.ToString())
                    HttpContext.Current.Response.[End]()
                End Using
            End Using
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Try
            For i As Integer = 0 To control.Controls.Count - 1
                Dim current As Control = control.Controls(i)
                If TypeOf current Is LinkButton Then
                    control.Controls.Remove(current)
                    control.Controls.AddAt(i, New LiteralControl(TryCast(current, LinkButton).Text))
                ElseIf TypeOf current Is ImageButton Then
                    control.Controls.Remove(current)
                    control.Controls.AddAt(i, New LiteralControl(TryCast(current, ImageButton).AlternateText))
                ElseIf TypeOf current Is HyperLink Then
                    control.Controls.Remove(current)
                    control.Controls.AddAt(i, New LiteralControl(TryCast(current, HyperLink).Text))
                ElseIf TypeOf current Is DropDownList Then
                    control.Controls.Remove(current)
                    control.Controls.AddAt(i, New LiteralControl(TryCast(current, DropDownList).SelectedItem.Text))
                ElseIf TypeOf current Is CheckBox Then
                    control.Controls.Remove(current)
                    ' control.Controls.AddAt(i, new LiteralControl(If(TryCast(current, CheckBox).Checked, "True", "False"))))
                End If
                If current.HasControls() Then
                    PrepareControlForExport(current)

                End If
            Next
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub



    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        Try
            GridView1.DataSource = Session("DATAVIEW")
            GridView1.PageIndex = e.NewPageIndex
            GridView1.DataBind()
            Session("DATAVIEW") = DirectCast(GridView1.DataSource, DataView)
            'BindData()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub




    Protected Sub GridView1_Sorting1(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles GridView1.Sorting

        Try
            Dim sortBy As String = Nothing

            If Session("ORDERBY").ToString() = "ASC" Then
                sortBy = "DESC"

            ElseIf Session("ORDERBY").ToString() = "DESC" Then
                sortBy = "ASC"
            End If

            Session("ORDERBY") = sortBy
            Dim dv As DataView = DirectCast(Session("DATAVIEW"), DataView)
            dv.Sort = Convert.ToString(e.SortExpression) & " " & sortBy
            GridView1.DataSource = dv
            GridView1.DataBind()
            Session("DATAVIEW") = DirectCast(GridView1.DataSource, DataView)
        Catch ex As Exception


        End Try


    End Sub

    Protected Sub btn_Search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Search.Click
        Try
            Dim dtag As New DataTable

            Dim type As String = If(DropDownListType.SelectedValue = "Select", "", DropDownListType.SelectedValue)
            Dim saleId As String = If(DropDownListSalesPerson.SelectedValue = "Select", "", DropDownListSalesPerson.SelectedValue)
            Dim agencyId As String = If(Request("hidtxtAgencyName") = "Agency Name or ID", "", Request("hidtxtAgencyName").Trim)

            Dim FromDate As String
            Dim ToDate As String
            If [String].IsNullOrEmpty(Request("From")) Then
                FromDate = ""
            Else

                FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "/" + Strings.Left((Request("From")).Split(" ")(0), 2) + "/" + Strings.Right((Request("From")).Split(" ")(0), 4)
                FromDate = FromDate + " " + "12:00:00 AM"
            End If
            If [String].IsNullOrEmpty(Request("To")) Then
                ToDate = ""
            Else
                ToDate = Mid((Request("To")).Split(" ")(0), 4, 2) & "/" & Left((Request("To")).Split(" ")(0), 2) & "/" & Right((Request("To")).Split(" ")(0), 4)
                ToDate = ToDate & " " & "11:59:59 PM"
            End If
            Dim DiSearch As String = ""
            If (ddl_stock.SelectedValue = "ALL") Then
                DiSearch = "ALL"
            ElseIf (ddl_stock.SelectedValue = "STAG") Then
                DiSearch = "STAG"
            End If
            dtag = STDom.GetAgencyDetailsDynamic(agencyId, type, saleId, FromDate, ToDate, Session("UID"), Session("UserType"), DiSearch).Tables(0) 'ST.GetAgencyDetails(Request("hidtxtAgencyName")).Tables(0)

            Dim dv As New DataView(dtag) '(ds.Tables(0))
            Session("DATAVIEW") = dv
            Session("ORDERBY") = "DESC"

            GridView1.DataSource = dtag
            GridView1.PageIndex = 0
            GridView1.DataBind()
            If Session("UserType") = "DI" Then
                If (Request("DIAG") = 1) Then
                    GridView1.Columns(0).Visible = False
                Else
                    GridView1.Columns(0).Visible = True
                End If
            End If

        Catch ex As Exception

        End Try


    End Sub

   

    Protected Sub btn_update_Click1(sender As Object, e As EventArgs)



    End Sub
End Class

