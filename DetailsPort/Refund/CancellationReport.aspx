﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CancellationReport.aspx.vb" Inherits="DetailsPort_Refund_CancellationReport" MasterPageFile="~/MasterPageSignIn.master" %>

<%@ Register Src="~/UserControl/LeftMenu.ascx" TagPrefix="uc1" TagName="LeftMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" /> 
    <%--<link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"  rel="stylesheet" />--%>
      

     <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
           /*margin-left: 15px;*/
        }
         .overfl {
             overflow: auto;
         }
    </style>
    
    <div class="row">
        <div class="container-fluid" >
            <%-- <div class="col-md-11 pull-right" style="padding:30px 30px 30px 100px;">--%>

            <div class="page-wrapperss">
                <div class="panel panel-primary">

                    <div class="panel-body">

                        <div class="row">
                            
                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">From Date</label>--%>
                                    <input type="text" name="From" id="From" placeholder="FROM DATE" class="form-control input-text full-width" readonly="readonly" />
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                                <div class="col-md-2">
                                <div class="input-group" >
                                    <%--<label for="exampleInputPassword1">To Date</label>--%>
                                    <input type="text" name="To" placeholder="TO DATE" id="To" class="form-control input-text full-width" readonly="readonly" />
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                                  


                            <div class="col-md-2">
                                <div class="input-group">
                                                                        <%--<label for="exampleInputPassword1">PNR</label>--%>
                                    <asp:TextBox ID="txt_PNR" runat="server"  placeholder="PNR" class="form-control input-text full-width"></asp:TextBox>
                                     <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-list-alt"></span>
                                    </span>
                                </div>
                            </div>

                                  


                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">OrderId</label>--%>
                                    <asp:TextBox ID="txt_OrderId" runat="server"  placeholder="REF. ID" class="form-control input-text full-width"></asp:TextBox>
                                     <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="fa fa-first-order"></span>
                                    </span>
                                </div>
                            </div>




                            

                    <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">Pax Name</label>--%>
                                    <asp:TextBox ID="txt_PaxName" runat="server" placeholder="NAME"  class="form-control input-text full-width"></asp:TextBox>
                                     <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-user"></span>
                                    </span>
                           
                                    </div>
                            </div>

                            <%--<div class="row">--%>
                      

                                 <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">Ticket No</label--%>
                                    <asp:TextBox ID="txt_TktNo" runat="server" placeholder="TICKET NO" class="form-control input-text full-width"></asp:TextBox>
                                     <span class="input-group-addon" style="background-color:#49cced">
                                        <span class="fa fa-ticket"></span>
                                        
                                       <%-- <a href="~/Images/icons/flight-ticket.svg"></a>--%>
                                        <%--<img src="<%=ResolveUrl("~/Images/icons/flight-ticket.svg")
                                          %>"/>--%>
                                    </span>

                                </div>
                            </div>
                           
                        </div>

                        <div class="row">
                            <div id="td_Agency" runat="server" >
                                <div id="tr_ExecID" runat="server" class="form-group">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <%--<label for="exampleInputPassword1"></label>--%>
                                            <asp:DropDownList ID="ddl_ExecID" runat="server" class="input-text full-width">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="input-group">
                                           <%-- <label for="exampleInputPassword1"></label>--%>
                                            <input type="text" placeholder="BRAND NAME" id="txtAgencyName" name="txtAgencyName" onfocus="focusObj(this);"
                                                onblur="blurObj(this);"  autocomplete="off" value="" class="form-control input-text full-width" />
                                            <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                             <span class="input-group-addon" style="background-color:#49cced">
                                        <span class="glyphicon glyphicon-briefcase"></span>
                                    </span>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <%--<label for="exampleInputPassword1"></label>--%>
                                            <asp:DropDownList ID="ddl_Status" runat="server" class=" input-text full-width">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                      <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">Airline</label>--%>
                                    <asp:TextBox ID="txt_AirPNR" runat="server" placeholder="AIRLINE" class="form-control input-text full-width"></asp:TextBox>
                                    <input type="hidden" id="aircode" name="aircode" value="" />
                                    <span class="input-group-addon" style="background-color: #49cced">
                                                <span class="glyphicon glyphicon-plane"></span>
                                            </span>



                                </div>
                            </div>


                                     <div class="col-md-2" id="divPartnerName" runat="server" >
                                <label for="exampleInputEmail1"></label>
                                <asp:DropDownList CssClass="selector" ID="txtPartnerName" runat="server">
                                </asp:DropDownList>
                            </div>
                                    <div class="col-md-2">
                                <div class="form-group" id="tdTripNonExec1" runat="server">
                                    <label for="exampleInputPassword1" id="tdTripNonExec2" runat="server"></label>
                                    <asp:DropDownList ID="ddlTripRefunDomIntl" CssClass=" input-text full-width" runat="server">
                                        <asp:ListItem  value="" disabled selected style="display: none;">TRIP TYPE</asp:ListItem>
                                        <%--<asp:ListItem Value="">-----Select-----</asp:ListItem>--%>
                                        <asp:ListItem Value="D">Domestic</asp:ListItem>
                                        <asp:ListItem Value="I">International</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                             <div class="col-md-2" >
                                <label for="exampleInputEmail1"></label>
                                <asp:DropDownList ID="ddldatefilter" CssClass=" input-text full-width" runat="server">
                                    <asp:ListItem  value="" disabled selected style="display: none;">DATE FILTER</asp:ListItem>
                                        <asp:ListItem Value="U">Updated Date</asp:ListItem>
                                        <asp:ListItem Value="R">Requested Date</asp:ListItem>
                                        <asp:ListItem Value="A">Accepted Date</asp:ListItem>
                                     
                                    </asp:DropDownList>
                                 </div>

                                    <div class="col-md-4">
                                <asp:Button ID="btn_result" runat="server" CssClass="btn btn-success" Text="Go" width="74px" />
                                   
                           
                                    <asp:Button ID="btn_export" runat="server" CssClass="btn btn-success" Text="Export" />
                                </div></div>

                                </div>
                            </div>
                           </div>
                        </div>


                               
                         
                      

                          <%--   <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Accept Date</label>
                                    <input type="text" name="acceptdate" id="acceptdate" class="form-control" readonly="readonly" />
                                </div>
                                </div>--%>
                              


                       
                           

                        <div class="row">
                            <%--  <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Update Date</label>
                                    <input type="text" name="updatedate" id="updatedate" class="form-control" readonly="readonly" />
                                </div>
                                </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Requested Date</label>
                                    <input type="text" name="requestdate" id="requestdate" class="form-control" readonly="readonly" />
                                </div>
                                </div>--%>
                              <div class="col-md-2" id="divPaymentMode" runat="server">
                                    <label for="exampleInputEmail1">PaymentMode :</label>
                                    <asp:DropDownList CssClass="selector" ID="txtPaymentmode" runat="server">                                 
                                   <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                   <asp:ListItem Text="PG" Value="pg"></asp:ListItem>
                                  <asp:ListItem Text="Wallet" Value="wallet"></asp:ListItem>                                                                
                                    </asp:DropDownList>
                                </div>
                           


                         
                       </div>

                        
                   
                                
                            <div class="row">  
                             <div class="col-md-9">
                                <div class="form-group">
                            <div style="color: #FF0000">
                               
                            </div>
                              </div>
                              </div>
                        </div>
                  
                    
             


                 <div class="row" id="divReport"  runat="server" visible="false">
                            <div class="col-md-12" >
                    <asp:UpdatePanel ID="UP" runat="server" style="background-color:#fff; overflow: auto; max-height:500px; ">
                        <ContentTemplate>
                            <asp:GridView ID="grd_report" runat="server" BackColor="White" AutoGenerateColumns="False" Width="100%" CssClass="table" GridLines="None"
                                AllowPaging="true" PageSize="30" style="text-transform:uppercase;">
                                <Columns>
                                      <asp:TemplateField HeaderText="Ref_Id">
                                            <ItemTemplate>

                                                <a id="ancher" href='../PnrSummaryIntl.aspx?OrderId=<%#Eval("OrderId")%>' target="_blank"
                                                    style="font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #004b91; font-weight: bold;" title="click to view">
                                                    <asp:Label ID="lbl_OrderId" runat="server" Text='<%#Eval("OrderId") %>'></asp:Label></a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Credit_Node">
                                        <ItemTemplate>
                                            <a target="_blank" href="../Accounts/CreditNodeDomDetails.aspx?RefundID=<%#Eval("RefundID")%>">
                                                <asp:Label ID="lblRefundID" runat="server" Text='<%#Eval("RefundID") %>' ForeColor="#004b91"
                                                    Font-Bold="True" Font-Underline="True"></asp:Label></a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Customer_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lbluserid" runat="server" Text='<%#Eval("UserID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Brand_Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblagencyname" runat="server" Text='<%#Eval("Agency_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="P Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblpaxtype" runat="server" Text='<%#Eval("pax_type") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="P_Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblpaxfname" runat="server" Text='<%#Eval("pax_fname") %>'></asp:Label>&nbsp;<asp:Label ID="lbllastname" runat="server" Text='<%#Eval("pax_lname") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="Pax LastName">
                                        <ItemTemplate>
                                            <asp:Label ID="lbllastname" runat="server" Text='<%#Eval("pax_lname") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Pnr">
                                        <ItemTemplate>
                                            <asp:Label ID="lblpnr" runat="server" Text='<%#Eval("pnr_locator") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ticket_Number">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltktno" runat="server" Text='<%#Eval("Tkt_No") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Airline">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVC" runat="server" Text='<%#Eval("VC") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sector">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldestination" runat="server" Text='<%#Eval("Sector") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Departure_Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldeptdate" runat="server" Text='<%#Eval("departure_date") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="T_Fare">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltotalfare" runat="server" Text='<%#Eval("TotalFare") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fare_After_Discount">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltotalfareafterdiscount" runat="server" Text='<%#Eval("TotalFareAfterDiscount") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Cancellation_Charge">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcharge" runat="server" Text='<%#Eval("CancellationCharge") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sevice_Charge">
                                        <ItemTemplate>
                                            <asp:Label ID="lblsrvcharge" runat="server" Text='<%#Eval("ServiceCharge") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Refunded_Fare">
                                        <ItemTemplate>
                                            <asp:Label ID="lblrefund" runat="server" Text='<%#Eval("RefundFare")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                      <asp:BoundField DataField="PGCharges" HeaderText="Convenience Fee" />

                                    <asp:TemplateField HeaderText="Comment">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcancel" runat="server" Text='<%#Eval("RegardingCancel") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Exec_ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblexecutive" runat="server" Text='<%#Eval("ExecutiveID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                      <asp:BoundField DataField="Status" HeaderText="Refund Status" />
                                      
                                      <asp:BoundField DataField="PgMode" HeaderText="Payment Mode" />
                                     <asp:BoundField DataField="CancelStatus" HeaderText="Cancel Status" />
                                      <asp:BoundField DataField="PartnerName" HeaderText="Partner Name" />
                                      <asp:BoundField DataField="SubmitDate" HeaderText="Requested Date" />
                                    
                                   
                                   
                                    <asp:TemplateField HeaderText="Accepted_Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldateA" runat="server" Text='<%#Eval("AcceptDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Updated_Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldateU" runat="server" Text='<%#Eval("UpdateDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Exec_Rejected_Remark">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRejectComment" runat="server" Text='<%#Eval("RejectComment") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Exec_Updated_Remark">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUpComment" runat="server" Text='<%#Eval("UpdateRemark") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                               
                                </Columns>
                                <RowStyle CssClass="RowStyle" />
                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                <PagerStyle CssClass="PagerStyle" />
                                <SelectedRowStyle CssClass="SelectedRowStyle" />
                                <HeaderStyle CssClass="HeaderStyle" />
                                <EditRowStyle CssClass="EditRowStyle" />
                                <AlternatingRowStyle CssClass="AltRowStyle" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UP">
                        <ProgressTemplate>
                            <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                            </div>
                            <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                Please Wait....<br />
                                <br />
                                <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                <br />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                                </div>
                         </div>
                        </div></div></div> 
     
      

                <script type="text/javascript">
                    var UrlBase = '<%=ResolveUrl("~/") %>';
                </script>

                <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

                <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

                <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
</asp:Content>
