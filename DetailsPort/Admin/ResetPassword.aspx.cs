﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using EXCEPTION_LOG;

public partial class DetailsPort_Admin_ResetPassword : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);    
    string msgout = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        ((Label)Page.Master.FindControl("lblBC")).Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Profile</a><a class='current' href='#'> Password Reset </a>";
        lblDistrId.Text = "";

        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        DivDetails.Visible = false;
        BalDetails.Visible = false;
        string CTime = DateTime.Now.ToString("dd-MMMM-yyyy H:mm:ss zzz");       
    }

    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            
            int flag = 0;
            #region Send sms and mail

            if (!string.IsNullOrEmpty(hdnUserId.Value) && !string.IsNullOrEmpty(hdnMobile.Value) && !string.IsNullOrEmpty(hdnAgencyId.Value) && hdnMobile.Value.Length == 10)
            {
                msgout = "";
                string NewPassword = GenerateOTP();
                SqlTransactionDom STDOM = new SqlTransactionDom();
                DataSet M = STDOM.GetMailingDetails(Convert.ToString(MAILING.RESETPWD), "");                
                //DataSet ds = objDA.GetAgencyDetails(hdnUserId.Value);                                
                try
                {                               
                        if (!string.IsNullOrEmpty(NewPassword))
                        {
                            string RandomNo = DateTime.Now.ToString("yyyyMMddHHmmssffffff");
                            string strOtp = NewPassword.Substring(6, 2);
                            string OTPId = RandomNo.Substring(2, 18) + NewPassword.Substring(6, 2);//(RandomNo.Substring(2, 18)+ OTP.Substring(4, 2)).Substring(18, 2)
                            flag = ResetPassword(hdnUserId.Value, hdnAgencyId.Value, NewPassword, false, hdnMobile.Value, true, hdnEmailID.Value, true, TxtRemark.Text, OTPId);
                            if (flag > 1) {

                                #region Send SMS
                                string smsMsg = "";
                                string smsStatus = "";
                                SMSAPI.SMS objSMSAPI = new SMSAPI.SMS();
                                SqlTransactionNew objSql = new SqlTransactionNew();
                                SqlTransaction objDA = new SqlTransaction();
                                DataSet SmsCrd = new DataSet();
                                SmsCrd = objDA.SmsCredential(Convert.ToString(SMS.RESETPWD));
                                if (SmsCrd != null && SmsCrd.Tables.Count > 0 && SmsCrd.Tables[0].Rows.Count > 0 && Convert.ToBoolean(SmsCrd.Tables[0].Rows[0]["Status"]) == true)
                                {
                                    try
                                    {
                                        DataTable dt = new DataTable();
                                        dt = SmsCrd.Tables[0];
                                        smsMsg = "Your password has been reset,Your User Id- " + hdnUserId.Value + " and Password- " + NewPassword + " for UmrawatiTrip login.";
                                        string MobileNo = Convert.ToString(hdnMobile.Value);
                                        smsStatus = objSMSAPI.SendSmsForAnyService(ref MobileNo, ref smsMsg, dt);
                                        objSql.SmsLogDetails(Convert.ToString(hdnUserId.Value), Convert.ToString(hdnMobile.Value), smsMsg.Replace(NewPassword, "*****"), smsStatus.Replace(NewPassword, "*****"));
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }
                                #endregion

                                #region Send Email
                                bool MailSent = false;
                                try
                                {
                                    int Sent = SendEmail(lblName.Text, hdnEmailID.Value, hdnUserId.Value, NewPassword);
                                    if (Sent > 0)
                                    {
                                        MailSent = true;
                                    }
                                    else
                                    {
                                        MailSent = false;
                                    }
                                }
                                catch (Exception ex)
                                {

                                }
                                #endregion
                                hdnAgencyId.Value = "";
                                hdnAgencyId.Value = "";
                                hdnMobile.Value = "";
                                TxtRemark.Text = "";
                                LblSTAgName.Text = "";
                                lblSTName.Text = "";
                                lblSTMobile.Text = "";
                                LblSTEmail.Text = "";
                                LblSmsEmail.Text = "";
                                BalDetails.Visible = false;
                                DivDetails.Visible = false;

                                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + msgout + "');", true);
                                return;
                            }
                            else
                            {
                                hdnAgencyId.Value = "";
                                hdnAgencyId.Value = "";
                                hdnMobile.Value = "";
                                TxtRemark.Text = "";
                                LblSTAgName.Text = "";
                                lblSTName.Text = "";
                                lblSTMobile.Text = "";
                                LblSTEmail.Text = "";
                                LblSmsEmail.Text = "";

                                BalDetails.Visible = false;
                                DivDetails.Visible = false;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again');", true);
                                return;
                            }
                                                    
                        }
                        else
                        {
                            hdnAgencyId.Value = "";
                            hdnAgencyId.Value = "";
                            hdnMobile.Value = "";
                            TxtRemark.Text = "";
                            LblSTAgName.Text = "";
                            lblSTName.Text = "";
                            lblSTMobile.Text = "";
                            LblSTEmail.Text = "";
                            LblSmsEmail.Text = "";
                            BalDetails.Visible = false;
                            DivDetails.Visible = false;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again');", true);
                            return;
                        }
                   
                   
                }
                catch (Exception ex)
                {

                    hdnAgencyId.Value = "";
                    hdnAgencyId.Value = "";
                    hdnMobile.Value = "";
                    TxtRemark.Text = "";
                    LblSTAgName.Text = "";
                    lblSTName.Text = "";
                    lblSTMobile.Text = "";
                    LblSTEmail.Text = "";
                    LblSmsEmail.Text = "";
                    BalDetails.Visible = false;
                    DivDetails.Visible = false;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again');", true);
                    return;
                }
               
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter valid agent id !!');", true);
                return;
            }
            #endregion
        }
        catch (Exception ex)
        {
            //clsErrorLog.LogInfo(ex);
            //EXCEPTION_LOG.ErrorLog.writeErrorLog(ex, "ResetPassword");
        }
    }

    public void BindValue(string AgentId)
    {
        try
        {
            LblSTAgName.Text = "";
            lblSTName.Text = "";
            lblSTMobile.Text = "";
            LblSTEmail.Text = "";
            LblSmsEmail.Text = "";
            SqlTransaction ST = new SqlTransaction();
            DataSet ds = ST.GetAgencyDetails(AgentId);
            #region Set value in text box
          
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                BalDetails.Visible = true;
                DivDetails.Visible = true;
                ds.Tables[0].Rows[0]["Title"].ToString();
               
                //TxtAgentCredit.Text = Convert.ToString(AgentCreditLimit);
                //lblCreditLimit.Text = Convert.ToString(AgentCreditLimit);
                //LblAvlBal.Text = Convert.ToString(CashLimit);
                //lblDueAmount.Text = Convert.ToString(DueAmt).Replace('-', ' ').Trim(); //Convert.ToString(DueAmt).Remove('-');
                lblAgentDetails.Text = Convert.ToString(ds.Tables[0].Rows[0]["Agency_Name"]); //Convert.ToString(ds.Tables[0].Rows[0]["Agency_Name"]) + "(" + Convert.ToString(ds.Tables[0].Rows[0]["User_Id"]) + ")";
                //LblUserId.Text = Convert.ToString(ds.Tables[0].Rows[0]["Agency_Name"]) + "(" + Convert.ToString(ds.Tables[0].Rows[0]["User_Id"]) + ")";
                lblAgencyID.Text = Convert.ToString(ds.Tables[0].Rows[0]["AgencyId"]);
                hdnAgencyId.Value = Convert.ToString(ds.Tables[0].Rows[0]["AgencyId"]);
                hdnUserId.Value = Convert.ToString(ds.Tables[0].Rows[0]["User_Id"]);
                lblName.Text = Convert.ToString(ds.Tables[0].Rows[0]["Name"]);

                lblMobileNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["Mobile"]);
                lblEmailId.Text = Convert.ToString(ds.Tables[0].Rows[0]["Email"]);

                hdnMobile.Value = Convert.ToString(ds.Tables[0].Rows[0]["Mobile"]);
                hdnEmailID.Value = Convert.ToString(ds.Tables[0].Rows[0]["Email"]);

                //Title, Fname, LnameIsWhiteLabel

                

                if (Convert.ToString(ds.Tables[0].Rows[0]["Distr"]).ToUpper() == "FWU")
                {
                    LblSTAgName.Text = "UmrawatiTrip";                    
                    lblSTName.Text = "";
                    lblSTMobile.Text = "";
                    LblSTEmail.Text = "";                    
                }
                else
                {
                    try 
                    {
                        DataSet STDS = ST.GetAgencyDetails(Convert.ToString(ds.Tables[0].Rows[0]["Distr"]));
                        LblSTAgName.Text = Convert.ToString(STDS.Tables[0].Rows[0]["Agency_Name"]);
                        lblSTName.Text ="<br />Stockist Name : "+ Convert.ToString(STDS.Tables[0].Rows[0]["Name"]);
                        lblSTMobile.Text = "<br />Stockist Mobile : " + Convert.ToString(STDS.Tables[0].Rows[0]["Mobile"]);
                        LblSTEmail.Text = "<br />Stockist Email: " + Convert.ToString(STDS.Tables[0].Rows[0]["Email"]);
                    }
                    catch(Exception ex)
                    {

                    }                    
                }

                if (Convert.ToString(ds.Tables[0].Rows[0]["Distr"]).ToUpper() == "FWU" || Convert.ToBoolean(ds.Tables[0].Rows[0]["IsWhiteLabel"]) ==false)
                {
                    BtnSubmit.Visible = true;                    
                    //lblDistrId.Text = "Stockist User Id: " + Convert.ToString(ds.Tables[0].Rows[0]["Distr"]);
                    LblSmsEmail.Text = "Alert: Password send user registerd Mobile No- <b><u>" + Convert.ToString(ds.Tables[0].Rows[0]["Mobile"]) + "</u></b> and Email Id- <b><u>" + Convert.ToString(ds.Tables[0].Rows[0]["Email"]) + "</u></b>.";
                }
                else
                {
                    BtnSubmit.Visible = false;
                    //lblDistrId.Text = "<br />Stockist User Id : " + Convert.ToString(ds.Tables[0].Rows[0]["Distr"]);
                    LblSmsEmail.Text = "Alert: Password reset not allowed,because whitelabel type agent.";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Password reset not allowed,because whitelabel type agent.');", true);
                    return;
                }

            }
            else
            {
                BalDetails.Visible = false;
                DivDetails.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Record not found.');", true);
                return;
            }
            #endregion

        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    private int ResetPassword(string UserId, string AgencyId, string OTP, bool Status, string MobileNo, bool MStatus, string EmailId, bool EmailStatus, string Remark, string OTPId)
    {
        int flag = 0;
        try
        {
            string CreatedBy = Convert.ToString(Session["UID"]);           
            string IPAddress;
            IPAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (IPAddress == "" || IPAddress == null)
                IPAddress = Request.ServerVariables["REMOTE_ADDR"];

            SqlCommand cmd = new SqlCommand("SP_INSERT_OTP", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@AgencyId", AgencyId);
            cmd.Parameters.AddWithValue("@OTP", OTP);
            cmd.Parameters.AddWithValue("@Status", Status);
            cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
            cmd.Parameters.AddWithValue("@MStatus", MStatus);
            cmd.Parameters.AddWithValue("@EmailId", EmailId);
            cmd.Parameters.AddWithValue("@EmailStatus", EmailStatus);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@IPAddress", IPAddress);
            cmd.Parameters.AddWithValue("@Remark", Remark);
            cmd.Parameters.AddWithValue("@OTPId", OTPId);
            cmd.Parameters.AddWithValue("@ActionType", "RESETPASSWORD");
            cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
            cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
            if (con.State == ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
            msgout = cmd.Parameters["@Msg"].Value.ToString();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            con.Close();
        }
        return flag;
    }

    protected void BtnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            string TxtAgencyName = Request["txtAgencyName"] == "Agency Name or ID" ? "" : Request["txtAgencyName"].Trim();
            string HiddenAgentId = Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"].Trim();
            #region Validation

            string AgencyId = "";
            string UserId = "";
            LblSmsEmail.Text = "";
            if (!string.IsNullOrEmpty(TxtAgencyName))
            {
                if (string.IsNullOrEmpty(HiddenAgentId))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
                    return;
                }
                if (TxtAgencyName.Split('-').Length > 1)
                {
                    if (TxtAgencyName.Split('-')[1].Split('(').Length > 1)
                    {
                        AgencyId = TxtAgencyName.Split('-')[1].Split('(')[0];
                        UserId = TxtAgencyName.Split('-')[1].Split('(')[1].Replace(")", "");
                        if (UserId != HiddenAgentId)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
                            return;
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
                        return;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
                    return;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter agent id!!');", true);
                return;
            }

            #endregion validation

            if (!string.IsNullOrEmpty(HiddenAgentId))
            {
                #region Bind Value
                //string AgentId = Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"].Trim();
                BindValue(HiddenAgentId);
                #endregion
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter valid agent id !!');", true);
                return;
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='SetCreditLimit.aspx'; ", true);
            //return;
        }
    }

    protected string GenerateOTP()
    {
        //string NewOtP = "";
        string alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        string small_alphabets = "abcdefghijklmnopqrstuvwxyz";
        string SpecialChar = "@#-_$";
        string numbers = "1234567890";
        string Length = "8";  // use for length
        string OTPType = "2";  //Alphanumeric=2 //Numeric=1  SpecialAlphanumeric =3      

        string characters = numbers;
        //if (rbType.SelectedItem.Value == "1")
        if (OTPType == "2")
        {
            characters += alphabets + small_alphabets + numbers;
        }
        if (OTPType == "3")
        {
            characters += alphabets + small_alphabets + numbers + SpecialChar;
        }
        //int length = int.Parse(ddlLength.SelectedItem.Value);
        int length = int.Parse(Length);
        string otp = string.Empty;
        for (int i = 0; i < length; i++)
        {
            string character = string.Empty;
            do
            {
                int index = new Random().Next(0, characters.Length);
                character = characters.ToCharArray()[index].ToString();
            } while (otp.IndexOf(character) != -1);
            otp += character;
        }
        return otp;

        #region OTP MAKING
        //<asp:DropDownList ID="ddlLength" runat="server">
        //        <asp:ListItem Text="5" Value="5" />
        //        <asp:ListItem Text="8" Value="8" />
        //        <asp:ListItem Text="10" Value="10" />
        //    </asp:DropDownList>

        //<asp:RadioButtonList ID="rbType" runat="server" RepeatDirection="Horizontal">
        //        <asp:ListItem Text="Alphanumeric" Value="1" Selected="True" />
        //        <asp:ListItem Text="Numeric" Value="2" />
        //    </asp:RadioButtonList>
        #endregion
    }


    public string MailBody(string Name, string UesrId,string Password)
    {
        string mailbody = "";        
        mailbody += "<table width='100%' cellpadding='0' cellspacing='0' border='0' style='border:solid 5px #ffffff;font-family:arial;color:#000033' align='center' bgcolor='#FEF9E7'>";
        mailbody += "<tbody>";
        mailbody += "<tr>";
        mailbody += "<td align='left' valign='top'>";
        mailbody += "<img src='' class='CToWUd'>";
        mailbody += "</td>";
        mailbody += "</tr>";
        mailbody += "<tr>";
        mailbody += "<td style='padding-left:20px;padding-right:20px'>";
        mailbody += "<table width='100%' style='margin-top:20px' align='center'>";
        mailbody += "<tbody>";
        mailbody += "<tr>";
        mailbody += "<td align='left'>Dear " + Name + "</td>";   //Set Value                             
        mailbody += "</tr>";
        mailbody += "<tr>";
        mailbody += "<td style='height:20px'>&nbsp;</td>";
        mailbody += "</tr>";
        mailbody += "<tr>";
        mailbody += "<td align='left'>";
        mailbody += "As per your request, your password has been reset.";
        mailbody += "</td>";
        mailbody += "</tr>";
        mailbody += "<tr>";
        mailbody += "<td align='left'>User ID: " + UesrId + "</td>";     //Set Value          
        mailbody += "</tr>";
        mailbody += "<tr>";
        mailbody += "<td align='left'><span class='il'>Password</span>: " + Password + "</td>";  //Set Value       
        mailbody += "</tr>";
        mailbody += "<tr>";
        mailbody += "<td style='height:20px'>&nbsp;</td>";
        mailbody += "</tr>";
        mailbody += "<tr>";
        mailbody += "<td>";
        mailbody += "This is an auto generated <span class='il'>password</span>. You are advised to change your <span class='il'>password</span> as per your convenience.";
        mailbody += "</td>";
        mailbody += "</tr>";
        mailbody += "<tr>";
        mailbody += "<td style='height:20px'>";
        mailbody += "&nbsp;";
        mailbody += "</td>";
        mailbody += "</tr>";
        mailbody += "<tr>";
        mailbody += "<td align='left'>";
        mailbody += "Please do not reply to this email. In case you need to report a problem/give a suggestion, please contact:";
        mailbody += "<ul>";
        mailbody += "<li>";
        mailbody += "<img src='' style='float: left;'>";
        mailbody += "Email:  info@UmrawatiTrip.com";
        mailbody += "</li>";
        mailbody += "<br />";
        mailbody += "<li>";
        mailbody += "<img src='' style='float: left;'>";
        mailbody += "Phone: +91-11-47 67 77 77,+91-11-48 44 44 44";
        mailbody += "</li>";
        mailbody += "</ul>";
        mailbody += "</td>";
        mailbody += "</tr>";
        mailbody += "<tr>";
        mailbody += "<td style='height:20px'>";
        mailbody += "&nbsp;";
        mailbody += "</td>";
        mailbody += "</tr>";
        mailbody += "<tr>";
        mailbody += "<td align='left'><b>Regards,</b></td>";
        mailbody += "</tr>";
        mailbody += "<tr>";
        mailbody += "<td align='left'><br />Team Support</td>";
        mailbody += "</tr>";
        mailbody += "<tr>";
        mailbody += "<td style='height:20px'>";
        mailbody += "&nbsp;";
        mailbody += "</td>";
        mailbody += "</tr>";
        mailbody += "</tbody>";
        mailbody += "</table>";
        mailbody += "</td>";
        mailbody += "</tr>";
        mailbody += "</tbody>";
        mailbody += "</table>";       
        return mailbody;
    }

    private int SendEmail(string Name, string ToEmailId, string UesrId, string Password)
    {
        int SendMail = 0;
        try
        {
            DataSet MailDs = new DataSet();
            DataTable MailDt = new DataTable();
            SqlTransactionDom STDom = new SqlTransactionDom();
            // MailDt = STDom.GetMailingDetails(MAILING.REGISTRATION_AGENT.ToString().Trim(), "");
            MailDs = STDom.GetMailingDetails(MAILING.RESETPWD.ToString().Trim(), "FWS");
            if (MailDs != null && MailDs.Tables.Count > 0 && MailDs.Tables[0].Rows.Count > 0)
            {
                MailDt = MailDs.Tables[0];
            }

            string mailbody = MailBody(Name, UesrId, Password);
           
            try
            {
                if ((MailDt.Rows.Count > 0))
                {
                    bool Status = false;
                    Status = Convert.ToBoolean(MailDt.Rows[0]["Status"].ToString());                    
                    if (Status == true)
                    {                       
                        //Reset your account password ()
                        string MailSubject = "Your UmrawatiTrip account password reset successfully ,Time: " + DateTime.Now.ToString("dd-MMMM-yyyy H:mm:ss");                       
                        SendMail = STDom.SendMail(ToEmailId, Convert.ToString(MailDt.Rows[0]["MAILFROM"]), Convert.ToString(MailDt.Rows[0]["BCC"]), Convert.ToString(MailDt.Rows[0]["CC"]), Convert.ToString(MailDt.Rows[0]["SMTPCLIENT"]), Convert.ToString(MailDt.Rows[0]["UserId"]), Convert.ToString(MailDt.Rows[0]["Pass"]), mailbody, MailSubject, "");
                    }
                }
            }
            catch (Exception ex)
            {
                clsErrorLog.LogInfo(ex);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        return SendMail;
    }
}