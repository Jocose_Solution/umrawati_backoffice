﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UpdateImportPnrSetting.aspx.cs" Inherits="DetailsPort_Admin_UpdateImportPnrSetting" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />

    <%--  <link href="CSS/bootstrap.css" rel="stylesheet" type="text/css" />--%>
    <link href="../../CSS/bootstrap.min.css" rel="stylesheet" />

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">

    <script type="text/javascript">
        var UrlBase = '/';
        function MyFunc() {
            alert("Updated Successfully")
            window.open('ImportPnrSetting.aspx', '_parent');
            window.close();
        }
        function TryAgain() {
            alert("try again")
            window.open('ImportPnrSetting.aspx', '_parent');
            window.close();
        }
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>     
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearchFlight.js") %>"></script>--%>
    <script language="javascript" type="text/javascript">
        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }

        function CheckDeal() {
            if ($("#ctl00_ContentPlaceHolder1_TxtCharges").val() == "") {
                alert("Enter hold booking charges :.");
                $("#ctl00_ContentPlaceHolder1_TxtCharges").focus();
                return false;
            }
        }

        </script>
    <style type="text/css">
        .tablesss {
            margin-bottom: 10px;
        }

        .button {
            position: relative;
            display: inline-block;
            padding: 12px 24px;
            margin: .3em 0 1em 0;
            width: 100%;
            vertical-align: middle;
            color: #fff;
            font-size: 16px;
            line-height: 20px;
            border-radius: 2px;
            -webkit-font-smoothing: antialiased;
            text-align: center;
            letter-spacing: 1px;
            background: transparent;
            border: 0;
            border-bottom: 2px solid #af3e3a;
            cursor: pointer;
            -webkit-transition: all 0.15s ease;
            transition: all 0.15s ease;
        }

        .buttonBlue {
            background: #d9534f;
        }

            .buttonBlue:hover {
                background: #af3e3a;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class=" col-lg-12 col-sm-12 col-xs-12">
            <div class="panel panel-primary">
                 <div class="panel-heading">
                    <h4>Import Pnr Setting</h4>
                </div>

                <div class="panel-body">
                     <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                        <h3> </h3>
                    </div>
                </div>
                    <div class="row tablesss">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Trip Type
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:DropDownList ID="DdlTripType" runat="server" CssClass="form-control" TabIndex="1">
                                    <asp:ListItem Value="D" Text="Domestic"></asp:ListItem>
                                    <asp:ListItem Value="I" Text="International"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                GroupType :
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:DropDownList ID="ddl_ptype" CssClass="form-control" runat="server" AppendDataBoundItems="true" TabIndex="2">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>


                    <div class="row tablesss">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Agent Id
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <input type="text" id="txtAgencyName" name="txtAgencyName" onfocus="focusObj(this);"
                                    onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="<%=TxtAgentId%>"
                                    class="form-control" tabindex="3" />
                                <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="<%=HdnAgentId%>" />
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Airline :
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <input type="text" placeholder="Search By Airlines" class="form-control" name="txtAirline" value="<%=TxtAirline%>" id="txtAirline" tabindex="4" />
                                <input type="hidden" id="hidtxtAirline" name="hidtxtAirline" value="<%=HdnAirline%>" />
                            </div>
                        </div>
                    </div>

                    <div class="row tablesss">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                              Charges :
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                    <asp:TextBox ID="TxtCharges" runat="server" CssClass="form-control" TabIndex="5" placeholder="Hold Booking Charges" Text="0" MaxLength="5" onKeyPress="return keyRestrict(event,'.0123456789');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                  Status
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:DropDownList ID="DdlStatus" runat="server" CssClass="form-control" TabIndex="13">
                                    <asp:ListItem Value="True" Text="ACTIVE" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="False" Text="DEACTIVE"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>

                    <div class="row tablesss">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                               
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                              
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:Button ID="BtnSubmit" runat="server" Text="Update" OnClick="BtnSubmit_Click" CssClass="button buttonBlue"  OnClientClick="return CheckDeal();"/>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <asp:HiddenField ID="HdnId" runat="server" />
            <asp:HiddenField ID="HdnHoldId" runat="server" />
        </div>
    </form>
</body>
</html>

