﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false" CodeFile="QCTicketReport.aspx.vb" Inherits="DetailsPort_Admin_QCTicketReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/Hotel/css/B2Bhotelengine.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
<%--    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />--%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/gridview-readonly-script.js")%>"></script>
      <script type="text/javascript">
          $(document).ready(function () {
              $("#From").datepicker({ dateFormat: 'dd-mm-yy' }).val();
              $(".cd").click(function () {
                  $("#From").focus();
              }
              );
              $("#To").datepicker({ dateFormat: 'dd-mm-yy' }).val();
              $(".cd1").click(function () {
                  $("#To").focus();
              }
              );
          });
    </script>
    <script type="text/javascript">
        function fareRuleToolTip(id) {
            $.ajax({
                type: "POST",
                url: "QCTicketReport.aspx/GetFairRule",
                data: '{paxid: "' + id + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    debugger;
                    if (msg.d != "") {
                        $("#ddd").html(msg.d);
                        $("#outerdiv").show();

                    }
                    else {
                        alert("Fare Rule Not Available")
                    }
                },
                Error: function (x, e) {
                    alert("error")
                }
            });
        }
    </script>

      <style type="text/css">
        .panel-body
        {
            height: 100%;
            overflow: inherit;
        }

        #slidediv
        {
            /*color: #F25022;
            background-color: #fff;
            border: 2px solid #00A4EF;*/
            display: none;
            /*height: 550px;*/
        }

            #slidediv p
            {
                margin: 15px;
                font-size: 0.917em;
            }

        #contentdiv
        {
            clear: both;
            margin: 0 auto;
            max-width: initial;
        }
    </style>

    <style>
        .overlay
        {
            position: fixed;
            /*top: 102px;
            left: 287px;*/
            right: -12px;
            bottom: 0;
            width: auto;
            height: 70%;
            /*background: #fff;
            opacity: 1.5;
            z-index: 1;
            display: inline-block;*/
        }

 
    </style>


       <script type="text/javascript">

           myfunction = function () {
               debugger;
               $('#slidediv').toggle('slide', { direction: 'right' }, 0);
             


           }
           //$("#cancle_update").click(function () {
           //    $('#slidediv').toggle('slide', { direction: 'left' }, 0);
           
           //});


    </script>

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        /*.overfl {
            overflow: auto;
        }*/

        /*.tooltip1 {
            position: relative;
        }

        .tooltiptext {
            visibility: hidden;
            width: 120px;
            background-color: black;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 5px 0;
            /* Position the tooltip */
            /*position: absolute;
            z-index: 1;
        }*/

        /*.tooltip1:hover .tooltiptext {
            visibility: visible;
        }*/

        /*.popupnew2 {
            position: absolute;
            top: 10px;
            left: 7%;
            width: 900PX;
            height: 500px !important;
            z-index: 1;
            box-shadow: 0px 5px 5px #f3f3f3;
            border: 2px solid #004b91;
            background-color: #fff;
            background-color: #ffffff !important;
            padding: 10px 20px;
            overflow-x: hidden;
        }*/

        /*.vew321 {
            background-color: #fff;
            width: 75%;
            float: right;
            padding: 5px 10px;
            text-align: justify;
            height: 300px;
            overflow-x: auto !important;
            overflow-y: auto !important;
            z-index: 1;
            position: fixed;
            top: 100px;
            left: 20%;
            border: 5px solid #d1d1d1;
        }*/
    </style>
    
    <div class="row">
        <div class="container-fluid" style="padding-right: 35px"  >
            <div class="page-wrapperss">
                <div class="panel panel-primary">
         
                    <div class="panel-body" style="margin-right: -146px">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="input-group">
                                   <%-- <label for="exampleInputPassword1">From Date</label>--%>
                                    <input type="text" placeholder="From Date" name="From" id="From" class="form-control input-text full-width" readonly="readonly" />
                                  <span class="input-group-addon" style="background-color:#49cced">
                                        <span class="glyphicon glyphicon-calendar cd" style="cursor:pointer"></span>
                                    </span>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="input-group">
                                   <%-- <label for="exampleInputPassword1">To Date</label>--%>
                                    <input type="text"  placeholder="To Date" name="To" id="To" class="form-control input-text full-width" readonly="readonly" />
                                    <span class="input-group-addon" style="background-color:#49cced">
                                        <span class="glyphicon glyphicon-calendar cd1" style="cursor:pointer"></span>
                                    </span>

                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">PNR</label>--%>
                                    <asp:TextBox ID="txt_PNR"  placeholder="PNR" runat="server"  class="form-control input-text full-width"></asp:TextBox>
                                     <span class="input-group-addon" style="background-color:#49cced">
                                        <span class="glyphicon glyphicon-list-alt"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">Pax Name</label>--%>

                                    <asp:TextBox ID="txt_PaxName" runat="server" placeholder="PAX Name" class="form-control input-text full-width"></asp:TextBox>
                                    <span class="input-group-addon" style="background-color:#49cced">
                                        <span class="glyphicon glyphicon-user"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">Ticket No</label>--%>

                                    <asp:TextBox ID="txt_TktNo" runat="server" placeholder="Ticket No" class="form-control input-text full-width"></asp:TextBox>
                                    <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-ticket"></span>
                                        </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            
                            <div class="col-md-2" style="top:8px">
                                <div class="input-group" id="td_Agency" runat="server">
                                    <%--<label for="exampleInputPassword1">
                                        Brand Name
                                    </label>--%>
                                    <input type="text" id="txtAgencyName" placeholder="Brand Name" name="txtAgencyName" class="form-control input-text full-width" onfocus="focusObj(this);"
                                        onblur="blurObj(this);" defvalue="Brand Name or ID" autocomplete="off" />
                                    <input type="hidden" id="hidtxtAgencyName"  placeholder="Brand Name" class="form-control input-text full-width" name="hidtxtAgencyName" value="" />
                                    <span class="input-group-addon" style="background-color:#49cced">
                                        <span class="glyphicon glyphicon-briefcase"></span>
                                    </span>
                                </div>
                            </div>
                       
                          <div class="col-md-2" style="top:8px">
                                <div class="input-group">
                                   <%-- <label for="exampleInputPassword1">
                                        OrderId
                                    </label>--%>
                                    <asp:TextBox ID="txt_OrderId" runat="server"  placeholder=" OrderId" CssClass="form-control input-text full-width"></asp:TextBox>
                                        <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-first-order"></span>
                                        </span>
                                </div>
                            </div>
                            <div class="col-md-2" style="top:8px">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">TransactionId</label>--%>
                                    <asp:TextBox ID="txt_TransactionId" placeholder="TransactionId" runat="server"  CssClass="form-control input-text full-width"></asp:TextBox>
                                <span class="input-group-addon" style="background-color:#49cced">
                                     <span class="glyphicon glyphicon-usd"></span>
                                    </span>
                                </div>
                            </div>     
                            
                            
                                <div class="col-md-2" style="top:8px">
                                <div class="form-group">
                                    <%--<label for="exampleInputPassword1">Status</label>--%>
                                     <asp:DropDownList  CssClass=" input-text full-width" ID="dd_status" runat="server">
                                    <asp:ListItem>ALL</asp:ListItem>
                                    <asp:ListItem Text="Pending" Value="Request"></asp:ListItem>
                                    <asp:ListItem Text="Hold" Value="Confirm"></asp:ListItem>
                                    <asp:ListItem Text="HoldByAgent" Value="ConfirmByAgent"></asp:ListItem>
                                    <asp:ListItem Text="PreHoldByAgent" Value="PreConfirmByAgent"></asp:ListItem>
                                    <asp:ListItem Text="Rejected" Value="Rejected"></asp:ListItem>
                                    <asp:ListItem Text="Ticketed" Value="Ticketed"></asp:ListItem>
                                    <asp:ListItem Text="Failed" Value="Failed"></asp:ListItem>
                                    <asp:ListItem Text="InProcess" Value="Inprocess"></asp:ListItem>
                                </asp:DropDownList>
                                    <%--<span class="input-group-addon" style="background-color:#49cced">
                                    <span class="glyphicon glyphicon-search"></span>
                                        </span>--%>
                                </div>
                            </div> 
                                 <div class="col-md-2" style="top:8px">
                            
                                <div class="form-group">
                                    <asp:Button ID="btn_result" runat="server" Text="Find" Width="74px"  CssClass="btn btn-success"/>
                             
                               
                                    <asp:Button ID="btn_export" runat="server" CssClass="btn btn-success"  Text="Export To Excel" />
                              
                            </div>
                        </div>
                             </div>
                        <div class="row">
                           



                                                   
                           
                            <div class="col-md-4" id="divPaymentMode" runat="server" >
                                <%--<label for="exampleInputEmail1">PaymentMode :</label>--%>
                                <asp:DropDownList  CssClass="form-control input-text full-width" ID="txtPaymentmode" runat="server">
                                    <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                    <asp:ListItem Text="PG" Value="pg"></asp:ListItem>
                                    <asp:ListItem Text="Wallet" Value="wallet"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                       
                        <div class="row">
                             <div class="col-md-4" id="divPartnerName" runat="server">
                                <div class="form-group" id="Partnernameid" runat="server">
                                    <label for="exampleInputEmail1">PartnerName :</label>
                                    <asp:DropDownList CssClass="form-control" ID="txtPartnerName" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                       
                            </div>
                        <div class="row" style="display:none;">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">
                                        Total Ticket Sale :
                                    </label>
                                    <asp:Label ID="lbl_Total" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div id="slidediv" class="model overlay" style="z-index: 5;">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <span class="badge cyan" style="background-color: #49cced; box-shadow: 3px 3px #2dadce;"><label  for="exampleInputPassword1" style="color:white">
                                        Total Records :
                                        <asp:Label ID="lbl_counttkt" runat="server"></asp:Label>
                                    </label></span>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group" style="font-size: 15px; line-height: 20px; text-align: justify; color: Red;">
                                  
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-11">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow: auto; max-height: 500px;">
                                    <ContentTemplate>
                                        <asp:GridView ID="ticket_grdview" runat="server" AllowPaging="True" AllowSorting="True"
                                            AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover"
                                            GridLines="None" PageSize="30" style="text-transform:uppercase;">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Booking">
                                                    <ItemTemplate>

<%--                                                        <a href='Update_BookingOrder.aspx?OrderId=<%#Eval("OrderId")%>&status=<%#Eval("Status")%> &TransID='
                                                            rel="lyteframe" rev="width: 1200px; height: 500px; overflow:hidden;" target="_blank"
                                                            style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">Booking&nbsp;Details
                                                        </a>--%>
                                                        
                                                        <a href='Update_BookingOrder.aspx?OrderId=<%#Eval("OrderId")%>&status=<%#Eval("Status")%>&mod_status=<%#Eval("MORDIFYSTATUS")%>&Tkt_No=<%#Eval("TicketNumber")%>&TransID='
                                                            
                                                           >Booking&nbsp;Details
                                                        </a> 

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="Booking_Date">
                                                    <ItemTemplate>

                                                             <a href='BookingTime.aspx?OrderId=<%#Eval("OrderId")%> '
                                                            rel="lyteframe" rev="width: 1200px; height: 500px; overflow:hidden;" target="_blank"
                                                            style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91"><b> <asp:Label ID="lblCreateDate_" runat="server" Text='<%#Eval("CreateDate")%>'></asp:Label></b>
                                                        </a>
                                                           
                                                       
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Pax&nbsp;Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="PaxType" runat="server" Text='<%#Eval("PaxType")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Pax&nbsp;ID">
                                                    <ItemTemplate>

                                                        <a href='../PnrSummaryIntl.aspx?OrderId=<%#Eval("OrderId")%> &TransID=<%#Eval("PaxId")%>'
                                                            rel="lyteframe" rev="width: 900px; height: 500px; overflow:hidden;" target="_blank"
                                                            style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                            <asp:Label ID="TID" runat="server" Text='<%#Eval("PaxId")%>'></asp:Label>(TktDetail)
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Order&nbsp;ID">
                                                    <ItemTemplate>
                                                        <div class="tag">
                                                            <a href='../PnrSummaryIntl.aspx?OrderId=<%#Eval("OrderId")%> &TransID=' rel="lyteframe" class="gridViewToolTip"
                                                                rev="width: 900px; height: 500px; overflow:hidden;" target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label></a>
                                                            <div id="tooltip" style="display: none;">
                                                                <div style="float: left;">
                                                                    <table width="100%" cellpadding="11" cellspacing="11" border="0">
                                                                        <tr>
                                                                            <td style="width: 110px; font-weight: bold;">Refund_Request:
                                                                            </td>
                                                                            <td style="width: 166px;">
                                                                                <%#Eval("SubmitDate")%>  
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 110px; font-weight: bold;">Accepted:
                                                                            </td>
                                                                            <td style="width: 166px;">
                                                                                <%#Eval("AcceptDate")%>  
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td style="width: 110px; font-weight: bold;">Refunded:
                                                                            </td>
                                                                            <td style="width: 166px;">
                                                                                <%#Eval("UpdateDate")%>  
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Pnr">
                                                    <ItemTemplate>                         
                                                        <asp:Label ID="GdsPNR" runat="server" Text='<%#Eval("GdsPnr")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Ticket&nbsp;No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="TktNo" runat="server" Text='<%#Eval("TicketNumber")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="AgencyID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAgencyID" runat="server" Text='<%#Eval("AgencyId")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="User&nbsp;ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="AgentID" runat="server" Text='<%#Eval("UserID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Agency&nbsp;Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
                                                    <ItemTemplate>
                                                        <asp:Label ID="AgencyName" runat="server" Text='<%#Eval("AgencyName")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Excutive&nbsp;ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="ExcutiveID" runat="server" Text='<%#Eval("ExecutiveId")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="AirLine">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Airline" runat="server" Text='<%#Eval("VC")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Sector" DataField="sector"></asp:BoundField>
                                                <asp:BoundField HeaderText="Net&nbsp;Fare" DataField="TotalAfterDis">
                                                    <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Status" DataField="Status"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Modify Status">
                                                    <ItemTemplate>
                                                        <div class="tooltip1">
                                                            <%#Eval("MORDIFYSTATUS")%>
                                                            <span class="tooltiptext"><%#Eval("msg")%></span>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Trip" DataField="Trip"></asp:BoundField>
                                                <%--<asp:BoundField HeaderText="Booking&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" DataField="CreateDate"></asp:BoundField>--%>
                                                <asp:BoundField HeaderText="Partner Name" DataField="PartnerName"></asp:BoundField>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LB_CahngeStatus" runat="server" CommandName="ChangeStatus" CommandArgument='<%#Eval("OrderId")%>' Visible='<%# IsVisible(Eval("Status"))%>'
                                                            Font-Bold="True" Font-Underline="False" ForeColor="Red" OnClientClick="javascript:return validate();">Change Status to Hold</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Payment_Mode">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPaymentMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Convenience_Fee">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPGreport" runat="server" Text='<%#Eval("PgCharges")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Online_Cancel_Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCancelstatus" runat="server" Text='<%#Eval("CancelStatus")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="FareRule">
                                                    <ItemTemplate>
                                                        <span class="fareRuleToolTip">
                                                            <img src='<%#ResolveClientUrl("~/Images/air(ex).png")%>' class="cursorpointer" alt="Click to View Full Details" title="Click to View Full Details" style="height: 20px; cursor: pointer;" onclick="fareRuleToolTip('<%#Eval("PaxId") %>')" />
                                                        </span>
                                                        </div>             
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                   <asp:TemplateField HeaderText="SearchId">
                                                    <ItemTemplate>
                                                       <%-- <asp:Label ID="lblEasyTranId" runat="server" Text='<%#Eval("SearchId")%>'></asp:Label>--%>
                                                         <asp:Label ID="lblEasyTranId" runat="server" Text='<%#Eval("SearchIdName")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="BookingId">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPNRId" runat="server" Text='<%#Eval("PNRId")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="TicketingId">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTicketId" runat="server" Text='<%#Eval("TicketId")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                               
                                                   <asp:TemplateField HeaderText="PG_Transaction_Id">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPgTransaId" runat="server" Text='<%#Eval("PGTransactionid")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="FareType">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFareType" runat="server" Text='<%#Eval("RESULTTYPE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="GSTNO">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPgGSTNO" runat="server" Text='<%#Eval("GSTNO")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="GST_Company_Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGST_Company_Name" runat="server" Text='<%#Eval("GST_Company_Name")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="GST_Company_Address">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGST_Company_Address" runat="server" Text='<%#Eval("GST_Company_Address")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="GST_PhoneNo">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGST_PhoneNo" runat="server" Text='<%#Eval("GST_PhoneNo")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="GST_Email">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGST_Email" runat="server" Text='<%#Eval("GST_Email")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="AgentRemark">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAgentRemark" runat="server" Text='<%#Eval("GSTRemark")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="text-decoration-line: initial; display: none" id="outerdiv" class="vew321">
        <div onclick="closethis();" title="click to close" style="background: url(../../Images/closebox.png); cursor: pointer; float: right; z-index: 9999; min-height: 30px; min-width: 30px;"></div>
        <div id="ddd"></div>
    </div>
    <input type="hidden" runat="server" id="lblhdbn" />
    <script type="text/javascript">
        //function updateText() {
        //    var txt = $("#lblhdbn").val();
        //    if ($("#GvFlightHeader_ctl02_lblStatus").text() == "Request") {
        //        $("#GvFlightHeader_ctl02_lblStatus").text("InProcess");
        //        $($($("#ctl00_ContentPlaceHolder1_ticket_grdview tr")[parseInt(txt)]).find("td")[12]).text("InProcess");
        //    }
        //}
        function validate() {
            if (confirm("Are you sure you want to change status to hold!")) {
                return true;
            } else {
                return false;
            }
        }
        function closethis() {
            $("#outerdiv").hide();
        }

        $(function () {
            InitializeToolTip();
        });
    </script>
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
</asp:Content>

