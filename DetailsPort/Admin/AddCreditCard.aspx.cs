﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DetailsPort_Admin_AddCreditCard : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    string Id = "0";
    protected void Page_Load(object sender, EventArgs e)
    {
        ((Label)Page.Master.FindControl("lblBC")).Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Flight Setting </a><a class='current' href='#'>Add Credit Card</a>";
        try
        {
            if (Session["UID"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
            if (string.IsNullOrEmpty(Convert.ToString(Session["User_Type"])))
            {
                Response.Redirect("~/Login.aspx");
            }
             //Id = Request.QueryString["id"];
             //if (Id=="1")
             //{
             //    Id = "0";
             //    //BindGrid();
             //    //Response.Redirect("~/AddCreditCard.aspx");
             //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "window.location='AddCreditCard.aspx';", true);
             //}
            if (!IsPostBack)
            {
                BindGrid();
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
           #region Insert
            string Airline="", HotelSupplier = "", Product="";
                string AirlineName = Request["hidtxtAirline"];            
                string Pcc = txtpcc.Text.Trim();
                //string Airline = AirlineName.Split(',')[1]; //Convert.ToString(hidtxtAirline.Value.Split(',')[1]);
                string Trip = ddltrip.SelectedValue.Trim();
                string CardType = ddlCardType.SelectedValue.Trim();
                string HName = txtname.Text.Trim();
                string CardNumber = txtcardnumber.Text.Trim();
                string ExpiryDate = txtexpiry.Text.Trim();
                string Status = ddlstatus.SelectedValue.Trim();
                int cvv = txtCVV.Text == "" ? 0 : Convert.ToInt32( txtCVV.Text.Trim());
            if(ddlHotelSupplier.SelectedIndex >0)
                HotelSupplier = ddlHotelSupplier.SelectedValue;
            if (ddlProduct.SelectedIndex > 0)
                Product = ddlProduct.SelectedValue;
            if (AirlineName != "")
                Airline = AirlineName.Split(',')[1].Trim();

            int flag = InsertCreditCardDetails(Pcc, Airline, Trip, CardType, HName, CardNumber, ExpiryDate, Status, Product, HotelSupplier, cvv);
                if (flag > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Added successfully.');window.location='AddCreditCard.aspx'; ", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Card details exist.');window.location='AddCreditCard.aspx'; ", true);
                }
                #endregion
            }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='AddCreditCard.aspx'; ", true);
        }

    }
    private int InsertCreditCardDetails(string Pcc, string Airline, string Trip, string CardType, string HName, string CardNumber, string ExpiryDate, string Status, string Product, string HotelSupplier, int CVV)
    {
        int flag = 0; 
        string CreatedBy = Convert.ToString(Session["UID"]);
        string ActionType = "insert";
        SqlCommand cmd = new SqlCommand("USP_CREDITCARDDETAILS", con);
        try
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Pcc", Pcc);
            cmd.Parameters.AddWithValue("@Airline", Airline);
            cmd.Parameters.AddWithValue("@Trip", Trip);
            cmd.Parameters.AddWithValue("@CardType", CardType);
            cmd.Parameters.AddWithValue("@HName", HName);
            cmd.Parameters.AddWithValue("@CardNumber", CardNumber);
            cmd.Parameters.AddWithValue("@ExpiryDate", ExpiryDate);
            cmd.Parameters.AddWithValue("@Status", Status);
            cmd.Parameters.AddWithValue("@ActionType", ActionType);
            cmd.Parameters.AddWithValue("@Product", Product);
            cmd.Parameters.AddWithValue("@SupplierName", HotelSupplier);
            cmd.Parameters.AddWithValue("@CVV", CVV);
            if (con.State == ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);          
        }
        finally
            {
                con.Close();
                cmd.Dispose();
            }
        return flag;

    }
    public void BindGrid()
    {
        try
        {
            grd_CCDetails.DataSource = GetCreditCardDetails();
            grd_CCDetails.DataBind();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }
    public DataTable GetCreditCardDetails()
    {
        DataTable dt = new DataTable();
        try
        {
            adap = new SqlDataAdapter("USP_CREDITCARDDETAILS", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@ActionType", "select");
            adap.Fill(dt);
        }
        catch (Exception ex)
        {

            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            con.Close();
            adap.Dispose();
        }
        return dt;
    }
    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grd_CCDetails.PageIndex = e.NewPageIndex;
        this.BindGrid();
    }
    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string item = e.Row.Cells[0].Text;
            foreach (Button button in e.Row.Cells[2].Controls.OfType<Button>())
            {
                if (button.CommandName == "Delete")
                {
                    button.Attributes["onclick"] = "if(!confirm('Do you want to delete?')){ return false; };";
                }
            }
        }
    }
    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int flag = 0;
            try
            {
                Label lblSNo = (Label)(grd_CCDetails.Rows[e.RowIndex].FindControl("lblId"));
                int Id = Convert.ToInt16(lblSNo.Text.Trim().ToString());
                //string IPAddress = Request.ServerVariables["REMOTE_ADDR"];
                SqlCommand cmd = new SqlCommand("USP_CREDITCARDDETAILS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Counter", Id);
                cmd.Parameters.AddWithValue("@ActionType", "delete");
                if (con.State == ConnectionState.Closed)
                    con.Open();
                flag = cmd.ExecuteNonQuery();
                con.Close();

            }
            catch (SqlException ex)
            {
                con.Close();
                clsErrorLog.LogInfo(ex);
            }

            BindGrid();
            if (flag > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Problen in deleting record.');", true);
            }
        }
        catch (Exception ex)
        {
            //ErrorLogTrace.WriteErrorLog(ex, "Flight")
            clsErrorLog.LogInfo(ex);
        }
    }
   
   
}