﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DetailsPort_Admin_PNRCreationCrd : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    string msgout = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        ((Label)Page.Master.FindControl("lblBC")).Text = "<a title='' class='tip-bottom' href='PrivilegePanel/Dashboard.aspx' data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Flight Setting </a><a class='current' href='#'>PNR Creation Credential</a>";
        try
        {
            if (Session["UID"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
            if (string.IsNullOrEmpty(Convert.ToString(Session["User_Type"])))
            {
                Response.Redirect("~/Login.aspx");
            }
            //if (Convert.ToString(Session["User_Type"]).ToUpper() != "ADMIN")
            //{
            //    Response.Redirect("~/Login.aspx");
            //}     
            
            if (!IsPostBack)
            {
                BindGrid();
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }
    public void BindGrid()
    {
        try
        {
            Grid1.DataSource = GetCredential();
            Grid1.DataBind();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }

    public DataTable GetCredential()
    {
        DataTable dt = new DataTable();
        try
        {
            adap = new SqlDataAdapter("SpPnrCreationCredential", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@ActionType", "select");
            adap.SelectCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
            adap.SelectCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;            
            adap.Fill(dt);
        }
        catch (Exception ex)
        {

            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            con.Close();
            adap.Dispose();
        }
        return dt;
    }
    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int flag = 0;
            try
            {
                Label lblSNo = (Label)(Grid1.Rows[e.RowIndex].FindControl("lblId"));
                int Id = Convert.ToInt16(lblSNo.Text.Trim().ToString());
                //string IPAddress = Request.ServerVariables["REMOTE_ADDR"];
                SqlCommand cmd = new SqlCommand("SpPnrCreationCredential", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Counter", Id);
                cmd.Parameters.AddWithValue("@ActionType", "delete");
                cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
                cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                flag = cmd.ExecuteNonQuery();
                con.Close();

            }
            catch (SqlException ex)
            {
                con.Close();
                clsErrorLog.LogInfo(ex);
            }

            BindGrid();
            if (flag > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Problen in deleting record.');", true);
            }
        }
        catch (Exception ex)
        {
            //ErrorLogTrace.WriteErrorLog(ex, "Flight")
            clsErrorLog.LogInfo(ex);
        }
    }

    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string item = e.Row.Cells[0].Text;
            foreach (Button button in e.Row.Cells[2].Controls.OfType<Button>())
            {
                if (button.CommandName == "Delete")
                {
                    button.Attributes["onclick"] = "if(!confirm('Do you want to delete?')){ return false; };";
                }
            }
        }
    }
    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Grid1.PageIndex = e.NewPageIndex;
        this.BindGrid();
    }
    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (DdlProvider.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select provider');", true);
                return;
            }
            else
            {
                #region Insert
                string Airline = Convert.ToString(Request["hidtxtAirline"]);
                string FlightName = Convert.ToString(Request["txtAirline"]);
                string AirCode = "";
                string AirlineName = "";

                if (!string.IsNullOrEmpty(FlightName))
                {
                    if (!string.IsNullOrEmpty(Airline))
                    {
                        AirlineName = Airline.Split(',')[0];
                        if (Airline.Split(',').Length > 1)
                        {
                            AirCode = Airline.Split(',')[1];
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select again airline!!');", true);
                            return;
                        }
                    }
                }
                else
                {
                    AirlineName = "ALL";
                    AirCode = "ALL";
                }
                string Trip = Convert.ToString(DdlTripType.SelectedValue);
                string TripTypeName = Convert.ToString(DdlTripType.SelectedItem.Text);


                string CorporateID = Convert.ToString(TxtCorporateID.Text);
                string UserID = Convert.ToString(TxtUserID.Text);
                string Password = Convert.ToString(TxtPassword.Text);              
                string Provider = Convert.ToString(DdlProvider.Text);
                string CarrierAcc = Convert.ToString(TxtCarrierAcc.Text);
                string Status = Convert.ToString(DdlStatus.SelectedValue);
                string ActionType = "insert";
                string CrdType = Convert.ToString(DdlCrdType.SelectedValue);
                msgout = "";
                int flag = PnrServiceCredential(CorporateID, UserID, Password, Provider, CarrierAcc, AirCode, Trip, ActionType, 0, Status, CrdType);
                if (flag > 0)
                {
                    TxtCorporateID.Text="";
                    TxtUserID.Text="";
                    TxtPassword.Text="";
                    DdlProvider.Text="";
                    TxtCarrierAcc.Text = "";
                    BindGrid();
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Added successfully.');window.location='ServiceCredentials.aspx'; ", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Added successfully.');", true);
                }
                else
                {
                    if (msgout == "EXISTS")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Already exists,Please update..');", true);                       
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again.');", true);
                    }
                    BindGrid();                    
                }
                #endregion
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');", true);
        }

    }

    private int PnrServiceCredential(string CorporateID, string UserID, string Password, string Provider, string CarrierAcc, string VC, string Trip, string ActionType, int Counter, string Status,string CrdType)
    {
        int flag = 0;
        string CreatedBy = Convert.ToString(Session["UID"]);
        //string ActionType = "insert";
        try
        {
            SqlCommand cmd = new SqlCommand("SpPnrCreationCredential", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Counter", Counter);
            cmd.Parameters.AddWithValue("@CorporateID", CorporateID);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@Password", Password);            
            cmd.Parameters.AddWithValue("@Provider", Provider);
            cmd.Parameters.AddWithValue("@CarrierAcc", CarrierAcc);
            cmd.Parameters.AddWithValue("@VC", VC);
            cmd.Parameters.AddWithValue("@Trip", Trip);
            cmd.Parameters.AddWithValue("@Status", Convert.ToBoolean(Status));        
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@ActionType", ActionType);
            cmd.Parameters.AddWithValue("@CrdType", CrdType);
            cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
            cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
            if (con.State == ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
            msgout = cmd.Parameters["@Msg"].Value.ToString();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            con.Close();
        }
        return flag;

    }

    protected void Grid1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            Grid1.EditIndex = e.NewEditIndex;
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }

    protected void Grid1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            Grid1.EditIndex = -1;
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    protected void Grid1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            int result = 0;
            Label lblSNo = (Label)(Grid1.Rows[e.RowIndex].FindControl("lblId"));
            int Counter = Convert.ToInt16(lblSNo.Text.Trim().ToString());

            //DropDownList ddlGrdProvider = (DropDownList)Grid1.Rows[e.RowIndex].FindControl("ddlGrdProvider");
            //string Provider = ddlGrdProvider.SelectedValue;

            TextBox txtGrdCorporateId = (TextBox)(Grid1.Rows[e.RowIndex].FindControl("txtGrdCorporateId"));
            string CrpId = Convert.ToString(txtGrdCorporateId.Text);
            if (string.IsNullOrEmpty(CrpId))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Enter Corporate Id!');", true);
                return;
            }

            TextBox txtGrdUserId = (TextBox)(Grid1.Rows[e.RowIndex].FindControl("txtGrdUserId"));
            string USERID = Convert.ToString(txtGrdUserId.Text);

            if (string.IsNullOrEmpty(USERID))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Enter UserId!');", true);
                return;
            }
            TextBox txtGrdPWD = (TextBox)(Grid1.Rows[e.RowIndex].FindControl("txtGrdPWD"));
            string PWD = Convert.ToString(txtGrdPWD.Text);

            if (string.IsNullOrEmpty(PWD))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Enter Password!');", true);
                return;
            }

            TextBox txtGrdPCC = (TextBox)(Grid1.Rows[e.RowIndex].FindControl("txtGrdPCC"));
            string PCC = Convert.ToString(txtGrdPCC.Text);
            if (string.IsNullOrEmpty(PCC))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Enter PCC!');", true);
                return;
            }
            DropDownList ddlGrdStatus = (DropDownList)Grid1.Rows[e.RowIndex].FindControl("ddlGrdStatus");
           // string Online = ddlGrdStatus.SelectedValue;
            string Status = ddlGrdStatus.SelectedValue;//"true";
            //if (Online == "0")
            //{
            //    Status = "false";
            //}
            string ActionType = "update";
            //result = TicketingCredential(Counter, CrpId, USERID, PWD, PCC, QNO, "", OnlineTkt, "", Provider, TicketThrough, QueuePCC, QNOForAPI, ForceToHold, ActionType);
            result = PnrServiceCredential(CrpId, USERID, PWD, "", PCC, "", "", ActionType, Counter,Status,"");

            if (result > 0)
            {
                Grid1.EditIndex = -1;
                BindGrid();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('Record successfully updated.');", true);
            }
            else
            {
                Grid1.EditIndex = -1;
                BindGrid();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('try again.');", true);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('" + ex.Message + "');", true);
        }
    }
}