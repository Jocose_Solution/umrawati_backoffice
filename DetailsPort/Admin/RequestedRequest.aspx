﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false" CodeFile="RequestedRequest.aspx.vb" Inherits="DetailsPort_Admin_RequestedRequest" %>

<%@ Register Src="~/UserControl/LeftMenu.ascx" TagPrefix="uc1" TagName="LeftMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <script src="<%=ResolveUrl("~/Scripts/ReissueRefund.js?v=1")%>" type="text/javascript"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/PopupScript.js?V=1")%>"></script>

    <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
             overflow: auto;
        }
    </style>

    <script type="text/javascript">
        $(function () {           
            SearchText();
        });
        function SearchText() {

            $(".autosuggest").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "RequestedRequest.aspx/GetAutoCompleteData",
                        data: "{'username':'" + $('#<%=txtSearch.ClientID%>').val() + "'}",
                        dataType: "json",
                        success: function (data) {
                            if (data.d.length > 0) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('/')[0],
                                        val: item.split('/')[1]
                                    }
                                }));
                            }
                            else {
                                response([{ label: 'No Records Found', val: -1 }]);
                            }
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                },
                select: function (event, ui) {
                    if (ui.item.val == -1) {
                        return false;
                    }
                    $('#aircode').val(ui.item.val);
                }
            });
        }
       
    </script>
    
 
    <div class="row">
        <div class="container-fluid" style="padding-right: 35px">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
             
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">From Date</label>--%>
                                    <input type="text" name="From" id="From" placeholder="FROM DATE" class="form-control input-text full-width" readonly="readonly" />
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">To Date</label>--%>
                                    <input type="text" name="To" id="To"   placeholder="TO DATE" class="form-control input-text full-width" readonly="readonly" />
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>

                          <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">OrderId</label>--%>
                                    <asp:TextBox ID="txt_OrderId" runat="server"  placeholder="ORDER ID" class="form-control input-text full-width"></asp:TextBox>
                                     <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="fa fa-first-order"></span>
                                    </span>
                                </div>
                            </div>
                            
                              <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">Pax Name</label>--%>
                                    <asp:TextBox ID="txt_PaxName" runat="server" placeholder="NAME"  class="form-control input-text full-width"></asp:TextBox>
                                     <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-user"></span>
                                    </span>
                                </div>
                            </div>
                        
                          <%--  <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Airline</label>                                   
                                    <asp:DropDownList ID="ddlairline" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>--%>

                            <div class="col-md-2" id="airid"  runat="server">
                           <%-- <div class="col-md-2" style="top:10px">
                            --%>    <div class="input-group">
                                    <%--<label for="exampleInputPassword1">Airline</label>--%>
                                    <asp:TextBox ID="txtSearch" runat="server" placeholder="AIRLINE" class="form-control input-text full-width"></asp:TextBox>
                                    <input type="hidden" id="aircode" name="aircode" value="" />
                                    <span class="input-group-addon" style="background-color: #49cced">
                                                <span class="glyphicon glyphicon-plane"></span>
                                            </span>
                                </div>
                            </div>




                            <div class="col-md-2" runat="server" id="tripid">
                                <div class="form-group" id="tdTripNonExec2" runat="server" cssclass="input-text full-width">
                                   <%-- <label for="exampleInputPassword1">Trip</label>--%>
                                    <asp:DropDownList ID="ddlTripDomIntl" runat="server" CssClass="input-text full-width">
                                        <asp:ListItem  value="" disabled selected style="display: none;">SELECT TRIP TYPE</asp:ListItem>
                                        
                                        <asp:ListItem Value="D">DOMESTIC</asp:ListItem>
                                        <asp:ListItem Value="I">INTERNATIONAL</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-md-2">
                                <div class="form-group" id="divtype" runat="server" cssclass="form-control">
                                    <label for="exampleInputPassword1"></label>
                                    <asp:DropDownList ID="ddlsertype" runat="server" CssClass="input-text full-width" AutoPostBack="true" OnSelectedIndexChanged="Role_SelectedIndexChanged">
                                        <asp:ListItem  value="" disabled selected style="display: none;">SUB CATEGORIES</asp:ListItem>
                                        <asp:ListItem Value="F">FLIGHT</asp:ListItem>
                                        <asp:ListItem Value="H">HOTEL</asp:ListItem>
                                        <asp:ListItem Value="B">BUS</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                       
                     
                       <div class="col-md-2" style="margin-top:8px">
                                <div class="form-group" id="div1" runat="server" cssclass="form-control">
                              <asp:Button ID="btn_result" runat="server" CssClass="btn btn-success" Text="Go" Width="74px" />
                                </div>
                            </div>
                               
                         </div>

                        <div class="row">
                            <div class="col-md-3" id="divcount" runat="server">
                                Total No Of Request:
                                <asp:Label ID="totalcount" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                        <br />
                        <br />

                      <%--  For Flight--%>
                        <hr />
                        <div class="row" id="divtkt" runat="server" visible="false">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff;  overflow: auto; max-height: 500px;">
                                <ContentTemplate>
                                    <asp:GridView ID="ticket_grdview" runat="server" AllowPaging="True" AllowSorting="True"
                                        AutoGenerateColumns="False" CssClass="table" GridLines="None" Width="100%"
                                        PageSize="30" >
                                        <Columns>
                                            <asp:TemplateField HeaderText="Order Id">
                                                <ItemTemplate>
                                                    <asp:Label ID="OrderId" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sector">
                                                <ItemTemplate>
                                                    <asp:Label ID="sector" runat="server" Text='<%#Eval("sector")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Airline">
                                                <ItemTemplate>
                                                    <asp:Label ID="VC" runat="server" Text='<%#Eval("VC")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="Status" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Booking Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="CreateDate" runat="server" Text='<%#Eval("CreateDate")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Trip">
                                                <ItemTemplate>
                                                    <asp:Label ID="Trip" runat="server" Text='<%#Eval("Trip")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="TripType">
                                                <ItemTemplate>
                                                    <asp:Label ID="TripType" runat="server" Text='<%#Eval("TripType")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Booking Price">
                                                <ItemTemplate>
                                                    <asp:Label ID="TotalAfterDis" runat="server" Text='<%#Eval("TotalAfterDis")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="AgentId">
                                                <ItemTemplate>
                                                    <asp:Label ID="AgentId" runat="server" Text='<%#Eval("AgentId")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="AgencyName">
                                                <ItemTemplate>
                                                    <asp:Label ID="AgencyName" runat="server" Text='<%#Eval("AgencyName")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Pax Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="Pname" runat="server" Text='<%#Eval("Pname")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PaymentMode">
                                                <ItemTemplate>
                                                    <asp:Label ID="PaymentMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PaymentID" >
                                                <ItemTemplate>
                                                    <asp:Label ID="PaymentID" runat="server" Text='<%#Eval("PaymentID")%>' CommandArgument='<%#Eval("OrderID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PgCharges">
                                                <ItemTemplate>
                                                    <asp:Label ID="PgCharges" runat="server" Text='<%#Eval("PgCharges")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                                   <asp:TemplateField HeaderText="Changes Status">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkupdate" runat="server" Text="(Hold)"  ForeColor="Red" 
                                                   CommandName="lnkupdate" CommandArgument='<%#Eval("OrderID")%>' onclick="lnkupdate_Click"></asp:LinkButton><br/><br/>
                                                     <asp:LinkButton ID="LinkButton1" runat="server" Text="(Reject without payment reversal)"  ForeColor="blue" 
                                                   CommandName="lnkupdate" CommandArgument='<%#Eval("OrderID")%>' onclick="lnkupdate_Click2"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>  
                                            
                                             <%--<asp:TemplateField HeaderText="Changes Status">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkupdate" runat="server" Text="(click to Reject)"  ForeColor="Red" 
                                                   CommandName="lnkupdate" CommandArgument='<%#Eval("OrderID")%>' onclick="lnkupdate_Clickreject"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>                                        
                                        </Columns>
                                    </asp:GridView>           
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                <ProgressTemplate>
                                    <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                    </div>
                                    <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                        Please Wait....<br />
                                        <br />
                                        <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                        <br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </div>


                         <%--  For BUS--%>
                        <div class="row" id="divbus" runat="server" visible="false">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" style="background-color: #fff;  overflow: auto; max-height: 500px;">
                                <ContentTemplate>
                                    <asp:GridView ID="GridBus" runat="server" AllowPaging="True" AllowSorting="True"
                                        AutoGenerateColumns="False" CssClass="table" GridLines="None" Width="100%"
                                        PageSize="30">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Order Id">
                                                <ItemTemplate>
                                                    <asp:Label ID="OrderId" runat="server" Text='<%#Eval("ORDERID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="source">
                                                <ItemTemplate>
                                                    <asp:Label ID="sector" runat="server" Text='<%#Eval("SOURCE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Destination">
                                                <ItemTemplate>
                                                    <asp:Label ID="sector" runat="server" Text='<%#Eval("DESTINATION")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Airline">
                                                <ItemTemplate>
                                                    <asp:Label ID="VC" runat="server" Text='<%#Eval("BUSOPERATOR")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="Status" runat="server" Text='<%#Eval("BOOKINGSTATUS")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Booking Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="CreateDate" runat="server" Text='<%#Eval("CREATEDDATE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           
                                           
                                            <asp:TemplateField HeaderText="Booking Price">
                                                <ItemTemplate>
                                                    <asp:Label ID="TotalAfterDis" runat="server" Text='<%#Eval("TA_NET_FARE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="AgentId">
                                                <ItemTemplate>
                                                    <asp:Label ID="AgentId" runat="server" Text='<%#Eval("AGENTID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           
                                            <asp:TemplateField HeaderText="Pax Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="Pname" runat="server" Text='<%#Eval("PRIMARY_PAX_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PaymentMode">
                                                <ItemTemplate>
                                                    <asp:Label ID="PaymentMode" runat="server" Text='<%#Eval("Paymentmode")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PaymentID" >
                                                <ItemTemplate>
                                                    <asp:Label ID="PaymentID" runat="server" Text='<%#Eval("PaymentID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Changes Status">
                                                <ItemTemplate>
                                                <asp:LinkButton ID="lnkupdate" runat="server" Text="(Hold)"  ForeColor="Red" 
                                                   CommandName="lnkupdate" CommandArgument='<%#Eval("OrderID")%>' onclick="lnkupdate_Click"></asp:LinkButton>
                                                     <asp:LinkButton ID="LinkButton1" runat="server" Text="(Reject without payment reversal)"  ForeColor="Red" 
                                                   CommandName="lnkupdate" CommandArgument='<%#Eval("OrderID")%>' onclick="lnkupdate_Click2"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>  
                                            <%-- <asp:TemplateField HeaderText="Changes Status">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkupdate" runat="server" Text="(click to Reject)"  ForeColor="Red" 
                                                   CommandName="lnkupdate" CommandArgument='<%#Eval("OrderID")%>' onclick="lnkupdate_Clickreject"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>        
                                           
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                <ProgressTemplate>
                                    <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                    </div>
                                    <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                        Please Wait....<br />
                                        <br />
                                        <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                        <br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </div>


                         <%--  For Hotel--%>

                           <div class="row" id="divhtl" runat="server" visible="false">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" style="background-color: #fff;  overflow: auto; max-height: 500px;">
                                <ContentTemplate>
                                    <asp:GridView ID="GridViewHotel" runat="server" AllowPaging="True" AllowSorting="True"
                                        AutoGenerateColumns="False" CssClass="table" GridLines="None" Width="100%"
                                        PageSize="30">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Order Id">
                                                <ItemTemplate>
                                                    <asp:Label ID="OrderId" runat="server" Text='<%#Eval("OrderID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           
                                           
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="Status" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Booking Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="CreateDate" runat="server" Text='<%#Eval("BookingDate")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           <%-- <asp:TemplateField HeaderText="Trip">
                                                <ItemTemplate>
                                                    <asp:Label ID="Trip" runat="server" Text='<%#Eval("Trip")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="TripType">
                                                <ItemTemplate>
                                                    <asp:Label ID="TripType" runat="server" Text='<%#Eval("TripType")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Booking Price">
                                                <ItemTemplate>
                                                    <asp:Label ID="TotalAfterDis" runat="server" Text='<%#Eval("TotalCost")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           <%-- <asp:TemplateField HeaderText="AgentId">
                                                <ItemTemplate>
                                                    <asp:Label ID="AgentId" runat="server" Text='<%#Eval("AgentId")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="AgencyName">
                                                <ItemTemplate>
                                                    <asp:Label ID="AgencyName" runat="server" Text='<%#Eval("AgencyName")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                          <%--  <asp:TemplateField HeaderText="Pax Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="Pname" runat="server" Text='<%#Eval("Pname")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="PaymentMode">
                                                <ItemTemplate>
                                                    <asp:Label ID="PaymentMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PaymentID" >
                                                <ItemTemplate>
                                                    <asp:Label ID="PaymentID" runat="server" Text='<%#Eval("PaymentID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PgCharges">
                                                <ItemTemplate>
                                                    <asp:Label ID="PgCharges" runat="server" Text='<%#Eval("PgCharges")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Changes Status">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkupdate" runat="server" Text="(Hold)"  ForeColor="Red" 
                                                   CommandName="lnkupdate" CommandArgument='<%#Eval("OrderID")%>' onclick="lnkupdate_Click"></asp:LinkButton>
                                                     <asp:LinkButton ID="LinkButton1" runat="server" Text="(Reject without payment reversal)"  ForeColor="Red" 
                                                   CommandName="lnkupdate" CommandArgument='<%#Eval("OrderID")%>' onclick="lnkupdate_Click2"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>  
                                          <%--  <asp:TemplateField HeaderText="Changes Status">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkupdatereject" runat="server" Text="(click to Reject)"  ForeColor="Red" 
                                                   CommandName="lnkupdate" CommandArgument='<%#Eval("OrderID")%>' onclick="lnkupdate_Clickreject"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField> --%>         
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                <ProgressTemplate>
                                    <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                    </div>
                                    <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                        Please Wait....<br />
                                        <br />
                                        <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                        <br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div></div>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/alert.js")%>"></script>
   
  
</asp:Content>
            