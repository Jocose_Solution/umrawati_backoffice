﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false" CodeFile="reg.aspx.vb" Inherits="DetailsPort_Admin_reg" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
   

    <style>
#playground-container {
    height: 500px;
    overflow: hidden !important;
    -webkit-overflow-scrolling: touch;
}
body, html{
     height: 100%;
 	background-repeat: no-repeat;
 	background:url(https://i.ytimg.com/vi/4kfXjatgeEU/maxresdefault.jpg);
 	font-family: 'Oxygen', sans-serif;
	    background-size: cover;
}

.main{
 	margin:50px -260px;
}

h1.title { 
	font-size: 50px;
	font-family: Arial, Helvetica, sans-serif; 
	font-weight: 500; 

    
}

        h2 {
            font-size: 20px;
	font-family: Arial, Helvetica, sans-serif; 
	font-weight: 500; 
    color:#424242;
        }
 

hr{
	width: 100%;
	color: #fff;
}

.form-group{
	margin-bottom: 15px;
}

label{
	margin-bottom: 15px;
}

input,
input::-webkit-input-placeholder {
    font-size: 11px;
    padding-top: 3px;
}

.main-login{
 	background-color: #fff;
    /* shadows and rounded borders */
    -moz-border-radius: 2px;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);

}
.form-control {
    height: auto!important;
padding: 8px 12px !important;
}
.input-group {
    -webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.21)!important;
    -moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.21)!important;
    box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.21)!important;
}

.main-center{
 	margin-top: 30px;
 	margin: 0 auto;
 	
    padding: 10px 40px;
	background:#d3d5d6;
    /*background-image: linear-gradient(141deg, #9fb8ad 0%, #1fc8db 51%, #2cb5e8 75%);*/
	    color: #FFF;
    text-shadow: none;
	-webkit-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.31);
-moz-box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.31);
box-shadow: 0px 3px 5px 0px rgba(0,0,0,0.31);

}
span.input-group-addon i {
    color: #009edf;
    font-size: 17px;
}

.login-button{
	margin-top: 5px;
}

        #submit {
            border-radius:10px;
        }

.login-register{
	font-size: 11px;
	text-align: center;
}
input[type=text]:focus {
    background-color: lightblue;
}

    </style>

    <!--Captcha-->
    <style>
        msg-error {
  color: #c65848;
}
.g-recaptcha.error {
  border: solid 2px #c64848;
  padding: .2em;
  width: 19em;
}
.wrapper {
    text-align: center;
}

.button {
    position: absolute;
    top: 50%;
}
    </style>

    <!--captcha-->
  <%--  <script>
        grecaptcha.ready(function () {
            grecaptcha.execute('6LfEoXgUAAAAAAObq1aI46o_428WCqBZDPHtnJpw', { action: 'action_name' })
            .then(function (token) {
                // Verify the token on the server.
            });
        });
</script>--%>


    <script type="text/javascript">
        function checkitt(evt) {
            evt = (evt) ? evt : window.event
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (!(charCode > 47 && charCode < 58)) {
                return false;
            }
            status = "";
            return true;
        }
    </script>
       <script src="validation.js"></script>
    <form data-toggle="validator" role="form">
  	<div class="container">
			<div class="row main" id="form-wizard-1" style="margin-top:0px">
                 <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
				<div class="main-login main-center">
				<h1 id="reg1" runat="server"><i class="fa fa-user-plus"></i>&nbsp; SIGN UP HERE</h1>
                    <h1 id="reg2" runat="server" visible="false">Thanku for Registration!</h1>
					

                        <div class="w100" id="table_reg" runat="server"
                    visible="true" style="margin-top: 0px; padding-bottom: 20px;">

                        <!--Personal Information-->
                        
						<hr />
                        <h2><i class="fa fa-user"></i>&nbsp; Personal Information</h2>
                       


                        <div class="row">
                               <div class="col-md-4">
                                      <label>Title<span style="color:#cc0000">*</span></label>
                                        
                                        <asp:DropDownList ID="tit_drop" runat="server" CssClass="input-text full-width" required>
                                             <asp:ListItem  value="" disabled selected style="display: none;">-Select Title-</asp:ListItem>

                                            <asp:ListItem Value="Mr.">Mr.</asp:ListItem>
                                            <asp:ListItem Value="Ms.">Ms.</asp:ListItem>
                                            <asp:ListItem Value="Mrs.">Mrs.</asp:ListItem>
                                        </asp:DropDownList>
                                     
                                    </div>

                                
                            <div class="col-md-4">
                                <label>First Name<span style="color:#cc0000">*</span></label>  
                                <asp:TextBox ID="Fname_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static"
                                    onkeypress="return keyRestrict(event,' abcdefghijklmnopqrstuvwxyz');"  MaxLength="50" required></asp:TextBox>
                                
                            </div>


                                   
                                    <div class="col-md-4">
                                        <label>Last Name<span style="color:#cc0000">*</span></label>
                                        <asp:TextBox ID="Lname_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static;"
                                            onkeypress="return keyRestrict(event,'abcdefghijklmnopqrstuvwxyz');" MaxLength="50" required></asp:TextBox>
                                        
                                    </div>
                            </div>


                        <div class="row">

                            <div class="col-md-6">
                                <label>Address<span style="color:#cc0000">*</span></label>
                                <asp:TextBox ID="Add_txt" runat="server" CssClass="input-text full-width" Style="height: 50px;" TextMode="MultiLine" required></asp:TextBox>
                            </div>

                        </div>

                        <div class="row">
                             <div class="col-md-4">
                                            <label>Country<span style="color:#cc0000">*</span></label>
                                            <asp:DropDownList ID="ddl_country" runat="server" CssClass="input-text full-width" AutoPostBack="True" required>
                                                <asp:ListItem  value="" disabled selected style="display: none;">-Select Country-</asp:ListItem>
                                                <asp:ListItem  Value="India">India</asp:ListItem>
                                                <asp:ListItem Value="Other">Other</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="Coun_txt" CssClass="psb_dd input-text full-width " runat="server" MaxLength="30" Style="position: static"
                                                onkeypress="return keyRestrict(event,'abcdefghijklmnopqrstuvwxyz');" Visible="false"></asp:TextBox>
                                        </div>

                            <div class="col-md-4">
                                <label>State<span style="color:#cc0000">*</span></label>
                                <asp:DropDownList ID="ddl_state" runat="server" AutoPostBack="True" CssClass="input-text full-width">
                                </asp:DropDownList>
                                <asp:TextBox ID="Stat_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static"
                                    onkeypress="return vali();" Visible="false" ></asp:TextBox>
                              
                            </div>

                            <div class="col-md-4">
                                          
                                            <label>City<span style="color:#cc0000">*</span></label>
                                     <input type="text" id="ddl_city" runat="server" class="psb_dd input-text full-width" required/>
                                             <asp:TextBox ID="Other_City" CssClass="psb_dd input-text full-width" runat="server" Style="position: static"
                                    onkeypress="return vali();" Visible="false"></asp:TextBox>
                                        </div>
                        </div>

                        <div class="row">
                                <div class="col-md-4">
                                <label>Area</label>
                                <asp:TextBox ID="TextBox_Area" CssClass="psb_dd input-text full-width" runat="server" MaxLength="30" Style="position: static" ></asp:TextBox>
                                  
                            </div>

                            <div class="col-md-4">
                                <label>Pin Code<span style="color:#cc0000">*</span></label>
                                <asp:TextBox ID="Pin_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static"
                                    onkeypress="return keyRestrict(event,'1234567890');" MaxLength="8" required></asp:TextBox>
                            </div>

                        </div>

                        
                        
                        <br />
                        <br />
                        <h2><i class="fa fa-phone-square"></i>&nbsp; Contact Information</h2>
                    <div class="row">
                        <div class="col-md-4">
                                    <label>Phone</label>
                                    <asp:TextBox ID="Ph_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static"
                                        onkeypress="return keyRestrict(event,'0123456789');" MaxLength="12" ></asp:TextBox>
                                    
                                </div>

                        <div  class="col-md-4">
                                    <label>Mobile<span style="color:#cc0000">*</span></label>
                                    <asp:TextBox ID="Mob_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static"
                                       onkeypress="return checkitt(event)" MaxLength="10" required></asp:TextBox>

                                </div>
                    

                        
                             <div  class="col-md-4">
                                    <label>Email Id<span style="color:#cc0000">*</span></label>
                                    <asp:TextBox ID="Email_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static" MaxLength="50" data-error="Bruh, that email address is invalid" required></asp:TextBox>
                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="form-control" runat="server" ControlToValidate="Email_txt" ErrorMessage="*"
                                                Display="dynamic" ValidationGroup="group1"><span style="color:#cc0000"">*</span></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="Email_txt" ValidationGroup="group1" ErrorMessage=" enter valid email "
                                                ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="Red">
                                            </asp:RegularExpressionValidator>
                                     </div>

                            
                        </div>


                        <div class="row">

                            <div  class="col-md-4">
                                    <label>Email Id-2<span style="color:#cc0000">(Optional)</span></label>
                                   
                                     <asp:TextBox ID="Aemail_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static" MaxLength="50" ></asp:TextBox>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="form-control" runat="server" ControlToValidate="Aemail_txt" ErrorMessage="*"
                                                Display="dynamic" ValidationGroup="group1"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="Aemail_txt" ValidationGroup="group1" ErrorMessage=" enter valid email "
                                                ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="Red">
                                            </asp:RegularExpressionValidator>
                                </div>

                            <div  class="col-md-4">
                                    <label>Fax No.<span style="color:#cc0000">*</span></label>
                                    <asp:TextBox ID="Fax_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static" MaxLength="40" required></asp:TextBox>
                                </div>
                        </div>


                        <br />
                        <br />
                        <h2><i class="fa fa-briefcase" ></i>&nbsp; Brand Information</h2>
                        <div class="row">
                            <div class="col-md-4">
                                    <label>Brand Name<span style="color:#cc0000">*</span></label>
                                    <asp:TextBox ID="Agn_txt" CssClass="psb_dd input-text full-width" runat="server" MaxLength="50" onkeypress="return keyRestrict(event,' abcdefghijklmnopqrstuvwxyz0123456789');" required></asp:TextBox>
                                
                                </div>


                            <div class="col-md-4">
                                    <label>Brand Type<span style="color:#cc0000">*</span></label>
                                    <asp:DropDownList ID="Stat_drop" CssClass="input-text full-width" runat="server" >
                                        <asp:ListItem  value="" disabled selected style="display: none;">-Select Brand Type-</asp:ListItem>
                                            <asp:ListItem Value="TA" >Travel Agent</asp:ListItem>
                                            <asp:ListItem Value="DI">Merchandiser</asp:ListItem>
                                        </asp:DropDownList>
                                    <asp:TextBox ID="Web_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static;display:none;"></asp:TextBox>
                                
                                </div>

                             <div class="col-md-4">
                                    <label>S-Tax No.<span style="color:#cc0000">*</span></label>
                                    <asp:TextBox ID="Stax_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static" required></asp:TextBox>
                            
                                </div>
                        </div>

                        <div class="row">
                            
                            <div class="col-md-4">
                                    <label>Name on PAN<span style="color:#cc0000">*</span></label>
                                    <asp:TextBox ID="TextBox_NameOnPard" CssClass="psb_dd input-text full-width" runat="server" Style="position: static" required></asp:TextBox>
                                </div>

                              <div class="col-md-4">
                                    <label>PAN No.<span style="color:#cc0000">*</span></label>
                                    <asp:TextBox ID="Pan_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static" required></asp:TextBox>
                                </div>

                            <div class="col-md-4">
                                    <label>PAN Image</label>
                                    <asp:FileUpload ID="fld_pan" runat="server" CssClass="psb_dd" Height="22px" />                                    
                                    <div class="" style="font-size:11px;color:#337ab7;font-weight:bold">
                                        ( image JPG formate )
                                    </div>
                               </div>
                        </div>

                     

                        <div class="row">
                             <div class="col-md-4">
                                    <label>Remark</label>
                                    <asp:TextBox ID="Rem_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static" ></asp:TextBox>
                                </div>

                             <div class="col-md-4">
                                    <label>Ref. By<span style="color:#cc0000">(Optional)</span></label>
                                    <asp:DropDownList ID="Sales_DDL" runat="server" CssClass="input-text full-width" >
                                    </asp:DropDownList>
                                </div>

                               <div class="col-md-4">
                                    <label>Upload Logo</label>
                                    <asp:FileUpload ID="fld_1" runat="server" CssClass="psb_dd"  />
                                    <div style="font-size:11px;color:#337ab7;font-weight:bold"> ( Image must be in JPG formate(9070px))</div>

                                </div>
                        </div>


                    
                        <br />
                        <br />
                        <h2><i class="fa fa-lock"></i>&nbsp; Authentication Information</h2>
                        <div class="row">
                            <div class="col-md-4">
                                    <label>User Id<span style="color:#cc0000">*</span></label>
                                    <asp:TextBox ID="TxtUserId" runat="server" Style="position: static" MaxLength="20" CssClass="psb_dd input-text full-width" oncopy="return false" onpaste="return false"  onkeypress="return keyRestrict(event,'abcdefghijklmnopqrstuvwxyz1234567890');" required></asp:TextBox>
                                   
                                </div>

                            <div class="form-group col-md-4">
                                    <label>Password<span style="color:#cc0000">*</span></label>
                                    <asp:TextBox ID="Pass_text" runat="server" Style="position: static" data-minlength="6"
                                        TextMode="Password"  class="form-control"  CssClass="psb_dd input-text full-width" required></asp:TextBox>
                                                      <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="Pass_text" ErrorMessage="*"
                                                Display="dynamic" ValidationGroup="group1"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="Pass_text" ValidationGroup="group1" Display="dynamic" ValidationExpression="^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{5,}$" ErrorMessage="Minimum 5 characters atleast 1 Alphabet, 1 Number and 1 Special Character" ForeColor="Red" />
                              

                                </div>

                             <div class="form-group col-md-4">
                                     <label>Confirm Password<span style="color:#cc0000">*</span></label>
                                    <asp:TextBox ID="cpass_txt" class="form-control" CssClass="psb_dd input-text full-width" runat="server" Style="position: static"
                                         TextMode="Password"  data-match="#Pass_text" data-match-error="Whoops, these don't match" required ></asp:TextBox>
                                  <asp:CompareValidator ID="CompareValidator1" runat="server" 
     ControlToValidate="cpass_txt"
     CssClass="ValidationError"
     ControlToCompare="Pass_text"
     ErrorMessage="No Match" 
     ToolTip="Password must be the same" ForeColor="red" />

    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
     ErrorMessage="&laquo; (Required)" 
     ControlToValidate="cpass_txt"
     CssClass="ValidationError"
     ToolTip="Compare Password is a REQUIRED field" ValidationGroup="group1">
    </asp:RequiredFieldValidator>
                                 <%--<div class="help-block with-errors"></div>--%>
                                <br />
                                </div>

                        </div>


                       <%-- <div class="row">
                            
                            <div class="g-recaptcha" data-sitekey="6LfEoXgUAAAAAAObq1aI46o_428WCqBZDPHtnJpw"></div>
                        </div>--%>

                            




                        <br />
                        <br />
                        <br />

                        <div class="wrapper">
						<div class="form-group ">
							 <asp:Button ID="submit" Width="150" runat="server" Text="Submit" onclientclick="function()"
                                    CssClass="btn btn-success" style="color:white;border-radius: 10px;" ValidationGroup="group1"/>


						</div>
                            </div>
						</div>

                        <div id="table_Message" runat="server" visible="false">

                <div class="autoss" style="color:#014c5a">
                    Thanks You....!
                </div>

                <div class="w80 auto">
                    <div class="w100">


                        <div class="regss" style="align-items:center;color:#014c5a">
                            <b> User Id is : - </b>
                            <%=CID%>  <br />Brand Id successfully registered.<br />
                            <%=CID%> is still inactive.  <br />
                        </div>
                        <div class="clear1"></div>

                        <div class="regss" style="color:#c65848">
                            <b>Please activate  Brand id.</b>
                        </div>


                    </div>

                </div>

            </div>

					
				</div>
			</div>
		</div>
          </div>
        </form>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.7.1.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
     
 <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>--%>
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
        var autoCity = UrlBase + "AutoComplete.asmx/GETCITYSTATE";
        $("#ctl00_ContentPlaceHolder1_ddl_city").autocomplete({
            source: function (request, response) {
debugger;

                //if ($("#ctl00_ContentPlaceHolder1_ddl_state").val() == "") {
                //    $("#ctl00_ContentPlaceHolder1_ddl_state").focus();
                //    alert("Please Select state");
                //    return false;
               // }
                $.ajax({
                    url: autoCity,
                    data: "{ 'INPUT': '" + $("#ctl00_ContentPlaceHolder1_ddl_state").val() + "','SEARCH': '" + request.term + "' }",
                    dataType: "json", type: "POST",
                    contentType: "application/json; charset=utf-8",

                    success: function (data) {
debugger;
                        response($.map(data.d, function (item) {
                            return { label: item, value: item, id: $("#ctl00_ContentPlaceHolder1_ddl_state").val() }
                        }))
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
debugger;
                        alert(textStatus);
                    }
                })
            },
            autoFocus: true,
            minLength: 3,
            select: function (event, ui) {

            }
        });

      </script>

           <%--<script src='https://www.google.com/recaptcha/api.js?render=6LfEoXgUAAAAAAObq1aI46o_428WCqBZDPHtnJpw'></script>--%>
</asp:Content>

