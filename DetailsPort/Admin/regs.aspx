﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false"
    CodeFile="regs.aspx.vb" Inherits="regs" %>


<%--Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false"
    CodeFile="DomAirlineMarkup.aspx.vb" EnableViewStateMac="false" Inherits="Reports_Admin_DomAirlineMarkup"--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
        .clear1 {
    clear: both;
    padding:4px;
}
    .rthead {
    font-size: 25px;
    color: #fff;
    background-color: #0e4ca2;
    width: 100%;
    text-align: center;
    padding: 10px;
}
    .w70{
        width:70% !important;
    }

        .rtmainbgs {
            background: #fff;
            border-radius: 4px;
            margin-top: 20px;
            margin-bottom: 20px;
        }

        .rtbg {
            background-color: #0e4ca2;
        }

        .form-controlrt {
            display: block;
            width: 100%;
            height: 40px !important;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 0px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }

        
    </style>




    <!--Google Captcha CSS-->
    <style>
        msg-error {
  color: #c65848;
}
.g-recaptcha.error {
  border: solid 2px #c64848;
  padding: .2em;
  width: 19em;
}
    </style>



    <script>
        $('#submit').click(function () {
            var $captcha = $('#recaptcha'),
                response = grecaptcha.getResponse();

            if (response.length === 0) {
                $('.msg-error').text("reCAPTCHA is mandatory");
                if (!$captcha.hasClass("error")) {
                    $captcha.addClass("error");
                }
            } else {
                $('.msg-error').text('');
                $captcha.removeClass("error");
                alert('reCAPTCHA marked');
            }
        })
    </script>


   <%-- <link rel="stylesheet" href="chosen/chosen.css" />--%>

    <%--<script src="chosen/jquery-1.6.1.min.js" type="text/javascript"></script>

    <script src="chosen/chosen.jquery.js" type="text/javascript"></script>--%>
     <link href="CSS/jquery-ui-1.8.8.custom.css" rel="stylesheet" />


<%--    <script type="text/javascript">
        $(document).ready(function () {
            $(".chzn-select").chosen();
            $(".chzn-select-deselect").chosen({ allow_single_deselect: true });
        });
    </script>--%>

  <%--  <script type="text/javascript">
        function RefreshCaptcha() {
            var img = document.getElementById("imgCaptcha");
            img.src = "../../CAPTCHA.ashx?query=" + Math.random();
        }
    </script>--%>

    
    <%--<script>

    function HideTextBox(ddlId) {
        var ControlName = document.getElementById(ddlId.id);

        if (ControlName.value == "Other")  //it depends on which value Selection do u want to hide or show your textbox
        {
            document.getElementById('ctl00_ContentPlaceHolder1_ddl_city').style.display = 'none';
            document.getElementById('ctl00_ContentPlaceHolder1_City_txt').style.display = 'block';

        }
//        else {
//            document.getElementById('MyTextBox').style.display = '';

//        }
    }--%>


    <%--<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
                    </asp:ScriptManager>--%>
    <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>--%>
    <form data-toggle="validator" role="form">
   <div class="container-fluid" style="padding-right:35px" >
        <div class="" style="line-height: 25px; padding-bottom:10px;margin-left: 30px; ">
            <div class="row rtmainbgs">
                <div class="rthead" id="myDIV" style="background: #49cced" runat="server">Register Now</div>

               <div class="rthead" id="myDIV2" style="background: #49cced" runat="server" visible="false">Thanku for Registration!</div>
              


                <div class="w100" id="table_reg" runat="server"
                    visible="true" style="margin-top: 0px; padding-bottom: 20px;">
                      <div class="row">
        
                <div class="col-md-12 container-fluid">
                    <div class="heading w100">
                       
                            <div align="center">
                                <asp:Label ID="lbl_msg" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="20px"
                                    ForeColor="#FF3300"></asp:Label>
                            </div>
                        
                        <br />
                                <div class="clear1"></div>

                        <div class="row col-md-12">
                                <div class="col-md-6">
                                      <div class="">
                                <h3 style="width: 250px;background: #49cced;color:white;" >Personal Information</h3>
                            </div>
                                    <br />
                                    <%--<div class="col-lg-2 col-sm-2 col-xs-3">Title:<span style="color: #990000" class="lft"></span>

                                    </div>--%>
                                    <div class="col-md-4">
                                      <label>Title<span style="color:#cc0000">*</span></label>
                                        
                                        <asp:DropDownList ID="tit_drop" runat="server" CssClass="input-text full-width" required>
                                             <asp:ListItem  value="" disabled selected style="display: none;">Select Title</asp:ListItem>

                                            <asp:ListItem Value="Mr.">Mr.</asp:ListItem>
                                            <asp:ListItem Value="Ms.">Ms.</asp:ListItem>
                                            <asp:ListItem Value="Mrs.">Mrs.</asp:ListItem>
                                        </asp:DropDownList>
                                     
                                    </div>

                                  <%--   <div class="col-lg-2 col-sm-2 col-xs-3">
                                First Name:<span style="color: #990000"></span>
                            </div>--%>
                            <div class="col-md-4">
                                <label>First Name<span style="color:#cc0000">*</span></label>  
                                <asp:TextBox ID="Fname_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static"
                                    onkeypress="return keyRestrict(event,' abcdefghijklmnopqrstuvwxyz');"  MaxLength="50" required></asp:TextBox>
                                
                            </div>


                                   <%-- <div class="col-lg-2 col-sm-2 col-xs-3">
                                        Last Name:<span style="color: #990000"></span>
                                    </div>--%>
                                    <div class="col-md-4">
                                        <label>Last Name<span style="color:#cc0000">*</span></label>
                                        <asp:TextBox ID="Lname_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static;"
                                            onkeypress="return keyRestrict(event,'abcdefghijklmnopqrstuvwxyz');" MaxLength="50" required></asp:TextBox>
                                        
                                    </div>
                                   
                                    <div class="clear1"></div>

                                <%--     <div class="col-lg-2 col-sm-2 col-xs-3">
                                Address:<span style="color: #990000"></span>
                            </div>--%>

                            <div class="col-md-4">
                                <label>Address<span style="color:#cc0000">*</span></label>
                                <asp:TextBox ID="Add_txt" runat="server" CssClass="input-text full-width" Style="height: 50px;" required></asp:TextBox>
                            </div>
                                  
                                        <%--<div class="col-lg-2 col-sm-2 col-xs-3">
                                            Country:
                                        </div>--%>
                                        <div class="col-md-4">
                                            <label>Country<span style="color:#cc0000">*</span></label>
                                            <asp:DropDownList ID="ddl_country" runat="server" CssClass="input-text full-width" AutoPostBack="True" required>
                                                <asp:ListItem  value="" disabled selected style="display: none;">Select Country</asp:ListItem>
                                                <asp:ListItem  Value="India">India</asp:ListItem>
                                                <asp:ListItem Value="Other">Other</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="Coun_txt" CssClass="psb_dd input-text full-width " runat="server" MaxLength="30" Style="position: static"
                                                onkeypress="return keyRestrict(event,'abcdefghijklmnopqrstuvwxyz');" Visible="false"></asp:TextBox>
                                        </div>
          

                                  
                                    
                            <%--<div class="col-lg-2 col-sm-2 col-xs-3">
                                State:
                            </div>--%>
                            <div class="col-md-4">
                                <label>State<span style="color:#cc0000">*</span></label>
                                <asp:DropDownList ID="ddl_state" runat="server" AutoPostBack="True" CssClass="input-text full-width">
                                </asp:DropDownList>
                                <asp:TextBox ID="Stat_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static"
                                    onkeypress="return vali();" Visible="false" ></asp:TextBox>
                              
                            </div>
                                       <div class="clear1"></div>
                          <%-- <div class="col-lg-2 col-sm-2 col-xs-3">
                                            City:<span style="color: #990000"></span>
                                        </div>--%>
                                        <div class="col-md-4">
                                           <%-- <asp:DropDownList ID="ddl_city" runat="server" CssClass="form-controlrt">
                                            </asp:DropDownList>
                                            <asp:TextBox ID="City_txt" CssClass="psb_dd form-controlrt" runat="server" Style="position: static"
                                                onkeypress="return keyRestrict(event,'abcdefghijklmnopqrstuvwxyz');" MaxLength="50" Visible="false"></asp:TextBox>--%>
                                            <label>City<span style="color:#cc0000">*</span></label>
                                     <input type="text" id="ddl_city" runat="server" class="psb_dd input-text full-width" required/>
                                             <asp:TextBox ID="Other_City" CssClass="psb_dd input-text full-width" runat="server" Style="position: static"
                                    onkeypress="return vali();" Visible="false"></asp:TextBox>
                                        </div>
                             <%--<div class="col-lg-2 col-sm-2 col-xs-3">
                                Area:
                            </div>--%>
                            <div class="col-md-4">
                                <label>Area</label>
                                <asp:TextBox ID="TextBox_Area" CssClass="psb_dd input-text full-width" runat="server" MaxLength="30" Style="position: static" ></asp:TextBox>
                                  
                            </div>
                                  
                            
                            <%--<div class="col-lg-2 col-sm-2 col-xs-3">
                                Pincode:
                            </div>--%>
                            <div class="col-md-4">
                                <label>Pin Code<span style="color:#cc0000">*</span></label>
                                <asp:TextBox ID="Pin_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static"
                                    onkeypress="return keyRestrict(event,'1234567890');" MaxLength="8" required></asp:TextBox>
                            </div>

                                   
                                </div>
                          
                        <div class="col-md-6">
                            <div class="">
                                <h3 style="width: 250px;background: #49cced;color:white">Contact Information</h3>
                            </div>
                           <br />
                                <%--<div class="col-lg-2 col-sm-2 col-xs-3">
                                    Phone:
                                </div>--%>
                                <div class="col-md-4">
                                    <label>Phone</label>
                                    <asp:TextBox ID="Ph_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static"
                                        onkeypress="return keyRestrict(event,'0123456789');" MaxLength="12" ></asp:TextBox>
                                    
                                </div>

                              <%--<div class="col-lg-2 col-sm-2 col-xs-3">
                                    Mobile:<span style="color: #990000"></span>
                                </div>--%>
                                <div  class="col-md-4">
                                    <label>Mobile<span style="color:#cc0000">*</span></label>
                                    <asp:TextBox ID="Mob_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static"
                                        onkeypress="return keyRestrict(event,'0123456789');" MaxLength="10" required></asp:TextBox>
                                    
                                </div>

                            
                                
                               <%-- <div class="col-lg-2 col-sm-2 col-xs-3">
                                    Email Id:<span style="color: #990000"></span>
                                </div>--%>
                                <div  class="col-md-4">
                                    <label>Email Id<span style="color:#cc0000">*</span></label>
                                    <asp:TextBox ID="Email_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static" MaxLength="50" required></asp:TextBox>
                               
                                     </div>

                            <div class="clear1"></div>
                               
                            
                                <%--<div class="col-lg-2 col-sm-2 col-xs-3">
                                    Email 2:
                                </div>--%>
                                <div  class="col-md-4">
                                    <label>Email Id-2<span style="color:#cc0000">(Optional)</span></label>
                                   
                                     <asp:TextBox ID="Aemail_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static" MaxLength="50" ></asp:TextBox>
                                
                                </div>
                                <%--<div class="col-lg-2 col-sm-2 col-xs-3">
                                    Fax No.:
                                </div>--%>
                                <div  class="col-md-4">
                                    <label>Fax No.<span style="color:#cc0000">*</span></label>
                                    <asp:TextBox ID="Fax_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static" MaxLength="40" required></asp:TextBox>
                                </div>
                          
                           
                              
                               
                            </div>
                            </div>
                        <div style="border-left: 1px solid #6fa1b9;height: 326px;position: absolute;left: 50%;margin-left: -19px;top:1;"></div>
                        <div style="border-left: 1px solid #6fa1b9;height: 336px;position: absolute;left: 50%;margin-left: -19px;margin-top:363px;"></div>
                         <div class="clear1"><hr /> </div>
                          <div class="row col-md-12">
                        <div class="col-md-6">
                            <div class="">
                                <h3 style="width: 250px;background: #49cced;color:white">Brand Information</h3>
                            </div>
                            <br />
                                <%--<div class="col-lg-2 col-sm-2 col-xs-3">
                                    Brand Name:<span style="color: #990000"></span>
                                </div>--%>
                                <div class="col-md-4">
                                    <label>Brand Name<span style="color:#cc0000">*</span></label>
                                    <asp:TextBox ID="Agn_txt" CssClass="psb_dd input-text full-width" runat="server" MaxLength="50" onkeypress="return keyRestrict(event,' abcdefghijklmnopqrstuvwxyz0123456789');" required></asp:TextBox>
                                
                                </div>

                            
                               <%-- <div class="col-lg-2 col-sm-2 col-xs-3">
                                 
                                    Brand Type
                                </div>--%>
                                <div class="col-md-4">
                                    <label>Brand Type<span style="color:#cc0000">*</span></label>
                                    <asp:DropDownList ID="Stat_drop" CssClass="input-text full-width" runat="server" >
                                        <asp:ListItem  value="" disabled selected style="display: none;">Select Brand Type</asp:ListItem>
                                            <asp:ListItem Value="TA" >Travel Agent</asp:ListItem>
                                            <asp:ListItem Value="DI">Merchandiser</asp:ListItem>
                                        </asp:DropDownList>
                                    <asp:TextBox ID="Web_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static;display:none;"></asp:TextBox>
                                
                                </div>
                             <%--<div class="col-lg-2 col-sm-2 col-xs-3">
                                    Stax No:
                                </div>--%>
                                <div class="col-md-4">
                                    <label>S-Tax No.<span style="color:#cc0000">*</span></label>
                                    <asp:TextBox ID="Stax_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static" required></asp:TextBox>
                            
                                </div>

                            <div class="clear1"></div>
                               
                                 <%--<div class="col-lg-2 col-sm-2 col-xs-3">
                                    Name On Pan Card:<span style="color: #990000"></span>
                                </div>--%>
                                <div class="col-md-4">
                                    <label>Name on PAN<span style="color:#cc0000">*</span></label>
                                    <asp:TextBox ID="TextBox_NameOnPard" CssClass="psb_dd input-text full-width" runat="server" Style="position: static" required></asp:TextBox>
                                </div>
                                
                                <%--<div class="col-lg-2 col-sm-2 col-xs-3">
                                    Pan No:<span style="color: #990000"></span>
                                </div>--%>
                                <div class="col-md-4">
                                    <label>PAN No.<span style="color:#cc0000">*</span></label>
                                    <asp:TextBox ID="Pan_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static" required></asp:TextBox>
                                </div>
                               
                                <%--<div class="col-lg-2 col-sm-2 col-xs-3">
                                    Pan Image:  
                                </div>--%>
                                <div class="col-md-4">
                                    <label>PAN Image</label>
                                    <asp:FileUpload ID="fld_pan" runat="server" CssClass="psb_dd" Height="22px" />                                    
                                    <div class="" style="font-size:11px;color:#337ab7;font-weight:bold">
                                        ( image JPG formate )
                                    </div>
                               </div>

                            <div class="clear1"></div>

                                <%--<div class="col-lg-2 col-sm-2 col-xs-3">
                                    Remark:
                                </div>--%>
                                <div class="col-md-4">
                                    <label>Remark</label>
                                    <asp:TextBox ID="Rem_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static" ></asp:TextBox>
                                </div>
                               
                               
                                <%--<div class="col-lg-2 col-sm-2 col-xs-3">
                                    Ref. By :
                                </div>--%>
                                <div class="col-md-4">
                                    <label>Ref. By<span style="color:#cc0000">(Optional)</span></label>
                                    <asp:DropDownList ID="Sales_DDL" runat="server" CssClass="input-text full-width" >
                                    </asp:DropDownList>
                                </div>
                               

                                <%--<div class="col-lg-2 col-sm-2 col-xs-3">
                                    Upload Logo : 
                                </div>--%>
                                <div class="col-md-4">
                                    <label>Upload Logo</label>
                                    <asp:FileUpload ID="fld_1" runat="server" CssClass="psb_dd"  />
                                    <div style="font-size:11px;color:#337ab7;font-weight:bold"> ( Image must be in JPG formate(9070px))</div>

                                </div>

                            <div class="clear1"></div>
                             <%--<div class="col-lg-2 col-sm-2 col-xs-6">
                                    Security Question:<span style="color: #990000"></span>
                                </div>--%>
                            <%--    <div class="col-md-4">
                                    <label>Security Question<span style="color:#cc0000">*</span></label>
                                    <asp:DropDownList ID="SecQ_drop" CssClass="input-text full-width" runat="server" required>
                                        <asp:ListItem  value="" disabled selected style="display: none;">Select Your Question</asp:ListItem>
                                        <asp:ListItem Value="What is Your Pet Name?">What is Your Pet Name?</asp:ListItem>
                                        <asp:ListItem Value="What is your Favourite Color?">What is your Favourite Color?</asp:ListItem>
                                        <asp:ListItem Value="What is Your Date of Birth">What is Your Date of Birth</asp:ListItem>
                                    </asp:DropDownList>
                                </div>--%>
                               
                                <%--<div class="col-lg-2 col-sm-2 col-xs-6">
                                    Security Answer:<span style="color: #990000"></span>&nbsp;
                                </div>--%>
                              <%--  <div class="col-md-4">
                                    <label>Security Answer<span style="color:#cc0000">*</span></label>
                                    <asp:TextBox ID="Ans_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static" required></asp:TextBox>
                                </div>--%>
                                
                       

                        
                        </div>
                        <div class="col-md-6">
                            <div class="">
                                <h3 style="width: 250px;background: #49cced;color:white">Authentication Information</h3>
                            </div>
                            <br />

                           
                                <%--<div class="col-lg-2 col-sm-2 col-xs-3">
                                    User Id:<span style="color: #990000"></span>
                                </div>--%>
                                <div class="col-md-4">
                                    <label>User Id<span style="color:#cc0000">*</span></label>
                                    <asp:TextBox ID="TxtUserId" runat="server" Style="position: static" MaxLength="20" CssClass="psb_dd input-text full-width" oncopy="return false" onpaste="return false"  onkeypress="return keyRestrict(event,'abcdefghijklmnopqrstuvwxyz1234567890');" required></asp:TextBox>
                                    <%--<asp:TextBox ID="TxtUserId" runat="server" Style="position: static" MaxLength="50" CssClass="psb_dd form-controlrt" onkeypress="return keyRestrict(event,'abcdefghijklmnopqrstuvwxyz1234567890');"></asp:TextBox>--%>
                                </div>
                                <%--<div class="col-lg-2 col-sm-1 col-xs-3">
                                    Password:<span style="color: #990000"></span>
                                </div>--%>
                                <div class="col-md-4">
                                    <label>Password<span style="color:#cc0000">*</span></label>
                                    <asp:TextBox ID="Pass_text" runat="server" Style="position: static"
                                        TextMode="Password" MaxLength="16" CssClass="psb_dd input-text full-width" ValidationGroup="validate"></asp:TextBox>
                                    

                                      <asp:RequiredFieldValidator id="confirmPasswordReq"
           runat="server" 
           ControlToValidate="Pass_text"
           ErrorMessage="Password confirmation is required!"
           SetFocusOnError="True" 
           Display="Dynamic" ForeColor="Red" />

                                </div>

                          


                    
                                <div class="col-md-4">
                                     <label>Confirm Password<span style="color:#cc0000">*</span></label>
                                    <asp:TextBox ID="cpass_txt" CssClass="psb_dd input-text full-width" runat="server" Style="position: static"
                                         TextMode="Password" ValidationGroup="validate" MaxLength="16" ></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ControlToValidate="Pass_text" ControlToCompare="cpass_txt" ErrorMessage="Password confirmation is required!" SetFocusOnError="True" Display="Dynamic" ForeColor="Red" />
                                </div>


                          
                           

                                <div class="clear1"></div>
                                <%--<div class="col-lg-2 col-sm-2 col-xs-3">Captcha Information:</div>--%>
                                

                                <%--<div class="col-lg-2 col-sm-2 col-xs-3">Enter Text from Image</div>--%>
                              
                  <%--          
                            <div class="col-lg-2 col-sm-2 col-xs-6" >
                                <asp:Button ID="submit" Width="150" runat="server" Text="Submit" OnClientClick="return validateSearch()"
                                    CssClass="btn btn-lg btn-primary" />
                            </div>--%>
 
                        </div>

                        <div class="clear1">
                              <div class="clear1"><hr /> </div>
                    </div>
                              </div>
 
                </div>

                    <div class="row">
                     <span class="msg-error error"></span>
                             <div id="recaptcha" class="g-recaptcha" data-sitekey="6Ld4Jh8TAAAAAD2tURa21kTFwMkKoyJCqaXb0uoK"></div>
                        </div>

                    <div class="row">
                                   <div class="col-lg-2 col-sm-2 col-xs-6" style="margin-left: 560px;" >
                                <div class="form-group">
                                     <br />
                                      <asp:Button ID="submit" Width="150" runat="server" Text="Submit" onclientclick="function()"
                                    CssClass="btn btn-success; margin-left: 550px;" style="background-color:#51a351;color:white" ValidationGroup="validate" />


                                    <asp:CompareValidator id="comparePasswords" runat="server"  ControlToValidate="Pass_text" ErrorMessage="Your passwords do not match up!" Display="Dynamic" Font-Names="Verdana" ForeColor="#666666"  ValidationGroup="validate"/>


                                </div>
                            </div>
                    </div>
                    </div>
                
            </div>
            
        </div>
                <div id="table_Message" runat="server" visible="false">

                <div class="autoss">
                    Thanks You....!
                </div>

                <div class="w80 auto">
                    <div class="w100">


                        <div class="regss" style="align-items:center;">
                            <b> User Id is : - </b>
                            <%=CID%>  <br />Brand Id successfully registered.<br />
                            <%=CID%> is still inactive.  <br />
                        </div>
                        <div class="clear1"></div>

                        <div class="regss">
                            <b>Please activate  Brand id.</b>
                        </div>


                    </div>

                </div>

            </div>
    </div>
            </div>
       </div>
        </form>


  


<%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.7.1.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>--%>
    <%--<script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
        var autoCity = UrlBase + "AutoComplete.asmx/GETCITYSTATE";
        $("#ctl00_ContentPlaceHolder1_ddl_city").autocomplete({
            source: function (request, response) {

                //if ($("#ctl00_ContentPlaceHolder1_ddl_state").val() == "") {
                //    $("#ctl00_ContentPlaceHolder1_ddl_state").focus();
                //    alert("Please Select state");
                //    return false;
               // }
                $.ajax({
                    url: autoCity,
                    data: "{ 'INPUT': '" + $("#ctl00_ContentPlaceHolder1_ddl_state").val() + "','SEARCH': '" + request.term + "' }",
                    dataType: "json", type: "POST",
                    contentType: "application/json; charset=utf-8",

                    success: function (data) {

                        response($.map(data.d, function (item) {
                            return { label: item, value: item, id: $("#ctl00_ContentPlaceHolder1_ddl_state").val() }
                        }))
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {

                        alert(textStatus);
                    }
                })
            },
            autoFocus: true,
            minLength: 3,
            select: function (event, ui) {

            }
        });

      </script>--%>
    
            <%--<script type="text/javascript">
                var UrlBase = '<%=ResolveUrl("~/") %>';
                var autoCity = UrlBase + "AutoComplete.asmx/GETCITYSTATE";
                $("#ctl00_ContentPlaceHolder1_ddl_city").autocomplete({
                    source: function (request, response) {

                        ////if ($("#ctl00_ContentPlaceHolder1_ddl_state").val() == "") {
                        ////    $("#ctl00_ContentPlaceHolder1_ddl_state").focus();
                        ////    alert("Please Select state");
                        ////    return false;
                        //}
                        $.ajax({
                            url: autoCity,
                            data: "{ 'INPUT': '" + $("#ctl00_ContentPlaceHolder1_ddl_state").val() + "','SEARCH': '" + request.term + "' }",
                            dataType: "json", type: "POST",
                            contentType: "application/json; charset=utf-8",

                            success: function (data) {

                                if (data.d.length > 0) {
                                    response($.map(data.d, function (item) {
                                        return { label: item, value: item, id: $("#ctl00_ContentPlaceHolder1_ddl_state").val() }
                                    }))

                                }
                                else {
                                    response([{ label: 'City Not Found of Selected State', val: -1 }]);
                                }
                                //response($.map(data.d, function (item) {
                                //    return { label: item, value: item, id: $("#ctl00_ContentPlaceHolder1_ddl_state").val() }
                                //}))
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {

                                alert(textStatus);
                            }
                        })
                    },
                    autoFocus: true,
                    minLength: 3,
                    select: function (event, ui) {
                        if (ui.item.val == -1) {
                            $(this).val("");
                            return false;
                        }
                    }
                });

      </script>--%>
           <%-- <script type="text/javascript">
                var UrlBase = '<%=ResolveUrl("~/") %>';
                var autoCity = UrlBase + "AutoComplete.asmx/GETCITYSTATE";
                $("#ctl00_ContentPlaceHolder1_ddl_city").autocomplete({
                    source: function (request, response) {

                        //if ($("#ctl00_ContentPlaceHolder1_ddl_city").val()!=data.d.item) {
                        //    $("#ctl00_ContentPlaceHolder1_ddl_city").focus();
                        //    alert("Please Select appropriate city");
                        //    return false;
                        //}
                        $.ajax({
                            url: autoCity,
                            data: "{ 'INPUT': '" + $("#ctl00_ContentPlaceHolder1_ddl_state").val() + "','SEARCH': '" + request.term + "' }",
                            dataType: "json", type: "POST",
                            contentType: "application/json; charset=utf-8",

                            success: function (data) {

                                if (data.d.length > 0) {
                                    response($.map(data.d, function (item) {
                                        return { label: item, value: item, id: $("#ctl00_ContentPlaceHolder1_ddl_state").val() }
                                    }))

                                }
                                else {
                                    response([{ label: 'City Not Found', val: -1 }]);
                                }
                                //response($.map(data.d, function (item) {
                                //    return { label: item, value: item, id: $("#ctl00_ContentPlaceHolder1_ddl_state").val() }
                                //}))
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {

                                alert(textStatus);
                            }
                        })
                    },
                    autoFocus: true,
                    minLength: 3,
                    select: function (event, ui) {
                        if (ui.item.val == -1) {
                            $(this).val("");
                            return false;
                        }
                    },
                    autoFocus: true,
                    minLength: 3,
                    change: function (event, ui) {
                        if (ui.item == null) {
                            this.value = '';
                            alert('Please select City from the City list');
                        }
                    }
                });

      </script>--%>


</asp:Content>
