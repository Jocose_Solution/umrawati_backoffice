﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DetailsPort_Admin_FlightPnrUpdate : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    string msgout = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["UID"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
            if (string.IsNullOrEmpty(Convert.ToString(Session["User_Type"])))
            {
                Response.Redirect("~/Login.aspx");
            }
            //if (Convert.ToString(Session["User_Type"]).ToUpper() != "ADMIN")
            //{
            //    Response.Redirect("~/Login.aspx");
            //}      

            //if (!IsPostBack)
            //{
            //    BindGrid();
            //}
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    protected void btn_result_Click(object sender, EventArgs e)
    {
        
        if (!string.IsNullOrEmpty(txt_OrderId.Text.Trim()))
        {          
            BindGrid();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Enter Order Id');", true);
            return;
        }
    }
    
    public void BindGrid()
    {
        try
        {
            DataSet ds = GetRecord(txt_OrderId.Text);
            if (ds != null && ds.Tables.Count>0)
            {

                GridFltHeader.DataSource = ds.Tables[0];
                GridFltHeader.DataBind();
                if (ds.Tables.Count > 1)
                {
                    Grid1.DataSource = ds.Tables[1];
                    Grid1.DataBind();
                }
            }
            
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    public DataSet GetRecord(string OrderId)
    {
        DataSet ds = new DataSet();
        try
        {
            string TripType = ""; 
            adap = new SqlDataAdapter("SpUpdatePnrByExec", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@OrderId", OrderId);
            adap.SelectCommand.Parameters.AddWithValue("@ServiceType", "FLIGHT");
            adap.SelectCommand.Parameters.AddWithValue("@ActionType", "GETFLT");            
            adap.SelectCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 50);
            adap.SelectCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;
            adap.Fill(ds);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            adap.Dispose();
        }
        return ds;
    }

    protected void Grid1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            Grid1.EditIndex = e.NewEditIndex;
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }

    protected void Grid1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            Grid1.EditIndex = -1;
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    protected void Grid1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            int result = 0;            
            Label lblGPaxId = (Label)(Grid1.Rows[e.RowIndex].FindControl("lblPaxId"));
            int PaxId = Convert.ToInt32(lblGPaxId.Text.Trim().ToString());

            Label lblPaxOrderId = (Label)(Grid1.Rows[e.RowIndex].FindControl("lblOrderId"));
            string PaxOrderId = lblPaxOrderId.Text;

            TextBox txtGrdTicketNumber = (TextBox)(Grid1.Rows[e.RowIndex].FindControl("txtGrdTicketNumber"));
            string PaxTicketNo = Convert.ToString(txtGrdTicketNumber.Text);
            if (string.IsNullOrEmpty(PaxTicketNo))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Enter Pax Ticket No!');", true);
                return;
            }
           
            if (!string.IsNullOrEmpty(PaxOrderId) && !string.IsNullOrEmpty(Convert.ToString(PaxId)) && !string.IsNullOrEmpty(PaxTicketNo))
            {
                string ActionType = "TICKETUPDATE";
                result = UpdateFlightTicketAndPnr(0, PaxId, PaxOrderId, "", "", PaxTicketNo, ActionType);
                // (int HeaderId, int PaxId,string OrderId, string GdsPnr, string AirlinePnr, string TicketNumber,  string ActionType)

                if (result > 0)
                {
                    Grid1.EditIndex = -1;
                    BindGrid();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('Record successfully updated.');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('try again.');", true);
                }
            }
            else
            {
                Grid1.EditIndex = -1;
                BindGrid();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('try again.');", true);
            }
           
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('" + ex.Message + "');", true);
        }
    }
    
    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Grid1.PageIndex = e.NewPageIndex;
        this.BindGrid();
    }

    protected void GridFltHeader_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            GridFltHeader.EditIndex = e.NewEditIndex;
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    protected void GridFltHeader_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            GridFltHeader.EditIndex = -1;
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    protected void GridFltHeader_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            int result = 0;
            Label lblGHeaderId = (Label)(GridFltHeader.Rows[e.RowIndex].FindControl("lblHeaderId"));
            int HeaderId = Convert.ToInt32(lblGHeaderId.Text.Trim().ToString());

            Label lblGOrderId = (Label)(GridFltHeader.Rows[e.RowIndex].FindControl("lblOrderId"));
            string OrderId = lblGOrderId.Text;

            TextBox txtGDSPNR = (TextBox)(GridFltHeader.Rows[e.RowIndex].FindControl("txtGdsPnr"));
            string GDSPNR = Convert.ToString(txtGDSPNR.Text);
            if (string.IsNullOrEmpty(GDSPNR))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Enter Gds Pnr!');", true);
                return;
            }
            TextBox txtAirlinePnr = (TextBox)(GridFltHeader.Rows[e.RowIndex].FindControl("txtGrdAirlinePnr"));
            string AirlinePnr = Convert.ToString(txtAirlinePnr.Text);
            if (string.IsNullOrEmpty(AirlinePnr))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Enter Airline Pnr !');", true);
                return;
            }

            if (!string.IsNullOrEmpty(OrderId) && !string.IsNullOrEmpty(Convert.ToString(HeaderId)) && !string.IsNullOrEmpty(GDSPNR) && !string.IsNullOrEmpty(AirlinePnr))
            {
                string ActionType = "PNRUPDATE";
                result = UpdateFlightTicketAndPnr(HeaderId, 0, OrderId, GDSPNR, AirlinePnr, "", ActionType);
                // (int HeaderId, int PaxId,string OrderId, string GdsPnr, string AirlinePnr, string TicketNumber,  string ActionType)

                if (result > 0)
                {
                    GridFltHeader.EditIndex = -1;
                    BindGrid();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('Record successfully updated.');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('try again.');", true);
                }
            }
            else
            {
                GridFltHeader.EditIndex = -1;
                BindGrid();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('try again.');", true);
            }

        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('" + ex.Message + "');", true);
        }
    }

    private int UpdateFlightTicketAndPnr(int HeaderId, int PaxId,string OrderId, string GdsPnr, string AirlinePnr, string TicketNumber, string ActionType)
    {
        int flag = 0;
        string CreatedBy = Convert.ToString(Session["UID"]);
        string ServiceType = "FLIGHT";
        try
        {

            string IPAddress = null;
            IPAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(IPAddress) || IPAddress == null)
            {
                IPAddress = Request.ServerVariables["REMOTE_ADDR"];
            }
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SpUpdatePnrByExec", con);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@HeaderId", HeaderId);
            cmd.Parameters.AddWithValue("@PaxId", PaxId);
            cmd.Parameters.AddWithValue("@OrderId", OrderId);
            cmd.Parameters.AddWithValue("@GdsPnr", GdsPnr);
            cmd.Parameters.AddWithValue("@AirlinePnr", AirlinePnr);
            cmd.Parameters.AddWithValue("@TicketNumber", TicketNumber);
            cmd.Parameters.AddWithValue("@ServiceType", ServiceType);            
            cmd.Parameters.AddWithValue("@ActionType", ActionType);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@IPAddress", IPAddress);
            cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 50);
            cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
            if (con.State == System.Data.ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
            msgout = cmd.Parameters["@Msg"].Value.ToString();

        }
        catch (Exception ex)
        {
            con.Close();
            clsErrorLog.LogInfo(ex);
        }
        return flag;

    }    

}