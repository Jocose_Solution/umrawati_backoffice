﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class DetailsPort_Admin_EmulateAgentId_ : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);    
    string msgout = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        ((Label)Page.Master.FindControl("lblBC")).Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Profile </a><a class='current' href='#'>Emulate Agent Id With OTP</a>";

        lblDistrId.Text = "";

        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        DivDetails.Visible = false;
        BalDetails.Visible = false;

        //if (Session["User_Type"].ToString().ToUpper() != "ADMIN")
        //{
        //    Response.Redirect("~/Login.aspx");
        //}

        //if (!string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        //{
        //}
        //else
        //{
        //    Response.Redirect("~/Login.aspx");
        //}
    }

    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string AgentLimit = "";// TxtAgentCredit.Text;
            int flag = 0;
            #region Send OTP and mail

            if (!string.IsNullOrEmpty(hdnUserId.Value) && !string.IsNullOrEmpty(hdnMobile.Value) && !string.IsNullOrEmpty(hdnAgencyId.Value) && hdnMobile.Value.Length == 10)
            {
                msgout = "";
                string OTP = GenerateOTP();

                SqlTransactionDom STDOM = new SqlTransactionDom();
                DataSet M = STDOM.GetMailingDetails("OTP", "");
                try
                {
                    string smsMsg = "";
                    string smsStatus = "";
                    bool MailSent = false;
                    bool OtpStatus = false;
                    SMSAPI.SMS objSMSAPI = new SMSAPI.SMS();
                    SqlTransactionNew objSql = new SqlTransactionNew();
                    DataSet SmsCrd = new DataSet();
                    SqlTransaction objDA = new SqlTransaction();
                    SmsCrd = objDA.SmsCredential(Convert.ToString(SMS.EMULATE));
                    if (!string.IsNullOrEmpty(OTP) && SmsCrd != null && SmsCrd.Tables.Count > 0 && SmsCrd.Tables[0].Rows.Count > 0 && Convert.ToBoolean(SmsCrd.Tables[0].Rows[0]["Status"]) == true)
                    {
                        OtpStatus = true;
                        //if (flag>0)
                        //{
                        try
                        {
                            DataTable dt = new DataTable();
                            dt = SmsCrd.Tables[0];
                            smsMsg = "Your One Time Password(OTP) is " + OTP + " for login and valid for next 20 mins.";
                            string MobileNo = Convert.ToString(hdnMobile.Value);
                            smsStatus = objSMSAPI.SendSmsForAnyService(ref MobileNo, ref smsMsg, dt);
                            objSql.SmsLogDetails(Convert.ToString(hdnUserId.Value), Convert.ToString(hdnMobile.Value), smsMsg, smsStatus);
                        }
                        catch (Exception ex)
                        {

                        }
                        try
                        {
                            int Sent = SendEmail(lblName.Text, OTP, hdnEmailID.Value);
                            if (Sent > 0)
                            {
                                MailSent = true;
                            }
                            else
                            {
                                MailSent = false;
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                        string RandomNo = DateTime.Now.ToString("yyyyMMddHHmmssffffff");
                        string strOtp = OTP.Substring(4, 2);
                        string OTPId = RandomNo.Substring(2, 18) + OTP.Substring(4, 2);//(RandomNo.Substring(2, 18)+ OTP.Substring(4, 2)).Substring(18, 2)
                        flag = InsertOTP(hdnUserId.Value, hdnAgencyId.Value, OTP, OtpStatus, hdnMobile.Value, true, hdnEmailID.Value, MailSent, TxtRemark.Text, OTPId);
                        if (flag > 1)
                        {

                            string CreatedBy = Convert.ToString(Session["UID"]);
                            //For Local  //http://localhost:53943/OTPValidate.aspx
                            //ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('http://localhost:53943/OTPValidate.aspx?Param=" + hdnAgencyId.Value.ToString() + "&ProductID=" + OTPId + "&ExecID=" + CreatedBy + "');", true);
                           
                            //For Pre Production
                            //ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('http://UmrawatiTrip.co/flytest/OTPValidate.aspx?Param=" + hdnAgencyId.Value.ToString() + "&ProductID=" + OTPId + "&ExecID=" + CreatedBy + "');", true);

                            //For Production
                            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('http://UmrawatiTrip.com/OTPValidate.aspx?Param=" + hdnAgencyId.Value.ToString() + "&ProductID=" + OTPId + "&ExecID=" + CreatedBy + "');", true);
                            
                            hdnAgencyId.Value = "";
                            hdnAgencyId.Value = "";
                            hdnMobile.Value = "";
                            TxtRemark.Text = "";
                            BalDetails.Visible = false;
                            DivDetails.Visible = false;
                           
                        }
                    }
                    //}
                    else
                    {

                        hdnAgencyId.Value = "";
                        hdnAgencyId.Value = "";
                        hdnMobile.Value = "";
                        TxtRemark.Text = "";
                        BalDetails.Visible = false;
                        DivDetails.Visible = false;
                        msgout = "Plesae try again..";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + msgout + "');", true);
                    }
                }
                catch (Exception ex)
                {
                }
                //BindGrid(hdnUserId.Value);
                //TxtRemark.Text = "";
            }
            else
            {
                hdnAgencyId.Value = "";
                hdnAgencyId.Value = "";
                hdnMobile.Value = "";
                TxtRemark.Text = "";
                BalDetails.Visible = false;
                DivDetails.Visible = false;
                msgout = "Plesae try again..";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter valid agent id !!');", true);
                return;
            }
            #endregion
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            BalDetails.Visible = false;
            DivDetails.Visible = false;
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='SetCreditLimit.aspx'; ", true);
            //return;
        }

    }

    public void BindValue(string AgentId)
    {
        try
        {
            SqlTransaction ST = new SqlTransaction();
            DataSet ds = ST.GetAgencyDetails(AgentId);
            #region Set value in text box
            double CashLimit = 0;
            double AgentCreditLimit = 0;
            double DueAmt = 0;
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                BalDetails.Visible = true;
                DivDetails.Visible = true;
                ds.Tables[0].Rows[0]["Title"].ToString();
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["Crd_Limit"])))
                {
                    CashLimit = Convert.ToDouble(ds.Tables[0].Rows[0]["Crd_Limit"]);
                    //(ds.Tables[0].Rows[0]["Crd_Limit"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["AgentLimit"])))
                {
                    AgentCreditLimit = Convert.ToDouble(ds.Tables[0].Rows[0]["AgentLimit"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["DueAmount"])))
                {
                    DueAmt = Convert.ToDouble(ds.Tables[0].Rows[0]["DueAmount"]);
                }
                if (CashLimit > AgentCreditLimit)
                {
                    CashLimit = CashLimit - AgentCreditLimit;
                }
                else
                {
                    CashLimit = 0;
                }
                //TxtAgentCredit.Text = Convert.ToString(AgentCreditLimit);
                lblCreditLimit.Text = Convert.ToString(AgentCreditLimit);
                LblAvlBal.Text = Convert.ToString(CashLimit);
                lblDueAmount.Text = Convert.ToString(DueAmt).Replace('-', ' ').Trim(); //Convert.ToString(DueAmt).Remove('-');
                lblAgentDetails.Text = Convert.ToString(ds.Tables[0].Rows[0]["Agency_Name"]); //Convert.ToString(ds.Tables[0].Rows[0]["Agency_Name"]) + "(" + Convert.ToString(ds.Tables[0].Rows[0]["User_Id"]) + ")";
                //LblUserId.Text = Convert.ToString(ds.Tables[0].Rows[0]["Agency_Name"]) + "(" + Convert.ToString(ds.Tables[0].Rows[0]["User_Id"]) + ")";
                lblAgencyID.Text = Convert.ToString(ds.Tables[0].Rows[0]["AgencyId"]);
                hdnAgencyId.Value = Convert.ToString(ds.Tables[0].Rows[0]["AgencyId"]);
                hdnUserId.Value = Convert.ToString(ds.Tables[0].Rows[0]["User_Id"]);
                lblName.Text = Convert.ToString(ds.Tables[0].Rows[0]["Name"]);

                lblMobileNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["Mobile"]);
                lblEmailId.Text = Convert.ToString(ds.Tables[0].Rows[0]["Email"]);

                hdnMobile.Value = Convert.ToString(ds.Tables[0].Rows[0]["Mobile"]);
                hdnEmailID.Value = Convert.ToString(ds.Tables[0].Rows[0]["Email"]);

                //Title, Fname, Lname

                //if (Convert.ToString(ds.Tables[0].Rows[0]["Distr"]).ToUpper() == "FWU")
                if (Convert.ToString(ds.Tables[0].Rows[0]["IsWhitelabel"]).ToLower() == "false" || string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["IsWhitelabel"])))                   
                {
                    BtnSubmit.Visible = true;
                    //lblDistrId.Text = "Stockist Id: " + Convert.ToString(ds.Tables[0].Rows[0]["Distr"]);
                }
                else
                {
                    BtnSubmit.Visible = false;
                    lblDistrId.Text = "Stockist Id: " + Convert.ToString(ds.Tables[0].Rows[0]["Distr"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Emulate not allowed,because whitelabel agent.');", true);
                    return;
                }

            }
            else
            {
                BalDetails.Visible = false;
                DivDetails.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Record not found.');", true);
                return;
            }
            #endregion

        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    private int InsertOTP(string UserId, string AgencyId, string OTP, bool Status, string MobileNo, bool MStatus, string EmailId, bool EmailStatus, string Remark, string OTPId)
    {
        int flag = 0;
        try
        {
            string CreatedBy = Convert.ToString(Session["UID"]);
            string RandomNo = DateTime.Now.ToString("yyyyMMddHHmmssffffff");
            string InvoiceNo = "CL" + RandomNo.Substring(7, 13);
            string IPAddress;
            IPAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (IPAddress == "" || IPAddress == null)
                IPAddress = Request.ServerVariables["REMOTE_ADDR"];

            SqlCommand cmd = new SqlCommand("SP_INSERT_OTP", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@AgencyId", AgencyId);
            cmd.Parameters.AddWithValue("@OTP", OTP);
            cmd.Parameters.AddWithValue("@Status", Status);            
            cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
            cmd.Parameters.AddWithValue("@MStatus", MStatus);
            cmd.Parameters.AddWithValue("@EmailId", EmailId);
            cmd.Parameters.AddWithValue("@EmailStatus", EmailStatus);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@IPAddress", IPAddress);
            cmd.Parameters.AddWithValue("@Remark", Remark);
            cmd.Parameters.AddWithValue("@OTPId", OTPId);
            cmd.Parameters.AddWithValue("@ActionType", "INSERTOTP");
            cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
            cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
            if (con.State == ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
            msgout = cmd.Parameters["@Msg"].Value.ToString();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            con.Close();
        }
        return flag;
    }

    protected void BtnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            string TxtAgencyName = Request["txtAgencyName"] == "Agency Name or ID" ? "" : Request["txtAgencyName"].Trim();
            string HiddenAgentId = Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"].Trim();
            #region Validation

            string AgencyId = "";
            string UserId = "";
            if (!string.IsNullOrEmpty(TxtAgencyName))
            {
                if (string.IsNullOrEmpty(HiddenAgentId))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
                    return;
                }
                if (TxtAgencyName.Split('-').Length > 1)
                {
                    if (TxtAgencyName.Split('-')[1].Split('(').Length > 1)
                    {
                        AgencyId = TxtAgencyName.Split('-')[1].Split('(')[0];
                        UserId = TxtAgencyName.Split('-')[1].Split('(')[1].Replace(")", "");
                        if (UserId != HiddenAgentId)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
                            return;
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
                        return;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select valid User Id or Agency Id.');", true);
                    return;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter agent id!!');", true);
                return;
            }

            #endregion validation

            if (!string.IsNullOrEmpty(HiddenAgentId))
            {
                #region Bind Value
                //string AgentId = Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"].Trim();
                BindValue(HiddenAgentId);
                #endregion
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter valid agent id !!');", true);
                return;
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='SetCreditLimit.aspx'; ", true);
            //return;
        }
    }

    protected string GenerateOTP()
    {
        //string NewOtP = "";
        string alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        string small_alphabets = "abcdefghijklmnopqrstuvwxyz";
        string numbers = "1234567890";
        string Length = "6";
        string OTPType = "2";


        string characters = numbers;
        //if (rbType.SelectedItem.Value == "1")
        if (OTPType == "1")
        {
            characters += alphabets + small_alphabets + numbers;
        }
        //int length = int.Parse(ddlLength.SelectedItem.Value);
        int length = int.Parse(Length);
        string otp = string.Empty;
        for (int i = 0; i < length; i++)
        {
            string character = string.Empty;
            do
            {
                int index = new Random().Next(0, characters.Length);
                character = characters.ToCharArray()[index].ToString();
            } while (otp.IndexOf(character) != -1);
            otp += character;
        }
        return otp;

        #region OTP MAKING
        //<asp:DropDownList ID="ddlLength" runat="server">
        //        <asp:ListItem Text="5" Value="5" />
        //        <asp:ListItem Text="8" Value="8" />
        //        <asp:ListItem Text="10" Value="10" />
        //    </asp:DropDownList>

        //<asp:RadioButtonList ID="rbType" runat="server" RepeatDirection="Horizontal">
        //        <asp:ListItem Text="Alphanumeric" Value="1" Selected="True" />
        //        <asp:ListItem Text="Numeric" Value="2" />
        //    </asp:RadioButtonList>
        #endregion
    }


    public string MailBody(string Name, string OTP)
    {
        string mailbody = "";

        mailbody += "<table border='0' cellpadding='0' cellspacing='0' width='575' style='border-collapse:collapse;width:431pt'>";
        mailbody += "<tbody>";
        mailbody += "<tr height='102' style='height:76.5pt'>";
        mailbody += "<td height='102' class='m_4924402671878462581xl66' width='575' style='height:76.5pt;width:431pt'>";
        mailbody += "Dear &nbsp;&nbsp; " + Name + ",<br> <br> &nbsp; " + OTP + " &nbsp;is your one time password (<span class='il'>OTP</span>). Please enter the <span class='il'>OTP</span> to proceed.<br>";
        mailbody += "<br> Thank you,";
        mailbody += "<br> Team UmrawatiTrip";
        mailbody += "</td>";
        mailbody += "</tr>";
        mailbody += "</tbody>";
        mailbody += "</table>";

        //      mailbody +=  "<div id=':2ze' class='a3s aXjCH m15f5ed4bd573d618'>Dear Customer,<br>";
        //mailbody += "<br>";
        //mailbody += "Your One-Time Password (<span class='il'>OTP</span>) is 711610 for secure login .<br>";
        //mailbody+= "<br>";
        //mailbody+= "Sincerely,<br>";
        //mailbody+= "<br>";
        //mailbody+=  "RBL Bank<br>";
        //mailbody+=  "<br>";
        //mailbody+=  "This is a system-generated mail. Please do not reply to this email address. If you have a query or need any clarification, please contact Customer Care on<br>";
        ////For Debit Card: +91-22-61156300<br>
        ////For Credit Card: 1800 121 9050<br>
        //mailbody+=  "<br>";
        //mailbody+=  "<br>";
        //mailbody+=  "CONFIDENTIALITY INFORMATION AND DISCLAIMER<br>";
        //mailbody+=  "This e-mail message and its attachments may contain confidential, proprietary<br>";
        //mailbody+=  "or legally privileged information and is intended solely for the use of the individual or entity to whom it is addressed.<br>";
        //mailbody+=  "If you have received this message erroneously, please delete it immediately and notify the sender.<br>";
        //mailbody+=  "If you are not the intended recipient of this e-mail message, you should not disseminate, distribute or copy this e-mail.<br>";
        //mailbody += "E-mail transmission cannot be guaranteed to be secure or error-free, as information could be intercepted, corrupted, lost, destroyed or incomplete</div>";

        return mailbody;
    }

    private int SendEmail(string Name, string OTP, string ToEmailId)
    {
        int SendMail = 0;
        try
        {
            DataSet MailDs = new DataSet();
            DataTable MailDt = new DataTable();
            SqlTransactionDom STDom = new SqlTransactionDom();
            // MailDt = STDom.GetMailingDetails(MAILING.REGISTRATION_AGENT.ToString().Trim(), "");
            MailDs = STDom.GetMailingDetails(MAILING.REGISTRATION_AGENT.ToString().Trim(), "FWS");
            if (MailDs != null && MailDs.Tables.Count > 0 && MailDs.Tables[0].Rows.Count > 0)
            {
                MailDt = MailDs.Tables[0];
            }
            string strBody;
            string mailbody = "";
            mailbody += "<table border='0' cellpadding='0' cellspacing='0' width='575' style='border-collapse:collapse;width:431pt'>";
            mailbody += "<tbody>";
            mailbody += "<tr height='102' style='height:76.5pt'>";
            mailbody += "<td height='102' class='m_4924402671878462581xl66' width='575' style='height:76.5pt;width:431pt'>";
            mailbody += "Dear &nbsp;&nbsp; " + Name + ",<br> <br>" + OTP + " &nbsp;is your one time password (<span class='il'>OTP</span>). <br>Please enter the <span class='il'>OTP</span> to proceed and valid for next 20 mins.<br>";
            mailbody += "<br> Thank you,";
            mailbody += "<br><br> Team Support";
            mailbody += "</td>";
            mailbody += "</tr>";
            mailbody += "</tbody>";
            mailbody += "</table>";
            try
            {
                if ((MailDt.Rows.Count > 0))
                {
                    bool Status = false;
                    Status = Convert.ToBoolean(MailDt.Rows[0]["Status"].ToString());
                    if (Status == true)
                    {
                        string MailSubject = "your one time password(OTP) is   " + OTP + "  for login ";
                        //(ByVal toEMail As String, ByVal from As String, ByVal bcc As String, ByVal cc As String, ByVal smtpClient As String, ByVal userID As String, ByVal pass As String, ByVal body As String, ByVal subject As String, ByVal AttachmentFile As String) As Integer
                        SendMail = STDom.SendMail(ToEmailId, Convert.ToString(MailDt.Rows[0]["MAILFROM"]), Convert.ToString(MailDt.Rows[0]["BCC"]), Convert.ToString(MailDt.Rows[0]["CC"]), Convert.ToString(MailDt.Rows[0]["SMTPCLIENT"]), Convert.ToString(MailDt.Rows[0]["UserId"]), Convert.ToString(MailDt.Rows[0]["Pass"]), mailbody, MailSubject, "");
                    }
                }
            }
            catch (Exception ex)
            {
                clsErrorLog.LogInfo(ex);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        return SendMail;
    }
}