﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false"
    CodeFile="IntlAirlineMarkup.aspx.vb" Inherits="DetailsPort_Admin_IntlAirlineMarkup" %>

<%-- <%@ Register Src="~/UserControl/Settings.ascx" TagPrefix="uc1" TagName="Settings" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <script type="text/javascript">
        function isNumberKey(evt) {
            try {
                var e = event || evt; // for trans-browser compatibility
                var charCode = e.which || e.keyCode;
                if (charCode == 46)
                    return true
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }
            catch (exception) {
            }
        }
      
    </script>

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="row">
       <div class="col-md-12"  >
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight Setting > Intl. Airline Markup</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group" style="display:none;">
                                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                        <asp:ListItem Text="Domestic" Value="D"></asp:ListItem>
                                        <asp:ListItem Text="International" Selected="True" Value="I"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Agent Type:</label>
                                    <div class="large-3 medium-3 small-8 columns">
                                        <asp:DropDownList runat="server" data-placeholder="Choose a Airline..." TabIndex="2"
                                            ID="DropDownListType" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Airline Code:</label>
                                    <div>
                                        <asp:DropDownList runat="server" data-placeholder="Choose a Airline..." TabIndex="2"
                                            ID="airline_code" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                             </div>
                             <div class="col-md-4">                                
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Markup Type :</label>
                                    <div class="large-3 medium-3 small-8 columns">
                                        <asp:DropDownList ID="ddl_MarkupType" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="F" Selected="true">Fixed</asp:ListItem>
                                            <asp:ListItem Value="P">Percentage</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label for="exampleInputPassword1">Mark Up per pax</label>
                                    <asp:TextBox CssClass="form-control" runat="server" ID="mk"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFVMK" runat="server" ControlToValidate="mk" ErrorMessage="*"
                                        Display="dynamic"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                </div>
                              </div>                            
                            </div>
                        </div>

                        <div class="row">
                            
                          <div class="col-md-4">   
                                <asp:Button ID="btnAdd" runat="server" Text="New Entry" CssClass="button buttonBlue" />
                            </div>
                            <div class="col-md-4">
                                <asp:Button ID="btn_submit" runat="server" Text="Search" CssClass="button buttonBlue" />
                            </div>
                            </div>


         
                      <div class="row">
                           <div class="col-md-6">    
                          <asp:Label ID="lbl" runat="server" Style="color: #CC0000;" Font-Bold="True" Font-Size="15px"></asp:Label>
                          </div>
                          </div>

                        <div class="row">
                            <div class="col-md-12">
                                <asp:UpdatePanel ID="UP" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="lbl_msg" runat="server"
                                            Style="color: #CC0000;" Font-Bold="True" Visible="False"></asp:Label>
                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="counter"
                                            OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting"
                                            OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" PageSize="8"
                                            CssClass="table" GridLines="None" Width="100%">
                                            <Columns>
                                                <asp:CommandField ShowEditButton="True" />

                                                <%-- <asp:BoundField DataField="counter" HeaderText="Sr.No" ReadOnly="True" />--%>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_ID" runat="server" Text='<%#Eval("Counter") %>' CssClass="hide"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="UserId" HeaderText="Agent Type" ControlStyle-CssClass="textboxflight1"
                                                    ReadOnly="true">
                                                    <ControlStyle CssClass="textboxflight1"></ControlStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AirlineCode" HeaderText="AirlineCode" ControlStyle-CssClass="textboxflight1"
                                                    ReadOnly="true">
                                                    <ControlStyle CssClass="textboxflight1"></ControlStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Trip" HeaderText="Trip" ControlStyle-CssClass="textboxflight1"
                                                    ReadOnly="true">
                                                    <ControlStyle CssClass="textboxflight1"></ControlStyle>
                                                </asp:BoundField>
                                               <%-- <asp:BoundField DataField="MarkupValue"  HeaderText="Mark Up"  ControlStyle-CssClass="textboxflight1" >
                                                    <ControlStyle CssClass="textboxflight1" ></ControlStyle>
                                                </asp:BoundField>--%>
                                                <asp:TemplateField HeaderText="MarkUp">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMarkup" runat="server" Text='<%# Eval("MarkupValue")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtMarkUpAmt" runat="server" Text='<%# Eval("MarkupValue")%>' MaxLength="4" onkeypress="return isNumberKey(event)" ></asp:TextBox>                                              
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="MarkUp Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelMrkType" runat="server" Text='<%# Eval("MarkupType")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>

                                                        <asp:DropDownList ID="ddl_MarkupTypeE" runat="server" SelectedValue='<%# Eval("MarkupType")%>'>
                                                            <asp:ListItem Value="F" Selected="true">Fixed</asp:ListItem>
                                                            <asp:ListItem Value="P">Percentage</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <%--<asp:BoundField DataField="MarkupType" HeaderText="MarkUp Type" ControlStyle-CssClass="textboxflight1">
                                        <ControlStyle CssClass="textboxflight1"></ControlStyle>
                                    </asp:BoundField>--%>
                                                <asp:CommandField ShowDeleteButton="True" />
                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UP">

                                    <ProgressTemplate>
                                        <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                        </div>
                                        <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                            Please Wait....<br />
                                            <br />
                                            <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                            <br />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

</asp:Content>
