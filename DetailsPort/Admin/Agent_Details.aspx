﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false"
    CodeFile="Agent_Details.aspx.vb" Inherits="DetailsPort_Admin_Agent_Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<link href="<%=ResolveUrl("~/CSS/lytebox.css")%>" rel="stylesheet" type="text/css" />--%>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#From").datepicker({ dateFormat: 'dd-mm-yy' }).val();
            $(".cd").click(function () {
                $("#From").focus();
            }
            );
            $("#To").datepicker({ dateFormat: 'dd-mm-yy' }).val();
            $(".cd1").click(function () {
                $("#To").focus();
            }
            );
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%= btn_update.ClientID%>').click(function () {
                <%--var old = $('#<%= txtOldPassword.ClientID%>').val();--%>
                var newPass = $('#<%= txtNewPassword.ClientID%>').val();
                var confirmPass = $('#<%= txtConfirmPassword.ClientID%>').val();

                if (newPass == '') {
                    if (confirm("Are you sure!")) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    if (confirmPass == '') {
                        alert("Enter confirm password.");
                        return false;
                    }

                    var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,16}$/;
                    if (!regex.test(newPass)) {
                        alert("Password must contain:8-16 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character'");
                        document.newPass.focus();
                        return false;
                    }



                    if (confirmPass != newPass) {
                        alert("Password not confirm.");
                        $('#txtConfirmPassword').focus();
                        return false;
                    }
                    if (confirm("Are you sure!")) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            })
        });
    </script>

    <script language="javascript">



        function validatetype() {
            //if (document.getElementById("ddl_type").value == "--Select Type--") {
            //    alert('Please Select Agent Type');
            //    document.getElementById("ddl_type").focus();
            //    return false;
            //}
            //if (confirm("Are You Sure!!"))
            //    return true;
            //return false;
        }
        function checkitt(evt) {
            evt = (evt) ? evt : window.event
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (!(charCode > 47 && charCode < 58)) {
                return false;
            }
            status = "";
            return true;
        }

        function MyFunc() {
            alert("Agency record updated successfully.")
            window.open('Agent_Details.aspx', '_parent');
            window.close();
        }


        function showBox() {
            $("#td_displayBox").show();
            $("#showHeadingBox").hide();
        }
        function hideBox() {
            $("#td_displayBox").hide();
            $("#showHeadingBox").show();
        }
    </script>

    <script language="javascript" type="text/javascript">
        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }
    </script>

    <%-- <link href="../../css/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />--%>
    <%-- <style type="text/css">
        input[type="text"], input[type="password"], select, radio, legend, fieldset
        {
            border: 1px solid #004b91;
            padding: 2px;
            font-size: 1em;
            color: #444;
            width: 150px;
            font-family: arial, Helvetica, sans-serif;
            font-size: 12px;
            font-weight: normal;
            border-radius: 3px 3px 3px 3px;
            -webkit-border-radius: 3px 3px 3px 3px;
            -moz-border-radius: 3px 3px 3px 3px;
            -o-border-radius: 3px 3px 3px 3px;
        }
    </style>--%>
    <%--<link href="../../CSS/lytebox.css" rel="stylesheet" type="text/css" />--%>
    <%--<script src="../../JS/lytebox.js" type="text/javascript"></script>--%>
    <%--  <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />--%>
    <%--<style type="text/css">
        .txtBox
        {
            width: 140px;
            height: 18px;
            line-height: 18px;
            border: 2px #D6D6D6 solid;
            padding: 0 3px;
            font-size: 11px;
        }
        .txtCalander
        {
            width: 100px;
            background-image: url(../../images/cal.gif);
            background-repeat: no-repeat;
            background-position: right;
            cursor: pointer;
            border: 1px #D6D6D6 solid;
        }
    </style>--%>

    <!--slide right on button click-->

    <style type="text/css">
        .panel-body
        {
            height: 100%;
            overflow: inherit;
        }

        #slidediv
        {
            color: #F25022;
            background-color: #fff;
            border: 2px solid #00A4EF;
            display: none;
            height: 550px;
        }

            #slidediv p
            {
                margin: 15px;
                font-size: 0.917em;
            }

        #contentdiv
        {
            clear: both;
            margin: 0 auto;
            max-width: initial;
        }
    </style>

    <script type="text/javascript">

        myfunction = function () {
            debugger;
            $('#slidediv').toggle('slide', { direction: 'right' }, 0);
            $("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
            $("#backgroundPopup").fadeIn(0001);


        }
        $("#cancle_update").click(function () {
            $('#slidediv').toggle('slide', { direction: 'left' }, 0);
            $("#backgroundPopup").fadeOut("normal");
            popupStatus = 0;  // and set value to 0
        });


    </script>

    <%--<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>--%>
    <script>
        function openNav() {
            document.getElementById("slidediv").style.width = "100%";
        }

        function closeNav() {
            document.getElementById("slidediv").style.width = "0%";
        }
    </script>


    <!--Password Change-->
    <script>
        function showbox() {
            $("#td_displaybox").show();
            $("#showHeadingBox").hide();
        }

        function hidebox() {
            $("#td_displaybox").hide();
            $("#showHeadingBox").show();
        }
    </script>

    <!--blur filter-->


    <%-- <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />--%>
    <style type="text/css">
        .page-wrapperss
        {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>



    <!--overlay-->
    <style>
        .overlay
        {
            position: fixed;
            top: 102px;
            left: 287px;
            right: 0;
            bottom: 0;
            width: auto;
            height: 72%;
            background: #fff;
            opacity: 1.5;
            z-index: 1;
            display: inline-block;
        }

        #backgroundPopup
        {
            z-index: 3;
            position: fixed;
            display: none;
            height: 100%;
            width: 100%;
            background: #0000009e;
            top: 0px;
            left: 0px;
        }
    </style>

    <div id="backgroundPopup">
    </div>
    <div class="row">
        <div id="slidediv" class="model overlay" style="z-index: 5;">
            <!--Hidden Field-->
            <%--                            <a href="#slidediv" data-rel="slide left" class="ui-btn-right ui-btn ui-btn-b ui-corner-all ui-btn-icon-notext ui-icon-delete ui-shadow">Close</a>--%>
            <%--<p>--%><div class=" col-lg-12 col-sm-12 col-xs-12" style="margin-left: 16px;">
                <%--   <div class="panel panel-primary">

                <div class="panel-body">--%>
                
                <div class="row col-md-12" style="background-color: aliceblue; margin-left: -15px;">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="exampleInputPassword1">User ID:</label>
                            <asp:Label ID="td_AgentID" CssClass="input-text full-width" runat="server"></asp:Label>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Avilable Balance</label>
                            <asp:Label ID="td_CrLimit" CssClass="input-text full-width" runat="server"></asp:Label>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">

                            <label for="exampleInputPassword1">Credit Limit</label>
                            <asp:Label ID="lblAgentLimit" CssClass="input-text full-width" runat="server"></asp:Label>

                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Due Amount :</label>
                            <asp:Label ID="lblDueAmount" CssClass="input-text full-width" runat="server"></asp:Label>
                        </div>
                    </div>


                    <div class="col-md-4" style="display: none;">
                        <!--Hiddenfield-->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Tds:</label>
                                <asp:Label ID="td_tds" CssClass="input-text full-width" runat="server"></asp:Label>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Last Transaction Date</label>
                                <asp:Label ID="td_LTDate" CssClass="input-text full-width" runat="server"></asp:Label>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                        <h3>Brand Details</h3>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Address</label>
                            <asp:TextBox ID="txt_Address" placeholder="Address" CssClass="input-text full-width" runat="server"></asp:TextBox>


                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Brand Name :</label>
                            <asp:TextBox ID="txt_AgencyName" placeholder="Brand Name" runat="server" Enabled="False" CssClass="input-text full-width" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz0123456789');" MaxLength="50"></asp:TextBox>
                        </div>

                    </div>



                    <div class="col-md-2">
                        <div class="form-group">
                            <label>City</label>
                            <asp:TextBox ID="txt_City" placeholder="City" runat="server" Enabled="False" CssClass="input-text full-width" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz');" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>State</label>
                            <asp:TextBox ID="txt_State" placeholder="State" runat="server" Enabled="False" CssClass="input-text full-width" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz');" MaxLength="50"></asp:TextBox>
                        </div>

                    </div>




                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Zip Code</label>
                            <asp:TextBox ID="txt_zip" placeholder="Zip Code" runat="server" Enabled="False" MaxLength="8" CssClass="input-text full-width" onkeypress="return checkitt(event)"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="regexpcontactZipCode" runat="server" ControlToValidate="txt_zip"
                                ValidationGroup="contactValidation" ForeColor="Red" ErrorMessage="Enter Only Digit"
                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Country</label>
                            <asp:TextBox ID="txt_Country" placeholder="Country" runat="server" Enabled="False" CssClass="input-text full-width" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz');" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <%--   <div class="col-lg-12 col-sm-12 col-xs-12">
                        <h3>Brand Details</h3>
                    </div>--%>



                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Title</label>
                            <asp:TextBox ID="txt_title" placeholder="Title" runat="server" Enabled="False" Width="100px" CssClass="input-text full-width"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>First Name</label>
                            <asp:TextBox ID="txt_Fname" placeholder="First Name" runat="server" Enabled="False" Width="90%" CssClass="input-text full-width"></asp:TextBox>
                        </div>

                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Last Name</label>
                            <asp:TextBox ID="txt_Lname" placeholder="Last Name" runat="server" Enabled="False" Width="90%" CssClass="input-text full-width"></asp:TextBox>
                        </div>
                    </div>


                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Mobile</label>
                            <asp:TextBox ID="txt_Mobile" placeholder="Mobile" runat="server" MaxLength="10" Enabled="False" onkeypress="return checkitt(event)" CssClass="input-text full-width"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Email</label>

                            <asp:TextBox ID="txt_Email" placeholder="Email" runat="server" Enabled="False" CssClass="input-text full-width" MaxLength="50"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txt_Email" ValidationGroup="group1" ErrorMessage=" enter valid email " ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>PAN Name</label>
                            <asp:TextBox ID="TxtNameOnPancard" placeholder="PAN Name" runat="server" Enabled="False" CssClass="input-text full-width" MaxLength="50" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz');"></asp:TextBox>
                        </div>
                    </div>
                </div>



                <div class="row">


                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Pan No</label>
                            <asp:TextBox ID="txt_Pan" placeholder="PAN No." runat="server" Enabled="False" MaxLength="15" CssClass="input-text full-width" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz1234567890');"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Fax No</label>
                            <asp:TextBox ID="txt_Fax" placeholder="Fax No." runat="server" Enabled="False" CssClass="input-text full-width" onKeyPress="return keyRestrict(event,'1234567890');"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>OTP Login</label>
                            <asp:CheckBox ID="OPTLOGIN" runat="server" />
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="from-group">
                            <label>Exp Password Login</label>
                            <asp:CheckBox ID="EXPPASS" runat="server" />
                        </div>
                    </div>

                    <div class="col-md-2" style="display: none;">
                        <div class="col-md-12" id="td_pwd" runat="server">
                            <div class="form-group">
                                <label>Password</label>

                                <asp:TextBox ID="txt_pwd" runat="server" Enabled="False" CssClass="input-text full-width"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                        <h3>Update Details</h3>
                    </div>


                    <div class="col-md-2">
                        <div class="form-group">
                            <%--<label>Type</label>--%>
                            <asp:DropDownList ID="ddl_type" runat="server" CssClass="full-width">
                                <%-- <asp:ListItem  value="" disabled selected style="display: none;">Type</asp:ListItem>--%>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <%--<label> Sales Ref</label>--%>
                            <%--<asp:TextBox ID="txt_saleref" runat="server" CssClass="form-control"></asp:TextBox>--%>
                            <asp:DropDownList ID="Sales_DDL" runat="server" CssClass="input-text full-width">
                                <%--        <asp:ListItem  value="" disabled selected style="display: none;">Sales Ref</asp:ListItem>--%>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <%-- <label> Activation</label>--%>
                            <asp:DropDownList ID="ddl_activation" runat="server" CssClass="input-text full-width">
                                <asp:ListItem Value="" disabled Selected style="display: none;">Activation</asp:ListItem>
                                <asp:ListItem Value="ACTIVE">ACTIVE</asp:ListItem>
                                <asp:ListItem Value="NOT ACTIVE">NOT ACTIVE</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <%--<label>Ticketing Activation</label>--%>
                            <asp:DropDownList ID="ddl_TicketingActiv" runat="server" CssClass="input-text full-width">
                                <asp:ListItem Value="" disabled Selected style="display: none;">Ticketing Activation</asp:ListItem>
                                <asp:ListItem Value="ACTIVE">ACTIVE</asp:ListItem>
                                <asp:ListItem Value="NOT ACTIVE">NOT ACTIVE</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-2" id="Div1" runat="server">
                        <div class="form-group">
                            <%--<label>Agent Credit</label>--%>
                            <asp:TextBox ID="TxtAgentCredit" placeholder="Agent Credit" runat="server" CssClass="input-text full-width" ReadOnly="true"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-2" style="margin-top: 27px;">
                        <div class="row">
                            <div class="form-group" style="margin-top: -24px; margin-left: 16px;">
                                <asp:Button ID="btn_update" runat="server" ValidationGroup="group1" Text="Update" CssClass="btn btn-success" OnClientClick="return validatetype()" />
                                &nbsp 
                              <asp:Button ID="cancle_update" runat="server" Text="Cancel" CssClass="btn btn-danger" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%--Update Password box--%>
            <div id="showHeadingBox">
                <div class="row">
                    <%--<div class="col-lg-12 col-sm-12 col-xs-12">
                            <asp:HyperLink ID="btnHyper" Text="Click here to update password" runat="server" onClick="showBox();"  style="    margin-left: 32px;"></asp:HyperLink>
                        </div>--%>
                </div>
            </div>
            <div id="td_displayBox" style="display: none; width: 95%; margin: auto; padding-bottom: 10px; border: 1px solid #94aee5; border-radius: 5px;">
                <br />
                <div style="display: none">
                    <h4 style="font-size: 16px; font-weight: bold;">Update Agency Password</h4>
                    <br />

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <asp:Label ID="lbl_currentPassMsg" runat="server" Text="Current Password:" Visible="false" Font-Size="12px"></asp:Label>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="lbl_currentPassword" runat="server" Text="" Visible="false" Font-Size="12px" Font-Bold="true"></asp:Label>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <%--<label>New Password</label>--%>

                                <asp:TextBox ID="txtNewPassword" placeholder="New Password" runat="server" TextMode="Password" MaxLength="16" CssClass="input-text full-width"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <%--<label>Confirm Password</label>--%>

                                <asp:TextBox ID="txtConfirmPassword" placeholder="Confirm Password" runat="server" TextMode="Password" MaxLength="16" CssClass="input-text full-width"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="large-2 medium-2 small-12">
                        <a onclick="hideBox();" href="#">
                            <img src="<%= ResolveUrl("~/Images/close.png")%>" align="right" alt="Close" title="Close this box" /></a>
                    </div>
                </div>
                <br />
            </div>


            <%--End password updation--%>


            <%-- </div>

                    </div>--%>
            <%--</p>--%>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 container-fluid" style="padding-right: 35px;">
            <div class="page-wrapperss">
                <div class="panel panel-primary">


                    <div class="panel-body">
                        <div id="agent_details" runat="server">
                        <asp:Label ID="lblerror" CssClass="input-text full-width" runat="server"></asp:Label>
                            <div class="row">
                                <div class="col-xs-2">
                                    <div class="input-group">
                                        <%--<label for="exampleInputPassword1"></label>--%>
                                        <input type="text" name="From" id="From" class="form-control input-text lg-width" readonly="readonly" placeholder="REG. FROM :" />
                                        <span class="input-group-addon" style="background-color: #49cced">
                                            <span class="glyphicon glyphicon-calendar cd" style="cursor:pointer;"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <div class="input-group">
                                        <%--    <label for="exampleInputPassword1">REGISTRATION TO :</label>--%>
                                        <input type="text" name="To" id="To" class="form-control input-text lg-width" readonly="readonly" placeholder="REG. TO :" />
                                        <span class="input-group-addon" style="background-color: #49cced">
                                            <span class="glyphicon glyphicon-calendar cd1" style="cursor:pointer;"></span>
                                        </span>
                                    </div>
                                </div>

                                <div class="col-xs-2">
                                    <div class="input-group">
                                        <%--  <label for="exampleInputPassword1">BRAND:</label>--%>
                                        <input type="text" id="txtAgencyName" name="txtAgencyName" placeholder="BRAND NAME"
                                            autocomplete="off" value=""
                                            class="form-control input-text med-width" />
                                        <span class="input-group-addon" style="background-color: #49cced">
                                            <span class="fa fa-black-tie"></span>
                                        </span>
                                        <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />

                                    </div>
                                </div>
                                <div class="col-xs-2" style="margin-top: -5px">
                                    <div class="input-group" id="td_SBS" runat="server">
                                        <label for="exampleInputPassword1" id="td_ddlSBS" runat="server"></label>
                                        <asp:DropDownList ID="ddl_stock" runat="server" CssClass="input-text med-width" Width="180px">
                                            <asp:ListItem Text="MERCHANDISER:" Value=""></asp:ListItem>
                                            <asp:ListItem Text="All MENCHANDISER" Value="ALL"></asp:ListItem>
                                            <asp:ListItem Text="BRAND" Value="STAG"></asp:ListItem>
                                        </asp:DropDownList>
                                        <%--                                        <span class="input-group-addon" style="background-color: #49cced">
                        <span class="glyphicon glyphicon-cog"></span>
                                               </span>--%>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group" id="tr_AgentType" runat="server">
                                        <%-- <label for="exampleInputPassword1" id="tr_GroupType" runat="server">BRAND Type :</label>--%>
                                        <asp:DropDownList ID="DropDownListType" runat="server" CssClass="input-text sm-width" Width="180px">
                                            <asp:ListItem Text="BRAND TYPE" Value=""></asp:ListItem>
                                            <%--<asp:ListItem Text="Select" Value="Select" Selected="True"></asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-sm-2" style="margin-top: 5px">
                                    <div class="form-group">
                                        <asp:Button ID="btn_Search" runat="server" Text="Go" Width="50px" CssClass="btn btn-success" />

                                        <asp:Button ID="export" runat="server" Text="Export To Excel" CssClass="btn btn-success" />
                                    </div>
                                </div>



                            </div>
                            <div class="row">

                                <div class="col-md-2">
                                    <div class="form-group" id="tr_SalesPerson" runat="server">
                                        <label for="exampleInputPassword1" id="tr_ddlSalesPerson" runat="server">Search Sales Person :</label>
                                        <asp:DropDownList ID="DropDownListSalesPerson" runat="server" AppendDataBoundItems="true"
                                            CssClass="selector">
                                            <asp:ListItem Text="Select" Value="Select" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <%--</div>--%>
                            </div>
                        </div>


                    </div>














                    <div class="row" id="grd_view" style="background-color: #fff; overflow: auto;" runat="server">


                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                            AllowSorting="True" PageSize="25" CssClass="table table-striped table-bordered table-hover" GridLines="None" Width="100%" OnRowCommand="GridView1_RowCommand">
                            <%-- OnRowEditing="GridView1_RowEditing"--%>
                            <Columns>
                                <asp:TemplateField HeaderText="UPDATE">
                                    <ItemTemplate>

                                        <%--<p><asp:Button ID="btnright"   CssClass="btn btn-info" runat="server" Text="Edit"  Font-Bold="true" /></p> --%>
                                        <%--<button type="button" id="btnright" class="btn btn-info"   onclick="btnright_Click" >Edit</button>--%>
                                        <asp:ImageButton ID="lnkvendorname" ImageUrl="~/Images/icons/edit_editor_pen_pencil_write-512.png" style="width: 23px;" runat="server" CommandArgument='<%#Eval("user_id")%>' Text="EDIT"></asp:ImageButton>


                                        <%--<asp:Button id="btnright" type="button" cssclass="btn btn-info"  href="#slidediv" onclientclick="btnright_Click" runat="server" text="Edit" CausesValidation="False"  EnableViewState="True"/>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Debit/Credit" SortExpression="user_id" Visible="false">
                                    <ItemTemplate>
                                        <a target="_blank" href="../Distr/UploadAmount.aspx?AgentID=<%#Eval("user_id")%>"
                                            rel="lyteframe" rev="width: 900px; height: 280px; overflow:hidden;" target="_blank"
                                            style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">Debit/Credit </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="BRAND NAME" SortExpression="Agency_Name" ItemStyle-ForeColor="Black">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFirstName" runat="server" Text='<%#Eval("Agency_Name")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--   <asp:TemplateField HeaderText="USER_ID" SortExpression="user_id" ItemStyle-ForeColor="Black">
                                                <ItemTemplate>
                                                    <a href='Update_Agent.aspx?AgentID=<%#Eval("user_id")%>' rel="lyteframe" rev="width: 900px; height: 400px; overflow:hidden;"
                                                        target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                        <asp:Label ID="Label1" runat="server" Text='<%#Eval("user_id")%>'></asp:Label>
                                                    </a>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                <asp:BoundField DataField="AgencyId" HeaderText="BRAND ID" SortExpression="AgencyId" />
                                <asp:BoundField DataField="Agent_Type" HeaderText="BRAND TYPE" SortExpression="Agent_Type" />
                                <%-- <asp:BoundField DataField="Crd_Limit" HeaderText="Credit Limit" SortExpression="Crd_Limit" />--%>
                                <asp:BoundField DataField="timestamp_create" HeaderText="REGISTRATION DATE" SortExpression="timestamp_create" />
                                <asp:BoundField DataField="Crd_Trns_Date" HeaderText="TRANSACTION DATE" SortExpression="timestamp_create" />
                                <asp:BoundField DataField="Mobile" HeaderText="MOBILE" SortExpression="Mobile" />
                                <asp:BoundField DataField="Email" HeaderText="EMAIL" SortExpression="Email" />
                                <asp:BoundField DataField="SalesExecID" HeaderText="TRADESPERSON Ref." SortExpression="SalesExecID" />
                                <asp:BoundField DataField="Agent_status" HeaderText="STATUS" SortExpression="Agent_status" />
                                <asp:BoundField DataField="Online_Tkt" HeaderText="ONLINE TKT" SortExpression="Online_Tkt" />

                                <asp:BoundField DataField="Balance" HeaderText="BALANCE" SortExpression="Balance" />
                                <asp:BoundField DataField="DueAmount" HeaderText="DUEAMOUNT" SortExpression="DueAmount" />
                                <asp:BoundField DataField="AgentLimit" HeaderText="LIMIT" SortExpression="AgentLimit" />
                                <asp:BoundField DataField="NamePanCard" HeaderText="PANCARD NAME" SortExpression="NamePanCard" />
                                <asp:BoundField DataField="PanNo" HeaderText="PAN NO." SortExpression="AgentLimit" />


                            </Columns>




                            <HeaderStyle CssClass="HeaderStyle" Font-Bold="False" Font-Strikeout="False" />


                        </asp:GridView>

                    </div>



                </div>
            </div>
        </div>
    </div>




    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>





</asp:Content>
