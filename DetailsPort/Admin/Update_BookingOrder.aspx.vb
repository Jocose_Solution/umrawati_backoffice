﻿
Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports YatraBilling
Imports System.IO

Imports HtmlAgilityPack
Imports System.Linq

Partial Class Update_BookingOrder
    Inherits System.Web.UI.Page

    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    Private ObjIntDetails As New IntlDetails()
    Dim objTktCopy As New clsTicketCopy
    ''Dim objTktCopy As New IntlDetails()
    Dim SelectDetails As New IntlDetails
    Private ID As New IntlDetails
    Dim adap As SqlDataAdapter
    Dim objSql As New SqlTransactionNew
    Dim STDom As New SqlTransactionDom
    Private ST As New SqlTransaction()
    Dim ImageUrl As String
    Dim GrdDS As New DataSet()
    Private clsInsSelectflt As New clsInsertSelectedFlight()
    Private ClsCorp As New ClsCorporate
    Dim objUMSvc As New FltSearch1()
    Dim objDA As New SqlTransaction
    Dim trackId As String
    Dim OBFltDs, IBFltDs As DataSet
    Dim FltHeaderList As New DataTable()
    Dim fltTerminalDetails As New DataTable()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CType(Page.Master.FindControl("lblBC"), Label).Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Flight</a><a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/Admin/QCTicketReport.aspx") + " data-original-title=''>Booking Lookup</a><a class='current' href='#'>Update Booking orderId(" + (Request.QueryString("OrderId")) + ")</a>"
        Try
            If Not IsPostBack Then
                Dim OrderId As String = Request.QueryString("OrderId")
                Dim TransTD As String = Request.QueryString("TransID")
                Dim Status As String = Request.QueryString("Status").Trim()
                ''Dim mod_status As String = Request.QueryString("mod_status").Trim()
             

                ViewState("OrderId") = OrderId





                If (Status = "PNR ON HOLD") Then 'chandan


                    btnaccept.Visible = True
                    btnreject.Visible = True
                    GvFlightHeader.Columns(14).Visible = False



                End If



                'tdRefNo.InnerText = OrderId.ToString()
                'Getting Header Details
                BindFltHeader()



                If (Status = "Rejected" OrElse Status = "Pending") Then
                    GvFlightHeader.Columns(14).Visible = False
                    'GvTravellerInformation.Columns(9).Visible = True
                    btn_update1.Visible = False




                End If

                BindLedgDetails()
                BindMealbagg()
                'Getting Pax Details
                BindTravellerInformation()

                Dim dtagentidd As New DataTable()
                dtagentidd = ObjIntDetails.SelectAgent(Request.QueryString("OrderId"))
                ViewState("agentid") = dtagentidd.Rows(0)("AgentID").ToString()
                BindAgency(dtagentidd.Rows(0)("AgentID").ToString())


                'Getting Flight Details
                BindFlightDetails()

                'Getting Fare Details
                lblFareInformation.Text = objTktCopy.FareDetail_N(OrderId, TransTD)

            End If

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try

    End Sub

    Public Sub BindFltHeader()
        Try
            Dim OrderId As String = Request.QueryString("OrderId")
            Dim dtHeader As New DataTable
            dtHeader = SelectHeaderDetail()

            ViewState("Status_val") = dtHeader.Rows(0)("Status")

            ViewState("flthrds") = dtHeader
            If Not String.IsNullOrEmpty(dtHeader.Rows(0)("Status")) Then
                If (ViewState("Status_val") = "InProcess") OrElse (ViewState("Status_val") = "ConfirmByAgent") Then 'chandan
                    btnaccept.Visible = False
                    btnreject.Visible = False
                    GvFlightHeader.Columns(14).Visible = True


                End If

                If (ViewState("Status_val") = "Ticketed") OrElse (ViewState("Status_val") = "PreConfirmByAgent") OrElse (ViewState("Status_val") = "Rejected") OrElse (ViewState("Status_val") = "Failed") Then
                    btnaccept.Visible = False
                    btnreject.Visible = False
                    GvFlightHeader.Columns(14).Visible = False


                End If
                If (ViewState("Status_val") = "Confirm") Then 'chandan
                    btnaccept.Visible = True
                    btnreject.Visible = True


                End If


            End If

            GvFlightHeader.DataSource = dtHeader
            GvFlightHeader.DataBind()

            grd_GST.DataSource = dtHeader
            grd_GST.DataBind()

        Catch ex As Exception

        End Try
    End Sub

    Public Sub BindAgency(ByVal Agencyid As String)
        Try
            'Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            Dim adap As New SqlDataAdapter("AgencyINFO", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@AGENCYID", Agencyid.Trim)
            Dim dt2 As New DataTable()
            adap.Fill(dt2)
            ViewState("Agency_Name") = dt2.Rows(0)("Agency_Name").ToString()
            ViewState("agent_balance") = dt2.Rows(0)("Crd_Limit").ToString()
            ''ViewState("Crd_limit") = dt2.Rows(0)("Crd_Limit").ToString()
            Grd_AgencyDetails.DataSource = dt2
            Grd_AgencyDetails.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Public Function BindAgencyaval(ByVal Agencyid As String) As Double
        Dim Crd_limit As Double
        Try
            'Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            Dim adap As New SqlDataAdapter("AgencyINFO", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@AGENCYID", Agencyid.Trim)
            Dim dt2 As New DataTable()
            adap.Fill(dt2)
            Crd_limit = Convert.ToDouble(dt2.Rows(0)("Crd_Limit").ToString())
        Catch ex As Exception

        End Try
        Return Crd_limit
    End Function

    Public Sub BindLedgDetails()
        Try
            Dim OrderId As String = Request.QueryString("OrderId")
            'Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            Dim adap As New SqlDataAdapter("ledgerInfo", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@orderid", OrderId.Trim)
            Dim dt2 As New DataTable()
            adap.Fill(dt2)
            Grd_LedgerDetails.DataSource = dt2
            Grd_LedgerDetails.DataBind()

            checkLedgr(dt2)
        Catch ex As Exception

        End Try
    End Sub

    Public Sub checkLedgr(ByVal dt As DataTable)
        Try

            Dim OrderId As String = Request.QueryString("OrderId")
            'Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            Dim adap As New SqlDataAdapter("sp_LookUpReacord", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@orderid", OrderId.Trim)
            Dim dt2 As New DataTable()
            adap.Fill(dt2)

            Status.Visible = False
            MessageHold.Visible = False
            ddtypediv.Visible = False



            'Status.Visible = True
            'MessageHold.Visible = True
            'ddtypediv.Visible = True

            If (Convert.ToInt32(dt2.Rows(0)("pax")) > 0 And Convert.ToInt32(dt2.Rows(0)("fare")) > 0 And Convert.ToInt32(dt2.Rows(0)("flt")) > 0) Then

                If (ViewState("Status_val") = "Request") Then
                    MessageHold.Visible = True
                    ddtypediv.Visible = True
                    Status.Visible = True
                    If (dt.Rows.Count > 0) Then

                    End If

                End If
            End If

        Catch ex As Exception

        End Try
    End Sub


    Public Sub BindMealbagg()
        Try
            Dim OrderId As String = Request.QueryString("OrderId")
            'Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            Dim adap As New SqlDataAdapter("sp_fltmealbagg", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@orderId", OrderId.Trim)
            Dim dt2 As New DataTable()
            adap.Fill(dt2)
            Grd_Mealbagg.DataSource = dt2
            Grd_Mealbagg.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    'personal info
    Public Sub BindTravellerInformation()
        Try
            ' Dim adap As New SqlDataAdapter("select * from fltPaxDetails where OrderId = '" & OrderId & "' ", con)
            'Dim dtPaxDetails As New DataTable
            'adap.Fill(dtPaxDetails)
            Dim OrderId As String = Request.QueryString("OrderId")
            'Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            Dim adap As New SqlDataAdapter("usp_FltPaxDetails_lookup", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@orderid", OrderId.Trim)
            Dim dt2 As New DataTable()
            adap.Fill(dt2)



            GvTravellerInformation.DataSource = dt2
            GvTravellerInformation.DataBind()

        Catch ex As Exception

        End Try

    End Sub

    'flight info
    Public Function SelectHeaderDetail() As DataTable
        Dim dt2 As New DataTable()
        Try
            ' Dim adap As New SqlDataAdapter("select * from fltPaxDetails where OrderId = '" & OrderId & "' ", con)
            'Dim dtPaxDetails As New DataTable
            'adap.Fill(dtPaxDetails)
            Dim OrderId As String = Request.QueryString("OrderId")
            'Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            Dim adap As New SqlDataAdapter("usp_Get_FltHeaderDetailsWithPnrMessage_n", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@orderid", OrderId.Trim)

            adap.Fill(dt2)


        Catch ex As Exception

        End Try
        Return dt2
    End Function
    'flight details
    Public Sub BindFlightDetails()
        Try
            Dim OrderId As String = Request.QueryString("OrderId")
            'Dim adapFlightDetails As New SqlDataAdapter("select * from FltDetails where OrderId = '" & OrderId & "' ", con)
            Dim adap As New SqlDataAdapter("sp_fltdetails", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@orderid", OrderId.Trim)
            Dim dt2 As New DataTable()
            adap.Fill(dt2)
            GvFlightDetails.DataSource = dt2
            GvFlightDetails.DataBind()
        Catch ex As Exception

        End Try


    End Sub

    'Protected Sub ITZ_Accept_Click(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GvFlightHeader.RowUpdating

    '    Try
    '        Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    '        Dim OrderId As String = Request.QueryString("OrderId")

    '        Dim txt_AirlinePNR As TextBox = TryCast(DirectCast(GvFlightHeader.Rows(e.RowIndex).FindControl("txtAirlinePnr"), TextBox), TextBox)
    '        Dim txt_GDSPNR As TextBox = TryCast(DirectCast(GvFlightHeader.Rows(e.RowIndex).FindControl("txtGdsPnr"), TextBox), TextBox)
    '        Dim txtTicketIdr As TextBox = TryCast(DirectCast(GvFlightHeader.Rows(e.RowIndex).FindControl("txtTicketIdr"), TextBox), TextBox)

    '        ''Dim Partnername As String = txtPartnerName.SelectedItem.Value
    '        Dim Partnername As String = "TBO"
    '        Try
    '            Dim cmd As New SqlCommand("USP_updatePnrSp_PP")
    '            cmd.CommandType = CommandType.StoredProcedure
    '            cmd.Parameters.AddWithValue("@Order_id", OrderId)
    '            cmd.Parameters.AddWithValue("@AirLinePnr", txt_AirlinePNR.Text.Trim())
    '            cmd.Parameters.AddWithValue("@GdsPnr", txt_GDSPNR.Text.Trim())
    '            cmd.Parameters.Add("@PartnerName", SqlDbType.VarChar).Value = Partnername
    '            cmd.Connection = con
    '            con.Open()
    '            cmd.ExecuteNonQuery().ToString()
    '            con.Close()
    '        Catch ex As Exception
    '            clsErrorLog.LogInfo(ex)
    '        End Try

    '        Dim dt As New DataTable
    '        'dt = ViewState("TFADIS")
    '        dt = ID.SelectHeaderDetail(OrderId)

    '        Dim i As Integer = 0
    '        'For Each rw As RepeaterItem In Repeater_Traveller.Items
    '        '    i += 1

    '        '    Dim TktNo As TextBox = DirectCast(rw.FindControl("txt_TktNo"), TextBox)
    '        '    Dim TID As Label = DirectCast(rw.FindControl("lbl_TransID"), Label)
    '        '    Dim PaxType As Label = DirectCast(rw.FindControl("lbl_paxtype"), Label)
    '        '    Dim Fare As Label = DirectCast(rw.FindControl("lbl_Fare"), Label)
    '        '    If dt.Rows(0)("VC").ToString.Trim.ToUpper <> "6E" And dt.Rows(0)("VC").ToString.Trim.ToUpper <> "SG" And dt.Rows(0)("VC").ToString.Trim.ToUpper <> "G8" Then
    '        '        ID.UpdateTicketIntl(OrderId, TID.Text, TktNo.Text.Trim())
    '        '        STDom.UpdateTktDomIntlOnLedger(OrderId, Convert.ToInt32(TID.Text.Trim), TktNo.Text.Trim())
    '        '    Else
    '        '        ID.UpdateTicketIntl(OrderId, TID.Text, TktNo.Text.Trim() & i.ToString)
    '        '        STDom.UpdateTktDomIntlOnLedger(OrderId, Convert.ToInt32(TID.Text.Trim), TktNo.Text.Trim() & i.ToString)
    '        '    End If
    '        'Next

    '        'NAV METHOD CALL START
    '        Try
    '            ''Call ITQFINACIAL PUSH
    '            'Dim objITQ As New LNBITQFINANCEAPI.ITQFinance()
    '            'objITQ.PushToITQFinance(OrderId, "Air", "D")
    '            'Dim objNav As New AirService.clsConnection(trackIdOneWay, "0", "0")
    '            'objNav.airBookingNav(trackIdOneWay, "", 0)

    '        Catch ex As Exception

    '        End Try


    '        'NAV METHOD  CALL START
    '        Try
    '            UpdateSearchNBookID(OrderId, txtTicketIdr.Text.Trim)
    '        Catch ex As Exception

    '        End Try

    '        Try



    '        Catch ex As Exception
    '            clsErrorLog.LogInfo(ex)
    '        End Try
    '        'Nav METHOD END'
    '        Try

    '            'STYTR.InsertYatra_MIRHEADER(OrderId, txt_GDSPNR.Text.Trim())
    '            'STYTR.InsertYatra_PAX(OrderId, txt_GDSPNR.Text.Trim())
    '            'STYTR.InsertYatra_SEGMENT(OrderId, txt_GDSPNR.Text.Trim())
    '            'STYTR.InsertYatra_FARE(OrderId, txt_GDSPNR.Text.Trim())
    '            'STYTR.InsertYatra_DIFTLINES(OrderId, txt_GDSPNR.Text.Trim())

    '            'Dim AirObj As New AIR_YATRA
    '            'AirObj.ProcessYatra_Air(OrderId, txt_GDSPNR.Text.Trim(), "B")
    '        Catch ex As Exception
    '            clsErrorLog.LogInfo(ex)
    '        End Try
    '        Try

    '            Dim dtflight As New DataTable()
    '            dtflight = ID.SelectFlightDetail(OrderId)
    '            mailTktCopy(dt.Rows(0)("VC").ToString.Trim, dtflight.Rows(0)("FltNumber").ToString.Trim, dt.Rows(0)("Sector").ToString.Trim, dtflight.Rows(0)("DepDate").ToString.Trim, dt.Rows(0)("FareType").ToString.Trim, dt.Rows(0)("AirlinePnr").ToString.Trim, dt.Rows(0)("GdsPnr").ToString.Trim, dt.Rows(0)("Status").ToString.Trim, OrderId, dt.Rows(0)("PgEmail").ToString.Trim)
    '        Catch ex As Exception
    '            clsErrorLog.LogInfo(ex)
    '        End Try
    '        Try
    '            Dim smsStatus As String = ""
    '            Dim smsMsg As String = ""
    '            Dim objSMSAPI As New SMSAPI.SMS
    '            Dim dtpnr As New DataTable()
    '            dtpnr = ID.SelectHeaderDetail(OrderId)
    '            Dim dtflight As New DataTable()
    '            dtflight = ID.SelectFlightDetail(OrderId)
    '            Dim SmsCrd As DataTable
    '            Dim objDA As New SqlTransaction
    '            SmsCrd = objDA.SmsCredential(SMS.AIRHOLDCNF.ToString()).Tables(0)
    '            If SmsCrd.Rows.Count > 0 AndAlso SmsCrd.Rows(0)("Status") = True Then
    '                smsStatus = objSMSAPI.sendSms(OrderId, dtpnr.Rows(0)("PgMobile").ToString.Trim, dtpnr.Rows(0)("sector").ToString.Trim, dtpnr.Rows(0)("VC").ToString.Trim, dtflight.Rows(0)("FltNumber").ToString.Trim, dtflight.Rows(0)("DepDate"), txt_GDSPNR.Text, smsMsg, SmsCrd)
    '                objSql.SmsLogDetails(OrderId, dtpnr.Rows(0)("PgMobile").ToString.Trim, smsMsg, smsStatus)
    '            End If

    '        Catch ex As Exception
    '            clsErrorLog.LogInfo(ex)
    '        End Try
    '        'Response.Write("<script language='javascript'>alert('Ticket Updated Successfully.');window.location.reload();</script>")
    '        'Response.Write("<script language='javascript'>alert('Ticket Updated Sucessfully')</script>")
    '        ' Response.Write("<script language=javascript>window.opener.location.reload();self.close();</script>")
    '        'Response.Write("<script language='javascript'>self.close();</script>")
    '        ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Ticket Updated Successfully.');javascript: window.close();window.opener.location=window.opener.location.href;", True)

    '    Catch ex As Exception
    '        clsErrorLog.LogInfo(ex)
    '    End Try

    'End Sub

    Protected Sub btn_update1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_update1.Click
        Try
            Dim ddlStatus As DropDownList = CType(GvFlightHeader.Rows(0).Cells(0).FindControl("ddlStatus"), DropDownList)
            Dim statusddl As String = ddlStatus.SelectedValue
            'Dim txtstatus As TextBox = CType(GvFlightHeader.Rows(0).Cells(0).FindControl("txtStatus"), TextBox)

            'chandan add
            Dim dtHeader As New DataTable


            dtHeader = SelectHeaderDetail()
            Dim balance As String = ViewState("agent_balance").ToString()
            Dim agentBal As Double = Convert.ToDouble(balance)

            If (statusddl <> "Confirm") Then

                'If agentBal > Convert.ToDouble(dtHeader.Rows(0)("TotalBookingCost")) Then

                Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
                Dim OrderId As String = Request.QueryString("OrderId")

                'For Each row As GridViewRow In GvFlightHeader.Rows
                '    Dim txthobby As TextBox = DirectCast(row.FindControl("txtHobby"), TextBox)
                '    hobbies += txthobby.Text + ", "
                'Next


                Dim AirlinePnr As String = ""
                Dim GdsPnr As String = ""
                Dim TicketIdr As String = ""
                Dim SearchId As String = ""
                ' Dim TicketId As String = ""

                For j As Integer = 0 To GvFlightHeader.Rows.Count - 1
                    Dim txt_AirlinePNR As TextBox = CType(GvFlightHeader.Rows(j).Cells(0).FindControl("txtAirlinePnr"), TextBox)
                    Dim txt_GDSPNR As TextBox = CType(GvFlightHeader.Rows(j).Cells(0).FindControl("txtGdsPnr"), TextBox)
                    Dim txtTicketIdr As TextBox = CType(GvFlightHeader.Rows(j).Cells(0).FindControl("txtTicketIdr"), TextBox)
                    'change by chandan
                    Dim txtSearchId As TextBox = CType(GvFlightHeader.Rows(j).Cells(0).FindControl("txtSearchId"), TextBox)
                    'Dim txtTicketIdr As TextBox = CType(GvFlightHeader.Rows(j).Cells(0).FindControl("txtTicketIdr"), TextBox)

                    AirlinePnr = txt_AirlinePNR.Text
                    GdsPnr = txt_GDSPNR.Text
                    TicketIdr = txtTicketIdr.Text
                    'chandan
                    SearchId = txtSearchId.Text
                    ' TicketId = txtTicketIdr.Text

                Next

                'Dim txt_AirlinePNR As TextBox = DirectCast(GvFlightHeader.Rows(0).Cells(0).FindControl("txtAirlinePnr"), TextBox)
                'Dim txt_GDSPNR As TextBox = DirectCast(GvFlightHeader.Rows(0).Cells(0).FindControl("txtGdsPnr"), TextBox)
                'Dim txtTicketIdr As TextBox = DirectCast(GvFlightHeader.Rows(0).Cells(0).FindControl("txtTicketIdr"), TextBox)
                'Dim AirlinePnr As String = txt_AirlinePNR.Text
                'Dim GdsPnr As String = txt_GDSPNR.Text
                'Dim TicketIdr As String = txtTicketIdr.Text

                ''Dim Partnername As String = txtPartnerName.SelectedItem.Value
                Dim Partnername As String = ""
                Try
                    Dim cmd As New SqlCommand("USP_updatePnrSp_PP")
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@Order_id", OrderId)
                    cmd.Parameters.AddWithValue("@AirLinePnr", AirlinePnr.Trim())
                    cmd.Parameters.AddWithValue("@GdsPnr", GdsPnr.Trim())
                    cmd.Parameters.Add("@PartnerName", SqlDbType.VarChar).Value = Partnername
                    cmd.Parameters.AddWithValue("@SearchId", SearchId)
                    'cmd.Parameters.AddWithValue("@TicketId", TicketId)



                    cmd.Connection = con
                    con.Open()
                    cmd.ExecuteNonQuery().ToString()
                    con.Close()
                Catch ex As Exception
                    clsErrorLog.LogInfo(ex)
                End Try

                Dim dt As New DataTable
                'dt = ViewState("TFADIS")
                dt = ID.SelectHeaderDetail(OrderId)

                Dim i As Integer = 0

                For j As Integer = 0 To GvTravellerInformation.Rows.Count - 1
                    Dim TID As Label = CType(GvTravellerInformation.Rows(j).Cells(0).FindControl("lblPaxId"), Label)
                    'Dim txt_GDSPNR As TextBox = CType(GvTravellerInformation.Rows(j).Cells(0).FindControl("txtGdsPnr"), TextBox)
                    Dim TktNo As TextBox = CType(GvTravellerInformation.Rows(j).Cells(0).FindControl("txtTktNo"), TextBox)
                    If dt.Rows(0)("VC").ToString.Trim.ToUpper <> "6E" And dt.Rows(0)("VC").ToString.Trim.ToUpper <> "SG" And dt.Rows(0)("VC").ToString.Trim.ToUpper <> "G8" Then
                        ID.UpdateTicketIntl(OrderId, TID.Text, TktNo.Text.Trim())
                        STDom.UpdateTktDomIntlOnLedger(OrderId, Convert.ToInt32(TID.Text.Trim), TktNo.Text.Trim())
                    Else
                        ID.UpdateTicketIntl(OrderId, TID.Text, TktNo.Text.Trim() & i.ToString)
                        STDom.UpdateTktDomIntlOnLedger(OrderId, Convert.ToInt32(TID.Text.Trim), TktNo.Text.Trim() & i.ToString)
                    End If
                Next



                'NAV METHOD CALL START
                Try
                    ''Call ITQFINACIAL PUSH
                    'Dim objITQ As New LNBITQFINANCEAPI.ITQFinance()
                    'objITQ.PushToITQFinance(OrderId, "Air", "D")
                    'Dim objNav As New AirService.clsConnection(trackIdOneWay, "0", "0")
                    'objNav.airBookingNav(trackIdOneWay, "", 0)

                Catch ex As Exception

                End Try


                'NAV METHOD  CALL START
                Try
                    UpdateSearchNBookID(OrderId, TicketIdr.Trim)

                Catch ex As Exception

                End Try

                Try

                    'Dim objNav As New AirService.clsConnection(OrderId, "0", "0")
                    'objNav.airBookingNav(OrderId, "", 0)

                Catch ex As Exception
                    clsErrorLog.LogInfo(ex)
                End Try
                'Nav METHOD END'
                Try

                    'STYTR.InsertYatra_MIRHEADER(OrderId, txt_GDSPNR.Text.Trim())
                    'STYTR.InsertYatra_PAX(OrderId, txt_GDSPNR.Text.Trim())
                    'STYTR.InsertYatra_SEGMENT(OrderId, txt_GDSPNR.Text.Trim())
                    'STYTR.InsertYatra_FARE(OrderId, txt_GDSPNR.Text.Trim())
                    'STYTR.InsertYatra_DIFTLINES(OrderId, txt_GDSPNR.Text.Trim())

                    'Dim AirObj As New AIR_YATRA
                    'AirObj.ProcessYatra_Air(OrderId, txt_GDSPNR.Text.Trim(), "B")
                Catch ex As Exception
                    clsErrorLog.LogInfo(ex)
                End Try
                Try


                    Dim dtflight As New DataTable()
                    dtflight = ID.SelectFlightDetail(OrderId)
                    If (dt.Rows(0)("Status").ToString.Trim = "Ticketed") Then
                        btn_update1.Visible = False
                        GvFlightHeader.Columns(14).Visible = False
                    End If


                    GvFlightHeader.EditIndex = -1
                    GvTravellerInformation.EditIndex = -1

                    mailTktCopy(dt.Rows(0)("VC").ToString.Trim, dtflight.Rows(0)("FltNumber").ToString.Trim, dt.Rows(0)("Sector").ToString.Trim, dtflight.Rows(0)("DepDate").ToString.Trim, dt.Rows(0)("FareType").ToString.Trim, dt.Rows(0)("AirlinePnr").ToString.Trim, dt.Rows(0)("GdsPnr").ToString.Trim, dt.Rows(0)("Status").ToString.Trim, OrderId, dt.Rows(0)("PgEmail").ToString.Trim)
                    BindFltHeader()

                    BindLedgDetails()
                    BindMealbagg()
                    'Getting Pax Details
                    BindTravellerInformation()

                Catch ex As Exception
                    clsErrorLog.LogInfo(ex)
                End Try
                Try
                    Dim smsStatus As String = ""
                    Dim smsMsg As String = ""
                    Dim objSMSAPI As New SMSAPI.SMS
                    Dim dtpnr As New DataTable()
                    dtpnr = ID.SelectHeaderDetail(OrderId)
                    Dim dtflight As New DataTable()
                    dtflight = ID.SelectFlightDetail(OrderId)
                    Dim SmsCrd As DataTable
                    Dim objDA As New SqlTransaction
                    SmsCrd = objDA.SmsCredential(SMS.AIRHOLDCNF.ToString()).Tables(0)
                    If SmsCrd.Rows.Count > 0 AndAlso SmsCrd.Rows(0)("Status") = True Then
                        smsStatus = objSMSAPI.sendSms(OrderId, dtpnr.Rows(0)("PgMobile").ToString.Trim, dtpnr.Rows(0)("sector").ToString.Trim, dtpnr.Rows(0)("VC").ToString.Trim, dtflight.Rows(0)("FltNumber").ToString.Trim, dtflight.Rows(0)("DepDate"), GdsPnr, smsMsg, SmsCrd)
                        objSql.SmsLogDetails(OrderId, dtpnr.Rows(0)("PgMobile").ToString.Trim, smsMsg, smsStatus)
                    End If

                Catch ex As Exception
                    clsErrorLog.LogInfo(ex)
                End Try
                'Response.Write("<script language='javascript'>alert('Ticket Updated Successfully.');window.location.reload();</script>")
                'Response.Write("<script language='javascript'>alert('Ticket Updated Sucessfully')</script>")
                ' Response.Write("<script language=javascript>window.opener.location.reload();self.close();</script>")
                'Response.Write("<script language='javascript'>self.close();</script>")
                ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Ticket Updated Successfully.');javascript: window.close();window.opener.location=window.opener.location.href;", True)
                'Else

                '    ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Not Sufficient Balance.');javascript: window.close();window.opener.location=window.opener.location.href;", True)
                '    btn_update1.Visible = True
                'End If
            Else
                ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Please Select the Status.');javascript: window.close();window.opener.location=window.opener.location.href;", True)
                btn_update1.Visible = True

            End If






        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub

    Protected Sub GvFlightHeader_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GvFlightHeader.RowCancelingEdit
        Try
            GvFlightHeader.EditIndex = -1
            GvFlightHeader.Columns(5).Visible = False
            GvTravellerInformation.EditIndex = -1
            BindFltHeader()
            BindTravellerInformation()

        Catch ex As Exception

        End Try

    End Sub

    'Protected Sub GvFlightHeader_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GvFlightHeader.RowEditing
    '    Try
    '        GvFlightHeader.EditIndex = e.NewEditIndex
    '        GvFlightHeader.Columns(5).Visible = True



    '        GvTravellerInformation.EditIndex = e.NewEditIndex
    '        'y
    '        GvTravellerInformation.Columns(6).Visible = True
    '        ' GvTravellerInformation.Columns(7).Visible = True
    '        BindFltHeader()
    '        BindTravellerInformation()

    '    Catch ex As Exception

    '    End Try

    'End Sub

    'Protected Sub GvFlightHeader_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GvFlightHeader.RowUpdating

    '    Try
    '        Dim txtRemark As TextBox = TryCast(DirectCast(GvFlightHeader.Rows(e.RowIndex).FindControl("txtRemark"), TextBox), TextBox)
    '        If txtRemark.Text = "" Then
    '            ShowAlertMessage("Please Enter Remark")
    '        Else
    '            Dim OrderId As String = Request.QueryString("OrderId")
    '            Dim txtGdsPnr As TextBox = TryCast(DirectCast(GvFlightHeader.Rows(e.RowIndex).FindControl("txtGdsPnr"), TextBox), TextBox)
    '            Dim txtAirlinePnr As TextBox = TryCast(DirectCast(GvFlightHeader.Rows(e.RowIndex).FindControl("txtAirlinePnr"), TextBox), TextBox)
    '            Dim txtStatus As TextBox = TryCast(DirectCast(GvFlightHeader.Rows(e.RowIndex).FindControl("txtStatus"), TextBox), TextBox)

    '            SelectDetails.UpdateFlightHeader(txtGdsPnr.Text.Trim(), txtAirlinePnr.Text.Trim(), txtStatus.Text.Trim(), OrderId)
    '            SelectDetails.InsertQCFlightHeader(OrderId, "FltHeader", Session("UID").ToString(), txtRemark.Text)

    '            lblUpdateFltHeader.Visible = True
    '            lblUpdateFltHeader.Text = "Updated Successfully"
    '            GvFlightHeader.Columns(5).Visible = False
    '            GvFlightHeader.EditIndex = -1
    '            BindFltHeader()

    '        End If

    '    Catch ex As Exception

    '    End Try


    'End Sub

    'Protected Sub GvTravellerInformation_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GvTravellerInformation.RowCancelingEdit
    '    Try

    '        GvTravellerInformation.EditIndex = -1
    '        GvTravellerInformation.Columns(6).Visible = False
    '        BindTravellerInformation()


    '    Catch ex As Exception

    '    End Try

    'End Sub

    'Protected Sub GvTravellerInformation_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GvTravellerInformation.RowEditing
    '    Try
    '        GvTravellerInformation.EditIndex = e.NewEditIndex
    '        'GvTravellerInformation.Columns(4).Visible = True
    '        BindTravellerInformation()

    '    Catch ex As Exception

    '    End Try

    'End Sub

    Protected Sub GvTravellerInformation_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GvTravellerInformation.RowUpdating
        Try
            Dim txtRemark As TextBox = TryCast(DirectCast(GvTravellerInformation.Rows(e.RowIndex).FindControl("txtRemark"), TextBox), TextBox)
            If txtRemark.Text = "" Then
                ShowAlertMessage("Please Enter Remark")
            Else

                Dim OrderId As String = Request.QueryString("OrderId")
                Dim lblPaxId As Label = TryCast(DirectCast(GvTravellerInformation.Rows(e.RowIndex).FindControl("lblPaxId"), Label), Label)
                Dim txtTitle As TextBox = TryCast(DirectCast(GvTravellerInformation.Rows(e.RowIndex).FindControl("txtTitle"), TextBox), TextBox)
                Dim txtFname As TextBox = TryCast(DirectCast(GvTravellerInformation.Rows(e.RowIndex).FindControl("txtFname"), TextBox), TextBox)
                Dim txtLname As TextBox = TryCast(DirectCast(GvTravellerInformation.Rows(e.RowIndex).FindControl("txtLname"), TextBox), TextBox)
                Dim txtType As TextBox = TryCast(DirectCast(GvTravellerInformation.Rows(e.RowIndex).FindControl("txtType"), TextBox), TextBox)
                Dim txtTktNo As TextBox = TryCast(DirectCast(GvTravellerInformation.Rows(e.RowIndex).FindControl("txtTktNo"), TextBox), TextBox)

                SelectDetails.UpdatePaxInformation(txtTitle.Text.Trim(), txtFname.Text.Trim(), txtLname.Text.Trim(), txtType.Text.Trim(), txtTktNo.Text.Trim(), OrderId, lblPaxId.Text.Trim())
                SelectDetails.InsertQCFlightHeader(OrderId, "FltPaxDetails", Session("UID").ToString(), txtRemark.Text)
                lblUpdatePax.Visible = True
                lblUpdatePax.Text = "Updated Successfully"
                GvTravellerInformation.Columns(7).Visible = False
                GvTravellerInformation.EditIndex = -1
                BindTravellerInformation()

            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GvFlightDetails_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GvFlightDetails.RowEditing
        Try

            GvFlightDetails.EditIndex = e.NewEditIndex
            BindFlightDetails()
            GvFlightDetails.Columns(12).Visible = True


        Catch ex As Exception

        End Try

    End Sub

    Protected Sub GvFlightDetails_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GvFlightDetails.RowCancelingEdit
        Try

            GvFlightDetails.EditIndex = -1
            GvFlightDetails.Columns(12).Visible = False
            BindFlightDetails()

        Catch ex As Exception

        End Try


    End Sub

    Protected Sub GvFlightDetails_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles GvFlightDetails.RowUpdating

        Try
            Dim txtRemark As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtRemark"), TextBox), TextBox)
            If txtRemark.Text = "" Then
                ShowAlertMessage("Please Enter Remark")
            Else
                Dim OrderId As String = Request.QueryString("OrderId")
                Dim lblFltId As Label = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("lblFltId"), Label), Label)
                Dim txtDepcityName As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtDepcityName"), TextBox), TextBox)
                Dim txtDepcityCode As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtDepcityCode"), TextBox), TextBox)
                Dim txtArrcityName As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtArrcityName"), TextBox), TextBox)
                Dim txtArrcityCode As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtArrcityCode"), TextBox), TextBox)
                Dim txtAirlineName As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtAirlineName"), TextBox), TextBox)
                Dim txtAirlineCode As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtAirlineCode"), TextBox), TextBox)
                Dim txtFltNo As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtFltNo"), TextBox), TextBox)
                Dim txtDepDate As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtDepDate"), TextBox), TextBox)
                Dim txtDepTime As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtDepTime"), TextBox), TextBox)
                Dim txtArrTime As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtArrTime"), TextBox), TextBox)
                Dim txtAirCraft As TextBox = TryCast(DirectCast(GvFlightDetails.Rows(e.RowIndex).FindControl("txtAirCraft"), TextBox), TextBox)

                SelectDetails.UpdateFlightDetails(txtDepcityName.Text.Trim(), txtDepcityCode.Text.Trim(), txtArrcityName.Text.Trim(), txtArrcityCode.Text.Trim(), txtAirlineName.Text.Trim(), txtAirlineCode.Text.Trim(), txtFltNo.Text.Trim(), txtDepDate.Text.Trim(), txtDepTime.Text.Trim(), txtArrTime.Text.Trim(), txtAirCraft.Text.Trim(), OrderId, lblFltId.Text)
                SelectDetails.InsertQCFlightHeader(OrderId, "FltDetails", Session("UID").ToString(), txtRemark.Text)

                lblUpdateFlight.Visible = True
                lblUpdateFlight.Text = "Updated Successfully"
                GvFlightDetails.Columns(12).Visible = False
                GvFlightDetails.EditIndex = -1
                BindFlightDetails()


            End If



        Catch ex As Exception

        End Try
    End Sub

    Public Shared Sub ShowAlertMessage(ByVal [error] As String)
        Try


            Dim page As Page = TryCast(HttpContext.Current.Handler, Page)
            If page IsNot Nothing Then
                [error] = [error].Replace("'", "'")
                ScriptManager.RegisterStartupScript(page, page.[GetType](), "err_msg", "alert('" & [error] & "');", True)
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try

    End Sub

    Public Function Legr() As DataTable
        Dim dt2 As DataTable = New DataTable()
        Try
            Dim OrderId As String = Request.QueryString("OrderId")
            Dim adap As SqlDataAdapter = New SqlDataAdapter("ledgerInfo", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@orderid", OrderId.Trim)
            adap.Fill(dt2)
        Catch ex As Exception
        End Try
        Return dt2
    End Function

    Protected Sub Status_Click(sender As Object, e As EventArgs) Handles Status.Click
        Dim OrderId As String = Request.QueryString("OrderId")
        Dim crdlmt As Double
        crdlmt = BindAgencyaval(ViewState("agentid").ToString())
        If (typedd.SelectedValue = "Confirm") Then
            Dim dtt As DataTable = Legr()
            Try
                BindFltHeader()
                If (ViewState("Status_val") = "Request") Then
                    Status.Visible = True
                    MessageHold.Visible = True
                    typedd.Visible = True
                    Dim TotalAmount_Debit As Double
                    Dim TotalAmountD As Double
                    TotalAmount_Debit = Convert.ToDouble(ViewState("flthrds").Rows(0)("TotalAfterDis"))

                    If (dtt.Rows.Count) Then
                        TotalAmountD = Convert.ToDouble(dtt.Rows(0)("TotalDebit"))

                    Else
                        TotalAmountD = 0
                    End If
                    If (dtt.Rows.Count > 0 And TotalAmountD >= TotalAmount_Debit) Then
                        '' 1 case to update fltheader Status Request to Confirm if value exists on legr table
                        updateConfirm()
                        BindFltHeader()
                        Status.Visible = False
                        MessageHold.Visible = False
                        typedd.Visible = False
                    Else
                        '' 2nd case
                        Dim FltHdrDs As DataTable = ViewState("flthrds")
                        If (Convert.ToDouble(crdlmt) >= Convert.ToDouble(FltHdrDs.Rows(0)("TotalAfterDis"))) Then
                            Dim ProjectId As String = If(IsDBNull(FltHdrDs.Rows(0)("ProjectID")), Nothing, FltHdrDs.Rows(0)("ProjectID").ToString())
                            Dim BookedBy As String = If(IsDBNull(FltHdrDs.Rows(0)("BookedBy")), Nothing, FltHdrDs.Rows(0)("BookedBy").ToString())
                            Dim BillNoCorp As String = If(IsDBNull(FltHdrDs.Rows(0)("BillNoCorp")), Nothing, FltHdrDs.Rows(0)("BillNoCorp").ToString())
                            Dim GdsPnr As String = If(IsDBNull(FltHdrDs.Rows(0)("GdsPnr")), Nothing, FltHdrDs.Rows(0)("GdsPnr").ToString())
                            Dim VC As String = If(IsDBNull(FltHdrDs.Rows(0)("VC")), Nothing, FltHdrDs.Rows(0)("VC").ToString())
                            Dim it As Integer = Ledgerandcreditlimit_Transaction(ViewState("agentid").ToString(), Convert.ToDouble(FltHdrDs.Rows(0)("TotalAfterDis")), OrderId.Trim(), VC, GdsPnr, ViewState("Agency_Name"), Request.UserHostAddress.ToString(), ProjectId, BookedBy, BillNoCorp, crdlmt, "")
                            If (it > 0) Then
                                BindAgency(ViewState("agentid").ToString())
                                BindLedgDetails()
                                BindFltHeader()
                                BindMealbagg()
                                Status.Visible = False
                                MessageHold.Visible = False
                                typedd.Visible = False
                                ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Update Status and Inserted Successfully');", True)
                            End If
                        Else
                            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('insufficient Credit Limit');", True)
                            Status.Visible = False
                            MessageHold.Visible = False
                            typedd.Visible = False
                        End If
                    End If

                Else
                    Status.Visible = False
                    MessageHold.Visible = False
                    typedd.Visible = False
                End If
            Catch ex As Exception
                ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Not Updated, Try again After Sometimes');", True)
                Status.Visible = False
                MessageHold.Visible = False
                typedd.Visible = False
            End Try

        Else
            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Please -select- Status Before Processing To Hold PNR!');", True)
        End If
    End Sub

    Public Sub updateConfirm()
        Try
            Dim OrderId As String = Request.QueryString("OrderId")
            Dim con_new As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            If con_new.State = ConnectionState.Closed Then
                con_new.Open()
            End If
            Dim cmd As SqlCommand = New SqlCommand("UpdateStatus", con_new)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@OrderID", OrderId.Trim)
            Dim i As Integer = cmd.ExecuteNonQuery()
            con_new.Close()
            If (i > 0) Then
                ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Update Status Successfully');", True)
            End If
        Catch ex As Exception
            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Not Updated, Try again After Sometimes');", True)
        End Try
    End Sub


    Public Function Ledgerandcreditlimit_Transaction(ByVal AGENTID As String, ByVal TOTALAFETRDIS As Double, ByVal TRACKID As String, ByVal VC As String, ByVal GDSPNR As String, ByVal AGENCYNAME As String, ByVal IP As String, ByVal ProjectId As String, ByVal BookedBy As String, ByVal BillNo As String, ByVal AvailBal As Double, ByVal EasyID As String) As Integer
        Dim objDataAcess As New DataAccess(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
        Dim paramHashtable As New Hashtable
        paramHashtable.Clear()
        paramHashtable.Add("@AGENTID", AGENTID)
        paramHashtable.Add("@TOTALAFETRDIS", TOTALAFETRDIS)
        paramHashtable.Add("@TRACKID", TRACKID)
        paramHashtable.Add("@VC", VC)
        paramHashtable.Add("@GDSPNR", GDSPNR)
        paramHashtable.Add("@AGENCYNAME", AGENCYNAME)
        paramHashtable.Add("@IP", IP)
        paramHashtable.Add("@ProjectId", ProjectId)
        paramHashtable.Add("@BookedBy", BookedBy)
        paramHashtable.Add("@BillNo", BillNo)
        paramHashtable.Add("@AVAILBAL", AvailBal)
        paramHashtable.Add("@EasyID", EasyID)
        paramHashtable.Add("@Status", typedd.SelectedValue)
        Dim i As Integer = objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "SP_INSERT_LEDGERNEWREGES_TRANSACTION_NM", 2)
        '' Dim i As Integer = objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "SP_INSERT_LEDGERNEWREGES_TRANSACTION_NM_DEBIT", 2)
        Return i
        'End Ledger and Credit Limit With Transaction
    End Function

    Protected Sub btnaccept_Click(sender As Object, e As EventArgs)
        Try

            Dim OrderId As String = Request.QueryString("OrderId")
            Dim ds As New DataSet()
            adap = New SqlDataAdapter("Select ExecutiveId,Status from FltHeader where OrderId='" & OrderId & "'", con)
            adap.Fill(ds)
            Dim dt As New DataTable()
            dt = ds.Tables(0)
            Dim ExecutiveId As String = dt.Rows(0)("ExecutiveId").ToString()
            ' Dim Status As String = dt.Rows(0)("Status").ToString()
            Dim Status As String = dt.Rows(0)("Status").ToString()
            If ExecutiveId = "" AndAlso Status = "Confirm" Then 'Request add pending and confirm by chandan

                Dim ds1 As New DataSet()

                'ds = ID.UpdateIntlFltHeaderExecutiveID(OrderId, Session("UID").ToString, "InProcess")
                ds = ID.UpdateIntlFltHeaderExecutiveID(OrderId, Session("UID").ToString(), "InProcess")
                'Response.Write("<script>updateText()</script>")
                GvFlightHeader.Columns(14).Visible = True
                GvTravellerInformation.Columns(6).Visible = True 'chandan
                btnaccept.Visible = False
                btnreject.Visible = False
                btn_update1.Visible = True
                Btn_reject.Visible = True


                'change by chadan
                'For Each row As GridViewRow In GvFlightHeader.Rows
                '    If row.RowType = DataControlRowType.DataRow Then
                '        Dim txtTicketIdr As TextBox = TryCast(DirectCast(GvFlightHeader.Rows(row.RowIndex).FindControl("txtStatus"), TextBox), TextBox)
                '        Dim lblStatus As Label = TryCast(DirectCast(GvFlightHeader.Rows(row.RowIndex).FindControl("txtStatus"), Label), Label)
                '        txtTicketIdr.Text = "InProcess"
                '        lblStatus.Text = "InProcess"
                '    End If
                ' Next

                BindFltHeader()
            Else

                Response.Write("<script>alert('Already Allocated')</script>")
                BindFltHeader()
            End If


        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try

    End Sub

    Protected Sub btnreject_Click(sender As Object, e As EventArgs)
        ''divremark.Visible = True
        divremark.Style.Add("display", "block")
        btnaccept.Visible = False
        btnreject.Visible = False
        ''btn_update1.Visible = False




        'Try
        '    Dim OrderID As String = ""
        '    OrderID = ViewState("ID").ToString()

        '    Dim AgentUserId As String = ""
        '    Dim TotalRefundAmt As Double = 0
        '    Dim LedgerDebitAmt As Double = 0

        '    If txt_Reject.Text IsNot Nothing AndAlso txt_Reject.Text <> "" Then

        '        Dim ds As New DataSet()
        '        ds = ST.GetFltHeaderDetail(OrderID)
        '        Dim dtID As New DataTable()
        '        dtID = ds.Tables(0)
        '        Dim ChecksSatus As String = dtID.Rows(0)("Status").ToString()

        '        Dim objItzT As New Itz_Trans_Dal
        '        Dim inst As Boolean = False
        '        Dim objIzT As New ITZ_Trans
        '        ' Dim ablBalance As Double = 0




        '        Try

        '            AgentUserId = dtID.Rows(0)("AgentId").ToString()
        '            Dim dsTotalAmt As New DataSet()
        '            Dim ada As SqlDataAdapter = New SqlDataAdapter("select SUM(debit) as TotalDebit,SUM(Credit) as TotalCredit,SUM(debit)- SUM(Credit) as TotalRefund from  LedgerDetails  where InvoiceNo='" & OrderID & "' and AgentID ='" & AgentUserId & "'", con)
        '            ada.Fill(dsTotalAmt)
        '            Dim dtTotalAmt As New DataTable()
        '            dtTotalAmt = dsTotalAmt.Tables(0)
        '            Dim TotalRefund As String = Convert.ToString(dtTotalAmt.Rows(0)("TotalRefund"))
        '            If Not String.IsNullOrEmpty(TotalRefund) Then
        '                TotalRefundAmt = Convert.ToDouble(TotalRefund)
        '            End If

        '        Catch ex As Exception
        '            Dim ExMsg As String = ex.Message.ToString()
        '        End Try


        '        If (ChecksSatus = "Confirm") Then
        '            'Dim Aval_Bal As Double = ST.AddCrdLimit(dtID.Rows(0)("AgentId").ToString(), dtID.Rows(0)("TotalAfterDis").ToString())
        '            'Ledger
        '            'Adding Refund Amount in Agent balance
        '            'objParamCrd._DECODE = IIf(Session("_DCODE") <> Nothing, Session("_DCODE").ToString().Trim(), " ")

        '            'ST.RejectHoldPNRStatusIntl(OrderID, Session("UID").ToString(), "Rejected", txt_Reject.Text.Trim(), dtID.Rows(0)("GdsPnr").ToString(), "Rejected", dtID.Rows(0)("TotalAfterDis").ToString(), Aval_Bal, "Dom. PNR Rejected Against  OrderID=" & OrderID, dtID.Rows(0)("AgencyName").ToString(), dtID.Rows(0)("AgentId").ToString())
        '            'STDom.insertLedgerDetails(dtID.Rows(0)("AgentId").ToString(), dtID.Rows(0)("AgencyName").ToString(), OrderID, dtID.Rows(0)("GdsPnr").ToString(), "", "", "", "", Session("UID").ToString(), Request.UserHostAddress, 0, dtID.Rows(0)("TotalAfterDis").ToString(), Aval_Bal, "Dom. Rejection", "Dom. PNR Rejected Against  OrderID=" & OrderID, 0)


        '            ''Dim flag As Integer = ST.RejectHoldPNRStatusAndAmount(OrderID, Convert.ToDouble(dtID.Rows(0)("TotalAfterDis")), dtID.Rows(0)("AgentId").ToString(), dtID.Rows(0)("AgencyName").ToString(), Session("UID").ToString(), dtID.Rows(0)("GdsPnr").ToString(), "Rejected", txt_Reject.Text.Trim(), "Dom. PNR Rejected Against  OrderID=" & OrderID, "Dom. Rejection", Request.UserHostAddress)
        '            Dim flag As Integer = ST.RejectHoldPNRStatusAndAmount(OrderID, TotalRefundAmt, dtID.Rows(0)("AgentId").ToString(), dtID.Rows(0)("AgencyName").ToString(), Session("UID").ToString(), dtID.Rows(0)("GdsPnr").ToString(), "Rejected", txt_Reject.Text.Trim(), "Dom. PNR Rejected Against  OrderID=" & OrderID, "Dom. Rejection", Request.UserHostAddress)
        '            If (flag > 0) Then
        '                Response.Write("<script>alert('PNR Rejected Sucessfully')</script>")
        '            Else
        '                Response.Write("<script>alert('Please Check ledger,then refund')</script>")
        '            End If

        '            BindGrid()
        '            txt_Reject.Text = ""

        '        Else
        '            Response.Write("<script>alert('PNR Already Rejected')</script>")
        '            BindGrid()
        '            txt_Reject.Text = ""
        '        End If

        '        td_Reject.Visible = False

        '    End If
        'Catch ex As Exception
        '    clsErrorLog.LogInfo(ex)

        'End Try
    End Sub
    Protected Sub Btn_reject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_reject.Click
        ''divremark.Visible = True
        divremark.Style.Add("display", "block")
        btnaccept.Visible = False
        btnreject.Visible = False


        BindFltHeader()
        GvFlightHeader.Columns(14).Visible = False
        BindTravellerInformation()

    End Sub

    Private Sub mailTktCopy(ByVal VC As String, ByVal FltNo As String, ByVal Sector As String, ByVal DepDate As String, ByVal FT As String, ByVal AirlinePnr As String, ByVal GdsPnr As String, ByVal BkgStatus As String, ByVal OrderId As String, ByVal EmailId As String) 'As String
        'Try
        '    Dim objTktCopy As New clsTicketCopy
        '    Dim strTktCopy As String = "", strHTML As String = "", strFileName As String = "", strMailMsg As String = ""

        '    Dim rightHTML As Boolean = False
        '    strFileName = Convert.ToString(ConfigurationManager.AppSettings("TicketCopy")) & GdsPnr & "-" & FT & " Flight details-" & DateAndTime.Now.ToString.Replace(":", "").Trim & ".html"


        '    strFileName = strFileName.Replace("/", "-")
        '    'strTktCopy = objTktCopy.TicketDetail(OrderId, "")
        '    strTktCopy = TicketCopyExportPDF(OrderId, "")
        '    strHTML = "<html><head><title>Booking Details</title><style type='text/css'> .maindiv{border: #20313f 1px solid; margin: 10px auto 10px auto; width: 650px; font-size:12px; font-family:tahoma,Arial;}	 .text1{color:#333333; font-weight:bold;}	 .pnrdtls{font-size:12px; color:#333333; text-align:left;font-weight:bold;}	 .pnrdtls1{font-size:12px; color:#333333; text-align:left;}	 .bookdate{font-size:11px; color:#CC6600; text-align:left}	 .flthdr{font-size:11px; color:#CC6600; text-align:left; font-weight:bold}	 .fltdtls{font-size:11px; color:#333333; text-align:left;}	.text3{font-size:11px; padding:5px;color:#333333; text-align:right}	 .hdrtext{padding-left:5px; font-size:14px; font-weight:bold; color:#FFFFFF;}	 .hdrtd{background-color:#333333;}	  .lnk{color:#333333;text-decoration:underline;}	  .lnk:hover{color:#333333;text-decoration:none;}	  .contdtls{font-size:12px; padding-top:8px; padding-bottom:3px; color:#333333; font-weight:bold}	  .hrcss{color:#CC6600; height:1px; text-align:left; width:450px;}	 </style></head><body>" & strTktCopy & "</body></html>"
        '    rightHTML = SaveTextToFile(strHTML, strFileName)
        '    strMailMsg = "<p style='font-family:verdana; font-size:12px'>Dear Customer<br /><br />"
        '    strMailMsg = strMailMsg & "Greetings of the day !!!!<br /><br />"
        '    strMailMsg = strMailMsg & "Please find an attachment for your E-ticket, kindly carry the print out of the same for hassle-free travel. Your onward booking for " & Sector & " is confirmed on " & VC & "-" & FltNo & " for " & DepDate & ". Your airline  booking reference no is " & AirlinePnr & ". <br /><br />"
        '    strMailMsg = strMailMsg & "Have a nice &amp; wonderful trip.<br /><br />"

        '    If BkgStatus = "Ticketed" Then


        '        Dim MailDt As New DataTable
        '        MailDt = STDom.GetMailingDetails(MAILING.AIR_BOOKING.ToString(), Session("UID").ToString()).Tables(0)
        '        Try
        '            If rightHTML Then
        '                STDom.SendMail(EmailId, MailDt.Rows(0)("MAILFROM").ToString(), MailDt.Rows(0)("BCC").ToString(), MailDt.Rows(0)("CC").ToString(), MailDt.Rows(0)("SMTPCLIENT").ToString(), MailDt.Rows(0)("UserId").ToString(), MailDt.Rows(0)("Pass").ToString(), strMailMsg, FT & MailDt.Rows(0)("SUBJECT").ToString(), strFileName)
        '            Else
        '                STDom.SendMail(EmailId, MailDt.Rows(0)("MAILFROM").ToString(), MailDt.Rows(0)("BCC").ToString(), MailDt.Rows(0)("CC").ToString(), MailDt.Rows(0)("SMTPCLIENT").ToString(), MailDt.Rows(0)("UserId").ToString(), MailDt.Rows(0)("Pass").ToString(), strMailMsg, FT & MailDt.Rows(0)("SUBJECT").ToString(), "")
        '            End If
        '        Catch ex As Exception

        '        End Try
        '    End If

        'Catch ex As Exception

        'End Try
        ' Return strTktCopy

        Try
            Dim Body As String = ""
            Dim strFileNmPdf As String = ""
            Dim TicketFormate As String = ""
            Dim strMailMsg As String
            Dim writePDF As Boolean = False
            Dim status1 As Integer = 0
            Try
                TicketFormate = TicketCopyExportPDF(OrderId, "").Trim.ToString


                'strFileNmPdf = ConfigurationManager.AppSettings("TicketCopy").ToString().Trim() + Request.QueryString("OrderId").ToString + "-" + DateTime.Now.ToString().Replace(":", "").Replace("/", "-").Replace(" ", "-").Trim() + ".pdf"
                'Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4)
                'Dim writer As iTextSharp.text.pdf.PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, New FileStream(strFileNmPdf, FileMode.Create, FileAccess.ReadWrite, FileShare.None))
                'pdfDoc.Open()
                'Dim sr As New StringReader(TicketFormate)
                'iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr)
                'pdfDoc.Close()
                'writer.Dispose()
                'sr.Dispose()
                'pdfDoc.Dispose()
                'writePDF = True
            Catch ex As Exception
            End Try

            'strMailMsg = "<p style='font-family:verdana; font-size:12px'>Dear Customer<br /><br />"
            'strMailMsg = strMailMsg & "Greetings of the day !!!!<br /><br />"
            'strMailMsg = strMailMsg & "Please find an attachment for your E-ticket, kindly carry the print out of the same for hassle-free travel. Your onward booking for " & Sector & " is confirmed on " & VC & "-" & FltNo & " for " & DepDate & ". Your airline  booking reference no is " & AirlinePnr & ". <br /><br />"
            'strMailMsg = strMailMsg & "Have a nice &amp; wonderful trip.<br /><br />"

            strMailMsg = TicketFormate

            Dim MailDt As New DataTable
            MailDt = STDom.GetMailingDetails(MAILING.AIR_BOOKING.ToString(), Session("UID").ToString()).Tables(0)

            If (MailDt.Rows.Count > 0) Then
                Dim Status As Boolean = False
                Status = Convert.ToBoolean(MailDt.Rows(0)("Status").ToString())
                Try
                    If Status = True And writePDF = True Then
                        STDom.SendMail(EmailId, MailDt.Rows(0)("MAILFROM").ToString(), MailDt.Rows(0)("BCC").ToString(), MailDt.Rows(0)("CC").ToString(), MailDt.Rows(0)("SMTPCLIENT").ToString(), MailDt.Rows(0)("UserId").ToString(), MailDt.Rows(0)("Pass").ToString(), strMailMsg, FT & MailDt.Rows(0)("SUBJECT").ToString(), strFileNmPdf)
                    Else
                        STDom.SendMail(EmailId, MailDt.Rows(0)("MAILFROM").ToString(), MailDt.Rows(0)("BCC").ToString(), MailDt.Rows(0)("CC").ToString(), MailDt.Rows(0)("SMTPCLIENT").ToString(), MailDt.Rows(0)("UserId").ToString(), MailDt.Rows(0)("Pass").ToString(), strMailMsg, FT & MailDt.Rows(0)("SUBJECT").ToString(), "")
                    End If
                Catch ex As Exception

                End Try

            End If

        Catch ex As Exception

        End Try

    End Sub

    Public Function UpdateSearchNBookID(ByVal OrderID As String, ByVal TicketID As String) As Integer
        Dim objDataAcess As New DataAccess(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
        Dim paramHashtable As New Hashtable
        paramHashtable.Clear()
        paramHashtable.Add("@OrderID", OrderID)
        paramHashtable.Add("@TicketID", TicketID)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "UpdateSearchNBookID", 1)
    End Function

    'Protected Sub GvTravellerInformation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GvTravellerInformation.SelectedIndexChanged
    'Try
    '    'GvTravellerInformation.EditIndex = 0;
    '    GvTravellerInformation.Columns(4).Visible = True
    '    BindFltHeader()

    'Catch ex As Exception

    'End Try
    'End Sub

    'Protected Sub GvTravellerInformation_RowEditing1(sender As Object, e As GridViewEditEventArgs)
    '    Try
    '        GvTravellerInformation.EditIndex = e.NewEditIndex
    '        GvTravellerInformation.Columns(4).Visible = True
    '        BindFltHeader()

    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Protected Sub gvItem_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs)
    '    If e.CommandName = "EditRow" Then
    '        Dim id As String = e.CommandArgument.ToString()
    '        Dim rowInsert As Integer = (CType((CType(e.CommandSource, LinkButton)).NamingContainer, GridViewRow)).RowIndex
    '        GvTravellerInformation.EditIndex = rowInsert
    '        GvTravellerInformation.DataBind()
    '        BindFltHeader()
    '    End If
    'End Sub

    Protected Sub btn_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Cancel.Click
        Try
            Dim dtHeader As New DataTable
            dtHeader = SelectHeaderDetail() 'SelectDetails.SelectHeaderDetail(OrderId)

            ViewState("Status_val") = dtHeader.Rows(0)("Status")
            If ViewState("Status_val") = "Confirm" Or ViewState("Status_val") = "InProcess" Or ViewState("Status_val") = "ConfirmByAgent" Then
                'btn_update1.Visible = True
                'Btn_reject.Visible = True
                divremark.Style.Add("Display", "none")


                BindFltHeader()
                BindTravellerInformation()
            End If
            If ViewState("Status_val") = "PNR ON HOLD" Then 'Request
                divremark.Visible = False

                BindFltHeader()
                BindTravellerInformation()
                btnaccept.Visible = True
                btnreject.Visible = True
            End If

            'BindGrid()
            txt_Reject.Text = ""

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Protected Sub btn_Comment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Comment.Click
        Try
            'Dim OrderID As String = ""
            Dim OrderId1 As String = Request.QueryString("OrderId")

            Dim AgentUserId As String = ""
            Dim TotalRefundAmt As Double = 0
            Dim LedgerDebitAmt As Double = 0

            If txt_Reject.Text IsNot Nothing AndAlso txt_Reject.Text <> "" Then

                Dim ds As New DataSet()
                ds = ST.GetFltHeaderDetail(OrderId1)
                Dim dtID As New DataTable()
                dtID = ds.Tables(0)
                Dim ChecksSatus As String = dtID.Rows(0)("Status").ToString()

                Dim objItzT As New Itz_Trans_Dal
                Dim inst As Boolean = False
                Dim objIzT As New ITZ_Trans
                ' Dim ablBalance As Double = 0




                Try

                    AgentUserId = dtID.Rows(0)("AgentId").ToString()
                    Dim dsTotalAmt As New DataSet()
                    Dim ada As SqlDataAdapter = New SqlDataAdapter("select SUM(debit) as TotalDebit,SUM(Credit) as TotalCredit,SUM(debit)- SUM(Credit) as TotalRefund from  LedgerDetails  where InvoiceNo='" & OrderId1 & "' and AgentID ='" & AgentUserId & "'", con)
                    ada.Fill(dsTotalAmt)
                    Dim dtTotalAmt As New DataTable()
                    dtTotalAmt = dsTotalAmt.Tables(0)
                    Dim TotalRefund As String = Convert.ToString(dtTotalAmt.Rows(0)("TotalRefund"))
                    If Not String.IsNullOrEmpty(TotalRefund) Then
                        TotalRefundAmt = Convert.ToDouble(TotalRefund)
                    End If

                Catch ex As Exception
                    Dim ExMsg As String = ex.Message.ToString()
                End Try


                If (ChecksSatus = "Confirm") OrElse (ChecksSatus = "InProcess") OrElse (ChecksSatus = "ConfirmByAgent") Then
                    'Dim Aval_Bal As Double = ST.AddCrdLimit(dtID.Rows(0)("AgentId").ToString(), dtID.Rows(0)("TotalAfterDis").ToString())
                    'Ledger
                    'Adding Refund Amount in Agent balance
                    'objParamCrd._DECODE = IIf(Session("_DCODE") <> Nothing, Session("_DCODE").ToString().Trim(), " ")

                    'ST.RejectHoldPNRStatusIntl(OrderID, Session("UID").ToString(), "Rejected", txt_Reject.Text.Trim(), dtID.Rows(0)("GdsPnr").ToString(), "Rejected", dtID.Rows(0)("TotalAfterDis").ToString(), Aval_Bal, "Dom. PNR Rejected Against  OrderID=" & OrderID, dtID.Rows(0)("AgencyName").ToString(), dtID.Rows(0)("AgentId").ToString())
                    'STDom.insertLedgerDetails(dtID.Rows(0)("AgentId").ToString(), dtID.Rows(0)("AgencyName").ToString(), OrderID, dtID.Rows(0)("GdsPnr").ToString(), "", "", "", "", Session("UID").ToString(), Request.UserHostAddress, 0, dtID.Rows(0)("TotalAfterDis").ToString(), Aval_Bal, "Dom. Rejection", "Dom. PNR Rejected Against  OrderID=" & OrderID, 0)


                    ''Dim flag As Integer = ST.RejectHoldPNRStatusAndAmount(OrderID, Convert.ToDouble(dtID.Rows(0)("TotalAfterDis")), dtID.Rows(0)("AgentId").ToString(), dtID.Rows(0)("AgencyName").ToString(), Session("UID").ToString(), dtID.Rows(0)("GdsPnr").ToString(), "Rejected", txt_Reject.Text.Trim(), "Dom. PNR Rejected Against  OrderID=" & OrderID, "Dom. Rejection", Request.UserHostAddress)
                    Dim flag As Integer = ST.RejectHoldPNRStatusAndAmount(OrderId1, TotalRefundAmt, dtID.Rows(0)("AgentId").ToString(), dtID.Rows(0)("AgencyName").ToString(), Session("UID").ToString(), dtID.Rows(0)("GdsPnr").ToString(), "Rejected", txt_Reject.Text.Trim(), "Dom. PNR Rejected Against  OrderID=" & OrderId1, "Dom. Rejection", Request.UserHostAddress)
                    If (flag > 0) Then
                        Response.Write("<script>alert('PNR Rejected Sucessfully')</script>")
                    Else
                        Response.Write("<script>alert('Please Check ledger,then refund')</script>")
                    End If

                    'BindGrid()
                    txt_Reject.Text = ""

                Else
                    Response.Write("<script>alert('PNR Already Rejected')</script>")
                    'BindGrid()
                    txt_Reject.Text = ""
                End If
                divremark.Visible = False
                BindFltHeader()
                BindTravellerInformation()
                BindMealbagg()
                BindLedgDetails()




                'td_Reject.Visible = False

            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub

    

    
    Public Function TicketCopyExportPDF(OrderId As String, TransID As String) As String

        Dim strFileNmPdf As String = ""
        Dim writePDF As Boolean = False
        Dim TktCopy As String = ""
        Dim Gtotal As Integer = 0
        Dim initialAdt As Integer = 0
        Dim initalChld As Integer = 0
        Dim initialift As Integer = 0
        Dim MealBagTotalPrice As Decimal = 0
        Dim AdtTtlFare As Decimal = 0
        Dim ChdTtlFare As Decimal = 0
        Dim INFTtlFare As Decimal = 0
        Dim fare As Decimal = 0

        'Dim OrderId As String = "1c2019deXCP9cVSU"
        'Dim TransID As String = ""


        Dim objTranDom As New SqlTransactionDom()
        Dim SqlTrasaction As New SqlTransaction()
        Dim objSql As New SqlTransactionNew()
        Dim FltPaxList As New DataTable()

        Dim FltDetailsList As New DataTable()
        Dim FltProvider As New DataTable()
        Dim FltBaggage As New DataTable()
        Dim dtagentid As New DataTable()
        Dim FltagentDetail As New DataTable()
        Dim fltTerminal As New DataTable()
        Dim fltFare As New DataTable()
        Dim fltMealAndBag As New DataTable()
        Dim fltMealAndBag1 As New DataTable()
        Dim fltAirportDetails As New DataSet()
        Dim SelectedFltDS As New DataSet()

        FltPaxList = SelectPaxDetail(OrderId, TransID)
        FltHeaderList = ObjIntDetails.SelectHeaderDetail(OrderId)
        FltDetailsList = ObjIntDetails.SelectFlightDetail(OrderId)
        FltProvider = (objTranDom.GetTicketingProvider(OrderId)).Tables(0)
        dtagentid = ObjIntDetails.SelectAgent(OrderId)
        SelectedFltDS = SqlTrasaction.GetFltDtls(OrderId, dtagentid.Rows(0)("AgentID").ToString())
        Dim Bag As Boolean = False
        If Not String.IsNullOrEmpty(Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("IsBagFare"))) Then
            Bag = Convert.ToBoolean(SelectedFltDS.Tables(0).Rows(0)("IsBagFare"))
        End If
        FltBaggage = (objTranDom.GetBaggageInformation(Convert.ToString(FltHeaderList.Rows(0)("Trip")), Convert.ToString(FltHeaderList.Rows(0)("VC")), Bag)).Tables(0)
        FltagentDetail = ObjIntDetails.SelectAgencyDetail(dtagentid.Rows(0)("AgentID").ToString())
        fltFare = ObjIntDetails.SelectFareDetail(OrderId, TransID)
        Dim dt As DateTime = Convert.ToDateTime(Convert.ToString(FltHeaderList.Rows(0)("CreateDate")))
        Dim [date] As String = dt.ToString("dd/MMM/yyyy").Replace("-", "/")

        Dim Createddate As String = [date].Split("/")(0) + " " + [date].Split("/")(1) + " " + [date].Split("/")(2)

        Dim fltmealbag As DataRow() = objSql.Get_MEAL_BAG_FareDetails(OrderId, TransID).Tables(0).Select("MealPrice>0 or BaggagePrice>0 ")
        fltMealAndBag1 = objSql.Get_MEAL_BAG_FareDetails(OrderId, TransID).Tables(0) '.Select("MealPrice>0 or BaggagePrice>0 ").CopyToDataTable()
        If fltmealbag.Length > 0 Then

            fltMealAndBag = fltMealAndBag1.Select("MealPrice>0 or BaggagePrice>0 ").CopyToDataTable()
        End If
        'If (fltFare.Rows.Count > 0) Then
        '    If (fltFare.Columns.Contains("ticketcopymarkupforTax")) Then

        '        If (Convert.ToString(fltFare.Rows(0)("ticketcopymarkupforTax")) <> "0") Then
        '            TaxNew.Text = "Tax: " & Convert.ToString(fltFare.Rows(0)("ticketcopymarkupforTax"))
        '        End If
        '    End If
        'End If
        Try
            'Dim strAirline As String = "SG6EG8"

            Dim TicketFormate As String = ""


            If (Convert.ToString(FltHeaderList.Rows(0)("Status")).ToLower().Trim() = "confirm" And Session("UserType") = "TA") Then

                TicketFormate += "<div style='clear:both;'> </div> "
                TicketFormate += "<table style='width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td>"

                TicketFormate += "<table style='width:100%;font-faimly:arial;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 15px; width: 15%; text-align: left; padding: 5px;'>"
                TicketFormate += "<b>Booking Reference No. " & OrderId & "</b>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 14px; width: 15%; text-align: left; padding: 5px;'>"
                ''TicketFormate += "The PNR-<b>" & FltHeaderList.Rows(0)("GdsPnr") & " </b>is on <b>HOLD</b> and contact customer care for issuance."
                TicketFormate += "The PNR-<b>" & FltHeaderList.Rows(0)("GdsPnr") & " </b>is on <b>HOLD</b>. Our operation team is working on it and may take 20 minutes to resolve. Please contact our customer care representative at <b>+ 91-11-4711 4711</b> for any further assistance"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<table style='border: 1px solid #cccce0; font-family: Verdana, Geneva, sans-serif; font-size: 11px;padding:0px !important;width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: left; background-color: #cccce0;color: #424242; font-size: 11px; font-weight: bold; padding: 5px;' colspan='4'>"
                TicketFormate += "Passenger Information"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='4' style='font-size:12px; padding: 0px; width: 100%'>"
                TicketFormate += "<table>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; text-align: left; padding: 5px;'>GDS PNR</td>"
                TicketFormate += "<td style='font-size: 11px; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("GdsPnr")
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; text-align: left; padding: 5px;'>Issued By</td>"
                TicketFormate += "<td style='font-size: 11px; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("AgencyName")
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; text-align: left; padding: 5px;'>Airline PNR</td>"
                TicketFormate += "<td style='font-size: 11px;text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("AirlinePnr")
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px;text-align: left; padding: 5px;'>Agency Info</td>"
                TicketFormate += "<td style='font-size: 11px;text-align: left; padding: 5px;'>"
                TicketFormate += FltagentDetail.Rows(0)("Mobile")
                TicketFormate += "<br/>"
                TicketFormate += FltagentDetail.Rows(0)("Email")
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px;  text-align: left; padding: 5px;'>Status</td>"
                TicketFormate += "<td style='font-size: 11px;  text-align: left; padding: 5px;'>"
                TicketFormate += IIf(Convert.ToString(FltHeaderList.Rows(0)("Status")).ToLower().Trim() = "confirm", "Hold", FltHeaderList.Rows(0)("Status"))
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; text-align: left; padding: 5px;'>Date Of Issue</td>"
                TicketFormate += "<td style='font-size: 11px; text-align: left; padding: 5px;'>"
                TicketFormate += Createddate
                TicketFormate += "</td>"

                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Fare Type</td>"
                TicketFormate += "<td style='font-size: 13px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("AdtFareType"))
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px;border-color: #cccce0; font-weight:bold; width: 20%; text-align: left; padding: 5px;'>Customer Info</td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("PgMobile")
                TicketFormate += "<br/>"
                TicketFormate += FltHeaderList.Rows(0)("PgEmail")
                TicketFormate += "</td>"
                TicketFormate += "</tr>"


                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Class</td>"
                TicketFormate += "<td style='font-size: 13px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += GetCabin(Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("Provider")), Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("AdtCabin")), Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("ValiDatingCarrier")))
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'></td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'></td>"
                TicketFormate += "</tr>"



                For p As Integer = 0 To FltPaxList.Rows.Count - 1
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 11px;font-weight:bold; width: 15%; text-align: left; padding: 5px; colspan=2;'>Passenger Name</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px; colspan=2;'>"
                    TicketFormate += FltPaxList.Rows(p)("Name") + " " + "(" + FltPaxList.Rows(p)("PaxType") + ")"
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px;font-weight:bold; width: 35%; text-align: left; padding: 5px;  colspan=2;'>"
                    TicketFormate += FltPaxList.Rows(p)("TicketNumber")
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                Next

                TicketFormate += "</table>"



                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: left; background-color: #cccce0; color: #424242; width: 100%; padding: 5px;' colspan='4'>"
                TicketFormate += "<table style='width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: left; color: #000; font-size: 11px; width: 25%;font-weight:bold;' colspan='1'>"
                TicketFormate += "Flight Information"
                TicketFormate += "</td>"
                TicketFormate += "<td colspan='3' style='font-size: 11px; color: black; font-weight: bold; width: 75%; text-align: left; '></td>"
                TicketFormate += "</tr>"
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='4' style='height:5px;'>&nbsp;</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='5' style='background-color: #cccce0;width:100%;'>"
                TicketFormate += "<table style='width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 10.5px; color: #424242; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>FLIGHT</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #424242; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>DEPART</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #424242; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #424242; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>DEPART AIRPORT</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #424242; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE AIRPORT</td>"
                TicketFormate += "</tr>"

                For f As Integer = 0 To FltDetailsList.Rows.Count - 1

                    TicketFormate += "</table>"

                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"

                    TicketFormate += "<tr>"
                    TicketFormate += "<td colspan='5' style='width:100%;'>"
                    TicketFormate += "<table style='width:100%;'>"
                    TicketFormate += "<tr>"

                    TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>"
                    TicketFormate += FltDetailsList.Rows(f)("AirlineCode") + " " + FltDetailsList.Rows(f)("FltNumber")

                    TicketFormate += "<img alt='Logo Not Found' src='http://UmrawatiTrip.com/AirLogo/sm" + FltDetailsList.Rows(f)("AirlineCode") + ".gif' ></img>"
                    TicketFormate += "<br/>"

                    TicketFormate += FltDetailsList.Rows(f)("AirlineName")
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>"
                    Dim strDepdt As String = Convert.ToString(FltDetailsList.Rows(f)("DepDate"))
                    strDepdt = IIf(strDepdt.Length = 8, STD.BAL.Utility.Left(strDepdt, 4) & "-" & STD.BAL.Utility.Mid(strDepdt, 4, 2) & "-" & STD.BAL.Utility.Right(strDepdt, 2), "20" & STD.BAL.Utility.Right(strDepdt, 2) & "-" & STD.BAL.Utility.Mid(strDepdt, 2, 2) & "-" & STD.BAL.Utility.Left(strDepdt, 2))
                    Dim deptdt As DateTime = Convert.ToDateTime(strDepdt)
                    strDepdt = deptdt.ToString("dd/MMM/yy").Replace("-", "/")

                    'Response.Write(strDepdt)

                    Dim depDay As String = Convert.ToString(deptdt.DayOfWeek)
                    strDepdt = strDepdt.Split("/")(0) + " " + strDepdt.Split("/")(1) + " " + strDepdt.Split("/")(2)
                    Dim strdeptime As String = Convert.ToString(FltDetailsList.Rows(f)("DepTime"))
                    strdeptime = strdeptime.Substring(0, 2) + " : " + strdeptime.Substring(2, 2)
                    TicketFormate += strDepdt
                    TicketFormate += "<br/>"
                    TicketFormate += "<br/>"
                    TicketFormate += strdeptime
                    TicketFormate += "</td>"

                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>"
                    Dim strArvdt As String = Convert.ToString(FltDetailsList.Rows(f)("ArrDate"))
                    strArvdt = IIf(strArvdt.Length = 8, STD.BAL.Utility.Left(strArvdt, 4) & "-" & STD.BAL.Utility.Mid(strArvdt, 4, 2) & "-" & STD.BAL.Utility.Right(strArvdt, 2), "20" & STD.BAL.Utility.Right(strArvdt, 2) & "-" & STD.BAL.Utility.Mid(strArvdt, 2, 2) & "-" & STD.BAL.Utility.Left(strArvdt, 2))
                    Dim Arrdt As DateTime = Convert.ToDateTime(strArvdt)
                    strArvdt = Arrdt.ToString("dd/MMM/yy").Replace("-", "/")
                    Dim ArrDay As String = Convert.ToString(Arrdt.DayOfWeek)
                    strArvdt = strArvdt.Split("/")(0) + " " + strArvdt.Split("/")(1) + " " + strArvdt.Split("/")(2)
                    Dim strArrtime As String = Convert.ToString(FltDetailsList.Rows(f)("ArrTime"))
                    strArrtime = strArrtime.Substring(0, 2) + " : " + strArrtime.Substring(2, 2)
                    TicketFormate += strArvdt
                    TicketFormate += "<br/>"
                    TicketFormate += "<br/>"
                    TicketFormate += strArrtime
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px;'>"
                    TicketFormate += FltDetailsList.Rows(f)("DepAirName") + "( " + FltDetailsList.Rows(f)("DFrom") + ")"

                    TicketFormate += "<br />"
                    TicketFormate += "<br />"
                    fltTerminalDetails = TerminalDetails(OrderId, FltDetailsList.Rows(f)("DFrom"), "")
                    If String.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows(0)("DepartureTerminal"))) Then
                        TicketFormate += fltTerminalDetails.Rows(0)("DepAirportName") + " - Trml: NA"
                    Else
                        TicketFormate += fltTerminalDetails.Rows(0)("DepAirportName") + " - Trml:" + fltTerminalDetails.Rows(0)("DepartureTerminal")
                    End If
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; '>"
                    TicketFormate += FltDetailsList.Rows(f)("ArrAirName") + " (" + FltDetailsList.Rows(f)("ATo") + ")"

                    fltTerminalDetails = TerminalDetails(OrderId, "", FltDetailsList.Rows(f)("ATo"))
                    If String.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows(0)("ArrivalTerminal"))) Then
                        TicketFormate += fltTerminalDetails.Rows(0)("ArrvlAirportName") + " - Trml: NA"
                    Else
                        TicketFormate += fltTerminalDetails.Rows(0)("ArrvlAirportName") + " - Trml:" + fltTerminalDetails.Rows(0)("ArrivalTerminal")
                    End If

                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "</table>"
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"

                    TicketFormate += "<tr>"
                    TicketFormate += "<td colspan='4' style='width:100%;'>"
                    TicketFormate += "<table style='width:100%;'>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 11px; width: 322%; text-align: left; font-weight:bold;'>"
                    'TicketFormate += "<img alt='Logo Not Found' src='http://UmrawatiTrip.com/AirLogo/sm" + FltDetailsList.Rows(f)("AirlineCode") + ".gif' ></img>"
                    TicketFormate += "<br/>"
                    'TicketFormate += FltDetailsList.Rows(f)("AirlineName")
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='width: 32%;'></td>"
                    TicketFormate += "<td style='width: 18%; font-size:12px;text-align:left;'></td>"
                    TicketFormate += "<td style='width: 18%; font-size: 11px; text-align: left; font-weight: bold;'></td>"
                    TicketFormate += "</tr>"

                Next
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</table>"

                '' Div_Main.Visible = False


            ElseIf (Convert.ToString(FltHeaderList.Rows(0)("Status")).ToLower().Trim() = "rejected" And Session("UserType") = "TA") Then

                TicketFormate += "<table style='width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align:left;font-size:15px;'>"
                TicketFormate += "<b>Booking Reference No. " & OrderId & "</b>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align:left;font-size:14px;'>"
                TicketFormate += "Please re-try the booking.Your booking has been rejected due to some technical issue at airline end."
                TicketFormate += "</td>"
                TicketFormate += "</tr></table>"
                TicketFormate += "<table style='border: 1px solid #cccce0; font-family: Verdana, Geneva, sans-serif; font-size: 13px;padding:0px !important;width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: left; background-color: #231f20; color: #424242; font-size: 11px; font-weight: bold; padding: 5px;' colspan='4'>"
                TicketFormate += "Passenger & Ticket Information"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='4' style='font-size:12px; padding: 0px; width: 100%'>"
                TicketFormate += "<table>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px;  text-align: left; padding: 5px;'>GDS PNR</td>"
                TicketFormate += "<td style='font-size: 11px;  text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("GdsPnr")
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; text-align: left; padding: 5px;'>Issued By</td>"
                TicketFormate += "<td style='font-size: 11px; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("AgencyName")
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px;  text-align: left; padding: 5px;'>Airline PNR</td>"
                TicketFormate += "<td style='font-size: 11px;  text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("AirlinePnr")
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px;  text-align: left; padding: 5px;'>Agency Info</td>"
                TicketFormate += "<td style='font-size: 11px; text-align: left; padding: 5px;'>"
                TicketFormate += FltagentDetail.Rows(0)("Mobile")
                TicketFormate += "<br/>"
                TicketFormate += FltagentDetail.Rows(0)("Email")
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; text-align: left; padding: 5px;'>Status</td>"
                TicketFormate += "<td style='font-size: 11px; text-align: left; padding: 5px;'>"
                TicketFormate += IIf(Convert.ToString(FltHeaderList.Rows(0)("Status")).ToLower().Trim() = "confirm", "Hold", FltHeaderList.Rows(0)("Status"))
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; text-align: left; padding: 5px;'>Date Of Issue</td>"
                TicketFormate += "<td style='font-size: 11px;  text-align: left; padding: 5px;'>"
                TicketFormate += Createddate
                TicketFormate += "</td>"
                TicketFormate += "</tr>"


                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; text-align: left; padding: 5px;'>Fare Type</td>"
                TicketFormate += "<td style='font-size: 13px;  text-align: left; padding: 5px; '>"
                TicketFormate += Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("AdtFareType"))
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px;  text-align: left; padding: 5px;'>Customer Info</td>"
                TicketFormate += "<td style='font-size: 11px; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("PgMobile")
                TicketFormate += "<br/>"
                TicketFormate += FltHeaderList.Rows(0)("PgEmail")
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Class</td>"
                TicketFormate += "<td style='font-size: 13px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += GetCabin(Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("Provider")), Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("AdtCabin")), Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("ValiDatingCarrier")))
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'></td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'></td>"
                TicketFormate += "</tr>"

                For p As Integer = 0 To FltPaxList.Rows.Count - 1
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Passenger Name</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                    TicketFormate += FltPaxList.Rows(p)("Name") + " " + "(" + FltPaxList.Rows(p)("PaxType") + ")"
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                    TicketFormate += FltPaxList.Rows(p)("TicketNumber")
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                Next

                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: left; background-color: #231f20; color: #424242; width: 100%; padding: 5px;' colspan='4'>"
                TicketFormate += "<table style='width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: left; color: #000; font-size: 11px; width: 25%;font-weight:bold;' colspan='1'>"
                TicketFormate += "Flight Information"
                TicketFormate += "</td>"
                TicketFormate += "<td colspan='3' style='font-size: 11px; color: black; font-weight: bold; width: 75%; text-align: left; '></td>"
                TicketFormate += "</tr>"
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='4' style='height:5px;'>&nbsp;</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='5' style='background-color: #cccce0;width:100%;'>"
                TicketFormate += "<table style='width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 10.5px; color: #424242; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>FLIGHT</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #424242; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>DEPART</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #424242; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #424242; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>DEPART AIRPORT</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #424242; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE AIRPORT</td>"
                TicketFormate += "</tr>"

                For f As Integer = 0 To FltDetailsList.Rows.Count - 1

                    TicketFormate += "</table>"
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td colspan='5' style='width:100%;'>"
                    TicketFormate += "<table style='width:100%;'>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='padding:10px;font-size: 11px; width: 10%; text-align: left;  vertical-align: top;'>"
                    TicketFormate += "<img alt='Logo Not Found' src='http://UmrawatiTrip.com/AirLogo/sm" + FltDetailsList.Rows(f)("AirlineCode") + ".gif' ></img>"
                    TicketFormate += "<br/>"
                    TicketFormate += FltDetailsList.Rows(f)("AirlineCode") + " " + FltDetailsList.Rows(f)("FltNumber")
                    TicketFormate += "<br/>"

                    TicketFormate += FltDetailsList.Rows(f)("AirlineName")
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>"
                    Dim strDepdt As String = Convert.ToString(FltDetailsList.Rows(f)("DepDate"))
                    strDepdt = IIf(strDepdt.Length = 8, STD.BAL.Utility.Left(strDepdt, 4) & "-" & STD.BAL.Utility.Mid(strDepdt, 4, 2) & "-" & STD.BAL.Utility.Right(strDepdt, 2), "20" & STD.BAL.Utility.Right(strDepdt, 2) & "-" & STD.BAL.Utility.Mid(strDepdt, 2, 2) & "-" & STD.BAL.Utility.Left(strDepdt, 2))
                    Dim deptdt As DateTime = Convert.ToDateTime(strDepdt)
                    strDepdt = deptdt.ToString("dd/MMM/yy").Replace("-", "/")

                    ''Response.Write(strDepdt)

                    Dim depDay As String = Convert.ToString(deptdt.DayOfWeek)
                    strDepdt = strDepdt.Split("/")(0) + " " + strDepdt.Split("/")(1) + " " + strDepdt.Split("/")(2)
                    Dim strdeptime As String = Convert.ToString(FltDetailsList.Rows(f)("DepTime"))
                    strdeptime = strdeptime.Substring(0, 2) + " : " + strdeptime.Substring(2, 2)
                    TicketFormate += strDepdt
                    TicketFormate += "<br/>"
                    TicketFormate += "<br/>"
                    TicketFormate += strdeptime
                    TicketFormate += "</td>"

                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>"
                    Dim strArvdt As String = Convert.ToString(FltDetailsList.Rows(f)("ArrDate"))
                    strArvdt = IIf(strArvdt.Length = 8, STD.BAL.Utility.Left(strArvdt, 4) & "-" & STD.BAL.Utility.Mid(strArvdt, 4, 2) & "-" & STD.BAL.Utility.Right(strArvdt, 2), "20" & STD.BAL.Utility.Right(strArvdt, 2) & "-" & STD.BAL.Utility.Mid(strArvdt, 2, 2) & "-" & STD.BAL.Utility.Left(strArvdt, 2))
                    Dim Arrdt As DateTime = Convert.ToDateTime(strArvdt)
                    strArvdt = Arrdt.ToString("dd/MMM/yy").Replace("-", "/")
                    Dim ArrDay As String = Convert.ToString(Arrdt.DayOfWeek)
                    strArvdt = strArvdt.Split("/")(0) + " " + strArvdt.Split("/")(1) + " " + strArvdt.Split("/")(2)
                    Dim strArrtime As String = Convert.ToString(FltDetailsList.Rows(f)("ArrTime"))
                    strArrtime = strArrtime.Substring(0, 2) + " : " + strArrtime.Substring(2, 2)
                    TicketFormate += strArvdt
                    TicketFormate += "<br/>"
                    TicketFormate += "<br/>"
                    TicketFormate += strArrtime
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; '>"
                    TicketFormate += FltDetailsList.Rows(f)("DepAirName") + "( " + FltDetailsList.Rows(f)("DFrom") + ")"
                    TicketFormate += "<br />"
                    TicketFormate += "<br />"
                    fltTerminalDetails = TerminalDetails(OrderId, FltDetailsList.Rows(f)("DFrom"), "")
                    If String.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows(0)("DepartureTerminal"))) Then
                        TicketFormate += fltTerminalDetails.Rows(0)("DepAirportName") + " - Trml: NA"
                    Else
                        TicketFormate += fltTerminalDetails.Rows(0)("DepAirportName") + " - Trml:" + fltTerminalDetails.Rows(0)("DepartureTerminal")
                    End If
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; '>"
                    TicketFormate += FltDetailsList.Rows(f)("ArrAirName") + " (" + FltDetailsList.Rows(f)("ATo") + ")"
                    TicketFormate += "<br />"
                    TicketFormate += "<br />"
                    fltTerminalDetails = TerminalDetails(OrderId, "", FltDetailsList.Rows(f)("ATo"))
                    If String.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows(0)("ArrivalTerminal"))) Then
                        TicketFormate += fltTerminalDetails.Rows(0)("ArrvlAirportName") + " - Trml: NA"
                    Else
                        TicketFormate += fltTerminalDetails.Rows(0)("ArrvlAirportName") + " - Trml:" + fltTerminalDetails.Rows(0)("ArrivalTerminal")
                    End If
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "</table>"
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td colspan='4' style='width:100%;'>"
                    TicketFormate += "<table style='width:100%;'>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 11px; width: 322%; text-align: left; font-weight:bold;'>"
                    'TicketFormate += "<img alt='Logo Not Found' src='http://UmrawatiTrip.com/AirLogo/sm" + FltDetailsList.Rows(f)("AirlineCode") + ".gif' ></img>"
                    TicketFormate += "<br/>"
                    'TicketFormate += FltDetailsList.Rows(f)("AirlineName")
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='width: 32%;'></td>"
                    TicketFormate += "<td style='width: 18%; font-size:12px;text-align:left;'></td>"
                    TicketFormate += "<td style='width: 18%; font-size: 11px; text-align: left; font-weight: bold;'></td>"
                    TicketFormate += "</tr>"
                Next
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</table>"

            ElseIf (Convert.ToString(FltHeaderList.Rows(0)("Status")).ToLower().Trim() = "inprocess" And Session("UserType") = "TA") Then

                TicketFormate += "<table style='width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align:center;font-size:15px;'>"
                TicketFormate += "<b>Booking Reference No. " & OrderId & "</b>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align:left;font-size:14px;'>"
                TicketFormate += "We are updating the details, Please wait for some time."
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<table style='border: 1px solid #cccce0; font-family: Verdana, Geneva, sans-serif; font-size: 11px;padding:0px !important;width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: left; background-color: #231f20; color: #fff; font-size: 11px; font-weight: bold; padding: 5px;' colspan='4'>"
                TicketFormate += "Passenger & Ticket Information"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='4' style='font-size:12px; padding: 0px; width: 100%height:5px;'>"
                TicketFormate += "<table>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>GDS PNR</td>"
                TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("GdsPnr")
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Issued By</td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("AgencyName")
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Airline PNR</td>"
                TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("AirlinePnr")
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Agency Info</td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += FltagentDetail.Rows(0)("Mobile")
                TicketFormate += "<br/>"
                TicketFormate += FltagentDetail.Rows(0)("Email")
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Status</td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += IIf(Convert.ToString(FltHeaderList.Rows(0)("Status")).ToLower().Trim() = "confirm", "Hold", FltHeaderList.Rows(0)("Status"))
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Date Of Issue</td>"
                TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                TicketFormate += Createddate
                TicketFormate += "</td>"

                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Fare Type</td>"
                TicketFormate += "<td style='font-size: 13px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("AdtFareType"))
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Customer Info</td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("PgMobile")
                TicketFormate += "<br/>"
                TicketFormate += FltHeaderList.Rows(0)("PgEmail")
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Class</td>"
                TicketFormate += "<td style='font-size: 13px; width: 30%; text-align: left; padding: 5px;'>"
                TicketFormate += GetCabin(Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("Provider")), Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("AdtCabin")), Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("ValiDatingCarrier")))
                TicketFormate += "</td>"
                TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'></td>"
                TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'></td>"
                TicketFormate += "</tr>"

                For p As Integer = 0 To FltPaxList.Rows.Count - 1
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Passenger Name</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                    TicketFormate += FltPaxList.Rows(p)("Name") + " " + "(" + FltPaxList.Rows(p)("PaxType") + ")"
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>"
                    TicketFormate += FltPaxList.Rows(p)("TicketNumber")
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                Next
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: left; background-color: #231f20; color: #424242; width: 100%; padding: 5px;' colspan='4'>"
                TicketFormate += "<table style='width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: left; color: #000; font-size: 11px; width: 25%;font-weight:bold;' colspan='1'>"
                TicketFormate += "Flight Information"
                TicketFormate += "</td>"
                TicketFormate += "<td colspan='3' style='font-size: 11px; color: black; font-weight: bold; width: 75%; text-align: left; '></td>"
                TicketFormate += "</tr>"
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='4' style='height:5px;'>&nbsp;</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='5' style='background-color: #cccce0;width:100%;'>"
                TicketFormate += "<table style='width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 10.5px; color: #424242; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>FLIGHT</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #424242; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>DEPART</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #424242; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #424242; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>DEPART AIRPORT</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #424242; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE AIRPORT</td>"
                TicketFormate += "</tr>"

                For f As Integer = 0 To FltDetailsList.Rows.Count - 1

                    TicketFormate += "</table>"
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td colspan='5' style='width:100%;'>"
                    TicketFormate += "<table style='width:100%;'>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='padding:10px;font-size: 11px; width: 10%; text-align: left;  vertical-align: top;'>"


                    TicketFormate += "<img alt='Logo Not Found' src='http://UmrawatiTrip.com/AirLogo/sm" + FltDetailsList.Rows(f)("AirlineCode") + ".gif' ></img>"
                    TicketFormate += "<br/>"
                    TicketFormate += "<br/>"
                    TicketFormate += FltDetailsList.Rows(f)("AirlineCode") + " " + FltDetailsList.Rows(f)("FltNumber")
                    TicketFormate += "<br/>"
                    TicketFormate += FltDetailsList.Rows(f)("AirlineName").ToString()
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>"
                    Dim strDepdt As String = Convert.ToString(FltDetailsList.Rows(f)("DepDate"))
                    strDepdt = IIf(strDepdt.Length = 8, STD.BAL.Utility.Left(strDepdt, 4) & "-" & STD.BAL.Utility.Mid(strDepdt, 4, 2) & "-" & STD.BAL.Utility.Right(strDepdt, 2), "20" & STD.BAL.Utility.Right(strDepdt, 2) & "-" & STD.BAL.Utility.Mid(strDepdt, 2, 2) & "-" & STD.BAL.Utility.Left(strDepdt, 2))
                    Dim deptdt As DateTime = Convert.ToDateTime(strDepdt)
                    strDepdt = deptdt.ToString("dd/MMM/yy").Replace("-", "/")

                    Dim depDay As String = Convert.ToString(deptdt.DayOfWeek)
                    strDepdt = strDepdt.Split("/")(0) + " " + strDepdt.Split("/")(1) + " " + strDepdt.Split("/")(2)
                    Dim strdeptime As String = Convert.ToString(FltDetailsList.Rows(f)("DepTime"))
                    strdeptime = strdeptime.Substring(0, 2) + " : " + strdeptime.Substring(2, 2)
                    TicketFormate += strDepdt
                    TicketFormate += "<br/>"
                    TicketFormate += "<br/>"
                    TicketFormate += strdeptime
                    TicketFormate += "</td>"

                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>"
                    Dim strArvdt As String = Convert.ToString(FltDetailsList.Rows(f)("ArrDate"))
                    strArvdt = IIf(strArvdt.Length = 8, STD.BAL.Utility.Left(strArvdt, 4) & "-" & STD.BAL.Utility.Mid(strArvdt, 4, 2) & "-" & STD.BAL.Utility.Right(strArvdt, 2), "20" & STD.BAL.Utility.Right(strArvdt, 2) & "-" & STD.BAL.Utility.Mid(strArvdt, 2, 2) & "-" & STD.BAL.Utility.Left(strArvdt, 2))
                    Dim Arrdt As DateTime = Convert.ToDateTime(strArvdt)
                    strArvdt = Arrdt.ToString("dd/MMM/yy").Replace("-", "/")
                    Dim ArrDay As String = Convert.ToString(Arrdt.DayOfWeek)
                    strArvdt = strArvdt.Split("/")(0) + " " + strArvdt.Split("/")(1) + " " + strArvdt.Split("/")(2)
                    Dim strArrtime As String = Convert.ToString(FltDetailsList.Rows(f)("ArrTime"))
                    strArrtime = strArrtime.Substring(0, 2) + " : " + strArrtime.Substring(2, 2)
                    TicketFormate += strArvdt
                    TicketFormate += "<br/>"
                    TicketFormate += "<br/>"
                    TicketFormate += strArrtime
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; '>"
                    TicketFormate += FltDetailsList.Rows(f)("DepAirName") + "( " + FltDetailsList.Rows(f)("DFrom") + ")"

                    TicketFormate += "<br />"
                    TicketFormate += "<br />"
                    fltTerminalDetails = TerminalDetails(OrderId, FltDetailsList.Rows(f)("DFrom"), "")
                    If String.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows(0)("DepartureTerminal"))) Then
                        TicketFormate += fltTerminalDetails.Rows(0)("DepAirportName") + " - Trml: NA"
                    Else
                        TicketFormate += fltTerminalDetails.Rows(0)("DepAirportName") + " - Trml:" + fltTerminalDetails.Rows(0)("DepartureTerminal")
                    End If

                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; '>"
                    TicketFormate += FltDetailsList.Rows(f)("ArrAirName") + " (" + FltDetailsList.Rows(f)("ATo") + ")"
                    TicketFormate += "<br />"
                    TicketFormate += "<br />"
                    fltTerminalDetails = TerminalDetails(OrderId, "", FltDetailsList.Rows(f)("ATo"))
                    If String.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows(0)("ArrivalTerminal"))) Then
                        TicketFormate += fltTerminalDetails.Rows(0)("ArrvlAirportName") + " - Trml: NA"
                    Else
                        TicketFormate += fltTerminalDetails.Rows(0)("ArrvlAirportName") + " - Trml:" + fltTerminalDetails.Rows(0)("ArrivalTerminal")
                    End If

                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "</table>"
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"

                    TicketFormate += "<tr>"
                    TicketFormate += "<td colspan='4' style='width:100%;'>"
                    TicketFormate += "<table style='width:100%;'>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 11px; width: 322%; text-align: left; font-weight:bold;'>"
                    'TicketFormate += "<img alt='Logo Not Found' src='http://UmrawatiTrip.com/AirLogo/sm" + FltDetailsList.Rows(f)("AirlineCode") + ".gif' ></img>"
                    TicketFormate += "<br/>"
                    'TicketFormate += FltDetailsList.Rows(f)("AirlineName")
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='width: 32%;'></td>"
                    TicketFormate += "<td style='width: 18%; font-size:12px;text-align:left;'></td>"
                    TicketFormate += "<td style='width: 18%; font-size: 11px; text-align: left; font-weight: bold;'></td>"
                    TicketFormate += "</tr>"
                Next
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</table>"

            Else
                'starstdflsdlfsdkjksdjksdjksdfjkdfjk

                TicketFormate += "<table style='width: 100%;font-family:arial;margin-top: -38px;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td>"
                TicketFormate += "<table style='width:100%;font-family:arial;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align:left; font-size:11px;'>"
                logo()
                'TicketFormate += "<img src='http://UmrawatiTrip.com/images/logo.png' alt='Logo' style='height:54px; width:104px' />"
                TicketFormate += "<img src='" + ImageUrl + "' alt='Logo' style='height:54px; width:104px' /><br/>"

                TicketFormate += "</td>"
                TicketFormate += "<td>"
                Dim dtAgent As DataTable
                'dtAgent = STDom.GetAgencyDetails(Session("UID").ToString).Tables(0)
                dtAgent = ObjIntDetails.SelectAgencyDetail(dtagentid.Rows(0)("AgentID").ToString())
                TicketFormate += "<div style='font-weight: bold;font-size:14px;'>" + dtAgent.Rows(0)("Agency_Name").ToString() + "</div> " + dtAgent.Rows(0)("Address").ToString() + "," + dtAgent.Rows(0)("City").ToString() + ",</br>" + dtAgent.Rows(0)("State").ToString() + "," + dtAgent.Rows(0)("Country").ToString() + "," + dtAgent.Rows(0)("zipcode").ToString()
                TicketFormate += "<br/>"
                TicketFormate += "Mobile - " + FltagentDetail.Rows(0)("Mobile")

                TicketFormate += "<br/>"
                TicketFormate += "Email - " + FltagentDetail.Rows(0)("Email")
                TicketFormate += "<br/>"
                TicketFormate += "</td>"
                TicketFormate += "<td><div id='barcodeTarget' style='float:right;' class='barcodeTarget'></div><canvas id='canvasTarget' style='width:150px; height:150px;'></canvas> "
                TicketFormate += "</td>"
                TicketFormate += "</tr>"


                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td>"
                TicketFormate += "<table style='width: 100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='width: 100%; text-align: justify; color: #231f20; font-size: 11px; padding: 0px;'>"
                TicketFormate += "This is travel itinerary and E-ticket receipt. You may need to show this receipt to enter the airport and/or to show return or onward travel to "
                TicketFormate += "customs and immigration officials."
                TicketFormate += "</td>"

                TicketFormate += "</tr>"
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td>"
                TicketFormate += "<table style='border: 1px solid #cccce0; font-family: Verdana, Geneva, sans-serif; font-size: 11px;padding:0px !important;width:100%;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td class='custom' style='text-align: center;background-color: #cccce0;color: #2b2b2b;font-size:12px;font-weight:bold;padding: 5px;' colspan='6'>"
                TicketFormate += "PNR & Ticket Information"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='6' style='font-size:12px; padding: 0px; width: 100%'>"
                TicketFormate += "<table style='width:100%;' border='1'>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px; font-weight:bold;  text-align: left; padding: 5px;' colspan='2'>GDS PNR</td>"
                TicketFormate += "<td style='font-size: 11px; font-weight:bold;  text-align: left; padding: 5px;'>Airline PNR</td>"
                TicketFormate += "<td style='font-size: 11px; font-weight:bold;  text-align: left; padding: 5px;'>Class</td>"
                TicketFormate += "<td style='font-size: 11px; font-weight:bold;  text-align: left; padding: 5px;'>Status</td>"

                TicketFormate += "<td style='font-size: 11px; font-weight:bold;  text-align: left; padding: 5px;'>Date Of Issue</td>"
                TicketFormate += "<td style='font-size: 11px; font-weight:bold; text-align: left; padding: 5px;'>Fare Type</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='font-size: 11px;  text-align: left; padding: 5px;'  colspan='2'>"
                TicketFormate += FltHeaderList.Rows(0)("GdsPnr")
                TicketFormate += "</td>"

                TicketFormate += "<td style='font-size: 11px;  text-align: left; padding: 5px;'>"
                TicketFormate += FltHeaderList.Rows(0)("AirlinePnr")
                TicketFormate += "</td>"

                TicketFormate += "<td style='font-size: 13px;  text-align: left; padding: 5px;'>"
                TicketFormate += GetCabin(Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("Provider")), Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("AdtCabin")), Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("ValiDatingCarrier")))
                TicketFormate += "</td>"
                'TicketFormate += "<td colspan='2'></td>"

                TicketFormate += "<td style='font-size: 11px; text-align: left; padding: 5px;'>"
                TicketFormate += IIf(Convert.ToString(FltHeaderList.Rows(0)("Status")).ToLower().Trim() = "confirm", "Hold", FltHeaderList.Rows(0)("Status"))
                TicketFormate += "</td>"
                ''TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 5px;'>Issued By</td>"
                ''TicketFormate += "<td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>"
                ''TicketFormate += FltHeaderList.Rows(0)("AgencyName")
                ''TicketFormate += "</td>"

                TicketFormate += "<td style='font-size: 11px;  text-align: left; padding: 5px;'>"
                TicketFormate += Createddate
                TicketFormate += "</td>"

                TicketFormate += "<td style='font-size: 13px; text-align: left; padding: 5px;'>"
                TicketFormate += Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("AdtFareType"))
                TicketFormate += "</td>"
                TicketFormate += "</tr >"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: center; background-color: #cccce0;color: #2b2b2b;font-size:12px;font-weight:bold;padding: 5px;' colspan='8'>"
                TicketFormate += "Passenger Information"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"

                TicketFormate += "</tr>"
                TicketFormate += "<tr>"


                TicketFormate += "<td style='font-size: 11px; font-weight:bold; text-align: left; padding: 5px; ' colspan='3'>Passenger Name</td>"
                TicketFormate += "<td style='font-size: 11px; font-weight:bold; text-align: left; padding: 5px; ' colspan='2'>Ticket Number</td>"
                TicketFormate += "<td style='font-size: 11px; font-weight:bold;border-bottom: 1px solid #7f7f8c; text-align: left; padding: 5px; ' colspan='2'>Customer Info</td>"


                TicketFormate += "</tr>"

                Dim maxcounts As Integer = 0
                maxcounts = FltPaxList.Rows.Count - 1
                For p As Integer = 0 To FltPaxList.Rows.Count - 1
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size: 11px; text-align: left;border-right: 1px solid #7f7f8c; padding: 5px;border-color: #cccce0;' colspan='3'>"
                    TicketFormate += FltPaxList.Rows(p)("Name") + " " + "(" + FltPaxList.Rows(p)("PaxType") + ")"
                    TicketFormate += "</td>"


                    TicketFormate += "<td style='font-size: 11px; text-align: left;border-right: 1px solid #7f7f8c; padding: 5px;border-color: #cccce0;' colspan='2'>"
                    TicketFormate += FltPaxList.Rows(p)("TicketNumber")
                    TicketFormate += "</td>"
                    If maxcounts = p Then
                        TicketFormate += "<td style='font-size: 11px;border-bottom: 1px solid #7f7f8c; text-align: left; border: 1px; padding: 5px;' colspan='2' >"
                        TicketFormate += FltHeaderList.Rows(0)("PgMobile")
                        TicketFormate += "<br/>"
                        TicketFormate += FltHeaderList.Rows(0)("PgEmail")
                        TicketFormate += "</td>"
                    Else
                        TicketFormate += "<td style='font-size: 11px; border: 1px; text-align: left; padding: 5px;' colspan='2' >&nbsp;</td>"
                    End If
                    TicketFormate += "</tr>"
                Next

                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                'TicketFormate += "</td>"
                'TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: center;background-color: #cccce0;color: #2b2b2b;font-size:12px;font-weight:bold;padding: 5px;' colspan='6'>"
                TicketFormate += "<table style='width:100%;'>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align: center; color: #000; font-size: 12px; width: 25%;font-weight:bold;' colspan='1'>"
                TicketFormate += "Flight Information"
                TicketFormate += "</td>"

                TicketFormate += "</tr>"
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='6' style='background-color: #fff;width:100%;'>"
                TicketFormate += "<table border='1' cellpadding='0' Cellspacing ='0' style='width:100%;'>"
                TicketFormate += "<tr  style=''>"
                TicketFormate += "<td style='font-size: 10.5px; color: #424242; width: 10%; text-align: left; padding: 7px; font-weight: bold;'>FLIGHT</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #424242; width: 20%; text-align: left; padding: 7px; font-weight: bold;'>DEPART</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #424242; width: 20%; text-align: left; padding: 7px; font-weight: bold;'>ARRIVE</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #424242; width: 25%; text-align: left; padding: 7px; font-weight: bold;'>DEPART AIRPORT</td>"
                TicketFormate += "<td style='font-size: 10.5px; color: #424242; width: 25%; text-align: left; padding: 7px; font-weight: bold;'>ARRIVE AIRPORT</td>"
                TicketFormate += "</tr>"

                For f As Integer = 0 To FltDetailsList.Rows.Count - 1

                    TicketFormate += "<tr>"
                    TicketFormate += "<td colspan='6'>"
                    TicketFormate += "<table style='width:100%;'>"
                    TicketFormate += "<tr>"


                    TicketFormate += "<td style='padding:10px;font-size: 11px; width: 10%; text-align: left;  vertical-align: top;'>"

                    TicketFormate += "<img alt='Logo Not Found' src='http://UmrawatiTrip.com/AirLogo/sm" + FltDetailsList.Rows(f)("AirlineCode") + ".gif' ></img>"
                    TicketFormate += "<br/>"

                    TicketFormate += FltDetailsList.Rows(f)("AirlineCode") + " " + FltDetailsList.Rows(f)("FltNumber")
                    TicketFormate += "<br/>"

                    TicketFormate += FltDetailsList.Rows(f)("AirlineName")
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>"
                    Dim strDepdt As String = Convert.ToString(FltDetailsList.Rows(f)("DepDate"))
                    'strDepdt = strDepdt.Substring(0, 2) + "-" + strDepdt.Substring(2, 2) + "-" + strDepdt.Substring(4, 2)
                    strDepdt = IIf(strDepdt.Length = 8, STD.BAL.Utility.Left(strDepdt, 4) & "-" & STD.BAL.Utility.Mid(strDepdt, 4, 2) & "-" & STD.BAL.Utility.Right(strDepdt, 2), "20" & STD.BAL.Utility.Right(strDepdt, 2) & "-" & STD.BAL.Utility.Mid(strDepdt, 2, 2) & "-" & STD.BAL.Utility.Left(strDepdt, 2))
                    Dim deptdt As DateTime = Convert.ToDateTime(strDepdt)
                    strDepdt = deptdt.ToString("dd/MMM/yy").Replace("-", "/")

                    ''Response.Write(strDepdt)


                    Dim depDay As String = Convert.ToString(deptdt.DayOfWeek)
                    strDepdt = strDepdt.Split("/")(0) + " " + strDepdt.Split("/")(1) + " " + strDepdt.Split("/")(2)
                    Dim strdeptime As String = Convert.ToString(FltDetailsList.Rows(f)("DepTime"))
                    strdeptime = strdeptime.Substring(0, 2) + " : " + If(strdeptime.Length > 4, strdeptime.Substring(3, 2), strdeptime.Substring(2, 2))
                    TicketFormate += strDepdt
                    TicketFormate += "<br/>"
                    TicketFormate += "<br/>"
                    TicketFormate += strdeptime
                    TicketFormate += "</td>"

                    TicketFormate += "<td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>"
                    Dim strArvdt As String = Convert.ToString(FltDetailsList.Rows(f)("ArrDate"))
                    'strArvdt = strArvdt.Substring(0, 2) + "-" + strArvdt.Substring(2, 2) + "-" + strArvdt.Substring(4, 2)
                    strArvdt = IIf(strArvdt.Length = 8, STD.BAL.Utility.Left(strArvdt, 4) & "-" & STD.BAL.Utility.Mid(strArvdt, 4, 2) & "-" & STD.BAL.Utility.Right(strArvdt, 2), "20" & STD.BAL.Utility.Right(strArvdt, 2) & "-" & STD.BAL.Utility.Mid(strArvdt, 2, 2) & "-" & STD.BAL.Utility.Left(strArvdt, 2))
                    Dim Arrdt As DateTime = Convert.ToDateTime(strArvdt)
                    strArvdt = Arrdt.ToString("dd/MMM/yy").Replace("-", "/")
                    Dim ArrDay As String = Convert.ToString(Arrdt.DayOfWeek)
                    strArvdt = strArvdt.Split("/")(0) + " " + strArvdt.Split("/")(1) + " " + strArvdt.Split("/")(2)
                    Dim strArrtime As String = Convert.ToString(FltDetailsList.Rows(f)("ArrTime"))
                    strArrtime = strArrtime.Substring(0, 2) + " : " + If(strArrtime.Length > 4, strArrtime.Substring(3, 2), strArrtime.Substring(2, 2))
                    TicketFormate += strArvdt
                    TicketFormate += "<br/>"
                    TicketFormate += "<br/>"
                    TicketFormate += strArrtime
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; '>"
                    TicketFormate += FltDetailsList.Rows(f)("DepAirName") + "( " + FltDetailsList.Rows(f)("DFrom") + ")"

                    TicketFormate += "<br />"
                    TicketFormate += "<br />"
                    fltTerminalDetails = TerminalDetails(OrderId, FltDetailsList.Rows(f)("DFrom"), "")
                    'if (!String.IsNullOrEmpty(Convert.ToString(fltTerminal.Rows[0]["DepartureTerminal"])))
                    '    TicketFormate += "Terminal:" + fltTerminal.Rows[0]["DepartureTerminal"];
                    'else
                    If String.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows(0)("DepartureTerminal"))) Then
                        TicketFormate += fltTerminalDetails.Rows(0)("DepAirportName") + " - Trml: NA"
                    Else
                        TicketFormate += fltTerminalDetails.Rows(0)("DepAirportName") + " - Trml:" + fltTerminalDetails.Rows(0)("DepartureTerminal")
                    End If

                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 25%; text-align: left; padding: 2px; '>"
                    TicketFormate += FltDetailsList.Rows(f)("ArrAirName") + " (" + FltDetailsList.Rows(f)("ATo") + ")"
                    TicketFormate += "<br />"
                    TicketFormate += "<br />"
                    'if (!String.IsNullOrEmpty(Convert.ToString(fltTerminal.Rows[f]["ArrivalTerminal"])))
                    '    TicketFormate += "Terminal:" + fltTerminal.Rows[f]["ArrivalTerminal"];
                    'else
                    fltTerminalDetails = TerminalDetails(OrderId, "", FltDetailsList.Rows(f)("ATo"))
                    If String.IsNullOrEmpty(Convert.ToString(fltTerminalDetails.Rows(0)("ArrivalTerminal"))) Then
                        TicketFormate += fltTerminalDetails.Rows(0)("ArrvlAirportName") + " - Trml: NA"
                    Else
                        TicketFormate += fltTerminalDetails.Rows(0)("ArrvlAirportName") + " - Trml:" + fltTerminalDetails.Rows(0)("ArrivalTerminal")
                    End If

                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "</table>"
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"






                Next
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr class='TRFI1 fclasshow'>"
                TicketFormate += "<td style='text-align: center;background-color: #cccce0;color: #2b2b2b;font-size:12px;font-weight:bold;padding: 5px;'>"
                TicketFormate += "Fare Information"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                If TransID = "" OrElse TransID Is Nothing Then


                    TicketFormate += "<tr class='TRFI1 fclasshow'>"
                    TicketFormate += "<td style='width:100%;'>"
                    TicketFormate += "<table cellspacing ='0' cellpadding='0' border='1' style='width:100%;'>"
                    TicketFormate += "<tr>"
                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>Pax Type</td>"
                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>Pax Count</td>"
                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Base fare</td>"
                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Fuel Surcharge</td>"
                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>Tax</td>"
                    'TicketFormate += "<td style='font-size:12px; color: #424242; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>STax</td>"
                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>Trans Fee</td>"
                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Trans Charge</td>"
                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>TOTAL</td>"
                    TicketFormate += "</tr>"
                    For fd As Integer = 0 To fltFare.Rows.Count - 1

                        If fltFare.Rows(fd)("PaxType").ToString() = "ADT" AndAlso initialAdt = 0 Then
                            Dim numberOfADT As Integer = FltPaxList.AsEnumerable().Where(Function(x) x("PaxType").ToString() = "ADT").ToList().Count
                            TicketFormate += "<tr class='TRFI1 fclasshow'>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'>"
                            TicketFormate += fltFare.Rows(fd)("PaxType")
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;' id='td_adtcnt'>" & numberOfADT & "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 4px; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("BaseFare")) * numberOfADT).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 4px; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("Fuel")) * numberOfADT).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'id='td_taxadt'>"
                            TicketFormate += ((Convert.ToDecimal(fltFare.Rows(fd)("Tax")) + Convert.ToDecimal(fltFare.Rows(fd)("TCharge"))) * numberOfADT).ToString
                            TicketFormate += "</td>"
                            'TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'>"
                            'TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("ServiceTax")) * numberOfADT).ToString
                            'TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("TFee")) * numberOfADT).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 4px; vertical-align: top;'id='td_tcadt'>"
                            ''TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("TCharge")) * numberOfADT).ToString
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("ticketcopymarkupforTC")) * numberOfADT).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 4px; vertical-align: top;' id='td_adttot'>"
                            AdtTtlFare = (Convert.ToDecimal(fltFare.Rows(fd)("Total")) * numberOfADT).ToString
                            TicketFormate += AdtTtlFare.ToString
                            TicketFormate += "</td>"

                            TicketFormate += "</tr>"

                            initialAdt += 1
                        End If

                        If fltFare.Rows(fd)("PaxType").ToString() = "CHD" AndAlso initalChld = 0 Then
                            Dim numberOfCHD As Integer = FltPaxList.AsEnumerable().Where(Function(x) x("PaxType").ToString() = "CHD").ToList().Count
                            TicketFormate += "<tr class='TRFI1 fclasshow'>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'>"
                            TicketFormate += fltFare.Rows(fd)("PaxType")
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;' id='td_chdcnt'>" & numberOfCHD & "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 4px; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("BaseFare")) * numberOfCHD).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 4px; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("Fuel")) * numberOfCHD).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'id='td_taxchd'>"
                            TicketFormate += ((Convert.ToDecimal(fltFare.Rows(fd)("Tax")) + Convert.ToDecimal(fltFare.Rows(fd)("TCharge"))) * numberOfCHD).ToString
                            TicketFormate += "</td>"
                            'TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'>"
                            'TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("ServiceTax")) * numberOfCHD).ToString
                            'TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("TFee")) * numberOfCHD).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 4px; vertical-align: top;'id='td_tcchd'>"
                            ''TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("TCharge")) * numberOfCHD).ToString
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("ticketcopymarkupforTC")) * numberOfCHD).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 4px; vertical-align: top;'id='td_chdtot'>"
                            ChdTtlFare = (Convert.ToDecimal(fltFare.Rows(fd)("Total")) * numberOfCHD).ToString
                            TicketFormate += ChdTtlFare.ToString
                            TicketFormate += "</td>"

                            TicketFormate += "</tr>"

                            initalChld += 1
                        End If
                        If fltFare.Rows(fd)("PaxType").ToString() = "INF" AndAlso initialift = 0 Then
                            Dim numberOfINF As Integer = FltPaxList.AsEnumerable().Where(Function(x) x("PaxType").ToString() = "INF").ToList().Count
                            TicketFormate += "<tr class='TRFI1 fclasshow'>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'>"
                            TicketFormate += fltFare.Rows(fd)("PaxType")
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;' id='td_infcnt'>" & numberOfINF & "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 4px; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("BaseFare")) * numberOfINF).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 4px; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("Fuel")) * numberOfINF).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("Tax")) * numberOfINF).ToString
                            TicketFormate += "</td>"
                            'TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'>"
                            'TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("ServiceTax")) * numberOfINF).ToString
                            'TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'>"
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("TFee")) * numberOfINF).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 4px; vertical-align: top;'>"
                            'TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("TCharge")) * numberOfINF).ToString
                            TicketFormate += (Convert.ToDecimal(fltFare.Rows(fd)("ticketcopymarkupforTC")) * numberOfINF).ToString
                            TicketFormate += "</td>"
                            TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; padding: 4px; vertical-align: top;'id='td_Inftot'>"
                            INFTtlFare = (Convert.ToDecimal(fltFare.Rows(fd)("Total")) * numberOfINF).ToString
                            TicketFormate += INFTtlFare.ToString
                            TicketFormate += "</td>"
                            TicketFormate += "</tr>"
                            initialift += 1

                        End If
                    Next
                    fare = AdtTtlFare + ChdTtlFare + INFTtlFare
                Else
                    TicketFormate += "<tr class='TRFI1 fclasshow'>"
                    TicketFormate += "<td colspan='2' style='width:100%;'>"
                    TicketFormate += "<table border='1' style='width:100%;'>"
                    TicketFormate += "<tr>"

                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Pax Type</td>"
                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Pax Count</td>"
                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>Base fare</td>"
                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>Fuel Surcharge	</td>"
                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>Tax</td>"
                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>Trans Fee</td>"
                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Trans Charge</td>"
                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>TOTAL</td>"
                    TicketFormate += "</tr>"

                    TicketFormate += "<tr class='TRFI1 fclasshow'>"
                    'Dim numberOfADT As Integer = FltPaxList.AsEnumerable().Where(Function(x) x("PaxType").ToString() = "ADT").ToList().Count
                    TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;' id='td_perpaxtype'>" + FltPaxList.Rows(0)("PaxType") + "</td>"




                    TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'>"
                    TicketFormate += fltFare.Rows(0)("Fuel").ToString
                    TicketFormate += "</td>"


                    TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;' id='td_perpaxtax'>"
                    TicketFormate += fltFare.Rows(0)("Tax").ToString
                    TicketFormate += "</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'>"
                    TicketFormate += fltFare.Rows(0)("BaseFare").ToString
                    TicketFormate += "</td>"
                    'TicketFormate += "<tr>"
                    'TicketFormate += "<td style='font-size:12px; width: 50%; text-align: left; vertical-align: top;'>STax</td>"
                    'TicketFormate += "<td style='font-size:12px; width: 50%; text-align: left; vertical-align: top;'>"
                    'TicketFormate += fltFare.Rows(0)("ServiceTax").ToString
                    'TicketFormate += "</td>"
                    'TicketFormate += "</tr>"


                    TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'>"
                    TicketFormate += fltFare.Rows(0)("TFee").ToString
                    TicketFormate += "</td>"

                    TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'id='td_perpaxtc'>"
                    TicketFormate += fltFare.Rows(0)("TCharge").ToString
                    TicketFormate += "</td>"


                    Dim ResuCharge As Decimal = 0
                    Dim ResuServiseCharge As Decimal = 0
                    Dim ResuFareDiff As Decimal = 0
                    If Convert.ToString(FltHeaderList.Rows(0)("ResuCharge")) IsNot Nothing AndAlso Convert.ToString(FltHeaderList.Rows(0)("ResuCharge")) <> "" Then
                        TicketFormate += "<tr class='TRFI1 fclasshow'>"
                        TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'>Reissue Charge</td>"
                        TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'>"
                        TicketFormate += FltHeaderList.Rows(0)("ResuCharge").ToString
                        ResuCharge = (Convert.ToDecimal(FltHeaderList.Rows(0)("ResuCharge"))).ToString
                        TicketFormate += "</td>"
                        TicketFormate += "</tr>"
                    End If
                    If Convert.ToString(FltHeaderList.Rows(0)("ResuServiseCharge")) IsNot Nothing AndAlso Convert.ToString(FltHeaderList.Rows(0)("ResuServiseCharge")) <> "" Then
                        TicketFormate += "<tr class='TRFI1 fclasshow'>"
                        TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'>Reissue Srv. Charge</td>"
                        TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'>"
                        TicketFormate += FltHeaderList.Rows(0)("ResuServiseCharge").ToString
                        ResuServiseCharge = (Convert.ToDecimal(FltHeaderList.Rows(0)("ResuServiseCharge"))).ToString
                        TicketFormate += "</td>"
                        TicketFormate += "</tr>"
                    End If
                    If Convert.ToString(FltHeaderList.Rows(0)("ResuFareDiff")) IsNot Nothing AndAlso Convert.ToString(FltHeaderList.Rows(0)("ResuFareDiff")) <> "" Then
                        TicketFormate += "<tr class='TRFI1 fclasshow'>"
                        TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'>Reissue Fare Diff</td>"
                        TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'>"
                        TicketFormate += FltHeaderList.Rows(0)("ResuFareDiff").ToString
                        ResuFareDiff = (Convert.ToDecimal(FltHeaderList.Rows(0)("ResuFareDiff"))).ToString
                        TicketFormate += "</td>"
                        TicketFormate += "</tr>"
                    End If
                    TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;'>TOTAL</td>"
                    TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; padding: 4px; vertical-align: top;' id='td_totalfare'>"
                    fare = (Convert.ToDecimal(fltFare.Rows(0)("BaseFare")) + Convert.ToDecimal(fltFare.Rows(0)("Fuel")) + Convert.ToDecimal(fltFare.Rows(0)("Tax")) + Convert.ToDecimal(fltFare.Rows(0)("ServiceTax")) + Convert.ToDecimal(fltFare.Rows(0)("TCharge")) + Convert.ToDecimal(fltFare.Rows(0)("TFee")) + ResuCharge + ResuServiseCharge + ResuFareDiff).ToString
                    TicketFormate += fare.ToString
                    TicketFormate += "</td>"

                    'fare = Convert.ToDecimal(fltFare.Rows[0]["Total"]) + ResuCharge + ResuServiseCharge + ResuFareDiff;
                    TicketFormate += "</tr>"
                End If
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                If fltMealAndBag.Rows.Count > 0 Then
                    TicketFormate += "<tr class='TRFI1 fclasshow'>"
                    TicketFormate += "<td colspan='7' style= 'background-color: #cccce0;width:100%;'>"
                    TicketFormate += "<table cellspacing ='0' cellpadding='0' border='1' style='width:100%;'>"
                    TicketFormate += "<tr class='TRFI1 fclasshow'>"
                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Pax Name</td>"
                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Trip Type</td>"
                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Meal Code</td>"
                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Meal Price</td>"
                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Baggage Code</td>"
                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Baggage Price</td>"
                    TicketFormate += "<td style='font-size:12px; color: #424242; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>TOTAL</td>"
                    TicketFormate += "</tr>"
                    TicketFormate += "</table>"
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"

                    TicketFormate += "<tr class='TRFI1 fclasshow'>"
                    TicketFormate += "<td colspan='7' style='width:100%;'>"
                    TicketFormate += "<table cellspacing ='0' cellpadding='0' border='1' style='width:100%;'>"


                    For i As Integer = 0 To fltMealAndBag.Rows.Count - 1
                        'If Convert.ToString(fltMealAndBag.Rows(i)("MealPrice")) <> "0.00" AndAlso Convert.ToString(fltMealAndBag.Rows(i)("BaggagePrice")) <> "0.00" Then
                        TicketFormate += "<tr class='TRFI1 fclasshow'>"
                        TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>"
                        TicketFormate += Convert.ToString(fltMealAndBag.Rows(i)("Name"))
                        TicketFormate += "</td>"
                        TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>"
                        TicketFormate += Convert.ToString(fltMealAndBag.Rows(i)("TripType"))
                        TicketFormate += "</td>"
                        TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>"
                        TicketFormate += Convert.ToString(fltMealAndBag.Rows(i)("MealCode"))
                        TicketFormate += "</td>"
                        TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>"
                        TicketFormate += Convert.ToString(fltMealAndBag.Rows(i)("MealPrice"))
                        TicketFormate += "</td>"
                        TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>"
                        TicketFormate += Convert.ToString(fltMealAndBag.Rows(i)("BaggageCode"))
                        TicketFormate += "</td>"
                        TicketFormate += "<td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>"
                        TicketFormate += Convert.ToString(fltMealAndBag.Rows(i)("BaggagePrice"))
                        TicketFormate += "</td>"
                        TicketFormate += "<td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>"
                        MealBagTotalPrice += Convert.ToDecimal(fltMealAndBag.Rows(i)("TotalPrice"))
                        TicketFormate += Convert.ToString(fltMealAndBag.Rows(i)("TotalPrice"))
                        TicketFormate += "</td>"

                        TicketFormate += "</tr>"
                        'End If
                    Next
                    TicketFormate += "</table>"
                    TicketFormate += "</td>"
                    TicketFormate += "</tr>"
                End If



                TicketFormate += "<tr class='TRFI1 fclasshow'>"
                TicketFormate += "<td style='background-color: #cccce0; color:#fff;font-size:14px;font-weight:bold; padding: 0px;'>"
                TicketFormate += "<table cellspacing ='0' cellpadding='0' border='1' style='width:100%;'>"
                TicketFormate += "<tr>"

                TicketFormate += "<td style='font-size:12px; width: 41%; text-align: left; vertical-align: top;'></td>"
                TicketFormate += "<td style='color: #000; font-size:14px; width: 17%; text-align: center; vertical-align: top;'>GRAND TOTAL</td>"
                TicketFormate += "<td style='color: #000; font-size:14px; width: 10%; text-align: center; vertical-align: top;'id='td_grandtot'>"
                TicketFormate += (fare + MealBagTotalPrice).ToString
                TicketFormate += "</td>"

                TicketFormate += "</tr>"
                TicketFormate += "</table>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<br/><br/>"
                TicketFormate += "<tr>"
                TicketFormate += "<td>"
                TicketFormate += "<ul style='list-style-image: url(http://UmrawatiTrip.com/Images/bullet.png);'>"
                TicketFormate += "<li style='font-size:10.5px;'>Kindly confirm the status of your PNR within 24 hrs of booking, as at times the same may fail on account of payment failure, internet connectivity, booking engine or due to any other reason beyond our control."
                TicketFormate += "For Customers who book their flights well in advance of the scheduled departure date it is necessary that you re-confirm the departure time of your flight between 72 and 24 hours before the Scheduled Departure Time.</li>"
                TicketFormate += "</ul>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td style='text-align:center;background-color: #cccce0; color: #000; font-size: 11px; font-weight: bold; padding: 5px;'>TERMS AND CONDITIONS :</td>"
                TicketFormate += "</tr>"

                TicketFormate += "<tr>"
                TicketFormate += "<td >"
                TicketFormate += "<ul style='list-style-image: url(http://UmrawatiTrip.com/Images/bullet.png);'>"
                TicketFormate += "<li style='font-size:10.5px;'>Guests are requested to carry their valid photo identification for all guests, including children.</li>"
                TicketFormate += "<li style='font-size:10.5px;'>We recommend check-in at least 2 hours prior to departure.</li>"
                TicketFormate += "<li style='font-size:10.5px;'>Boarding gates close 45 minutes prior to the scheduled time of departure. Please report at your departure gate at the indicated boarding time. Any passenger failing to report in time may be refused boarding privileges.</li>"
                TicketFormate += "<li style='font-size:10.5px;'>Cancellations and Changes permitted more than two (2) hours prior to departure with payment of change fee and difference in fare if applicable only in working hours (10:00 am to 06:00 pm) except Sundays and Holidays.</li>"
                TicketFormate += "<li style='font-size:10.5px;'>"
                TicketFormate += "Flight schedules are subject to change and approval by authorities."
                TicketFormate += "<br />"
                TicketFormate += "</li>"
                TicketFormate += "<li style='font-size:10.5px;'>"
                TicketFormate += "Name Changes on a confirmed booking are strictly prohibited. Please ensure that the name given at the time of booking matches as mentioned on the traveling Guests valid photo ID Proof."
                TicketFormate += "<br />"
                TicketFormate += " Travel Agent does not provide compensation for travel on other airlines, meals, lodging or ground transportation."
                TicketFormate += "</li>"
                TicketFormate += "<li style='font-size:10.5px;'>Bookings made under the Armed Forces quota are non cancelable and non- changeable.</li>"

                TicketFormate += "<li style='font-size:10.5px;'>Guests are advised to check their all flight details (including their Name, Flight numbers, Date of Departure, Sectors) before leaving the Agent Counter.</li>"
                TicketFormate += "<li style='font-size:10.5px;'>Cancellation amount will be charged as per airline rule.</li>"
                TicketFormate += "<li style='font-size:10.5px;'>Guests requiring wheelchair assistance, stretcher, Guests traveling with infants and unaccompanied minors need to be booked in advance since the inventory for these special service requests are limited per flight.</li>"
                TicketFormate += "</ul>"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                TicketFormate += "</table>"
                TicketFormate += "<table style='width: 100%;font-family:arial;'>"
                TicketFormate += "<tr>"
                TicketFormate += "<td colspan='4' style='background-color: #cccce0; color: #424242; font-size: 11px; font-weight: bold; padding: 5px;'>BAGGAGE INFORMATION :"
                TicketFormate += "</td>"
                TicketFormate += "</tr>"
                Bag = False
                If Not String.IsNullOrEmpty(Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("IsBagFare"))) Then
                    Bag = Convert.ToBoolean(SelectedFltDS.Tables(0).Rows(0)("IsBagFare"))
                End If

                Dim dtbaggage As New DataTable
                dtbaggage = objTranDom.GetBaggageInformation("D", FltHeaderList.Rows(0)("VC"), Bag).Tables(0)
                Dim bginfo As String = GetBagInfo(Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("Provider")), Convert.ToString(SelectedFltDS.Tables(0).Rows(0)("AirlineRemark")))

                If bginfo = "" Then

                    For Each drbg In dtbaggage.Rows

                        TicketFormate += "<tr>"
                        TicketFormate += "<td colspan='2'>" & drbg("BaggageName") & "</td>"
                        TicketFormate += "<td colspan='2'>" & drbg("Weight") & "</td>"
                        TicketFormate += "</tr>"
                    Next


                Else
                    TicketFormate += "<tr>"
                    TicketFormate += "<td colspan='2'></td>"
                    TicketFormate += "<td colspan='2'>" & bginfo & "</td>"
                    TicketFormate += "</tr>"

                End If




                TicketFormate += "</table>"

                TicketFormate += " <script type='text/javascript'>"
                TicketFormate += " decodeURI(window.location.search).substring(1).split('&');"
                TicketFormate += "  var btype = 'code128';"
                TicketFormate += "  var renderer = 'css';"
                TicketFormate += "  var quietZone = false;"
                TicketFormate += " if ($('#quietzone').is(':checked') || $('#quietzone').attr('checked'))"
                TicketFormate += "  { quietZone = true; }"
                TicketFormate += "  var settings = { output: renderer, bgColor: '#FFFFFF', color: '#000000', barWidth: '1', barHeight: '110', moduleSize: '5', posX: '10', posY: '20', addQuietZone: '1' };"
                TicketFormate += "   $('#canvasTarget').hide();"
                TicketFormate += "  $('#barcodeTarget').html('').show().barcode('" + OrderId + "', btype, settings);"
                TicketFormate += "  </script>"

                'TicketFormate += "</td"
                'TicketFormate += "</tr>"
                'TicketFormate += "</table>"
            End If

            'TicketFormate += "<table style='width: 100%;'>"
            'TicketFormate += "<tr>"
            'TicketFormate += "<td style='width: 100%; text-align: justify; color: #231f20; font-size: 11px; padding: 10px; font-size:10.5px;'>"
            ''TicketFormate += "For any assistance contact: ATPI International Pvt. Ltd. | Tel: 00-91-2240095555 | Fax: 00-91-2240095556 | "

            'TicketFormate += "</td>"
            'TicketFormate += "</tr>"
            'TicketFormate += "</table>"
            '#End Region
            'Dim Body As String = ""

            'Dim status As Integer = 0
            'Try

            '    strFileNmPdf = ConfigurationManager.AppSettings("HTMLtoPDF").ToString().Trim() + FltHeaderList.Rows(0)("GdsPnr") + "-" + DateTime.Now.ToString().Replace(":", "").Replace("/", "-").Replace(" ", "-").Trim() + ".pdf"
            '    Dim pdfDoc As New iTextSharp.text.Document(PageSize.A4)
            '    Dim writer As PdfWriter = PdfWriter.GetInstance(pdfDoc, New FileStream(strFileNmPdf, FileMode.Create, FileAccess.ReadWrite, FileShare.None))
            '    pdfDoc.Open()
            '    Dim sr As New StringReader(TicketFormate)
            '    iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr)
            '    pdfDoc.Close()
            '    writer.Dispose()
            '    sr.Dispose()
            '    writePDF = True
            '    Return TicketFormate
            'Catch ex As Exception
            'End Try
            Return TicketFormate
        Catch ex As Exception
            ' Response.Write(ex.Message & ex.StackTrace.ToString())
            clsErrorLog.LogInfo(ex)



        End Try
    End Function


    Public Function SelectPaxDetail(ByVal OrderId As String, ByVal TID As String) As DataTable
        Dim adap As New SqlDataAdapter()
        Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
        If String.IsNullOrEmpty(TID) Then
            Dim dt As New DataTable()

            adap = New SqlDataAdapter("SELECT PaxId, OrderId, Title + '  ' + FName + '  ' + MName + '  ' + LName AS Name, PaxType, TicketNumber,DOB,FFNumber,FFAirline,MealType,SeatType FROM   FltPaxDetails WHERE OrderId = '" & OrderId & "' ", con)
            'adap = New SqlDataAdapter("SELECT PaxId, OrderId, Title + '  ' + FName + '  ' + MName + '  ' + LName AS Name, PaxType, TicketNumber,DOB FROM   FltPaxDetails WHERE OrderId = '" & OrderId & "' ", con)
            adap.Fill(dt)

            Return dt
        Else
            Dim dt As New DataTable()
            adap = New SqlDataAdapter("SELECT PaxId, OrderId, PaxId, Title + '  ' + FName + '  ' + MName + '  ' + LName AS Name, PaxType, TicketNumber,DOB,FFNumber,FFAirline,MealType,SeatType FROM   FltPaxDetails WHERE OrderId = '" & OrderId & "' and PaxId= '" & TID & "' ", con)
            'adap = New SqlDataAdapter("SELECT PaxId, OrderId, PaxId, Title + '  ' + FName + '  ' + MName + '  ' + LName AS Name, PaxType, TicketNumber,DOB FROM   FltPaxDetails WHERE OrderId = '" & OrderId & "' and PaxId= '" & TID & "' ", con)
            adap.Fill(dt)
            Return dt
        End If
    End Function


    Public Function TerminalDetails(ByVal OrderID As String, ByVal DepCity As String, ByVal ArrvlCity As String) As DataTable
        Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
        Dim adap As New SqlDataAdapter("USP_TERMINAL_INFO", con1)
        adap.SelectCommand.CommandType = CommandType.StoredProcedure
        adap.SelectCommand.Parameters.AddWithValue("@DEPARTURECITY", DepCity)
        adap.SelectCommand.Parameters.AddWithValue("@ARRIVALCITY", ArrvlCity)
        adap.SelectCommand.Parameters.AddWithValue("@ORDERID", OrderID)
        Dim dt1 As New DataTable()
        con1.Open()
        adap.Fill(dt1)
        con1.Close()
        Return dt1
    End Function


    Public Function GetCabin(ByVal Provider As String, ByVal cabin As String, ByVal VC As String) As String
        Dim cabininfo As String = ""
        Try


            If Provider = "TB" And VC = "G8" Then

            ElseIf Provider = "TB" Then

                cabininfo = "Economy"


            Else

                If cabin.ToUpper().Trim() = "Y" Then

                    cabininfo = "Economy"
                ElseIf cabin.ToUpper().Trim() = "C" Then
                    cabininfo = "Business"
                ElseIf cabin.ToUpper().Trim() = "F" Then
                    cabininfo = "First"
                ElseIf cabin.ToUpper().Trim() = "W" Then
                    cabininfo = "Premium Economy"

                Else

                    cabininfo = cabin

                End If



            End If

        Catch ex As Exception

        End Try
        Return cabininfo
    End Function
    Public Sub logo()
        Try
            Dim filepath As String = Server.MapPath("~\AgentLogo") + "\" + Session("UID") + ".jpg" 'Server.MapPath("~/AgentLogo/" + LogoName)


            If (System.IO.File.Exists(filepath)) Then
                ImageUrl = "http://111.118.185.114/UmrawatiTripadmin/AgentLogo/" & Session("UID") & ".jpg"
            Else
                ImageUrl = "http://111.118.185.114/UmrawatiTripadmin/Images/new-logo.PNG"
            End If


        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Public Function GetBagInfo(ByVal Provider As String, ByVal Remark As String) As String

        Dim baginfo As String = ""
        If Provider = "TB" Then

            If Remark.Contains("Hand") Then
                baginfo = Remark

            End If
        ElseIf Provider = "YA" Then

            If Remark.Contains("Hand") Then
                baginfo = Remark

            ElseIf Not String.IsNullOrEmpty(Remark) Then

                baginfo = Remark & " Baggage allowance"

            End If


        ElseIf Provider = "1G" Then

            If Remark.Contains("PC") Then

                baginfo = Remark.Replace("PC", " Piece(s) Baggage allowance")
            ElseIf Remark.Contains("K") Then

                baginfo = Remark.Replace("K", " Kg Baggage allowance")

            End If

        End If
        Return baginfo

    End Function
    

    


    

    

    Public Function GetMerchantKey(ByVal orderID As String) As String


        Dim mrchntKey As String = ConfigurationManager.AppSettings("MerchantKey").ToString()

        Try


            Dim provider12 As String = ""
            Dim sqldom As New SqlTransactionDom()

            Dim dsp As New DataSet()

            dsp = sqldom.GetTicketingProvider(orderID)




            If (dsp.Tables.Count > 0) Then
                If (dsp.Tables(0).Rows.Count > 0) Then

                    provider12 = dsp.Tables(0).Rows(0)(0)

                End If


            End If


            If provider12.ToLower().Trim() = "yatra" Then

                mrchntKey = ConfigurationManager.AppSettings("YatraITZMerchantKey").ToString()
            End If

        Catch ex As Exception
            mrchntKey = ConfigurationManager.AppSettings("MerchantKey").ToString()
        End Try
        Session("MchntKeyITZ") = mrchntKey
        Return mrchntKey

    End Function

  

End Class




