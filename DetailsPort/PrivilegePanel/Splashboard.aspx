﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="Splashboard.aspx.cs" Inherits="Dashboard" %>




<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>
 <link href="<%=ResolveUrl("~/assets/css/material-dashboard.css?v=2.1.0") %>" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="<%=ResolveUrl("~/assets/demo/demo.css") %>" rel="stylesheet" />
      <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
  <!-- CSS Files -->

    	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="assets_1/vendor/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="assets_1/vendor/font-awesome/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="assets_1/vendor/linearicons/style.css"/>
	<link rel="stylesheet" href="assets_1/vendor/chartist/css/chartist-custom.css"/>
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="assets_1/css/main.css"/>
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets_1/css/demo.css"/>
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet"/>
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets_1/img/apple-icon.png"/>
	<link rel="icon" type="image/png" sizes="96x96" href="assets_1/img/favicon.png"/>


    <!--Chart Flow-->
   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript">
        // Load google charts
        google.charts.load('current', { 'packages': ['corechart'] });
        google.charts.setOnLoadCallback(drawChart);

        // Draw the chart and set the chart values
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
            ['Task', 'Hours per Day'],
            ['Work', 8]
            
           
            ]);

            // Optional; add a title and set the width and height of the chart
            var options = { 'width': 550, 'height': 400 };

            // Display the chart inside the <div> element with id="piechart"
            var chart = new google.visualization.PieChart(document.getElementById('Chart1'));
            chart.draw(data, options);
        }
</script>



    <style>
        .counter
{
    /*background-color: #eaecf0;*/
    text-align: center;
}
.employees,.customer,.design,.order
{
    margin-top: 70px;
    margin-bottom: 70px;
}
.counter-count
{
    font-size: 18px;
    background-color: #00b3e7;
    border-radius: 50%;
    position: relative;
    color: #ffffff;
    text-align: center;
    line-height: 92px;
    width: 92px;
    height: 92px;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -ms-border-radius: 50%;
    -o-border-radius: 50%;
    display: inline-block;
}

.employee-p,.customer-p,.order-p,.design-p
{
    font-size: 24px;
    color: #000000;
    line-height: 34px;
}
    </style>

    <script>
        $('.counter-count').each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 5000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
    </script>

    <div style="background-color:#f5f5f5;margin-top: -2px; ">
      <div class="row" runat="server">
              <div class="container-fluid" style="padding-left:40px; margin-top: 20px;" >
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats" style="background-image: linear-gradient(141deg, #9fb8ad 0%, #dce2e2 51%, #d4d7d8 75%);height:141px;">
                <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon" style="box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 1);">
                   <%-- <i class="fa fa-dashboard"></i>--%>
                      <img src="../../Images/Ticketed.png" alt="ticketed" style="width:64px;">
                      <%--<link rel="icon" href="../../Images/Ticketed.png" type="image/gif" />--%>
                      

                  </div>
                  <p class="card-category">Ticketed</p>
                  <h3 class="card-title" style="border:none; background-color: #f0f8ff00;"><asp:Label ID="lbticketed" runat="server">0</asp:Label>
                    <%--<small>GB</small>--%>
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                   <%-- <i class="material-icons text-danger">warning</i>--%>
                    <a href="<%=ResolveUrl("~/DetailsPort/TicketReport.aspx") %>" style="color:#5f5e5e">View Detail</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats" style="background-image: linear-gradient(141deg, #9fb8ad 0%, #dce2e2 51%, #d4d7d8 75%);" >
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon" style="box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 1);">
                    <%--<i class="material-icons">flight</i>--%>
                      <img src="../../Images/Ticket_Pending.png" alt="pending" style="width:64px;"/>
                      
                      
                  </div>
                  <p class="card-category">Pending</p>
                  <h3 class="card-title"  style="border:none; background-color: #f0f8ff00; color:#6f6c6c;"><asp:Label ID="lbpending" runat="server">0</asp:Label></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <%--<i class="material-icons" >date_range</i>--%>
                      <a href="<%=ResolveUrl("~/DetailsPort/TicketReport.aspx") %>" style="color:#5f5e5e">View Detail</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats" style="background-image: linear-gradient(141deg, #9fb8ad 0%, #dce2e2 51%, #d4d7d8 75%);">
                <div class="card-header card-header-danger card-header-icon">
                  <div class="card-icon" style="box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 1);">
                      <img src="../../Images/Refund.png" alt="pending" style="width:64px;"/>
                    
                  </div>
                  <p class="card-category">Refund</p>
                  <h3 class="card-title"  style="border:none; background-color: #f0f8ff00; color:#6f6c6c;"><asp:Label ID="lbrefund" runat="server">0</asp:Label></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <%--<i class="material-icons">date_range</i>--%>
                         <a href="#" style="color:##5f5e5e">View Detail</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats" style="background-image: linear-gradient(141deg, #9fb8ad 0%, #dce2e2 51%, #d4d7d8 75%);">
                <div class="card-header card-header-info card-header-icon">
                  <div class="card-icon" style="box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 1);">
                      <img src="../../Images/Reissue.png" alt="pending" style="width:64px;"/>
                    
                  </div>
                  <p class="card-category">Reissue</p>
                  <h3 class="card-title"  style="border:none; background-color: #f0f8ff00; color:#6f6c6c;"><asp:Label ID="lbreissue" runat="server">0</asp:Label></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <%--<i class="material-icons">date_range</i>--%>
                     <a href="<%=ResolveUrl("~/DetailsPort/TicketReport.aspx") %>" style="color:#5f5e5e">View Detail</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
              </div>


     <div class="col-md-8" style="max-width: 79.66667%;">



         <div class="form-group" style="box-shadow:0 1px 4px 0 rgba(0, 0, 0, 0.14);margin-top: 1px;height:366px;width:1000px">
           <div class="container-fluid" style="float: left;height: 364px;width: 248px;background-color:#ffffff">
               
         <%-- <div>
          <asp:DropDownList Class="dropdown" ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" style="margin-top: 8px; background:#418cf0;color:#fff;">
        
       <asp:ListItem Value="0" disabled selected style="display: none;">Choose Chart Styles</asp:ListItem></asp:DropDownList>
               </div>--%>
               
               
               
               
               <div style="height: 266px">
                   &nbsp;

                   <div class="counter">
    <div class="container">
        <div class="row">
           

            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="customer">
                    <p class="counter-count" style="margin-top: -40px;margin-left: 44px;"><asp:Label runat="server" ID="totalcust">0</asp:Label></p>
                    <p class="customer-p" style="margin-left: 35px;">Brand&nbsp;Id</p>
                </div>
            </div>

          

            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="order">
                    <p class="counter-count" style="margin-top: 119px;    margin-left: -8px;"><asp:Label runat="server" ID="lbl_orderid">0</asp:Label></p>
                    <p class="order-p">Orders</p>
                </div>
            </div>
        </div>
    </div>
</div>


               </div>
               </div>
            <%--<div style="clear:both">&nbsp</div>--%>
             
             
              
    <div style="height:356px;float:left;width:750px">
            <asp:Chart ID="Chart1" runat="server" Width="855px" Height="365px">
                <ChartAreas>
                    <asp:ChartArea>
                        <AxisX IsLabelAutoFit="False" Title="Months Sales" Interval="1"></AxisX>
                       
                        <AxisY Title="Total Amount"></AxisY>
                       

                    </asp:ChartArea>
                </ChartAreas>
                

                <Series>
                    <asp:Series Name="Series1" >

                        
                       
                    </asp:Series>
                </Series>
                </asp:Chart>
          </div>

             

   <%--     <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
        </asp:DropDownList>--%>
     
                        
    
         </div>

        </div>

    </div>



                          

     
   
    <div style="background-color:#fff;width:283px;height:366px;float:left;box-shadow:0 1px 4px 0 rgba(0, 0, 0, 0.14);margin-left: -36px;">
        <br />
        <br />
        <div class="col-md-9" style="max-width: 100%;">
									<div class="weekly-summary text-right" style="font-size:small">
										<span class="number" style="font-size:-webkit-xxx-large;">2,315</span> <span class="percentage"><i class="fa fa-caret-up text-success"></i> 12%</span>
                                        <br />
                                        <br />
										<span class="info-label" style="font-size:small;">Total Booking</span>
									</div>
            <br />
            <br />
									<div class="weekly-summary text-right" style="font-size:small">
										<span class="number" style="font-size:-webkit-xxx-large;">₹5,758</span> <span class="percentage"><i class="fa fa-caret-up text-success"></i> 23%</span>
                                        <br />
                                        <br />
										<span class="info-label" style="font-size:small;">Monthly Income</span>
									</div>
            <br />
            <br />
									<div class="weekly-summary text-right" style="font-size:small">
										<span class="number" style="font-size:-webkit-xxx-large;">₹65,938</span> <span class="percentage"><i class="fa fa-caret-up text-success"></i> 8%</span>
                                        <br />
                                        <br />
										<span class="info-label" style="font-size:small;">Total Income</span>
									</div>
								</div>
        </div>
       
    


    <div style="clear:both">&nbsp</div>
        
  


</asp:Content>


