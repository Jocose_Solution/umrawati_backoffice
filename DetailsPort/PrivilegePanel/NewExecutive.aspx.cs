﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


public partial class NewExecutive : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       

        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        if (string.IsNullOrEmpty(Convert.ToString(Session["User_Type"])) || string.IsNullOrEmpty(Convert.ToString(Session["UID"])) || string.IsNullOrEmpty(Convert.ToString(Session["TypeID"])))
        {
            Response.Redirect("~/Login.aspx");
        }

        if (Convert.ToString(Session["User_Type"]).ToUpper() == "ADMIN" && Convert.ToString(Session["TypeID"]).ToUpper() == "AD1")
        {
            this.GridView1.Columns[2].Visible = true;
            this.GridView1.Columns[3].Visible = true;
            this.GridView1.Columns[4].Visible = true;
            //this.GridView1.Columns[9].Visible = true;
        }
        else
        {
            this.GridView1.Columns[2].Visible = false;
            //this.GridView1.Columns[3].Visible = false;
            this.GridView1.Columns[4].Visible = false;
            //this.GridView1.Columns[9].Visible = false;
        }
        ((Label)Page.Master.FindControl("lblBC")).Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Privilege Setting </a><a class='current' href='#'>New Executive</a>";
         //If (Session("TypeID").ToString() = "AD1") Then
         //       tr_BookingType.Visible = True
         //   End If
         //   If Session("User_Type") = "SALES" Then
         //       tr_UploadType.Visible = False
         //       tr_Cat.Visible = False
         //   End If
        if (!this.IsPostBack)
        {
            BindRole();
            BindGridview();
            Label1.InnerText = "";
        }
    }

    public void BindRole()
    {
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);

        SqlCommand cmd = new SqlCommand("BindRoleNewExecu_PP");

        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = con;
        con.Open();
        ddlRole_type.DataSource = cmd.ExecuteReader();
        ddlRole_type.DataTextField = "Role";
        ddlRole_type.DataValueField = "Role_id";
        ddlRole_type.DataBind();
  
        con.Close();
        ddlRole_type.Items.Insert(0, new ListItem("--Select Role--", "0"));
    }
    public string GetStatusVal(object val)
    {
        string value = "";
        bool result = Convert.ToBoolean(val);

        if (result == false)
        {
            value = "Inactive";
        }
        else
        {
            value = "Active";
        }
        return value;
    }
    protected void Submit_Click(object sender, EventArgs e)
    {

        Pro bo = new Pro();

        //int result = 0;
        string result = "";

        bo.user_id = txtemail.Text;
        bo.password = txtpassword.Text;
        bo.name = txtname.Text;
        bo.mobileno = txtmobile.Text;
        bo.email = txtemail.Text;
        bo.status = true;
        bo.Role_id = Convert.ToInt32(ddlRole_type.SelectedValue);
        //bo.Role_Type = ddlRole_type.SelectedItem.ToString();

        Dao d1 = new Dao();
        try
        {
            result = d1.insertdata(bo);
            if (result == "Insert")
            {
                BindGridview();
                Label1.InnerText = "Data submitted successfully.";
                txtemail.Text = "";
                txtpassword.Text = "";
                txtname.Text = "";
                txtmobile.Text = "";
            }
            else
            {
                BindGridview();
                Label1.InnerText = "Data Already Exist.";
                txtemail.Text = "";
                txtpassword.Text = "";
                txtname.Text = "";
                txtmobile.Text = "";
            }
        }

        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            bo = null;
            d1 = null;
        }

        ddlRole_type.SelectedIndex = 0;
    }


    protected void BindGridview()
    {
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);
        con.Open();

        SqlCommand cmd = new SqlCommand("SelectNewExecSp_PP");
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter sda = new SqlDataAdapter();

        cmd.Connection = con;
        sda.SelectCommand = cmd;
        DataTable dt = new DataTable();

        sda.Fill(dt);
        con.Close();
        GridView1.DataSource = dt;

        GridView1.DataBind();

    }

    protected void OnRowCancelingEdit(object sender, EventArgs e)
    {
        GridView1.EditIndex = -1;
        this.BindGridview();

    }

    protected void OnRowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        this.BindGridview();


        DropDownList ddlRoleName = GridView1.Rows[e.NewEditIndex].FindControl("ddlRole_Name") as DropDownList;
        //DropDownList ddlRoleType = GridView1.Rows[e.NewEditIndex].FindControl("ddlRole_Type") as DropDownList;

        Label lblRoleName = GridView1.Rows[e.NewEditIndex].FindControl("lblRoleNameHidden") as Label;
       // Label lblRoleType = GridView1.Rows[e.NewEditIndex].FindControl("lblRoleTypeHidden") as Label;

        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);



        SqlDataAdapter adp = new SqlDataAdapter("BindRoleNewExecu_PP", con);
        adp.SelectCommand.CommandType = CommandType.StoredProcedure;
        DataTable dt = new DataTable();
        adp.Fill(dt);

        ddlRoleName.DataSource = dt;
        ddlRoleName.DataTextField = "Role";
        ddlRoleName.DataValueField = "Role_id";
        ddlRoleName.DataBind();
        //ddlRoleName.Items.FindByValue((e.Row.FindControl("lblRoleNameHidden") as Label).Text).Selected = true;
        ddlRoleName.Items.FindByText(lblRoleName.Text).Selected = true;


        //ddlRoleType.DataSource = dt;
        //ddlRoleType.DataTextField = "Role_Type";
        //ddlRoleType.DataValueField = "Role_id";
        //ddlRoleType.DataBind();
        //ddlRoleType.Items.FindByText(lblRoleType.Text).Selected = true;

        //ddlRoleType.Items.FindByValue((e.Row.FindControl("lblRoleTypeHidden") as Label).Text).Selected = true;

    }

    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string user_id = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("DeleteSp1_PP"))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@user_id", user_id);
                cmd.Connection = con;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
        this.BindGridview();

    }


    protected void OnRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow row = GridView1.Rows[e.RowIndex];
        string user_id = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
        string status = (row.FindControl("ddlstatus") as DropDownList).SelectedItem.ToString();



        string roleId = (row.FindControl("ddlRole_Name") as DropDownList).SelectedValue.ToString();

        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("NewExecuUpdateSp_PP"))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@user_id", user_id);
                cmd.Parameters.AddWithValue("@RoleID", roleId);
                cmd.Parameters.AddWithValue("@status", status.ToUpper() == "ACTIVE" ? 1 : 0);


                cmd.Connection = con;
                con.Open();
                int i = Convert.ToInt32(cmd.ExecuteNonQuery());
                con.Close();
                if(i > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Record updated successfully.');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Record not updated.');", true);
                }
                
            }
        }
        GridView1.EditIndex = -1;
        this.BindGridview();

    }

    protected void Cancel_Click(object sender, EventArgs e)
    {
        txtemail.Text = "";
        txtpassword.Text = "";
        txtname.Text = "";
        txtmobile.Text = "";
        ddlRole_type.SelectedIndex = 0;
    }
    public DataTable CheckUserDetails()
    {
        DataTable dt = new DataTable();
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB1"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);
        try
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("CheckUserDetails");
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter();
            cmd.Connection = con;
            sda.SelectCommand = cmd;
            sda.Fill(dt);
            con.Close();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            con.Close();
        }

        return dt;
    }

    
}