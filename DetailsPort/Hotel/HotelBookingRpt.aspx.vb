﻿Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data
Imports HotelShared
Imports System.Configuration.ConfigurationManager

Partial Class DetailsPort_Hotel_HtlBookingRpt

    Inherits System.Web.UI.Page
    Private STDom As New SqlTransactionDom()
    Private ST As New HotelDAL.HotelDA()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            If Session("UID") = "" Or Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx", False)
            End If

            If Not IsPostBack Then
                If Session("User_Type") = "AGENT" Then
                    TDAgency.Visible = False
                End If
                Dim GrdDS As New DataSet()
                GrdDS = ST.HotelSearchRpt("", "", "", "", "", "", "", "", "", "Confirm", Session("UID").ToString, Session("User_Type").ToString)
                BindSales(GrdDS)
            End If
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub

    Protected Sub btn_result_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_result.Click
        Try
            Dim GrdDS As New DataSet()
            GrdDS = GetReportData()
            Bindgrid(GrdDS)
            BindSales(GrdDS)
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub
    Private Sub BindSales(ByVal GrdDS As DataSet)
        Try
            Dim dt As DataTable
            Dim Db As String = ""
            Dim sum As Double = 0
            dt = GrdDS.Tables(0)
            If dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    Db = dr("TotalCost").ToString()
                    If Db Is Nothing OrElse Db = "" Then
                        Db = 0
                    Else
                        sum += Db
                    End If
                Next
            End If
            lbl_Total.Text = "0"
            If sum <> 0 Then
                lbl_Total.Text = sum.ToString
            End If
            lbl_counttkt.Text = dt.Rows.Count
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub
    Private Sub Bindgrid(ByVal ds As DataSet)
        Try
            GrdReport.DataSource = ds
            GrdReport.DataBind()
            If Session("User_Type").ToString = "AGENT" Then
                GrdReport.Columns(10).Visible = True
            End If
            Session("Grdds") = ds
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub

    Protected Function GetReportData() As DataSet
        Dim GrdDS As New DataSet()
        Dim FromDate As String = "", ToDate As String = "", Trip As String = "", Checkin As String = "", status As String = "Confirm"
        Try
            If Not [String].IsNullOrEmpty(Request("From")) Then
                FromDate = Strings.Mid(Request("From").Split(" ")(0), 4, 2) + "/" + Strings.Left(Request("From").Split(" ")(0), 2) + "/" + Strings.Right(Request("From").Split(" ")(0), 4)
                FromDate = FromDate + " " + "12:00:00 AM"
            End If
            If Not [String].IsNullOrEmpty(Request("To")) Then
                ToDate = Mid((Request("To")).Split(" ")(0), 4, 2) & "/" & Left((Request("To")).Split(" ")(0), 2) & "/" & Right((Request("To")).Split(" ")(0), 4)
                ToDate = ToDate & " " & "11:59:59 PM"
            End If
            If Not [String].IsNullOrEmpty(Request("Checkin")) Then
                Dim chkdate() As String = Request("Checkin").Split("-")
                Checkin = chkdate(2) + "-" + chkdate(1) + "-" + chkdate(0)
            End If
            Dim AgentID As String = If([String].IsNullOrEmpty(Request("hidtxtAgencyName")) Or Request("hidtxtAgencyName") = "Agency Name or ID", "", Request("hidtxtAgencyName"))
            Dim BookingrID As String = If([String].IsNullOrEmpty(txt_bookingID.Text), "", txt_bookingID.Text.Trim)
            Dim orderID As String = If([String].IsNullOrEmpty(txt_OrderId.Text), "", txt_OrderId.Text.Trim)
            Dim HtlName As String = If([String].IsNullOrEmpty(txt_htlcode.Text), "", txt_htlcode.Text.Trim)
            Dim RoomName As String = If([String].IsNullOrEmpty(txt_roomcode.Text), "", txt_roomcode.Text.Trim)
            If Triptype.SelectedIndex > 0 Then
                Trip = Triptype.SelectedValue
            End If
            If ddlStatus.SelectedIndex > -1 Then
                status = ddlStatus.SelectedValue
            End If
            GrdDS = ST.HotelSearchRpt(FromDate, ToDate, BookingrID, orderID, HtlName, RoomName, Trip, AgentID, Checkin, status, Session("UID").ToString(), Session("User_Type").ToString())
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
        Return GrdDS
    End Function

    Protected Sub lnkRefund_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lb As LinkButton = CType(sender, LinkButton)
            'Color Selected Grid View row
            Dim gvr As GridViewRow = TryCast(lb.Parent.Parent, GridViewRow)
            gvr.BackColor = System.Drawing.ColorTranslator.FromHtml("#F0F8FF")
            Dim HtlDetailsDs As New DataSet()
            Dim HtlDetails As New DataTable()
            HtlDetailsDs = ST.htlintsummary(lb.CommandArgument, "")
            If (HtlDetailsDs.Tables.Count > 0) Then
                HtlDetails = HtlDetailsDs.Tables(0)
                ViewState("HtlDetails") = HtlDetailsDs
                If HtlDetails.Rows.Count > 0 Then
                    'Checking in HtlRefund Table it present in table or not
                    Dim status As Integer = ST.CheckHtlRefuStaus(HtlDetails.Rows(0)("BookingID"), HtlDetails.Rows(0)("OrderID").ToString())
                    Select Case status
                        Case 1
                            ShowAlertMessage("This Booking ID Allready Cancelled")
                        Case 2
                            ShowAlertMessage("This Booking ID is Pending for Cancellation")
                        Case 3
                            ShowAlertMessage("This Booking ID is Allready in Cancellation InProcess")
                        Case Else
                            If HtlDetails.Rows(0)("ModifyStatus").ToString() = "PartialCancel" Then
                                amt.InnerText = HtlDetails.Rows(0)("NetCost") - HtlDetails.Rows(0)("RefundAmt")
                            Else
                                amt.InnerText = HtlDetails.Rows(0)("NetCost")
                            End If
                            If HtlDetails.Rows(0)("ModifyStatus").ToString() = "Cancelled" Then
                                ShowAlertMessage("Ticket all ready canceled.")
                            Else
                                HotelName.InnerText = HtlDetails.Rows(0)("HotelName")
                                room.InnerText = HtlDetails.Rows(0)("RoomCount")
                                night.InnerText = HtlDetails.Rows(0)("NightCount")
                                adt.InnerText = HtlDetails.Rows(0)("AdultCount")
                                chd.InnerText = HtlDetails.Rows(0)("ChildCount")
                                policy.InnerHtml = "<span style='font-size:13px;font-weight: bold;'>CANCELLATION POLICIES</span>" & HtlDetails.Rows(0)("CancellationPoli")
                                RemarkTitle.InnerText = "Hotel Cancellation for order id (" & lb.CommandArgument & ")"
                                OrderIDS.Value = lb.CommandArgument
                                Page.ClientScript.RegisterStartupScript(GetType(Page), "MessagePopUp", "ShowHide('show','" & HtlDetails.Rows(0)("CheckIN").ToString() & "'); ", True)
                            End If
                    End Select
                End If
            Else
                ShowAlertMessage("We can not process Cancellation.")
            End If
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub
    'Protected Sub btn_Refund_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Refund.Click
    '    Try
    '        Dim HtlDetailsDs As New DataSet()
    '        HtlDetailsDs = ViewState("HtlDetails")
    '        Dim HotelDT As New DataTable()
    '        HotelDT = HtlDetailsDs.Tables(0)

    '        If HotelDT.Rows.Count > 0 Then
    '            'Checking in Hotel Cancellation Table it is present in table or not
    '            Dim status As Integer = ST.CheckHtlRefuStaus(HotelDT.Rows(0)("BookingID").ToString(), OrderIDS.Value)
    '            Select Case status
    '                Case 0
    '                    Try
    '                        Dim AdminMrk As Double = 0, fromcCancelDate As String = "", PreviousRfndAmt As Decimal = 0
    '                        Dim SearchDetails As New HotelCancellation
    '                        SearchDetails.BookingID = HotelDT.Rows(0)("BookingID").ToString()
    '                        SearchDetails.Orderid = OrderIDS.Value
    '                        SearchDetails.CheckInDate = HotelDT.Rows(0)("CheckIN").ToString()
    '                        SearchDetails.PGLastName = HtlDetailsDs.Tables(1).Rows(0)("GLName").ToString()
    '                        SearchDetails.PGEmail = HtlDetailsDs.Tables(1).Rows(0)("GEmail").ToString()
    '                        SearchDetails.AgentID = HotelDT.Rows(0)("Loginid").ToString()
    '                        SearchDetails.AgencyName = HotelDT.Rows(0)("Loginid").ToString()
    '                        SearchDetails.CancelStatus = StatusClass.Pending.ToString()
    '                        SearchDetails.NoofNight = HotelDT.Rows(0)("NightCount").ToString()
    '                        SearchDetails.Provider = HotelDT.Rows(0)("Provider").ToString()
    '                        SearchDetails.AdminMrkAmt = Convert.ToDecimal(HotelDT.Rows(0)("AdminMrkAmt"))
    '                        SearchDetails.AdminMrkPer = Convert.ToDecimal(HotelDT.Rows(0)("AdminMrkPer"))
    '                        SearchDetails.AdminMrkType = HotelDT.Rows(0)("AdminMrkType").ToString()
    '                        SearchDetails.ConfirmationNo = HotelDT.Rows(0)("ConfirmationNo").ToString()
    '                        SearchDetails.HotelCode = HotelDT.Rows(0)("HotelCode").ToString()
    '                        SearchDetails.HotelName = HotelDT.Rows(0)("HotelName").ToString()
    '                        SearchDetails.HtlType = HotelDT.Rows(0)("TripType").ToString()
    '                        SearchDetails.CancellationType = "ALL"
    '                        If SearchDetails.Provider = "TG" Then
    '                            SearchDetails.Provider = "TGBooking"
    '                        ElseIf SearchDetails.Provider = "GTA" Then
    '                            SearchDetails.CurrancyRate = Convert.ToDecimal(HotelDT.Rows(0)("ExchangeRate"))
    '                        End If

    '                        Dim orgbookingrate As Decimal = Math.Round(Convert.ToDecimal(HotelDT.Rows(0)("BookingAmt")), 2)

    '                        'Insert Cancle Data in HotelCancellation Table
    '                        Dim i As Integer = ST.InsHtlRefund(SearchDetails.Orderid, Session("UID").ToString(), StatusClass.Pending.ToString(), 0, 0, Convert.ToInt32(HotelDT.Rows(0)("NightCount")), "", Request("txtRemarkss").Trim(), HtlDetailsDs.Tables(1).Rows(0)("GTitle").ToString(), HtlDetailsDs.Tables(1).Rows(0)("GFName").ToString(), SearchDetails.PGLastName, SearchDetails.PGEmail)
    '                        'string RequestBy, string Status, decimal CancelCharge, int ServiceCharge, int CancelNight, string FromCancelDate,  string remark, DataTable RfndDT
    '                        If i = 3 Then
    '                            If Request("Parcial") = "false" Then
    '                                Dim objcancel As New HotelBAL.HotelAvailabilitySearch()
    '                                SearchDetails = objcancel.CancellationHotelBooking(SearchDetails)
    '                                If (SearchDetails.CancelStatus = "Cancelled") Then
    '                                    Dim cancelamt As Decimal = 0
    '                                    If SearchDetails.Provider = "TGBooking" Or SearchDetails.Provider = "RZ" Then
    '                                        If SearchDetails.CancellationCharge > 0 Then
    '                                            If orgbookingrate = Math.Round(SearchDetails.CancellationCharge, 2) Or (orgbookingrate = Math.Round(SearchDetails.CancellationCharge, 2) + 1) Or (orgbookingrate = Math.Round(SearchDetails.CancellationCharge, 2) + 2) Or (orgbookingrate = Math.Round(SearchDetails.CancellationCharge, 2) + 3) Or (orgbookingrate = Math.Round(SearchDetails.CancellationCharge, 2) + 4) Or (orgbookingrate = Math.Round(SearchDetails.CancellationCharge, 2) + 5) Or (orgbookingrate = Math.Round(SearchDetails.CancellationCharge, 2) + 6) Or (orgbookingrate = Math.Round(SearchDetails.CancellationCharge, 2) + 7) Or (orgbookingrate = Math.Round(SearchDetails.CancellationCharge, 2) + 8) Or (orgbookingrate = Math.Round(SearchDetails.CancellationCharge, 2) + 9) Or (orgbookingrate = Math.Round(SearchDetails.CancellationCharge, 2) + 10) Then
    '                                                SearchDetails.RefundAmt = Convert.ToDecimal(HotelDT.Rows(0)("NetCost"))
    '                                            Else
    '                                                If (orgbookingrate + 1 = Math.Round(SearchDetails.CancellationCharge, 2)) Or (orgbookingrate + 2 = Math.Round(SearchDetails.CancellationCharge, 2)) Or (orgbookingrate + 3 = Math.Round(SearchDetails.CancellationCharge, 2)) Or (orgbookingrate + 4 = Math.Round(SearchDetails.CancellationCharge, 2)) Or (orgbookingrate + 5 = Math.Round(SearchDetails.CancellationCharge, 2)) Or (orgbookingrate + 6 = Math.Round(SearchDetails.CancellationCharge, 2)) Or (orgbookingrate + 7 = Math.Round(SearchDetails.CancellationCharge, 2)) Or (orgbookingrate + 8 = Math.Round(SearchDetails.CancellationCharge, 2)) Or (orgbookingrate + 9 = Math.Round(SearchDetails.CancellationCharge, 2)) Or (orgbookingrate + 10 = Math.Round(SearchDetails.CancellationCharge, 2)) Then
    '                                                    SearchDetails.RefundAmt = Convert.ToDecimal(HotelDT.Rows(0)("NetCost"))
    '                                                Else
    '                                                    If SearchDetails.AdminMrkType = "Percentage" Then
    '                                                        Dim mrk As New HotelBAL.HotelMarkups()
    '                                                        Dim withAdminMrkDesiyaRefundamt As Decimal = mrk.DiscountMarkupCalculation(SearchDetails.AdminMrkPer, 0.0, SearchDetails.AdminMrkType, "Percentage", SearchDetails.CancellationCharge, SearchDetails.servicetax)
    '                                                        ' cancelamt = mrkcancelamt - (mrkcancelamt * (Convert.ToDecimal(HotelDT.Rows(0)("CommisionPer")) / 100))
    '                                                        SearchDetails.RefundAmt = withAdminMrkDesiyaRefundamt  'Convert.ToDecimal(HotelDT.Rows(0)("NetCost")) - mrkcancelamt
    '                                                    Else
    '                                                        SearchDetails.RefundAmt = SearchDetails.CancellationCharge
    '                                                    End If
    '                                                    ' SearchDetails.RefundAmt = orgbookingrate - SearchDetails.CancellationCharge orgbookingrate
    '                                                End If
    '                                            End If
    '                                        Else
    '                                            SearchDetails.RefundAmt = 0
    '                                            cancelamt = Convert.ToDecimal(HotelDT.Rows(0)("NetCost"))
    '                                        End If
    '                                        Else
    '                                            If SearchDetails.CancellationCharge > 0 Then
    '                                                If orgbookingrate > Math.Round(SearchDetails.CancellationCharge, 2) Then
    '                                                    Dim mrk As New HotelBAL.HotelMarkups()
    '                                                    cancelamt = mrk.DiscountMarkupCalculation(SearchDetails.AdminMrkPer, 0.0, SearchDetails.AdminMrkType, "Fixed", SearchDetails.CancellationCharge, 1.24)
    '                                                    cancelamt = cancelamt - (cancelamt * (Convert.ToDecimal(HotelDT.Rows(0)("CommisionPer")) / 100))
    '                                                    'cancelamt = SearchDetails.CancellationCharge
    '                                                    SearchDetails.RefundAmt = Convert.ToDecimal(HotelDT.Rows(0)("NetCost")) - cancelamt
    '                                                End If
    '                                            Else
    '                                                SearchDetails.RefundAmt = Convert.ToDecimal(HotelDT.Rows(0)("NetCost"))
    '                                            End If
    '                                        End If

    '                                    'update in HotelCancellation Table and header
    '                                    Dim j As Integer = ST.SP_HTL_AutoRefund(SearchDetails, cancelamt)
    '                                    Dim ablBalance As Double = RefundAmountAndLadgerEntry(SearchDetails, SearchDetails.RefundAmt)

    '                                    ShowAlertMessage("Hotel Cancelled successfully. Rs " + SearchDetails.RefundAmt.ToString() + " Refunded your Acount.")
    '                                Else
    '                                    ShowAlertMessage("Hotel Cancelled is under process.")
    '                                End If
    '                            Else
    '                                ShowAlertMessage("Hotel Cancelled is under process. for more details, Please contact to customer care.")
    '                            End If
    '                        Else
    '                            ShowAlertMessage("Cancellation request can not be process for this booking. Please contact to customer care.")
    '                        End If
    '                    Catch ex As Exception
    '                        HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
    '                    End Try
    '                Case 1
    '                    ShowAlertMessage("This booking id allready refunded")
    '            End Select
    '        End If
    '    Catch ex As Exception
    '        HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
    '    End Try
    'End Sub
    'Protected Sub btnRemark_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemark.Click
    '    Try
    '        GrdDS = Session("Grdds")
    '        Dim filterArray As Array = GrdDS.Tables(0).Select("BookingID ='" & Request("Bookingid") & "'")
    '        If filterArray.Length > 0 Then
    '            'Checking in HtlRefund Table it present in table or not
    '            Dim status As Integer = ST.CheckHtlRefuStaus(Request("Bookingid"), filterArray(0)("OrderID").ToString())
    '            Select Case status
    '                Case 1
    '                    ShowAlertMessage("This Booking ID Allready Cancelled")
    '                Case 2
    '                    ShowAlertMessage("This Booking ID is Pending for Cancellation")
    '                Case 3
    '                    ShowAlertMessage("This Booking ID is Allready in Cancellation InProcess")
    '                Case Else
    '                    Dim RefundID As String = HtlLogObj.GetID("RFND")
    '                    'Insert data in CancellationIntl Table
    '                    Dim i As Integer = ST.InsHtlRefund(RefundID, Request("Bookingid"), "Pending", Session("UID").ToString(), Request("txtRemark"), filterArray)
    '                    If i > 0 Then
    '                        ShowAlertMessage("Refund Remark Submitted Succesfully")
    '                    Else
    '                        ShowAlertMessage("Try Later")
    '                    End If
    '            End Select
    '        Else
    '            ShowAlertMessage("Try Later")
    '        End If
    '    Catch ex As Exception
    '       HotelDAL.HotelDA.InsertHotelErrorLog(ex)
    '    End Try
    'End Sub
    Protected Sub btn_export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_export.Click
        Try
            Dim GrdDS As New DataSet()
            GrdDS = GetReportData()
            GrdDS.Tables(0).Columns.Remove(GrdDS.Tables(0).Columns("ModifyStatus"))
            GrdDS.Tables(0).Columns.Remove(GrdDS.Tables(0).Columns("Markup"))
            GrdDS.Tables(0).Columns.Remove(GrdDS.Tables(0).Columns("ConfirmationNo"))
            If Session("User_Type").ToString = "AGENT" Or Session("User_Type").ToString = "EXEC" Or Session("User_Type").ToString = "SALES" Then
                GrdDS.Tables(0).Columns.Remove(GrdDS.Tables(0).Columns("PurchaseCost"))
            End If
            STDom.ExportData(GrdDS)
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try

    End Sub
    Public Shared Sub CallScriptFunction(ByVal funName As String, ByVal Parameter1 As String, ByVal Parameter2 As String)
        Try
            Dim page As Page = TryCast(HttpContext.Current.Handler, Page)
            If page IsNot Nothing Then
                ScriptManager.RegisterStartupScript(page, page.[GetType](), "err_msg", funName & "('" & Parameter1 & "', '" & Parameter2 & "');", True)
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Public Shared Sub ShowAlertMessage(ByVal [error] As String)
        Try
            Dim page As Page = TryCast(HttpContext.Current.Handler, Page)
            If page IsNot Nothing Then
                [error] = [error].Replace("'", "'")
                ScriptManager.RegisterStartupScript(page, page.[GetType](), "err_msg", "alert('" & [error] & "');", True)
            End If
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub
    Protected Sub GrdReport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdReport.PageIndexChanging
        Try
            GrdReport.PageIndex = e.NewPageIndex
            Bindgrid(Session("Grdds"))
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub

    'Protected Function RefundAmountAndLadgerEntry(ByVal Canceldata As HotelCancellation, ByVal RefundAmount As Decimal) As Double
    '    Dim ablBalance As Double = 0
    '    Try
    '        Dim SqlT As New SqlTransaction()
    '        ablBalance = SqlT.UpdateNew_RegsRefund(Session("UID").ToString(), Convert.ToDouble(Canceldata.RefundAmt))

    '        Dim Result As Integer = STDom.LedgerEntry_Common(OrderIDS.Value, 0, Canceldata.RefundAmt, ablBalance, Canceldata.Provider, Canceldata.HotelName, Canceldata.BookingID, _
    '                                                  "HTLRFND", Canceldata.AgentID, Canceldata.AgencyName, Session("UID").ToString(), Request.UserHostAddress.ToString(), _
    '                                                  "", "", "", "Hotel Refund agence " + Canceldata.Orderid, Canceldata.HtlType.Substring(0, 1), 0)
    '    Catch ex As Exception
    '        HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
    '    End Try
    '    Return ablBalance
    'End Function
    'Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        GrdDS = Session("Grdds")
    '        Dim filterArray As Array = GrdDS.Tables(0).Select("BookingID ='" & ViewState("BookingID").ToString & "'")

    '        If filterArray.Length <> 0 Then
    '            'Checking in HtlRefund Table it present in table or not
    '            Dim status As Integer = ST.CheckHtlRefuStaus(ViewState("BookingID"), filterArray(0)("OrderID").ToString())
    '            Select Case status
    '                Case 1
    '                    ShowAlertMessage("This Booking ID Allready Cancelled")
    '                Case 2
    '                    ShowAlertMessage("This Booking ID is Pending for Cancellation")
    '                Case 3
    '                    ShowAlertMessage("This Booking ID is Allready in Cancellation InProcess")
    '                Case Else
    '                    Dim RefundID As String = HtlLogObj.GetID("RFND")
    '                    Dim txtremark As TextBox = DirectCast(GrdReport.Rows(Convert.ToInt32(ViewState("RowIndex"))).FindControl("txtRemark"), TextBox)
    '                    'Insert data in CancellationIntl Table
    '                    ST.InsHtlRefund(RefundID, filterArray(0)("OrderId").ToString, ViewState("BookingID").ToString, filterArray(0)("ConfirmationNo").ToString, _
    '                                    "Pending", filterArray(0)("CheckIN").ToString, filterArray(0)("CheckOut").ToString, filterArray(0)("BookingDate").ToString, _
    '                                    filterArray(0)("StarRating").ToString, filterArray(0)("Inventory").ToString, filterArray(0)("TotalCost").ToString, _
    '                                    filterArray(0)("NetCost").ToString, filterArray(0)("TotalMrk").ToString, filterArray(0)("HtlType").ToString, filterArray(0)("Currency").ToString, _
    '                                    filterArray(0)("HtlName").ToString, filterArray(0)("RoomName").ToString, filterArray(0)("Country").ToString(), _
    '                                    filterArray(0)("City").ToString(), filterArray(0)("PgFname").ToString(), filterArray(0)("PgLname").ToString(), Convert.ToInt32(filterArray(0)("RoomCount")), _
    '                                    Convert.ToInt32(filterArray(0)("NightCount")), Session("UID").ToString(), filterArray(0)("AgencyName").ToString(), txtremark.Text.Trim, _
    '                                   filterArray(0)("CancilationPoli").ToString())

    '                    ShowAlertMessage("Refund Remark Submitted Succesfully")
    '            End Select
    '        End If
    '    Catch ex As Exception
    '       HotelDAL.HotelDA.InsertHotelErrorLog(ex)
    '    End Try
    'End Sub

    'Protected Sub lnkHides_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        GrdReport.Columns(15).Visible = False
    '        GrdReport.Columns(16).Visible = False
    '    Catch ex As Exception
    '       HotelDAL.HotelDA.InsertHotelErrorLog(ex)
    '    End Try
    'End Sub

    'Protected Sub RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GrdReport.RowCommand
    '    Try
    '        If e.CommandName = "Refund" Then
    '            ViewState("BookingID") = e.CommandArgument
    '            GrdReport.Columns(15).Visible = True
    '            GrdReport.Columns(16).Visible = True
    '            Dim lb As LinkButton = TryCast(e.CommandSource, LinkButton)
    '            Dim gvr As GridViewRow = TryCast(lb.Parent.Parent, GridViewRow)
    '            Dim RowIndex As Integer = gvr.RowIndex
    '            ViewState("RowIndex") = RowIndex
    '            Dim lnkSubmit As LinkButton = DirectCast(GrdReport.Rows(RowIndex).FindControl("lnkSubmit"), LinkButton)
    '            Dim txtRemark As TextBox = DirectCast(GrdReport.Rows(RowIndex).FindControl("txtRemark"), TextBox)
    '            Dim lnkHides As LinkButton = DirectCast(GrdReport.Rows(RowIndex).FindControl("lnkHides"), LinkButton)
    '            txtRemark.Visible = True
    '            lnkHides.Visible = True
    '            lnkSubmit.Visible = True
    '            txtRemark.Focus()
    '            'gvr.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFF275")
    '            gvr.BackColor = System.Drawing.ColorTranslator.FromHtml("#F0F8FF")
    '        Else
    '            GrdReport.Columns(15).Visible = False
    '            GrdReport.Columns(16).Visible = False
    '            Dim lb As LinkButton = TryCast(e.CommandSource, LinkButton)
    '            Dim gvr As GridViewRow = TryCast(lb.Parent.Parent, GridViewRow)
    '            Dim RowIndex As Integer = gvr.RowIndex
    '            Dim lnkSubmit As LinkButton = DirectCast(GrdReport.Rows(RowIndex).FindControl("lnkSubmit"), LinkButton)
    '            Dim txtRemark As TextBox = DirectCast(GrdReport.Rows(RowIndex).FindControl("txtRemark"), TextBox)
    '            Dim lnkHides As LinkButton = DirectCast(GrdReport.Rows(RowIndex).FindControl("lnkHides"), LinkButton)
    '            txtRemark.Visible = False
    '            lnkHides.Visible = False
    '            lnkSubmit.Visible = False
    '            gvr.BackColor = Nothing
    '        End If
    '    Catch ex As Exception
    '       HotelDAL.HotelDA.InsertHotelErrorLog(ex)
    '    End Try
    'End Sub


End Class
