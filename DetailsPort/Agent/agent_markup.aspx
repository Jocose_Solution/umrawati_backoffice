﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false"
    CodeFile="agent_markup.aspx.vb" Inherits="DetailsPort_Agent_agent_markup" %>

<%@ Register Src="~/UserControl/Settings.ascx" TagPrefix="uc1" TagName="Settings" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <%-- <link href="../../css/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />--%>

    <script type="text/javascript" src="../../js/chrome.js"></script>

    <script type="text/javascript">
        function phone_vali() {
            if ((event.keyCode > 47 && event.keyCode < 58) || (event.keyCode == 32) || (event.keyCode == 45))
                event.returnValue = true;
            else
                event.returnValue = false;
        }
    </script>
   
    <div class="mtop80"></div>
    <div class="large-3 medium-3 small-12 columns">

        <uc1:Settings runat="server" ID="Settings" />

    </div>

    <div class="large-9 medium-9 small-12 columns">

        <div class="large-12 medium-12 small-12">

            <div class="large-12 medium-12 small-12 bld blue">
                Agent Markup
            </div>
            <div class="clear1"></div>
            <div class="large-12 medium-12 small-12">
                <div class="large-3 medium-3 small-3  columns">
                    Agent ID:
                </div>
                <div class="large-3 medium-3 small-9 columns">
                    <asp:TextBox runat="server" ID="uid" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="large-3 medium-3 small-3 large-push-1 medium-push-1 columns">
                    Airline Code:
                </div>
                <div class="large-3 medium-3 small-9  columns">
                    <asp:DropDownList runat="server" ID="air" CssClass="w100">
                        <asp:ListItem Value="AI" Selected="true">Air India</asp:ListItem>
                        <asp:ListItem Value="G8S">GoAir Special</asp:ListItem>
                        <asp:ListItem Value="AIS">Air India Special</asp:ListItem>
                        <asp:ListItem Value="SGS">Spice Jet Special</asp:ListItem>
                        <asp:ListItem Value="9W">Jet Airways</asp:ListItem>
                        <asp:ListItem Value="SG">Spice Jet</asp:ListItem>
                        <asp:ListItem Value="9WS">Jet Airways Special</asp:ListItem>
                         <asp:ListItem Value="6E">Indigo</asp:ListItem>
                        <asp:ListItem Value="G8">GoAir</asp:ListItem>
                        <asp:ListItem Value="6ES">Indigo Special</asp:ListItem>
                    </asp:DropDownList>
                </div>

             <div class="large-3 medium-3 small-3 columns">
                    MarkUp Per Pax:
                </div>
                <div class="large-3 medium-3 small-9 columns">
                    <asp:TextBox runat="server" ID="mk"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RFVMK" runat="server" ControlToValidate="mk" ErrorMessage="*"
                        Display="dynamic"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                </div>
                <div class="large-3 medium-3 small-3 large-push-1 medium-push-1 columns">
                    MarkUpType:
                </div>
                <div class="large-3 medium-3 small-9 columns">
                    <%--<asp:TextBox runat="server" ID="mktyp" Text="Fixed" ReadOnly="true"></asp:TextBox>--%>
                    <asp:DropDownList ID="ddl_MarkupType" runat="server">
                        <asp:ListItem Value="F" Selected="true">Fixed</asp:ListItem>
                        <asp:ListItem Value="P" ForeColor="#999999">Percentage</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="clear1"></div>


            <div class="row">
                <div class="large-12 medium-12 small-12">
                    <asp:UpdatePanel ID="UP" runat="server">
                        <ContentTemplate>

                            <div class="large-12 medium-12 small-12">

                                <div class="large-4 medium-4 small-6 medium-push-8 large-push-8 small-push-6 columns">
                                    <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="New Entry" />
                                </div>
                                <div class="large-4 medium-4 small-6 medium-push-4 large-push-4 small-push-3 columns">
                                    <asp:Label ID="lbl" runat="server" Style="color: #CC0000;"></asp:Label>
                                </div>
                                <div class="clear1"></div>
                                <div class="large-12 medium-12 small-12">
                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="counter"
                                        OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting"
                                        OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" PageSize="8"
                                        CssClass="GridViewStyle" Width="100%">
                                        <Columns>
                                            <asp:CommandField ShowEditButton="True" />
                                            <asp:BoundField DataField="counter" HeaderText="Sr.No" ReadOnly="True" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger" />
                                            <asp:BoundField DataField="user_id" HeaderText="Agent ID" ControlStyle-CssClass="textboxflight1"
                                                ReadOnly="true" />
                                            <asp:BoundField DataField="Airline" HeaderText="Airline" ControlStyle-CssClass="textboxflight1"
                                                ReadOnly="true" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger" />
                                            <asp:BoundField DataField="Markup" HeaderText="Mark Up" ControlStyle-CssClass="textboxflight1" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger" />
                                            <asp:BoundField DataField="Markup_type" HeaderText="MarkUpType" ControlStyle-CssClass="textboxflight1"
                                                ReadOnly="true" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger" />
                                            <asp:CommandField ShowDeleteButton="True" />
                                        </Columns>
                                        <RowStyle CssClass="RowStyle" />
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <PagerStyle CssClass="PagerStyle" />
                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                        <HeaderStyle CssClass="HeaderStyle" />
                                        <EditRowStyle CssClass="EditRowStyle" />
                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                    </asp:GridView>
                                </div>

                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UP">
                        <ProgressTemplate>
                            <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                            </div>
                            <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                Please Wait....<br />
                                <br />
                                <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                <br />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
            </div>
        </div>
    </div>

</asp:Content>


