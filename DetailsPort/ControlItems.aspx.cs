﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Net.Mail;
using System.Globalization;
public partial class ControlItems : System.Web.UI.Page
{
    double Width, Height, DisplayTime;
    string Name = "", STRResult = "";
    //ControlItems ObjCI = new ControlItems();
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        ((Label)Page.Master.FindControl("lblBC")).Text = "<a title='' class='tip-bottom' href='PrivilegePanel/splashboard.aspx' data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Control Setting</a><a class='current' href='#'>Control Items</a>";
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        try
        {
            //ID = Convert.ToInt32(txt_id.Text.Trim().ToString());
            Width = Convert.ToDouble(txt_width.Text.Trim().ToString());
            Height = Convert.ToDouble(txt_height.Text.Trim().ToString());
            DisplayTime = Convert.ToDouble(txt_displaytime.Text.Trim().ToString());
            Name = txt_name.Text.Trim().ToString();
            STRResult = InsertControlDetails(Name, Width, Height, DisplayTime, Session["UID"].ToString(), "InsertControl");
            ClearInputs(Page.Controls);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + STRResult + "');", true);
        }
        catch (Exception)
        {
        }
    }
    void ClearInputs(ControlCollection ctrls)
    {
        try
        {
            foreach (Control ctrl in ctrls)
            {
                if (ctrl is TextBox)
                    ((TextBox)ctrl).Text = string.Empty;
                ClearInputs(ctrl.Controls);
            }
        }
        catch (Exception ex)
        {
        }
    }
    public string InsertControlDetails(string ControlName, double Width, double Height, double DisplayTime, string CreatedBy, string Cmd_Type)
    {
        string ResultVal = "";
        try
        {
            SqlCommand cmd = new SqlCommand("USP_INSERTCONTROL", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ControlName", ControlName);
            cmd.Parameters.AddWithValue("@Width", Width);
            cmd.Parameters.AddWithValue("@Height", Height);
            cmd.Parameters.AddWithValue("@DisplayTime", DisplayTime);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@Cmd_Type", Cmd_Type);
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            else
            {
                con.Open();
            }
            ResultVal = Convert.ToString(cmd.ExecuteScalar());
            con.Close();
        }
        catch (Exception ex)
        {

        }
        return ResultVal;
    }
}
