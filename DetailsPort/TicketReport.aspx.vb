﻿
Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Web.Services

Partial Class DetailsPort_TicketReport

    Inherits System.Web.UI.Page
    Private STDom As New SqlTransactionDom()
    Private ST As New SqlTransaction()
    Private CllInsSelectFlt As New clsInsertSelectedFlight()
    Dim AgencyDDLDS, grdds, fltds As New DataSet()
    Private sttusobj As New Status()
    Dim con As New SqlConnection()
    Dim PaxType As String
    Dim clsCorp As New ClsCorporate()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim AgentID As String = ""
        divPaymentMode.Visible = False
        CType(Page.Master.FindControl("lblBC"), Label).Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Flight </a><a class='current' href='#'>Flight Ticket Report</a>"
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Try
            If Session("UID") = "" Or Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx")
            End If


            If Not IsPostBack Then
                BindPartner()
                If Session("User_Type") = "AGENT" Then
                    td_Agency.Visible = False
                    AgentID = Session("UID").ToString()
                    Dim dscancelled As DataSet = clsCorp.Get_Corp_BookedBy(Session("UID").ToString(), "CB")
                    If dscancelled.Tables(0).Rows.Count > 0 Then
                        DrpCancelledBy.AppendDataBoundItems = True
                        DrpCancelledBy.Items.Clear()
                        DrpCancelledBy.Items.Insert(0, "Select")
                        DrpCancelledBy.DataSource = dscancelled
                        DrpCancelledBy.DataTextField = "BOOKEDBY"
                        DrpCancelledBy.DataValueField = "BOOKEDBY"
                        DrpCancelledBy.DataBind()
                    End If
                End If
                If Session("User_Type") = "EXEC" Then
                    tdTripNonExec2.Visible = True
                End If
                'Dim curr_date = Now.Date() & " " & "12:00:00 AM"
                'Dim curr_date1 = Now()


                Dim curr_date = ""
                Dim curr_date1 = ""

                Dim Partnername As String = txtPartnerName.SelectedItem.Value
                Dim PaymentMode As String = txtPaymentmode.SelectedItem.Value
                Dim Provider As String = ddlProvider.SelectedItem.Value
                Dim trip As String = ""
                'If Session("User_Type") = "EXEC" Then
                '    If [String].IsNullOrEmpty(Session("TripExec")) Then
                '        trip = ""
                '    Else
                '        trip = Session("TripExec").ToString().Trim()
                '    End If
                'Else
                trip = If([String].IsNullOrEmpty(ddlTripDomIntl.SelectedItem.Value), "", ddlTripDomIntl.SelectedItem.Value.Trim())
                'End If
                grdds.Clear()
                grdds = USP_GetTicketDetail_Intl(Session("UID").ToString, Session("User_Type").ToString, curr_date, curr_date1, "", "", "", "", "", AgentID, trip.Trim(), StatusClass.Ticketed, Partnername, PaymentMode, Provider)
                If (grdds.Tables(0).Rows().Count() > 0) Then
                    'BindSales(grdds)
                    BindGrid(grdds)
                Else
                    lbl_Total.Text = "0"
                    lbl_counttkt.Text = "0"
                End If

            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Private Sub BindSales(ByVal GrdDS As DataSet)
        Try
            Dim dt As DataTable
            Dim Db As String = ""
            Dim sum As Double = 0
            dt = GrdDS.Tables(0)
            If dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    Db = dr("TotalAfterDis").ToString()
                    If Db Is Nothing OrElse Db = "" Then
                        Db = 0
                    Else
                        sum += Db
                    End If
                Next
            End If
            If sum <> 0 Then
                lbl_Total.Text = sum.ToString
            End If
            lbl_counttkt.Text = dt.Rows.Count
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Private Sub BindGrid(ByVal Gds As DataSet)
        Try
            Dim dt As DataTable
            Dim Db As String = ""
            Dim sum As Double = 0
            dt = grdds.Tables(0)
            If dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    Db = dr("TotalAfterDis").ToString()
                    If Db Is Nothing OrElse Db = "" Then
                        Db = 0
                    Else
                        sum += Db
                    End If
                Next
            End If
            lbl_Total.Text = "0"
            If sum <> 0 Then
                lbl_Total.Text = sum.ToString '("C", New CultureInfo("en-IN"))
            End If
            lbl_counttkt.Text = dt.Rows.Count
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
        Try
            Session("Grdds") = Gds
            ticket_grdview.DataSource = Gds
            ticket_grdview.DataBind()
            If Gds.Tables(0).Rows.Count > 0 Then
                divReport.Visible = True
            Else
                divReport.Visible = False
            End If
            If Session("User_Type").ToString = "EXEC" Or Session("User_Type").ToString = "ADMIN" Or Session("User_Type").ToString = "SALES" Or Session("User_Type").ToString = "ACC" Then
                ticket_grdview.Columns(15).Visible = False
                ticket_grdview.Columns(16).Visible = False
                ticket_grdview.Columns(18).Visible = True
            Else
                ticket_grdview.Columns(18).Visible = False
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Protected Sub lnkreissue_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lb As LinkButton = CType(sender, LinkButton)
        Dim gvr As GridViewRow = TryCast(lb.Parent.Parent, GridViewRow)
        gvr.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFF275")
        RemarkRefundReissue(lb)
    End Sub
    Protected Sub lnkrefund_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lb As LinkButton = CType(sender, LinkButton)
        Dim gvr As GridViewRow = TryCast(lb.Parent.Parent, GridViewRow)
        gvr.BackColor = System.Drawing.ColorTranslator.FromHtml("#F0F8FF")
        RemarkRefundReissue(lb)
    End Sub
    Protected Sub btnRemark_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemark.Click
        Try
            Dim fltds As New DataSet()
            Dim ObjIntDetails As New IntlDetails()
            'Select requested record for Reissue and Refund
            fltds = ST.GetTicketdIntl(Request("txtPaxid").Trim(), Request("txtPaxType").Trim())
            If fltds.Tables(0).Rows.Count <> 0 Then
                Dim fltTD As DataTable = fltds.Tables(0)
                'Checking Status in ReIssueIntl Table it present in table or not
                Dim Status As Integer = ST.CheckTktNo(Convert.ToInt32(Request("txtPaxid").Trim()), fltTD.Rows(0).Item("OrderId"), fltTD.Rows(0).Item("PNR"))
                If Status = 0 Then
                    Dim i As Integer = 0
                    Dim txtremark As String = Request("txtRemark").Trim()
                    Dim projectID As String = If(IsDBNull(fltTD.Rows(0).Item("ProjectID")), Nothing, fltTD.Rows(0).Item("ProjectID").ToString().Trim())
                    Dim mgtFee As Decimal = If(IsDBNull(fltTD.Rows(0).Item("MgtFee")), 0, Convert.ToDecimal(fltTD.Rows(0).Item("MgtFee").ToString().Trim()))
                    Dim CancelledBy As String = If(DrpCancelledBy.Visible = True, If(DrpCancelledBy.SelectedValue.ToLower() <> "select", DrpCancelledBy.SelectedValue, Nothing), Nothing)
                    Dim BillNoAir As String = If(IsDBNull(fltTD.Rows(0).Item("BillNoCorp")), Nothing, fltTD.Rows(0).Item("BillNoCorp").ToString().Trim())
                    Dim BillNoCorp As String = Nothing
                    If (Request("RemarksType").Trim() = "Reissue") Then
                        Dim ReIssueID As String = sttusobj.GetID("RISU")
                        i = ST.InsReIssueCancelIntl(fltTD.Rows(0).Item("PNR"), fltTD.Rows(0).Item("TicketNo"), fltTD.Rows(0).Item("Sector"), fltTD.Rows(0).Item("Departure"), _
                                                fltTD.Rows(0).Item("Destination"), fltTD.Rows(0).Item("Title").ToString, fltTD.Rows(0).Item("FName"), fltTD.Rows(0).Item("LName"), _
                                                fltTD.Rows(0).Item("PaxType"), fltTD.Rows(0).Item("BookinfDate"), fltTD.Rows(0).Item("DepartDate"), fltTD.Rows(0).Item("TotalFare"), _
                                                fltTD.Rows(0).Item("TotalFareAfterDis"), fltTD.Rows(0).Item("UserID"), fltTD.Rows(0).Item("AgencyName").ToString, _
                                                fltTD.Rows(0).Item("BaseFare"), fltTD.Rows(0).Item("Tax"), fltTD.Rows(0).Item("YQ"), fltTD.Rows(0).Item("STax"), _
                                                fltTD.Rows(0).Item("TFee"), fltTD.Rows(0).Item("Dis"), fltTD.Rows(0).Item("CB"), fltTD.Rows(0).Item("TDS"), _
                                                fltTD.Rows(0).Item("TotalFare"), fltTD.Rows(0).Item("TotalFareAfterDis"), fltTD.Rows(0).Item("VC"), fltTD.Rows(0).Item("OrderId"), _
                                                fltTD.Rows(0).Item("DepTime"), fltTD.Rows(0).Item("AirlinePnr"), fltTD.Rows(0).Item("FNo"), txtremark, ReIssueID, _
                                                Convert.ToInt32(Request("txtPaxid").Trim()), fltTD.Rows(0).Item("Trip"), "R", StatusClass.Pending, projectID, mgtFee, CancelledBy, BillNoCorp, BillNoAir)
                        Try
                            Dim dt As New DataTable()
                            Dim strMailMsgHold As String
                            strMailMsgHold = "<table>"
                            strMailMsgHold = strMailMsgHold & "<tr>"
                            strMailMsgHold = strMailMsgHold & "<td><h2> Reissue Request </h2>"
                            strMailMsgHold = strMailMsgHold & "</td>"
                            strMailMsgHold = strMailMsgHold & "</tr>"
                            strMailMsgHold = strMailMsgHold & "<tr>"
                            strMailMsgHold = strMailMsgHold & "</tr>"
                            strMailMsgHold = strMailMsgHold & "<tr>"
                            strMailMsgHold = strMailMsgHold & "<td><b>Order ID: </b>" + fltTD.Rows(0).Item("OrderId")
                            strMailMsgHold = strMailMsgHold & "</td>"
                            strMailMsgHold = strMailMsgHold & "</tr>"
                            strMailMsgHold = strMailMsgHold & "<tr>"
                            strMailMsgHold = strMailMsgHold & "<td><b>PnrNo: </b>" + fltTD.Rows(0).Item("PNR")
                            strMailMsgHold = strMailMsgHold & "</td>"
                            strMailMsgHold = strMailMsgHold & "</tr>"
                            strMailMsgHold = strMailMsgHold & "<tr>"
                            strMailMsgHold = strMailMsgHold & "<td><b>Pax Name: </b>" + fltTD.Rows(0).Item("Title").ToString + " " + fltTD.Rows(0).Item("FName") + " " + fltTD.Rows(0).Item("LName")
                            strMailMsgHold = strMailMsgHold & "</td>"
                            strMailMsgHold = strMailMsgHold & "</tr>"
                            strMailMsgHold = strMailMsgHold & "<tr>"
                            strMailMsgHold = strMailMsgHold & "<td><b>Pax Type: </b>" + fltTD.Rows(0).Item("PaxType")
                            strMailMsgHold = strMailMsgHold & "</td>"
                            strMailMsgHold = strMailMsgHold & "</tr>"
                            strMailMsgHold = strMailMsgHold & "<tr>"
                            strMailMsgHold = strMailMsgHold & "<td><b>Net Amount: </b>" + Convert.ToString(fltTD.Rows(0).Item("TotalFareAfterDis"))
                            strMailMsgHold = strMailMsgHold & "</td>"
                            strMailMsgHold = strMailMsgHold & "</tr>"
                            strMailMsgHold = strMailMsgHold & "<tr>"
                            strMailMsgHold = strMailMsgHold & "<td><b>Remark: </b>" + Request("txtRemark").Trim()
                            strMailMsgHold = strMailMsgHold & "</td>"
                            strMailMsgHold = strMailMsgHold & "</tr>"
                            strMailMsgHold = strMailMsgHold & "</table>"
                            dt = ObjIntDetails.Email_Credentilas(fltTD.Rows(0).Item("OrderId"), "ReIssue_REJECTED", Request("txtPaxid").Trim())
                            Dim STDOM As New SqlTransactionDom
                            Dim MailDt As New DataTable
                            MailDt = STDOM.GetMailingDetails(MAILING.AIR_PNRSUMMARY.ToString(), Session("UID").ToString()).Tables(0)

                            Try
                                If (MailDt.Rows.Count > 0) Then
                                    For j As Integer = 0 To dt.Rows.Count - 1
                                        STDOM.SendMail(dt.Rows(j)(1).ToString(), "info@UmrawatiTrip.com", "", MailDt.Rows(0)("CC").ToString(), MailDt.Rows(0)("SMTPCLIENT").ToString(), MailDt.Rows(0)("UserId").ToString(), MailDt.Rows(0)("Pass").ToString(), strMailMsgHold, "Ticket Reissue Request", "")
                                    Next
                                End If
                            Catch ex As Exception

                            End Try
                        Catch ex As Exception
                        End Try
                    ElseIf (Request("RemarksType").Trim() = "Refund") Then
                        Dim RefundID As String = sttusobj.GetID("RFND")
                        'Insert data in ReIssueIntl Table
                        ' i = ST.InsReIssueCancelIntl(fltTD, Convert.ToInt32(Request("txtPaxid").Trim()), RefundID, Request("txtRemark").Trim(), "C", StatusClass.Pending)
                        'Insert data in CancellationIntl Table
                        'BillNoCorp = clsCorp.GenerateBillNoCorp("CN")
                        If (Session("IsCorp") = True) Then
                            BillNoCorp = clsCorp.GenerateBillNoCorp("CN")
                        Else
                            CancelledBy = Nothing
                        End If

                        i = ST.InsReIssueCancelIntl(fltTD.Rows(0).Item("PNR"), fltTD.Rows(0).Item("TicketNo"), fltTD.Rows(0).Item("Sector"), fltTD.Rows(0).Item("Departure"), _
                                                fltTD.Rows(0).Item("Destination"), fltTD.Rows(0).Item("Title").ToString, fltTD.Rows(0).Item("FName"), fltTD.Rows(0).Item("LName"), _
                                                fltTD.Rows(0).Item("PaxType"), fltTD.Rows(0).Item("BookinfDate"), fltTD.Rows(0).Item("DepartDate"), fltTD.Rows(0).Item("TotalBookingCost"), _
                                                fltTD.Rows(0).Item("TotalAfterDis"), fltTD.Rows(0).Item("UserID"), fltTD.Rows(0).Item("AgencyName").ToString, fltTD.Rows(0).Item("BaseFare"), _
                                                fltTD.Rows(0).Item("Tax"), fltTD.Rows(0).Item("YQ"), fltTD.Rows(0).Item("STax"), fltTD.Rows(0).Item("TFee"), fltTD.Rows(0).Item("Dis"), _
                                                fltTD.Rows(0).Item("CB"), fltTD.Rows(0).Item("TDS"), fltTD.Rows(0).Item("TotalFare"), fltTD.Rows(0).Item("TotalFareAfterDis"), _
                                                fltTD.Rows(0).Item("VC"), fltTD.Rows(0).Item("OrderId"), fltTD.Rows(0).Item("DepTime"), fltTD.Rows(0).Item("AirlinePnr"), _
                                                fltTD.Rows(0).Item("FNo"), txtremark, RefundID, Convert.ToInt32(Request("txtPaxid").Trim()), fltTD.Rows(0).Item("Trip"), "C", StatusClass.Pending, projectID, mgtFee, CancelledBy, BillNoCorp, BillNoAir)
                        Try
                            Dim dt As New DataTable()
                            Dim strMailMsgHold As String
                            strMailMsgHold = "<table>"
                            strMailMsgHold = strMailMsgHold & "<tr>"
                            strMailMsgHold = strMailMsgHold & "<td><h2> Refund Request </h2>"
                            strMailMsgHold = strMailMsgHold & "</td>"
                            strMailMsgHold = strMailMsgHold & "</tr>"
                            strMailMsgHold = strMailMsgHold & "<tr>"
                            strMailMsgHold = strMailMsgHold & "</tr>"
                            strMailMsgHold = strMailMsgHold & "<tr>"
                            strMailMsgHold = strMailMsgHold & "<td><b>Order ID: </b>" + fltTD.Rows(0).Item("OrderId")
                            strMailMsgHold = strMailMsgHold & "</td>"
                            strMailMsgHold = strMailMsgHold & "</tr>"
                            strMailMsgHold = strMailMsgHold & "<tr>"
                            strMailMsgHold = strMailMsgHold & "<td><b>PnrNo: </b>" + fltTD.Rows(0).Item("PNR")
                            strMailMsgHold = strMailMsgHold & "</td>"
                            strMailMsgHold = strMailMsgHold & "</tr>"
                            strMailMsgHold = strMailMsgHold & "<tr>"
                            strMailMsgHold = strMailMsgHold & "<td><b>Pax Name: </b>" + fltTD.Rows(0).Item("Title").ToString + " " + fltTD.Rows(0).Item("FName") + " " + fltTD.Rows(0).Item("LName")
                            strMailMsgHold = strMailMsgHold & "</td>"
                            strMailMsgHold = strMailMsgHold & "</tr>"
                            strMailMsgHold = strMailMsgHold & "<tr>"
                            strMailMsgHold = strMailMsgHold & "<td><b>Pax Type: </b>" + fltTD.Rows(0).Item("PaxType")
                            strMailMsgHold = strMailMsgHold & "</td>"
                            strMailMsgHold = strMailMsgHold & "</tr>"
                            strMailMsgHold = strMailMsgHold & "<tr>"
                            strMailMsgHold = strMailMsgHold & "<td><b>Net Amount: </b>" + Convert.ToString(fltTD.Rows(0).Item("TotalFareAfterDis"))
                            strMailMsgHold = strMailMsgHold & "</td>"
                            strMailMsgHold = strMailMsgHold & "</tr>"
                            strMailMsgHold = strMailMsgHold & "<tr>"
                            strMailMsgHold = strMailMsgHold & "<td><b>Remark: </b>" + Request("txtRemark").Trim()
                            strMailMsgHold = strMailMsgHold & "</td>"
                            strMailMsgHold = strMailMsgHold & "</tr>"
                            strMailMsgHold = strMailMsgHold & "</table>"
                            dt = ObjIntDetails.Email_Credentilas(fltTD.Rows(0).Item("OrderId"), "Refund_REJECTED", Request("txtPaxid").Trim())
                            Dim STDOM As New SqlTransactionDom
                            Dim MailDt As New DataTable
                            MailDt = STDOM.GetMailingDetails(MAILING.AIR_PNRSUMMARY.ToString(), Session("UID").ToString()).Tables(0)
                            Try
                                If (MailDt.Rows.Count > 0) Then
                                    For k As Integer = 0 To dt.Rows.Count - 1
                                        STDOM.SendMail(dt.Rows(k)(1).ToString(), "info@UmrawatiTrip.com", "", MailDt.Rows(0)("CC").ToString(), MailDt.Rows(0)("SMTPCLIENT").ToString(), MailDt.Rows(0)("UserId").ToString(), MailDt.Rows(0)("Pass").ToString(), strMailMsgHold, "Ticket Refund Request", "")
                                    Next
                                End If
                            Catch ex As Exception

                            End Try
                        Catch ex As Exception
                        End Try
                    End If
                    If i > 0 Then
                        ShowAlertMessage(Request("RemarksType") & " Remark Submitted Succesfully")
                    Else
                        ShowAlertMessage("Try later")
                    End If
                    CallScriptFunction("ResetInputTextvalue", " ", " ", " ", " ", " ", " ")
                End If
            End If

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Public Shared Sub CallScriptFunction(ByVal funName As String, ByVal Parameter1 As String, ByVal Parameter2 As String, ByVal Parameter3 As String, ByVal Parameter4 As String, ByVal Parameter5 As String, ByVal Parameter6 As String)
        Try
            Dim page As Page = TryCast(HttpContext.Current.Handler, Page)
            If page IsNot Nothing Then
                ScriptManager.RegisterStartupScript(page, page.[GetType](), "err_msg", funName & "('" & Parameter1 & "', '" & Parameter2 & "', '" & Parameter3 & "', '" & Parameter4 & "', '" & Parameter5 & "', '" & Parameter6 & "');", True)
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Public Sub showPoup(ByVal lb As LinkButton)
        Try
            Dim gridViewds As New DataSet()
            gridViewds = Session("Grdds")
            Dim filterArray As Array = gridViewds.Tables(0).Select("PaxId ='" & lb.CommandArgument & "'")
            If filterArray.Length > 0 Then
                CallScriptFunction("popupLoad", lb.CommandArgument, lb.CommandName, filterArray(0)("GdsPnr").ToString(), filterArray(0)("TicketNumber").ToString(), filterArray(0)("FName").ToString() & " " & filterArray(0)("LName").ToString(), filterArray(0)("PaxType").ToString())
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Public Sub RemarkRefundReissue(ByVal lb As LinkButton)
        Try
            Dim Status As Integer
            Dim gridViewds As New DataSet()
            gridViewds = Session("Grdds")
            Dim filterArray As Array = gridViewds.Tables(0).Select("PaxId ='" & lb.CommandArgument & "'")
            If filterArray.Length > 0 Then
                Status = ST.CheckTktNo(Convert.ToInt32(lb.CommandArgument), filterArray(0)("Orderid").ToString(), filterArray(0)("GdsPnr").ToString())
                Select Case Status
                    Case 1
                        ShowAlertMessage("This Ticket Number Allready Refunded")
                    Case 2
                        ShowAlertMessage("This Ticket Number is Pending for Refund")
                    Case 3
                        ShowAlertMessage("This Ticket Number is Allready in Refund InProcess")
                    Case 4
                        ShowAlertMessage("This Ticket Number Allready ReIssued")
                    Case 5
                        ShowAlertMessage("This Ticket Number is Pending for ReIssue")
                    Case 6
                        ShowAlertMessage("This Ticket Number is Allready in ReIssue InProcess")
                    Case 0
                        showPoup(lb)
                End Select
            End If
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles ticket_grdview.RowCommand
        Try
            If e.CommandName = "Reissue" Then
                ViewState("PaxId") = e.CommandArgument
                ticket_grdview.Columns(16).Visible = True
                ticket_grdview.Columns(17).Visible = True
                Dim lb As LinkButton = TryCast(e.CommandSource, LinkButton)
                Dim gvr As GridViewRow = TryCast(lb.Parent.Parent, GridViewRow)
                Dim RowIndex As Integer = gvr.RowIndex
                ViewState("RowIndex") = RowIndex
                Dim btnReIssue As LinkButton = DirectCast(ticket_grdview.Rows(RowIndex).FindControl("btnReIssue"), LinkButton)
                Dim btnremark As LinkButton = DirectCast(ticket_grdview.Rows(RowIndex).FindControl("btnRemark"), LinkButton)
                Dim lblReIssue As Label = DirectCast(ticket_grdview.Rows(RowIndex).FindControl("lblReIssue"), Label)
                Dim lblRefund As Label = DirectCast(ticket_grdview.Rows(RowIndex).FindControl("lblRefund"), Label)
                Dim txtremark As TextBox = DirectCast(ticket_grdview.Rows(RowIndex).FindControl("txtRemark"), TextBox)
                Dim lnkHides As LinkButton = DirectCast(ticket_grdview.Rows(RowIndex).FindControl("lnkHides"), LinkButton)
                txtremark.Visible = True
                lnkHides.Visible = True
                btnremark.Visible = False
                btnReIssue.Visible = True
                lblReIssue.Visible = True
                lblRefund.Visible = False
                txtremark.Focus()
                gvr.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFF275")
            ElseIf e.CommandName = "Refund" Then
                ViewState("PaxId") = e.CommandArgument
                ticket_grdview.Columns(16).Visible = True
                ticket_grdview.Columns(17).Visible = True
                Dim lb As LinkButton = TryCast(e.CommandSource, LinkButton)
                Dim gvr As GridViewRow = TryCast(lb.Parent.Parent, GridViewRow)
                Dim RowIndex As Integer = gvr.RowIndex
                ViewState("RowIndex") = RowIndex
                Dim btnReIssue As LinkButton = DirectCast(ticket_grdview.Rows(RowIndex).FindControl("btnReIssue"), LinkButton)
                Dim btnremark As LinkButton = DirectCast(ticket_grdview.Rows(RowIndex).FindControl("btnRemark"), LinkButton)
                Dim lblReIssue As Label = DirectCast(ticket_grdview.Rows(RowIndex).FindControl("lblReIssue"), Label)
                Dim lblRefund As Label = DirectCast(ticket_grdview.Rows(RowIndex).FindControl("lblRefund"), Label)
                Dim txtremark As TextBox = DirectCast(ticket_grdview.Rows(RowIndex).FindControl("txtRemark"), TextBox)
                Dim lnkHides As LinkButton = DirectCast(ticket_grdview.Rows(RowIndex).FindControl("lnkHides"), LinkButton)
                txtremark.Visible = True
                lnkHides.Visible = True
                btnremark.Visible = True
                btnReIssue.Visible = False
                lblReIssue.Visible = False
                lblRefund.Visible = True
                txtremark.Focus()
                gvr.BackColor = System.Drawing.ColorTranslator.FromHtml("#F0F8FF")
            Else
                ticket_grdview.Columns(16).Visible = False
                ticket_grdview.Columns(17).Visible = False
                Dim lb As LinkButton = TryCast(e.CommandSource, LinkButton)
                Dim gvr As GridViewRow = TryCast(lb.Parent.Parent, GridViewRow)
                Dim RowIndex As Integer = gvr.RowIndex
                Dim btnReIssue As LinkButton = DirectCast(ticket_grdview.Rows(RowIndex).FindControl("btnReIssue"), LinkButton)
                Dim btnremark As LinkButton = DirectCast(ticket_grdview.Rows(RowIndex).FindControl("btnRemark"), LinkButton)
                Dim lblReIssue As Label = DirectCast(ticket_grdview.Rows(RowIndex).FindControl("lblReIssue"), Label)
                Dim lblRefund As Label = DirectCast(ticket_grdview.Rows(RowIndex).FindControl("lblRefund"), Label)
                Dim txtremark As TextBox = DirectCast(ticket_grdview.Rows(RowIndex).FindControl("txtRemark"), TextBox)
                Dim lnkHides As LinkButton = DirectCast(ticket_grdview.Rows(RowIndex).FindControl("lnkHides"), LinkButton)
                txtremark.Visible = False
                lnkHides.Visible = False
                btnremark.Visible = False
                btnReIssue.Visible = False
                lblReIssue.Visible = False
                lblRefund.Visible = False
                gvr.BackColor = Nothing
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Public Sub CheckEmptyValue()
        Try
            Dim FromDate As String
            Dim ToDate As String
            If [String].IsNullOrEmpty(Request("From")) Then
                FromDate = ""
            Else
                ''FromDate = Strings.Left((Request("From")).Split(" ")(0), 2) + "/" + Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "/" + Strings.Right((Request("From")).Split(" ")(0), 4) 'Date Format 19/04/2017 12:00:00 AM'
                ''FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "/" + Strings.Left((Request("From")).Split(" ")(0), 2) + "/" + Strings.Right((Request("From")).Split(" ")(0), 4) 'Date Format 04/09/2017 12:00:00 AM'
                ''FromDate = FromDate + " " + "12:00:00 AM"

                FromDate = Strings.Right((Request("From")).Split(" ")(0), 4) + "-" + Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "-" + Strings.Left((Request("From")).Split(" ")(0), 2)  'Date Format 2017-04-09 12:00:00 AM'
                FromDate = FromDate + " " + "00:00:00.001"
            End If
            If [String].IsNullOrEmpty(Request("To")) Then
                ToDate = ""
            Else
                ''ToDate = Mid((Request("To")).Split(" ")(0), 4, 2) & "/" & Left((Request("To")).Split(" ")(0), 2) & "/" & Right((Request("To")).Split(" ")(0), 4)
                'ToDate = Left((Request("To")).Split(" ")(0), 2) & "/" & Mid((Request("To")).Split(" ")(0), 4, 2) & "/" & Right((Request("To")).Split(" ")(0), 4)
                'ToDate = ToDate & " " & "11:59:59 PM"
                ToDate = Right((Request("To")).Split(" ")(0), 4) & "-" & Mid((Request("To")).Split(" ")(0), 4, 2) & "-" & Left((Request("To")).Split(" ")(0), 2)
                ToDate = ToDate & " " & "23:59:59.999"

            End If
            Dim AgentID As String = If([String].IsNullOrEmpty(Request("hidtxtAgencyName")) Or Request("hidtxtAgencyName") = "Agency Name or ID", "", Request("hidtxtAgencyName"))
            Dim OrderID As String = If([String].IsNullOrEmpty(txt_OrderId.Text), "", txt_OrderId.Text.Trim)
            Dim PNR As String = If([String].IsNullOrEmpty(txt_PNR.Text), "", txt_PNR.Text.Trim)
            Dim PaxName As String = If([String].IsNullOrEmpty(txt_PaxName.Text), "", txt_PaxName.Text.Trim)
            Dim TicketNo As String = If([String].IsNullOrEmpty(txt_TktNo.Text), "", txt_TktNo.Text.Trim)
            ' Dim AirPNR As String = If([String].IsNullOrEmpty(txt_AirPNR.Text), "", txt_AirPNR.Text.Trim)
            Dim Airline As String = If([String].IsNullOrEmpty(Request("aircode")) Or Request("aircode") = "Airlines", "", Request("aircode"))
            Dim Partnername As String = txtPartnerName.SelectedItem.Value
            Dim Provider As String = ddlProvider.SelectedItem.Value
            Dim PaymentMode As String = txtPaymentmode.SelectedItem.Value
            Dim trip As String = If([String].IsNullOrEmpty(ddlTripDomIntl.SelectedItem.Value), "", ddlTripDomIntl.SelectedItem.Value.Trim())
            If Partnername = "0" Then
                Partnername = ""
            End If
            grdds.Clear()
            grdds = USP_GetTicketDetail_Intl(Session("UID").ToString, Session("User_Type").ToString, FromDate, ToDate, OrderID, PNR, PaxName, TicketNo, Airline, AgentID, trip.Trim(), StatusClass.Ticketed, Partnername, PaymentMode, Provider)
            BindGrid(grdds)

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Protected Sub btn_result_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_result.Click
        Try
            CheckEmptyValue()
            txtPartnerName.SelectedValue = "0"
            txtPaymentmode.SelectedValue = "All"
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Protected Sub ticket_grdview_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles ticket_grdview.PageIndexChanging
        Try
            ticket_grdview.PageIndex = e.NewPageIndex
            BindGrid(Session("Grdds"))

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Public Shared Sub ShowAlertMessage(ByVal [error] As String)
        Try


            Dim page As Page = TryCast(HttpContext.Current.Handler, Page)
            If page IsNot Nothing Then
                [error] = [error].Replace("'", "'")
                ScriptManager.RegisterStartupScript(page, page.[GetType](), "err_msg", "alert('" & [error] & "');", True)
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try

    End Sub
    Protected Sub btn_export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_export.Click
        Try

            Dim FromDate As String
            Dim ToDate As String
            If [String].IsNullOrEmpty(Request("From")) Then
                FromDate = ""
            Else
                'FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "/" + Strings.Left((Request("From")).Split(" ")(0), 2) + "/" + Strings.Right((Request("From")).Split(" ")(0), 4)
                'FromDate = FromDate + " " + "12:00:00 AM"
                FromDate = Strings.Right((Request("From")).Split(" ")(0), 4) + "-" + Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "-" + Strings.Left((Request("From")).Split(" ")(0), 2)  'Date Format 2017-04-09 12:00:00 AM'
                FromDate = FromDate + " " + "00:00:00.001"
            End If
            If [String].IsNullOrEmpty(Request("To")) Then
                ToDate = ""
            Else
                'ToDate = Mid((Request("To")).Split(" ")(0), 4, 2) & "/" & Left((Request("To")).Split(" ")(0), 2) & "/" & Right((Request("To")).Split(" ")(0), 4)
                'ToDate = ToDate & " " & "11:59:59 PM"
                ToDate = Right((Request("To")).Split(" ")(0), 4) & "-" & Mid((Request("To")).Split(" ")(0), 4, 2) & "-" & Left((Request("To")).Split(" ")(0), 2)
                ToDate = ToDate & " " & "23:59:59.999"
            End If
            Dim AgentID As String = If([String].IsNullOrEmpty(Request("hidtxtAgencyName")), "", Request("hidtxtAgencyName"))
            Dim OrderID As String = If([String].IsNullOrEmpty(txt_OrderId.Text), "", txt_OrderId.Text.Trim)
            Dim PNR As String = If([String].IsNullOrEmpty(txt_PNR.Text), "", txt_PNR.Text.Trim)
            Dim PaxName As String = If([String].IsNullOrEmpty(txt_PaxName.Text), "", txt_PaxName.Text.Trim)
            Dim TicketNo As String = If([String].IsNullOrEmpty(txt_TktNo.Text), "", txt_TktNo.Text.Trim)
            ' Dim AirPNR As String = If([String].IsNullOrEmpty(txt_AirPNR.Text), "", txt_AirPNR.Text.Trim)
            Dim Airline As String = If([String].IsNullOrEmpty(Request("aircode")) Or Request("aircode") = "Airlines", "", Request("aircode"))
            Dim Partnername As String = txtPartnerName.SelectedItem.Value
            Dim Provider As String = ddlProvider.SelectedItem.Value
            Dim PaymentMode As String = txtPaymentmode.SelectedItem.Value
            Dim trip As String = If([String].IsNullOrEmpty(ddlTripDomIntl.SelectedItem.Value), "", ddlTripDomIntl.SelectedItem.Value.Trim())
            If Partnername = "0" Then
                Partnername = ""
            End If
            grdds.Clear()
            grdds = USP_GetTicketDetail_Intl(Session("UID").ToString, Session("User_Type").ToString, FromDate, ToDate, OrderID, PNR, PaxName, TicketNo, Airline, AgentID, trip.Trim(), StatusClass.Ticketed, Partnername, PaymentMode, Provider)
            grdds.Tables(0).Columns.Remove(grdds.Tables(0).Columns("FareRule"))
            STDom.ExportData(grdds)
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Public Function GetVisibleStatus() As String

        If Session("User_Type").ToString = "EXEC" Or Session("User_Type").ToString = "ADMIN" Or Session("User_Type").ToString = "SALES" Or Session("User_Type").ToString = "ACC" Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub BindPartner()
        Dim constr As String = ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
        Dim con As New SqlConnection(constr)
        Dim cmd As New SqlCommand("BindPartnerNameSP_PP")
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = con
        con.Open()
        txtPartnerName.DataSource = cmd.ExecuteReader()
        txtPartnerName.DataTextField = "PartnerName"
        txtPartnerName.DataValueField = "PartnerName"
        txtPartnerName.DataBind()
        con.Close()
        txtPartnerName.Items.Insert(0, New ListItem("--Select PartnerName--", ""))
    End Sub
    <System.Web.Services.WebMethod>
    Public Shared Function GetFairRule(ByVal paxid As String) As String
        Dim abc As String
        Dim constr As String = ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
        Dim con As New SqlConnection(constr)
        Dim cmd As New SqlCommand("USP_Get_FairRule")
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@paxid", paxid)
        cmd.Connection = con
        con.Open()
        abc = Convert.ToString(cmd.ExecuteScalar)
        con.Close()
        Return abc
    End Function
    Public Function USP_GetTicketDetail_Intl(ByVal LoginId As String, ByVal usertype As String, ByVal FromDate As String, ByVal ToDate As String, ByVal OrderID As String, _
           ByVal pnr As String, ByVal paxname As String, ByVal TicketNo As String, ByVal AirPNR As String, ByVal AgentID As String, ByVal Trip As String, ByVal Status As StatusClass, ByVal Partnername As String, ByVal PaymentMode As String, ByVal Provider As String) As DataSet
        Dim cmd As New SqlCommand()
        Dim ds As New DataSet()
        Try
            Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            cmd.CommandText = "USP_GetTicketDetail_Intl_PP"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@usertype", SqlDbType.VarChar).Value = usertype
            cmd.Parameters.Add("@LoginID", SqlDbType.VarChar).Value = LoginId
            cmd.Parameters.Add("@FormDate", SqlDbType.VarChar).Value = FromDate
            cmd.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = ToDate
            cmd.Parameters.Add("@OderId", SqlDbType.VarChar).Value = OrderID
            cmd.Parameters.Add("@PNR", SqlDbType.VarChar).Value = pnr
            cmd.Parameters.Add("@AirlinePNR", SqlDbType.VarChar).Value = AirPNR
            cmd.Parameters.Add("@PaxName", SqlDbType.VarChar).Value = paxname
            cmd.Parameters.Add("@TicketNo", SqlDbType.VarChar).Value = TicketNo
            cmd.Parameters.Add("@AgentId", SqlDbType.VarChar).Value = AgentID
            cmd.Parameters.Add("@Trip", SqlDbType.VarChar).Value = Trip
            cmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = Status.ToString()
            cmd.Parameters.Add("@PartnerName", SqlDbType.VarChar).Value = Partnername
            cmd.Parameters.Add("@PaymentMode", SqlDbType.VarChar).Value = PaymentMode
            cmd.Parameters.Add("@provider", SqlDbType.VarChar).Value = Provider
            cmd.Connection = con1
            con1.Open()
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            cmd.Dispose()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
        Return ds
    End Function
    <WebMethod()> _
    Public Shared Function GetAutoCompleteData(ByVal username As String) As List(Of String)
        Dim result As New List(Of String)()
        Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
        Using cmd As New SqlCommand("select AL_Code,AL_Name FROM AirlineNames  where AL_Name LIKE '%'+@SearchText+'%'", con)
            con.Open()
            cmd.Parameters.AddWithValue("@SearchText", username)
            Dim dr As SqlDataReader = cmd.ExecuteReader()
            While dr.Read()
                result.Add(String.Format("{0}/{1}", dr("AL_Name"), dr("AL_Code")))
            End While
            Return result
        End Using
    End Function
End Class
