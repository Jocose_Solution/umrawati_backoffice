﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false"
    CodeFile="PendingCreditDetails.aspx.vb" Inherits="DetailsPort_Finance_PendingCreditDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <style type="text/css">
        .txtBox
        {
            width: 140px;
            height: 18px;
            line-height: 18px;
            border: 2px #D6D6D6 solid;
            padding: 0 3px;
            font-size: 11px;
        }
        .txtCalander
        {
            width: 100px;
            background-image: url(../../images/cal.gif);
            background-repeat: no-repeat;
            background-position: right;
            cursor: pointer;
            border: 1px #D6D6D6 solid;
        }
    </style>
<style>
        input[type="text"], input[type="password"], select
        {
            border: 1px solid #808080;
            padding: 2px;
            font-size: 1em;
            color: #444;
            width: 150px;
            font-family: arial, Helvetica, sans-serif;
            font-size: 12px;
            font-weight: normal;
            border-radius: 3px 3px 3px 3px;
            -webkit-border-radius: 3px 3px 3px 3px;
            -moz-border-radius: 3px 3px 3px 3px;
            -o-border-radius: 3px 3px 3px 3px;
        }
    </style>
    <script>
        function validatedate() 
        {
       
if(document.getElementById("ctl00_ContentPlaceHolder1_ddl_PaymentType").value=="ALL" || document.getElementById("ctl00_ContentPlaceHolder1_ddl_PaymentType").value=="IATA Credit")
{
if(document.getElementById("From").value=="")
{
alert('Please Enter From Date');
document.getElementById("From").focus();
return false;
}
if(document.getElementById("To").value=="")
{
alert('Please Enter To Date');
document.getElementById("To").focus();
return false;

}
}

        }
    </script>

    <table cellspacing="7" cellpadding="0" border="0" class="tbltbl" width="500px">
        <tr>
            <td class="Heading" colspan="2" align="center" style="font-size: 20px">
                Agency Pending Credit Detail
            </td>
        </tr>
        <tr>
            <td colspan="1" class="Text" height="25px">
                Payment Type
            </td>
            <td>
                <asp:DropDownList ID="ddl_PaymentType" runat="server">
                    <asp:ListItem Value="ALL">ALL</asp:ListItem>
                    <asp:ListItem Value="3 Days Credit">3 Days Credit</asp:ListItem>
                    <asp:ListItem Value="IATA Credit">IATA Credit</asp:ListItem>
                    <asp:ListItem Value="Weekly Credit">Weekly Credit</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="1" class="Text" height="25px">
                From Date
            </td>
            <td>
                <input type="text" name="From" id="From" class="txtCalander" readonly="readonly"
                    style="width: 100px" />
            </td>
        </tr>
        <tr>
            <td colspan="1" class="Text" height="25px">
                To Date
            </td>
            <td>
                <input type="text" name="To" id="To" class="txtCalander" readonly="readonly" style="width: 100px" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Button ID="btn_search" runat="server" Text="Search" CssClass="button" OnClientClick="return validatedate();" />
            &nbsp;<asp:Button ID="btn_word" runat="server" CssClass="button" 
                    Text="Export To Word" />
&nbsp;<asp:Button ID="btn_excel" runat="server" CssClass="button" Text="Export To Excel" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="font-family: Arial; font-size: 12px">
                <strong>Note:</strong>&nbsp; From date and to date required for payment type ALL
                and IATA credit only<br />
              <div style="font-size:10px"> <strong> 3 Days credit&nbsp; :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong>(Type1,Type7,Type10,Type11)<br />
                              <strong> IATA credit&nbsp; :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong>(Type2,Type5,Type8,Type13,Type14)<br />
                                             <strong> Weekly Credit :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong>(Type3,Type9,Type15)<br />

</div>
            </td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="center" style="padding-top: 10px">
                <asp:Label ID="lbl_result" runat="server" BorderColor="#161946" BorderStyle="Solid"
                    BorderWidth="1px"></asp:Label>
            </td>
        </tr>
    </table>

    <script type="text/javascript">
    var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

</asp:Content>
