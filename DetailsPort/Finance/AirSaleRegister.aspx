﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false"
    CodeFile="AirSaleRegister.aspx.vb" Inherits="DetailsPort_Finance_AirSaleRegister" %>
<%@ Register Src="~/UserControl/AccountsControl.ascx" TagPrefix="uc1" TagName="Account" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<link href="<%=ResolveUrl("~/CSS/lytebox.css?v=1")%>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language='javascript'>
        function callprint(strid) {
            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'left=0,top=0,width=750,height=500,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write("<html><head><title>Ticket Details</title></head><body>" + prtContent.innerHTML + "</body></html>");
            prtContent.innerHTML = "";
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();

            prtContent.innerHTML = "";
            //prtContent.innerHTML = strOldOne;
        }
    </script>
   <%-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">--%>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
      $(function () {
          $("#datepicker").datepicker({ dateFormat: 'dd-mm-yy', minDate: -730, maxDate: "+0D" }).val();
          $(".cd").click(function () {
              $("#datepicker").focus();
          });
          $("#datepicker1").datepicker({dateFormat: 'dd-mm-yy', minDate: -730, maxDate: "+0D" }).val();
          $(".cd1").click(function () {
              $("#datepicker1").focus();

          });
      });
  </script>

   <%-- <script type="text/javascript">
        $(document).ready(function () {
            $("#From").datepicker({ dateFormat: 'dd-mm-yy' }).val();
            $(".cd").click(function () {
                $("#From").focus();
            }
            );
            $("#To").datepicker({ dateFormat: 'dd-mm-yy' }).val();
            $(".cd1").click(function () {
                $("#To").focus();
            }
            );
        });
    </script>

  --%>

     <style>
         .mydatagrid th
         {
             /*background-color: black;
    color: white;*/
         }
     </style>


       <style type="text/css">
           .page-wrapperss
           {
               background-color: #fff;
               margin-left: 15px;
           }
       </style>
      
    <div class="row">
         <div class="container-fluid" style="padding-right:35px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-xs-2">
                                <div class="input-group">
                                    <input type="text" placeholder="FROM DATE" name="From" id="datepicker" class=" form-control input-text full-width" readonly="readonly"/>
                                <span class="input-group-addon"  style="background:#49cced">
                        <span class="glyphicon glyphicon-calendar  cd"></span>
                    </span>
                                </div>
                            </div>
                           

 
                            
                            <div class="col-md-2">
                                <div class="input-group">
                                 <input type="text" placeholder="TO DATE" name="To" id="datepicker1" class="form-control input-text full-width" readonly="readonly" />
                                    <span class="input-group-addon" style="background-color: #49cced">
                        <span class="glyphicon glyphicon-calendar  cd1"></span>
                    </span>
                                </div>
                            </div>
                            
                            
                            <div class="col-md-2">
                                <div class="input-group">
                                  <asp:TextBox ID="txt_PNR" placeholder="PNR" runat="server" CssClass="form-control input-text full-width"></asp:TextBox>
                                    <span class="input-group-addon" style="background:#49cced">
                                  <span class="glyphicon glyphicon-plane"></span>
                                        </span>
                                </div>
                            </div>



                            <div class="col-md-2">
                                <div class="input-group">
                                    <asp:TextBox ID="txt_OrderId" placeholder="REF. No." runat="server" CssClass="form-control input-text full-width"></asp:TextBox>
                                     <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-first-order"></span>
                                        </span>
                                </div>
                            </div>
                           
                            
                            <div class="col-md-2">
                                <div class="input-group">
                                    <asp:TextBox ID="txt_PaxName" placeholder="NAME" runat="server" CssClass="form-control input-text full-width"></asp:TextBox>
                                       <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-users"></span>
                                        </span>
                                </div>
                            </div>
                           
                            
                             <div class="col-md-2">
                                <div class="input-group">
                                   <asp:TextBox ID="txt_TktNo" placeholder="TICKET NO." runat="server" CssClass="form-control input-text full-width" ></asp:TextBox>
                                     <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-ticket"></span>
                                        </span>
                                </div>
                            </div>
                            
                            
                            <div class="col-md-2" style="margin-top: 16px;">
                                <div class="input-group">
                                    <asp:TextBox ID="txt_Airline" placeholder="AIRLINE" runat="server" CssClass="form-control input-text full-width"></asp:TextBox>
                                     <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-plane"></span>
                                        </span>
                                </div>
                            </div>
                            
                            
                            <div class="col-md-3" id="spn_Projects" runat="server">
                                <div class="form-group" id="spn_Projects1" runat="server">
                                    
                                     <asp:DropDownList ID="DropDownListProject" placeholder="Project ID" runat="server" CssClass="input-text full-width">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            
                            
                            <div class="col-md-2" id="td_Agency" runat="server" style="margin-top:16px;">
                                <div class="input-group">
                                   <input type="text" placeholder="BRAND NAME" id="txtAgencyName" name="txtAgencyName" <%--onfocus="focusObj(this);"onblur="blurObj(this);"--%> <%--defvalue="Agency Name or ID"--%> autocomplete="off" <%--value="Agency Name or ID"--%>  class="form-control input-text full-width"/>
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                    <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-black-tie"></span>
                                        </span>
                               </div>
                            </div>
                               <div class="col-md-3" id="div_trip" runat="server" style="margin-top:16px;">
                                <div class="input-group">
                                    <asp:DropDownList  ID="ddl_trip" runat="server"  style="width: 182px;" >
                                       <asp:ListItem Text="DOMESTIC" Value="D"></asp:ListItem>
                                   <asp:ListItem Text="INTERNATIONAL" Value="I"></asp:ListItem>
                                 </asp:DropDownList>
                               </div>
                            </div>   
                            



                           <div class="col-md-3" id="divPaymentMode" runat="server" >
                                    <asp:DropDownList CssClass="input-text full-width" ID="txtPaymentmode" runat="server">                                 
                                   <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                   <asp:ListItem Text="PG" Value="pg"></asp:ListItem>
                                  <asp:ListItem Text="Wallet" Value="wallet"></asp:ListItem>                                                                
                                    </asp:DropDownList>
                                </div>
                           
                      
                            <div class="col-md-3" style="margin-top: -1px;margin-left:-105px">
                                <div class="form-group"> 
                                    <br />                                   
                                    <asp:Button ID="btn_result" runat="server" Text="Go" Width="74px" CssClass="btn btn-success"  />

                             
                                    <asp:Button ID="btn_export" runat="server" Text="Export" CssClass="btn btn-success"  />
                                    </div>
                            </div>
                            </div>
                       <%-- <div style="color: #FF0000">
                            * N.B: To get Today's booking without above parameter,do not fill any field, only
                            click on search your booking.
                        </div>    
                        --%>
                        <div>&nbsp</div>
                        <div class="row" style="background-color:#fff; overflow: auto; " runat="server">                                                              
                        <div align="center">
                
                   <table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:white;">
                    <tr>
                        <td align="left">
                            <table id="PrintVisible" runat="server" visible="false">  <!--Hidden Fields-->
                                <tr>
                                    <td>
                                        <b>Print Invoice Pages :</b>&nbsp;&nbsp;
                                    </td>
                                    <td>
                                         <br />
                                        <asp:TextBox ID="TextBoxPrintNo"  Width="90px" Height="43px" runat="server" CssClass="input-text full-width"></asp:TextBox>
                                    </td>
                                  
                                    <td  style="padding-left:20px;padding-bottom: 28px;">
                                         <br /> <br />
                                        <asp:Button ID="ButtonPrint" runat="server" Text="Print" CssClass="btn btn-success"  />
                                        &nbsp;&nbsp;&nbsp;(Ex: 1-3 or 3-10)&nbsp;&nbsp;                              
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                              
                            <asp:UpdatePanel ID="up" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="grd_IntsaleRegis" runat="server" AllowPaging="true"  AutoGenerateColumns="false"
                                      cssclass="table table-striped table-bordered table-hover" GridLines="None" PageSize="30" OnPageIndexChanging="grd_IntsaleRegis_PageIndexChanging" style="text-transform: uppercase;">
                                        <Columns>
                                            <%--<asp:TemplateField HeaderText="TransactionId">
                                                <ItemTemplate>
                                                    <a href='Invoiceflt.aspx?OrderId=<%#Eval("OrderId") %>&amp;invno=<%#Eval("OrderId") %>&amp;tktno=<%#Eval("TicketNumber") %>&amp;AgentID=<%#Eval("UserId") %>'
                                                        style="color: #004b91; font-size: 11px; font-weight: bold" target="_blank">
                                                        <asp:Label ID="lbl_order" runat="server" Text='<%#Eval("OrderId") %>'></asp:Label>
                                                        &nbsp;(Invoice)</a>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
 <asp:TemplateField HeaderText="TransactionId">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_order" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>                                         
                                                            <asp:TemplateField HeaderText="Brand Id">
                                                                <ItemTemplate>
                                                                    <a href='CreditDebitNode.aspx?OrderId=<%#Eval("OrderID")%>&AgentId=<%#Eval("UserId")%>&TicketNo=<%#Eval("Ticketnumber")%>&TicketingCarier=<%#Eval("AirlineCode")%>&PNR=<%#Eval("GdsPnr")%>'
                                                                        rel="lyteframe" 
                                                                        target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                                        <asp:Label ID="lbl_AgentId" runat="server" Text='<%#Eval("AgencyID") %>'></asp:Label>
                                                                    </a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Company">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_sector0" runat="server" Text='<%#Eval("AgentCompany")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="GDSPNR">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_gdspnr" runat="server" Text='<%#Eval("GdsPnr") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="AIRLINE&nbsp;PNR">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_airpnrr" runat="server" Text='<%#Eval("AirlinePnr") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SECTOR">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_sector1" runat="server" Text='<%#Eval("Sector") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="AirlineCode">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_sector2" runat="server" Text='<%#Eval("AirlineCode")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="PassengerName">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_tittle" runat="server" Text='<%#Eval("PassengerName")%>'></asp:Label>
                                                                    <%--<asp:Label ID="lbl_tittle" runat="server" Text='<%#Eval("title") %>'></asp:Label>
                                                                    &nbsp;<asp:Label ID="lbl_fname" runat="server" Text='<%#Eval("fname") %>'></asp:Label>&nbsp;<asp:Label
                                                                        ID="lbl_mname" runat="server" Text='<%#Eval("mname") %>'></asp:Label>&nbsp;<asp:Label
                                                                            ID="lbl_lname" runat="server" Text='<%#Eval("lname") %>'></asp:Label>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="pax type">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_paxtype" runat="server" Text='<%#Eval("Paxtype") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="FlightNumber">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAirLine" runat="server" Text='<%#Eval("FlightNumber")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="TICKET&nbsp;NUMBER">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_tkt" runat="server" Text='<%#Eval("Ticketnumber") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="BASE_FARE">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_basefare" runat="server" Text='<%#Eval("basefare") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>                                             
                                                            <asp:TemplateField HeaderText="YQ">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_yq" runat="server" Text='<%#Eval("YQ") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="TAX">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_tottax" runat="server" Text='<%#Eval("Tax")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="SERVICE&nbsp;TAX">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_sertax" runat="server" Text='<%#Eval("servicetax") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="TRANFEE">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_tranfee" runat="server" Text='<%#Eval("TransactionFee") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="MGTFEE">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_tranfee" runat="server" Text='<%#Eval("MgtFee") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Total_Commsion">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_totdis" runat="server" Text='<%#Eval("Commsion")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="TDS">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_tds" runat="server" Text='<%#Eval("Tds") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="GrossFare">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_totfare" runat="server" Text='<%#Eval("GrossFare")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="InvoiceTotal">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_totbookcost" runat="server" Text='<%#Eval("InvoiceTotal")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="BookingDate">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_CDate" runat="server" Text='<%#Eval("BookingDate")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="PAYMENT_MODE">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_PaymentMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Convenience_Fee">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_PGCharges" runat="server" Text='<%#Eval("PgCharges")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>                                                          
                                                        </Columns>
                                                        <RowStyle CssClass="RowStyle" />
                                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                        <PagerStyle CssClass="PagerStyle" />
                                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                        <HeaderStyle CssClass="HeaderStyle" />
                                                        <EditRowStyle CssClass="EditRowStyle" />
                                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <%--<asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UP">
                        <ProgressTemplate>
                            <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden;
                                padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5;
                                z-index: 1000;">
                            </div>
                            <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center;
                                z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px;
                                font-weight: bold; color: #000000">
                                Please Wait....<br />
                                <br />
                                <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                <br />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>--%>
                                 
                        </td>
                        
                    </tr>
                </table>
               </div>
            </div>
            <div id="DivPrint" runat="server" visible="true">
                </div>
            </div>
        </div>
                </div>
            </div>
        </div>

       
            <script type="text/javascript">
                var UrlBase = '<%=ResolveUrl("~/") %>';
            </script>

            <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

            <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

            <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
</asp:Content>
