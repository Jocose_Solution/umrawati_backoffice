﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PG;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Security;

public partial class DetailsPort_Finance_CheckPGResponse : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        //string myScript = "<script language='javascript' type='text/javascript'>CloseWindow();<" + "/script>";
        //Page.ClientScript.RegisterStartupScript(GetType(), "myscript", myScript);
        try
        {
            string Response = string.Empty;
            string ApiStatus = string.Empty;
            string APIStatus = string.Empty;
            string UnmappedStatus = string.Empty;
            if (Session["UID"] != null && Convert.ToString(Session["UID"]) != "")
            {
                PG.PaymentGateway objPg = new PG.PaymentGateway();
                string OrderId = Request.QueryString["OrderId"];
                string AgentID = Request.QueryString["AgentID"];
                if (!string.IsNullOrEmpty(OrderId))
                {

                    Response = objPg.GetPaymmentStatusResponsePayU(OrderId.Trim());
                    td1.InnerText = OrderId;
                    if (!string.IsNullOrEmpty(Response))
                    {                       
                        if (Response.Contains("transaction_details")==true)
                        {
                            //ApiStatus = objPg.CheckPaymmentStatusPayU(OrderId.Trim());
                            Newtonsoft.Json.Linq.JObject account = Newtonsoft.Json.Linq.JObject.Parse(Response);
                            APIStatus = (string)account.SelectToken("transaction_details." + OrderId + ".status");
                            UnmappedStatus = (string)account.SelectToken("transaction_details." + OrderId + ".unmappedstatus");
                            ApiStatus = APIStatus + "/" + UnmappedStatus;

                        }

                        tdPgStatus.InnerText = ApiStatus;
                    }
                    else
                    {
                        Response = "Not getting any response from PG.";
                    }
                    tdResponse.InnerText = Response;
                }
                else
                {
                    Page.ClientScript.RegisterOnSubmitStatement(typeof(Page), "closePage", "window.onunload = CloseWindow();");
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Order id requried');", true);
                    //return;
                   
                }
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                //Page.ClientScript.RegisterOnSubmitStatement(typeof(Page), "closePage", "window.onunload = CloseWindow();");
                //Response.Redirect("Login.aspx?reason=Session TimeOut", false);


                //string myScript = "<script language='javascript' type='text/javascript'>CloseWindow();<" + "/script>";
                //Page.ClientScript.RegisterStartupScript(GetType(), "myscript", myScript);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
}