﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="Financial.aspx.cs" Inherits="Financial" %>



    <%@ Register Src="~/DetailsPort/Finance/Commission_Setup/commissionmaster.ascx" TagPrefix="Search" TagName="commissionmaster" %>
<%@ Register Src="~/DetailsPort/Finance/Commission_Setup/DealCodeMaster.ascx" TagPrefix="Search1" TagName="DealCodeMaster" %>
<%@ Register Src="~/DetailsPort/Finance/Commission_Setup/MISCSRVCHARGE.ascx" TagPrefix="Search2" TagName="MISCSRVCHARGE" %>
<%--<%@ Register Src="~/DetailsPort/Finance/Commission_Setup/commissionmaster.ascx" TagPrefix="Search1" TagName="commissionmaster" %>--%>




<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <style>
        .panel panel-primary{
            background-color: rgba(255, 255, 255, 0.13);
        }
    </style>


    <div class="wrapper">
            <div class="searchengine searchbg " id="multisit" style="height: 620px; background-image:url('Images/550b7afbf123fa534ac657745e1d512c.png')"> 
                <div class="container">
                    <div class="row" style="margin-bottom: 0px;">
                        <div style="clear: both;"></div>
                        <div class="col-xs-12 col-sm-12" style="padding-right: 0px;">
                            <ul class="nav nav-tabs tabstab">
                                <li class="active "><a href="#home" data-toggle="tab">
                                    <span class="fa fa-book"></span>
                                    Commission Master
                                </a></li>
                                <li class=" "><a href="#profile" data-toggle="tab">
                                    <span class="fa fa-plane" aria-hidden="true"></span>
                                    Deal Master
                                </a></li>
                                <li class=" "><a href="#bus" data-toggle="tab">
                                    <span class="fa fa-search" aria-hidden="true"></span>
                                    Miscrvcharge
                                </a></li>
                                <%--<li class=" "><a href="#bussss" data-toggle="tab">
                                    <span class="  sprte" aria-hidden="true"></span>
                                    <span class="hidden-xs"> Current</span> Booking
                                </a></li>--%>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="secndblak">
                    <div class="container"  >
                        <div class="col-md-12">
                            <!-- Tab panes -->
                            <div class="tab-content tabsbg" style="overflow:hidden">
                            
                                <div class="tab-pane active" id="home">
                                    <Search:commissionmaster ID="commissionmaster" runat="server"/>
                                </div>
                                <div class="tab-pane" id="profile">
                                    <Search1:DealCodeMaster runat="server" ID="DealCodeMaster"/>
                                </div>
                                <div class="tab-pane" id="bus">
                                    <Search2:MISCSRVCHARGE runat="server" ID="MISCSRVCHARGE"/>
                                </div>
                                <div class="tab-pane" id="car">
                                <%--    <uc1:DashBoard runat="server" ID="DashBoard" />--%>
                                </div>
                                <%--<uc1:HotelSearch runat="server" ID="HotelSearch" />--%>
                            </div>
                            <!-- Tab panes -->
                        </div>
                    </div>
                </div>
            </div>
    </div>
      




</asp:Content>

