﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.Net.Mail
Imports System.IO
Partial Class Invoiceflt
    Inherits System.Web.UI.Page
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    Dim ds As New DataSet
    Dim dt As New DataTable
    Dim sql As New SqlTransaction
    Dim adult As Double = 0
    Dim child As Double = 0
    Dim infant As Double = 0
    Dim totalfare As Double = 0
    Dim adulttax As Double = 0
    Dim childtax As Double = 0
    Dim infanttax As Double = 0
    Dim agentmrk As Double = 0
    Dim adminmrk As Double = 0
    Dim totalfaretax As Double = 0
    Dim total As Double = 0
    Dim GrandTotal As Double = 0
    Dim TotalST As Double = 0
    Dim TDS As Double = 0
    Dim CB As Double = 0
    Dim Dis As Double = 0
    Dim gdspnr As String = ""
    Private I As New Invoice()
    Dim ST As New SqlTransaction()
    Dim clsCorp As New ClsCorporate()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Try
                ''lbl_IntInvoice.Text = clsCorp.ShowInvoice(Request.QueryString("OrderId").ToString())
                ShowInvoice(Request.QueryString("OrderId").ToString())
                ''AgentAddress()
            Catch ex As Exception
                clsErrorLog.LogInfo(ex)
            End Try
        End If
    End Sub
    Public Function ShowInvoice(ByVal orderId As String) As String

        Dim adult As Double = 0
        Dim child As Double = 0
        Dim infant As Double = 0
        Dim totalfare As Double = 0
        Dim adulttax As Double = 0
        Dim childtax As Double = 0
        Dim infanttax As Double = 0
        Dim agentmrk As Double = 0
        Dim adminmrk As Double = 0
        Dim totalfaretax As Double = 0
        Dim total As Double = 0
        Dim GrandTotal As Double = 0
        Dim TotalST As Double = 0
        Dim TDS As Double = 0
        Dim CB As Double = 0
        Dim Dis As Double = 0
        Dim gdspnr As String = ""
        Dim result As String = ""
        Dim my_table As String = ""
        Try
            If (orderId <> "" AndAlso orderId IsNot Nothing) Then
                Dim id As String = orderId 'HttpContext.Current.Request.QueryString("OrderId").ToString()
                ds = sql.GetInvoice(id)
                dt = ds.Tables(0)
                Dim dtflt As New DataTable
                dtflt = ds.Tables(1)

                Dim projID As String = ""
                Dim bookedBy As String = ""
                Dim billNo As String = ""
                Dim ReissueId As String = ""

                If Not IsDBNull(dt.Rows(0)("ProjectID")) Then

                    projID = dt.Rows(0)("ProjectID").ToString()
                End If

                If Not IsDBNull(dt.Rows(0)("BookedBy")) Then

                    bookedBy = dt.Rows(0)("BookedBy").ToString()
                End If

                If Not IsDBNull(dt.Rows(0)("BillNoCorp")) Then

                    billNo = dt.Rows(0)("BillNoCorp").ToString()
                End If
                If Not IsDBNull(dt.Rows(0)("ResuId")) Then

                    ReissueId = dt.Rows(0)("ResuId").ToString()
                End If


                Dim mgtFee As Double = 0
                Dim dtAAdd As DataTable
                ''dtAAdd = ST.GetAgencyDetails(dt.Rows(0)("AgentId").ToString()).Tables(0)
                my_table += "<div style='float: left; height: 50px; width: 100px;'><img alt='' src='http://111.118.185.114/UmrawatiTripadmin/AirLogo/sm" + dt.Rows(0)("VC").ToString() + ".gif' style='width:87px;height:28px'></img></div>"

                my_table += "<div style='margin-left: 401px; height: 175px; width: 200px;'>"
                my_table += "<table>"
                my_table += "<tr>"
                my_table += "      <td>Tax invoice</td>"
                my_table += " </tr>"
                my_table += "<tr>"
                my_table += "<td>(Original For Recipient)</td>"
                my_table += " </tr>"
                my_table += " </table>"
                my_table += "</div>"
                Dim dtAgent As DataTable = sql.GetAgencyDetails(dt.Rows(0)("AgentId").ToString()).Tables(0)
                my_table += "<div style='float: right; height: 175px; width: 200px; margin-top: -176px;'>"
                my_table += "<table>"
                my_table += "<tr>"
                my_table += "<td>" & dtAgent.Rows(0)("Agency_Name").ToString() & "</td>"
                my_table += "</tr>"
                my_table += "<tr>"
                my_table += "<td>" & dtAgent.Rows(0)("Address").ToString() & "</td>"
                my_table += "</tr>"
                my_table += "<tr>"
                my_table += "<td>" & dtAgent.Rows(0)("City").ToString & "</td>"
                my_table += "</tr>"
                my_table += "<tr>"
                my_table += "<td>" & dtAgent.Rows(0)("Zipcode").ToString & "</td>"
                my_table += "</tr>"
                my_table += "<tr>"
                my_table += "<td>" & dtAgent.Rows(0)("State").ToString & "</td>"
                my_table += "</tr>"
                my_table += "<tr>"
                my_table += "  <td>" & dtAgent.Rows(0)("Country").ToString() & "</td>"
                my_table += "</tr>"
                my_table += "</table>"
                my_table += "</div>"
                my_table += "<div style='height: 234px; width: 1000px;'>"
                my_table += "<table>"
                my_table += "<tr style='height: 19px'>"
                my_table += " <td style='font-weight: bold'>Number:</td>"
                my_table += "<td>" & dt.Rows(0)("TicketNumber").ToString() & "</td>"
                my_table += "   </tr>"
                my_table += " <tr style='height:32px'>"
                Dim strDepdt As String = Convert.ToString(dtflt.Rows(0)("DepDate").ToString())
                Try
                    strDepdt = IIf(strDepdt.Length = 8, STD.BAL.Utility.Left(strDepdt, 4) & "-" & STD.BAL.Utility.Mid(strDepdt, 4, 2) & "-" & STD.BAL.Utility.Right(strDepdt, 2), "20" & STD.BAL.Utility.Right(strDepdt, 2) & "-" & STD.BAL.Utility.Mid(strDepdt, 2, 2) & "-" & STD.BAL.Utility.Left(strDepdt, 2))
                    Dim deptdt As DateTime = Convert.ToDateTime(strDepdt)
                    strDepdt = deptdt.ToString("dd/MMM/yy").Replace("-", "/")
                    Dim depDay As String = Convert.ToString(deptdt.DayOfWeek)
                    strDepdt = strDepdt.Split("/")(0) + " " + strDepdt.Split("/")(1) + " " + strDepdt.Split("/")(2)
                Catch ex As Exception
                    clsErrorLog.LogInfo(ex)
                End Try

                my_table += " <td  style='font-weight: bold'>Date :</td>"
                my_table += "<td>" & strDepdt & "</td>"
                my_table += "   </tr>"
                my_table += "<tr style='height: 24px'>"
                my_table += "<td  style='font-weight: bold'>Passenger name:</td>"
                For j As Integer = 0 To dt.Rows.Count - 1


                    my_table += "<td>" & dt.Rows(0)("FName").ToString() & " " & dt.Rows(0)("MName").ToString() & " " & dt.Rows(0)("LName").ToString() & "</td>"
                    my_table += ","
                Next
                my_table += "</tr>"
                my_table += "<tr style='height: 38px'>"
                my_table += "<td  style='font-weight: bold'>Pnr:</td>"
                my_table += "<td>" & dt.Rows(0)("GdsPnr").ToString() & "</td>"
                my_table += "<td style='padding-left: 50px;font-weight: bold'>Flight no:</td>"
                my_table += "<td>" & dtflt.Rows(0)("FltNumber").ToString() & "</td>"

                Dim sector() As String = (dt.Rows(0)("sector").ToString()).Split(":")

                my_table += "<td style='padding-left: 50px;font-weight: bold'>From:</td>"
                my_table += "<td>" & sector(0).ToString() & "</td>"
                my_table += "<td style='padding-left: 50px;font-weight: bold'>To:</td>"
                my_table += "<td>" & sector(1).ToString() & "</td>"
                my_table += "<td style='padding-left: 50px;font-weight: bold' >Place of Supply:</td>"
                my_table += "<td >Delhi</td>"
                my_table += "</tr>"
                my_table += "<tr style='height: 29px'>"
                my_table += "<td  style='font-weight: bold'>GSTIN in Customer :</td>"
                my_table += "</tr>"
                my_table += "<tr style='height: 32px'>"
                my_table += "<td  style='font-weight: bold'>GSTIN in Customer :</td>"
                my_table += "</tr>"
                my_table += "<tr style='height: 54px'>"
                my_table += "<td  style='font-weight: bold'>Currency:INR</td>"
                my_table += "</tr>"
                my_table += "</table>"
                my_table += "</div>"
                my_table += "<div style='clear: both'>&nbsp;</div>"
                my_table += "<div>"
                my_table += "<table style='width:97%;border-collapse:collapse;font-size:small;font-weight:bold;margin-left: 14px;'>"
                my_table += "<tr>"
                my_table += "<td style='border: thin solid #000000; padding-left: 60px'>Description</td>"
                my_table += "<td style='border: thin solid #000000; padding-left: 8px'>SAC Code</td>"
                my_table += "<td style='border: thin solid #000000; padding-left: 11px;width: 7%'>Taxable Value</td>"
                my_table += "<td style='border: thin solid #000000; padding-left: 8px;width: 8%'>NonTaxable /Exempted Value</td>"
                my_table += "<td style='border: thin solid #000000;padding-left: 26px'>Total</td>"
                my_table += "<td style='border: thin solid #000000;width: 8%;padding-left:23px'>IGST"
                my_table += "<table style='border-collapse:collapse;margin-left: -23px;line-height:35px' cssClass='table'>"
                my_table += "<tr>"
                my_table += "<td style='border-style: solid solid none none; border-width: thin; border-color: #000000;'>Tax%</td>"
                my_table += "<td style='border-style: solid none none none; border-width: thin; border-color: #000000;'>Amount</td>"
                my_table += " </tr>"
                my_table += " </table>"
                my_table += "</td>"

                my_table += " <td style='border: thin solid #000000;width: 8%;padding-left: 18px'>CGST"
                my_table += " <table style='border-collapse:collapse;line-height: 36px;margin-left: -20px'>"
                my_table += "<tr>"
                my_table += " <td style='border-style: solid solid none none; border-width: thin; border-color: #000000;'>Tax%</td>"
                my_table += " <td style='border-style: solid none none none; border-width: thin; border-color: #000000;'>Amount</td>"
                my_table += "</tr>"
                my_table += "</table>"
                my_table += "</td>"

                my_table += "<td style='border: thin solid #000000;width: 8%;padding-left: 5px'>SGST/UGST"
                my_table += "<table style='border-collapse:collapse;line-height: 36px;margin-left:-7px'>"
                my_table += "<tr>"
                my_table += "<td style='border-style: solid solid none none; border-width: thin; border-color: #000000;'>Tax%</td>"
                my_table += "<td style='border-style: solid none none solid; border-width: thin; border-color: #000000;'>Amount</td>"
                my_table += "</tr>"
                my_table += "</table>"
                my_table += "</td>"

                my_table += "<td style='border: thin solid #000000;padding-left:26px'>Total(Incl Taxes)</td>"
                my_table += "</tr>"

                my_table += "<tr>"
                my_table += "<td style='border: thin solid #000000'>Air Travel and related charges</td>"
                my_table += "<td style='border: thin solid #000000;padding-left: 19px'>996425</td>"
                my_table += "<td style='border: thin solid #000000;padding-left: 19px'>1,857.00</td>"
                my_table += "<td style='border: thin solid #000000;padding-left: 19px'>0.00</td>"
                my_table += "<td style='border: thin solid #000000;padding-left: 19px'>1,857.00</td>"
                my_table += "<td style='border: thin solid #000000;padding-left: -8px'>"
                my_table += "<table style='border-collapse:collapse'>"
                my_table += "<tr>"
                my_table += "<td style='border-style: none solid none none; border-width: thin; border-color: #000000;padding-left: 11px'>0.00</td>"
                my_table += "<td style='border-style: none none none solid; border-width: thin; border-color: #000000;padding-left: 19px'>0.00</td>"
                my_table += "</tr>"
                my_table += "</table>"
                my_table += "</td>"
                my_table += "<td style='border: thin solid #000000'>"
                my_table += "<table style='border-collapse:collapse'>"
                my_table += "<tr>"
                my_table += "<td style='border-style: none solid none none; border-width: thin; border-color: #000000;padding-left: 9px'>0.00</td>"
                my_table += "<td style='border-style: none none none solid; border-width: thin; border-color: #000000;padding-left: 19px'>0.00</td>"
                my_table += "</tr>"
                my_table += "</table>"
                my_table += "</td>"
                my_table += "<td style='border: thin solid #000000'>"
                my_table += "<table style='border-collapse:collapse'>"
                my_table += "<tr>"
                my_table += "<td style='border-style: none solid none none; border-width: thin; border-color: #000000;padding-left: 9px'>0.00</td>"
                my_table += "<td style='border-style: none none none solid; border-width: thin; border-color: #000000;padding-left: 19px'>0.00</td>"
                my_table += "</tr>"
                my_table += "</table>"
                my_table += "</td>"

                my_table += "<td style='border: thin solid #000000;padding-left: 75px'>1,949.00</td>"
                my_table += "</tr>"
                my_table += "<tr>"
                my_table += "<td style='border: thin solid #000000'>Airport Charges</td>"
                my_table += "<td style='border: thin solid #000000'></td>"
                my_table += "<td style='border: thin solid #000000;padding-left: 19px'>1,857.00</td>"
                my_table += "<td style='border: thin solid #000000;padding-left: 19px'>0.00</td>"
                my_table += "<td style='border: thin solid #000000;padding-left: 19px'>1,857.00</td>"
                my_table += "<td style='border: thin solid #000000;padding-left: 1px'>"
                my_table += "<table style='border-collapse:collapse'>"
                my_table += "<tr>"
                my_table += "<td style='border-style: none solid none none; border-width: thin; border-color: #000000;padding-left: 11px'>0.00</td>"
                my_table += "<td style='border-style: none none none solid; border-width: thin; border-color: #000000;padding-left: 19px'>0.00</td>"
                my_table += "</tr>"
                my_table += "</table>"
                my_table += "</td>"
                my_table += "<td style='border: thin solid #000000'>"
                my_table += "<table style='border-collapse:collapse'>"
                my_table += "<tr>"
                my_table += "<td style='border-style: none solid none none; border-width: thin; border-color: #000000;padding-left: 9px'>0.00</td>"
                my_table += "<td style='border-style: none none none solid; border-width: thin; border-color: #000000;padding-left: 19px'>0.00</td>"
                my_table += "</tr>"
                my_table += "</table>"
                my_table += "</td>"
                my_table += "            <td style='border: thin solid #000000'>"
                my_table += "                <table style='border-collapse:collapse'>"
                my_table += "<tr>"
                my_table += "          <td style='border-style: none solid none none; border-width: thin; border-color: #000000;padding-left: 9px'>0.00</td>"
                my_table += "                         <td style='border-style: none none none solid; border-width: thin; border-color: #000000;padding-left: 19px'>0.00</td>"
                my_table += "            </tr>"
                my_table += "               </table>"
                my_table += "           </td>"

                my_table += "              <td style='border: thin solid #000000;padding-left: 75px'>1,949.00</td>"
                my_table += "           </tr>"

                my_table += "<tr>"

                my_table += "<td style='border-style: solid none solid solid; border-width: thin; border-color: #000000;height: 50px;padding-left: 120px'>Grand Total</td>"
                my_table += "             <td style='border-style: solid solid solid none; border-width: thin; border-color: #000000;'></td>"
                my_table += "     <td style='border: thin solid #000000;padding-left: 19px'>1,857.0</td>"
                my_table += "      <td style='border: thin solid #000000;padding-left: 19px'>263.0</td>"
                my_table += "      <td style='border: thin solid #000000;padding-left: 19px'>2,120.00</td>"
                my_table += "      <td style='border: thin solid #000000;padding-left: 19px' class='auto-style3'>0.00</td>"
                my_table += "       <td style='border: thin solid #000000;padding-left: 19px' class='auto-style12'>46.00</td>"
                my_table += "       <td style='border: thin solid #000000;padding-left: 19px' class='auto-style13'>46.00</td>"
                my_table += "       <td style='border: thin solid #000000;padding-left: 75px'>2,212.00</td>"

                my_table += "    </tr>"
                my_table += " </table>"
                my_table += "</div>"
                my_table += "<div>"
                my_table += "<p><span  style='font-weight: bold'>1. Air Travel and related Charges :-</span> Includes all Charges related to air transportation of passengers"
                my_table += " <br/><span  style='font-weight: bold'>2. Airport Charges :-</span> Includes ADF,UDF,PSF and other airport charges collected on behalf of Airport Operator,as applicable"
                my_table += "<br/><span  style='font-weight: bold'>3. Misc. Services :-</span> Includes Charges of Lounge,Medical Assistance and Travel Certificate"
                my_table += "<br/><span  style='font-weight: bold'>4. Meal :-</span> Includes all prepaid meals purchased before travel"
                my_table += "<br/><span  style='font-weight: bold'>5. Good Karma :-</span> Includes contributions made towards IndiGo's Good Karma initiative and Clear the Air initiative<br/>made at the time of reservation"
                my_table += "<br/>6. Amounts have been rounded off.</p>"
                my_table += "</div>"
                my_table += "<div style='height:134px;width:226px'>"
                my_table += "<table>"
                my_table += "<tr><td></td></tr>"
                my_table += "<tr><td>Authorised Signatory</td></tr>"
                my_table += "</table>"
                my_table += "</div>"
                my_table += "<div>"
                my_table += "<table>"
                my_table += "<tr><td style='font-weight: bold'>travel villa</td></tr>"
                my_table += "<tr><td>travel@gmail.com</td></tr>"
                my_table += "<tr><td>655654646</td></tr>"
                my_table += "<tr><td>delhi</td></tr>"
                my_table += "<tr><td>121154</td></tr>"
                my_table += "</table>"
                my_table += "</div>"
                invdiv.InnerHtml = my_table

            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
        Return result
    End Function

    Protected Sub btn_PDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_PDF.Click
        Dim filename As String = ""
        filename = "PackageReport.pdf"
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=" & filename & "")
        Response.Charset = ""
        Response.ContentType = "application/pdf"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        invdiv.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub

    Protected Sub btn_Word_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Word.Click
        Try

            Dim filename As String = ""
            filename = "InvoiceDetail.doc"
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=" & filename & "")
            Response.Charset = ""
            Response.ContentType = "application/doc"
            Dim stringWrite As New System.IO.StringWriter()
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            invdiv.RenderControl(htmlWrite)
            Response.Write(stringWrite.ToString())
            Response.[End]()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub

    Protected Sub btn_Excel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Excel.Click
        Try
            Dim filename As String = ""
            filename = "InvoiceDetail.xls"
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=" & filename & "")

            Response.Charset = ""
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter()
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
            invdiv.RenderControl(htmlWrite)
            Response.Write(stringWrite.ToString())
            Response.[End]()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub

    Protected Sub btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn.Click
        Try
            Dim sw As New StringWriter()
            Dim w As New HtmlTextWriter(sw)
            invdiv.RenderControl(w)
            Dim s As String = sw.GetStringBuilder().ToString()
            Dim MailDt As New DataTable
            Dim STDOM As New SqlTransactionDom
            MailDt = STDOM.GetMailingDetails(MAILING.AIR_INVOICE.ToString(), Session("UID").ToString()).Tables(0)
            Dim email As String = Request("txt_email")
            If (MailDt.Rows.Count > 0) Then
                Dim Status As Boolean = False
                Status = Convert.ToBoolean(MailDt.Rows(0)("Status").ToString())
                Try
                    If Status = True Then
                        Dim i As Integer = STDOM.SendMail(txt_email.Text, MailDt.Rows(0)("MAILFROM").ToString(), MailDt.Rows(0)("BCC").ToString(), MailDt.Rows(0)("CC").ToString(), MailDt.Rows(0)("SMTPCLIENT").ToString(), MailDt.Rows(0)("UserId").ToString(), MailDt.Rows(0)("Pass").ToString(), s, MailDt.Rows(0)("SUBJECT").ToString(), "")
                        If i = 1 Then
                            mailmsg.Text = "Mail sent successfully."
                        Else
                            mailmsg.Text = "Unable to send mail.Please try again"
                        End If
                    End If
                    txt_email.Text = ""
                Catch ex As Exception
                    clsErrorLog.LogInfo(ex)
                    mailmsg.Text = ex.Message.ToString
                End Try
            Else
                mailmsg.Text = "Unable to send mail.Please contact to administrator"
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
End Class
