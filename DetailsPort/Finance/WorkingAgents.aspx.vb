﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class DetailsPort_Finance_WorkingAgents

    Inherits System.Web.UI.Page
    Private con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    Private dt As New DataTable()
    Private da As New SqlDataAdapter()

    Protected Sub btn_result_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_result.Click
        Try
            Dim FromDate As String
            Dim ToDate As String = ""
            If [String].IsNullOrEmpty(Request("From")) Then
                FromDate = ""
            Else

                FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "/" + Strings.Left((Request("From")).Split(" ")(0), 2) + "/" + Strings.Right((Request("From")).Split(" ")(0), 4)
                If (ddl_type.SelectedIndex > 0) Then
                    FromDate = FromDate + " " + "12:00:00 AM"
                Else
                    FromDate = FromDate + " " + "11:59:59 PM"
                End If
            End If

            If (ddl_type.SelectedIndex > 0) Then
                If [String].IsNullOrEmpty(Request("From")) Then
                    ToDate = ""
                Else

                    ToDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "/" + Strings.Left((Request("From")).Split(" ")(0), 2) + "/" + Strings.Right((Request("From")).Split(" ")(0), 4)
                    ToDate = ToDate + " " + "11:59:59 PM"
                End If
            End If
            dt.Clear()
            da = New SqlDataAdapter("SP_WORKINGAGENTS", con)
            da.SelectCommand.CommandType = CommandType.StoredProcedure
            da.SelectCommand.Parameters.AddWithValue("@DATE", FromDate)
            da.SelectCommand.Parameters.AddWithValue("@DATETO", ToDate)
            da.SelectCommand.Parameters.AddWithValue("@TYPE", ddl_type.SelectedValue)
            da.Fill(dt)
            BindGrid(dt)
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        Try
            GridView1.PageIndex = e.NewPageIndex
            BindGrid(Session("dt"))

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Private Sub BindGrid(ByVal dt As DataTable)
        Session("dt") = dt
        GridView1.Visible = True
        GridView1.DataSource = dt
        GridView1.DataBind()
        totcnt.InnerHtml = "<strong>Total Count : " & Convert.ToString(dt.Rows.Count - 1) & "</strong>"
    End Sub
    Protected Sub btn_export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_export.Click
        Try
            Dim FromDate As String
            Dim ToDate As String = ""
            If [String].IsNullOrEmpty(Request("From")) Then
                FromDate = ""
            Else

                FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "/" + Strings.Left((Request("From")).Split(" ")(0), 2) + "/" + Strings.Right((Request("From")).Split(" ")(0), 4)
                If (ddl_type.SelectedIndex > 0) Then
                    FromDate = FromDate + " " + "12:00:00 AM"
                Else
                    FromDate = FromDate + " " + "11:59:59 PM"
                End If
            End If

            If (ddl_type.SelectedIndex > 0) Then
                If [String].IsNullOrEmpty(Request("From")) Then
                    ToDate = ""
                Else

                    ToDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "/" + Strings.Left((Request("From")).Split(" ")(0), 2) + "/" + Strings.Right((Request("From")).Split(" ")(0), 4)
                    ToDate = ToDate + " " + "11:59:59 PM"
                End If
            End If
            Dim ds As New DataSet
            da = New SqlDataAdapter("SP_WORKINGAGENTS", con)
            da.SelectCommand.CommandType = CommandType.StoredProcedure
            da.SelectCommand.Parameters.AddWithValue("@DATE", FromDate)
            da.SelectCommand.Parameters.AddWithValue("@DATETO", ToDate)
            da.SelectCommand.Parameters.AddWithValue("@TYPE", ddl_type.SelectedValue)
            da.Fill(ds)
            Dim stdom As New SqlTransactionDom
            stdom.ExportData(ds)

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Or Session("UID") Is Nothing Then
            Response.Redirect("~/Login.aspx")
        End If
    End Sub
End Class
