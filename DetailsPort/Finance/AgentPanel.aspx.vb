﻿Imports System
Imports System.Collections.Generic

Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data.SqlClient
Imports System.Data
Imports System.Net.Mail
Imports System.Web.Security
Partial Class DetailsPort_Finance_AgentPanel

    Inherits System.Web.UI.Page

    Private STDom As New SqlTransactionDom
    Private ST As New SqlTransaction
    Private Distr As New Distributor
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        If Session("UID") = "" Or Session("UID") Is Nothing Then
            Response.Redirect("~/Login.aspx")
        End If
        Try
            If Not IsPostBack Then
                Dim DtAg As New DataTable
                DtAg = ST.GetAgencyDetails(Session("UID")).Tables(0)

                'Dim ds As New DataSet()
                'ds = upload.GetAgency(id)


                ddl_modepayment.AppendDataBoundItems = True
                ddl_modepayment.Items.Clear()
                ddl_modepayment.Items.Insert(0, "Select Payment Mode")
                ddl_modepayment.Items.Insert(1, "Cash")
                ddl_modepayment.Items.Insert(2, "Cash Deposite In Bank")
                ddl_modepayment.Items.Insert(3, "Cheque")
                ddl_modepayment.Items.Insert(4, "NetBanking")
                ddl_modepayment.Items.Insert(5, "RTGS")
                Dim DtBank As New DataTable
                DtBank = STDom.BankInformation(Session("UID")).Tables(0)
                ddl_BankName.AppendDataBoundItems = True
                ddl_BankName.Items.Clear()
                ddl_BankName.Items.Insert(0, "--Select Bank--")
                ddl_BankName.DataSource = DtBank
                ddl_BankName.DataTextField = "BankName"
                ddl_BankName.DataValueField = "BankName"
                ddl_BankName.DataBind()

                Dim DtOffice As New DataTable
                DtOffice = Distr.GetDepositeOffice(Session("UID")).Tables(0)
                ddl_Office.AppendDataBoundItems = True
                ddl_Office.Items.Clear()
                ddl_Office.Items.Insert(0, "--Select--")
                ddl_Office.DataSource = DtOffice
                ddl_Office.DataTextField = "OFFICE"
                ddl_Office.DataValueField = "OFFICE"
                ddl_Office.DataBind()
            End If
            Try
                Dim dtdist As New DataTable()
                dtdist = STDom.GetAgencyDetails(Session("UID")).Tables(0)
                Dim DistrId = dtdist.Rows(0)("Distr").ToString()
                If (DistrId.ToUpper = "SPRING") Then
                    td_UploadType.Visible = True
                End If
            Catch ex As Exception

            End Try

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Protected Sub ddl_modepayment_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddl_modepayment.SelectedIndexChanged
        Try
            If ddl_modepayment.SelectedItem.Text = "Cheque" Then
                check_info.Visible = True
                td_Bank.Visible = True
                td_Bank1.Visible = True
                td_BACode.Visible = True
                td_BACode1.Visible = True
                td_BCode.Visible = False
                td_BCode1.Visible = False
                td_transid.Visible = False
                td_transid1.Visible = False
                td_BranchAcc.Visible = False
                div_Bankinfo.Visible = False
                tr_Deposite.Visible = False
                tr_conper.Visible = False
                ddl_BankName.SelectedIndex = 0

            End If
            If ddl_modepayment.SelectedItem.Text = "Cash" Then
                tr_Deposite.Visible = True
                tr_conper.Visible = True
                check_info.Visible = False
                td_Bank.Visible = False
                td_Bank1.Visible = False
                td_BACode.Visible = False
                td_BACode1.Visible = False

                td_BCode.Visible = False
                td_BCode1.Visible = False
                td_transid.Visible = False
                td_transid1.Visible = False
                td_BranchAcc.Visible = False
                div_Bankinfo.Visible = False
                ddl_BankName.SelectedIndex = 0

            End If
            If ddl_modepayment.SelectedItem.Text = "Select Payment Mode" Then
                tr_Deposite.Visible = False
                tr_conper.Visible = False
                check_info.Visible = False
                td_Bank.Visible = False
                td_Bank1.Visible = False
                td_BACode.Visible = False
                td_BACode1.Visible = False
                td_BCode.Visible = False
                td_BCode1.Visible = False
                td_transid.Visible = False
                td_transid1.Visible = False
                td_BranchAcc.Visible = False
                div_Bankinfo.Visible = False
                ddl_BankName.SelectedIndex = 0
            End If
            If ddl_modepayment.SelectedItem.Text = "Cash Deposite In Bank" Then
                td_BCode.Visible = True
                td_BCode1.Visible = True
                td_Bank.Visible = True
                td_Bank1.Visible = True
                td_BACode.Visible = True
                td_BACode1.Visible = True
                td_BranchAcc.Visible = False
                div_Bankinfo.Visible = False
                check_info.Visible = False
                tr_Deposite.Visible = False
                tr_conper.Visible = False
                td_transid.Visible = False
                td_transid1.Visible = False
                ddl_BankName.SelectedIndex = 0

            End If

            If ddl_modepayment.SelectedItem.Text = "NetBanking" Or ddl_modepayment.SelectedItem.Text = "RTGS" Then
                td_transid.Visible = True
                td_transid1.Visible = True
                td_Bank.Visible = True
                td_Bank1.Visible = True
                td_BACode.Visible = True
                td_BACode1.Visible = True
                td_BranchAcc.Visible = False
                div_Bankinfo.Visible = False
                check_info.Visible = False
                tr_Deposite.Visible = False
                tr_conper.Visible = False
                td_BCode.Visible = False
                td_BCode1.Visible = False
                ddl_BankName.SelectedIndex = 0

            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Protected Sub btn_submitt_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_submitt.Click
        Try
            Dim DtAg As New DataTable
            DtAg = ST.GetAgencyDetails(Session("UID")).Tables(0)
            Dim AgencyName As String = DtAg.Rows(0)("Agency_Name").ToString
            Dim SalesExecID As String = DtAg.Rows(0)("SalesExecID").ToString
            Dim AgentType As String = DtAg.Rows(0)("Agent_Type").ToString
            Dim pendingStatus As String = If(RBL_UploadType.SelectedValue = "AD", "ADPending", "Pending")
            If ddl_modepayment.SelectedValue = "Cash" Then
                STDom.insertDeposite(AgencyName, Session("UID").ToString, txt_amount.Text.Trim, ddl_modepayment.SelectedValue, "", "", "", "", "", "", "", txt_city.Text.Trim, txt_depositedate.Text.Trim, txt_remark.Text.Trim, pendingStatus, RBL_UploadType.SelectedValue, ddl_Office.SelectedValue, txt_concernperson.Text.Trim, txt_ReceiptNo.Text.Trim, "", "", SalesExecID, AgentType)
            End If
            If ddl_modepayment.SelectedValue = "Cash Deposite In Bank" Then
                STDom.insertDeposite(AgencyName, Session("UID").ToString, txt_amount.Text.Trim, ddl_modepayment.SelectedValue, ddl_BankName.SelectedValue, ddl_Banch.SelectedValue, ddl_Account.SelectedValue, "", "", "", txt_areacode.Text.Trim, "", "", txt_remark.Text.Trim, pendingStatus, RBL_UploadType.SelectedValue, "", "", "", txt_BranchCode.Text.Trim, "", SalesExecID, AgentType)
            End If
            If ddl_modepayment.SelectedValue = "Cheque" Then
                STDom.insertDeposite(AgencyName, Session("UID").ToString, txt_amount.Text.Trim, ddl_modepayment.SelectedValue, ddl_BankName.SelectedValue, ddl_Banch.SelectedValue, ddl_Account.SelectedValue, txt_chequeno.Text.Trim, txt_chequedate.Text.Trim, "", txt_areacode.Text.Trim, "", "", txt_remark.Text.Trim, pendingStatus, RBL_UploadType.SelectedValue, "", "", "", "", txt_BankName.Text.Trim, SalesExecID, AgentType)
            End If
            If ddl_modepayment.SelectedValue = "NetBanking" Or ddl_modepayment.SelectedValue = "RTGS" Then
                STDom.insertDeposite(AgencyName, Session("UID").ToString, txt_amount.Text.Trim, ddl_modepayment.SelectedValue, ddl_BankName.SelectedValue, ddl_Banch.SelectedValue, ddl_Account.SelectedValue, "", "", txt_tranid.Text.Trim, txt_areacode.Text.Trim, "", "", txt_remark.Text.Trim, pendingStatus, RBL_UploadType.SelectedValue, "", "", "", "", "", SalesExecID, AgentType)
            End If


            ddl_modepayment.SelectedIndex = 0
            txt_amount.Text = ""
            tr_Deposite.Visible = False
            tr_conper.Visible = False
            check_info.Visible = False
            td_Bank.Visible = False
            td_Bank1.Visible = False
            td_BACode.Visible = False
            td_BACode1.Visible = False
            td_BCode.Visible = False
            td_BCode1.Visible = False
            td_transid.Visible = False
            td_transid1.Visible = False
            td_BranchAcc.Visible = False
            div_Bankinfo.Visible = False
            ddl_BankName.SelectedIndex = 0
            txt_remark.Text = ""
            'Dim curr_date2 = Now.Date() & " " & "12:00:00 AM"
            'Dim curr_date3 = Now()
            'grd_deposit.DataSource = STDom.GetDepositDetails(curr_date2, curr_date3, Session("UID"))
            'grd_deposit.DataBind()
            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Upload Request Sent Sucessfully');", True)


        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Public Sub ResetAll()
        txt_amount.Text = ""
        ddl_BankName.SelectedIndex = 0
        ddl_Banch.SelectedIndex = 0
        ddl_Account.SelectedIndex = 0
        txt_chequeno.Text = ""
        txt_chequedate.Text = ""
        txt_tranid.Text = ""
        txt_areacode.Text = ""
        txt_city.Text = ""
        txt_remark.Text = ""
    End Sub

    Public Sub sendmail()
        Try
            Dim DtAg As New DataTable
            DtAg = ST.GetAgencyDetails(Session("UID")).Tables(0)
            Dim AgencyName As String = DtAg.Rows(0)("Agency_Name").ToString
            Dim mytable As String = ""
            mytable += "<table cellspacing='2' cellpadding='2' width='90%' border='1' border-width='thin'>"
            mytable += "<tr>"
            mytable += "<td>"
            mytable += "Agency Name:"
            mytable += "</td>"
            mytable += "<td>"
            mytable += "" + AgencyName + ""
            mytable += "</td>"
            mytable += "<td>"
            mytable += "Agency ID:"
            mytable += "</td>"
            mytable += "<td>"
            mytable += "" + Session("UID").ToString + ""
            mytable += "</td>"
            mytable += "</tr>"
            mytable += "<tr>"
            mytable += "<td>"
            mytable += "Amount:"
            mytable += "</td>"
            mytable += "<td>"
            mytable += "" + txt_amount.Text + ""
            mytable += "</td>"
            mytable += "<td>"
            mytable += "Mode Of Payment:"
            mytable += "</td>"
            mytable += "<td>"
            mytable += "" + ddl_modepayment.SelectedItem.Text + ""
            mytable += "</td>"
            mytable += "</tr>"
            mytable += "<tr>"
            mytable += "<td>"
            mytable += "Bank Name:"
            mytable += "</td>"
            mytable += "<td>"
            mytable += "" + ddl_BankName.SelectedValue + ""
            mytable += "</td>"
            mytable += "<td>"
            mytable += "Cheque No:"
            mytable += "</td>"
            mytable += "<td>"
            mytable += "" + txt_chequeno.Text + ""
            mytable += "</td>"
            mytable += "</tr>"
            mytable += "<tr>"
            mytable += "<td>"
            mytable += "Cheque Date:"
            mytable += "</td>"
            mytable += "<td>"
            mytable += "" + txt_chequedate.Text + ""
            mytable += "</td>"
            mytable += "<td>"
            mytable += "Transaction ID:"
            mytable += "</td>"
            mytable += "<td>"
            mytable += "" + txt_tranid.Text + ""
            mytable += "</td>"
            mytable += "</tr>"
            mytable += "<tr>"
            mytable += "<td>"
            mytable += "Bank AreaCode:"
            mytable += "</td>"
            mytable += "<td>"
            mytable += "" + txt_areacode.Text + ""
            mytable += "</td>"
            mytable += "<td>"
            mytable += "Deposit City:"
            mytable += "</td>"
            mytable += "<td>"
            mytable += "" + txt_city.Text + ""
            mytable += "</td>"
            mytable += "</tr>"
            mytable += "</table>"
            Dim email As New MailMessage()
            email.From = New MailAddress("b2bsalessupport@UmrawatiTrip.com ")
            email.[To].Add("jeet@UmrawatiTrip.com ")
            email.Subject = "Payment Details"
            email.IsBodyHtml = True
            email.Body = mytable
            Dim smtp As New SmtpClient()
            smtp.Host = "203.185.191.71"
            smtp.Port = 25
            smtp.Credentials = New System.Net.NetworkCredential("b2badmin@UmrawatiTrip.com ", "america")
            smtp.Send(email)

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub


    Protected Sub ddl_BankName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_BankName.SelectedIndexChanged
        Try
            If ddl_BankName.SelectedValue = "--Select Bank--" Then
                td_BranchAcc.Visible = False
                div_Bankinfo.Visible = False
            Else
                td_BranchAcc.Visible = True
                div_Bankinfo.Visible = True
            End If

            Dim Branchdt As New DataTable
            Branchdt = STDom.GetBranchAccount(ddl_BankName.SelectedValue, "Branch" & Session("UID")).Tables(0)
            Dim Accdt As New DataTable
            Accdt = STDom.GetBranchAccount(ddl_BankName.SelectedValue, "Acc" & Session("UID")).Tables(0)

            'ddl_Banch.AppendDataBoundItems = True
            'ddl_Banch.Items.Clear()
            'ddl_Banch.Items.Insert(0, "--Select Branch--")
            ddl_Banch.DataSource = Branchdt
            ddl_Banch.DataTextField = "BranchName"
            ddl_Banch.DataValueField = "BranchName"
            ddl_Banch.DataBind()

            'ddl_Account.AppendDataBoundItems = True
            'ddl_Account.Items.Clear()
            'ddl_Account.Items.Insert(0, "--Select Account--")
            ddl_Account.DataSource = Accdt
            ddl_Account.DataTextField = "AccountNumber"
            ddl_Account.DataValueField = "AccountNumber"
            ddl_Account.DataBind()

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
End Class
