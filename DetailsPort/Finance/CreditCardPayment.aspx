<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false"
    CodeFile="CreditCardPayment.aspx.vb" Inherits="DetailsPort_Finance_CreditCardPayment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../../css/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />
    <style>
        input[type="text"], input[type="password"], select
        {
            border: 1px solid #808080;
            padding: 2px;
            font-size: 1em;
            color: #444;
            width: 150px;
            font-family: arial, Helvetica, sans-serif;
            font-size: 12px;
            font-weight: normal;
            border-radius: 3px 3px 3px 3px;
            -webkit-border-radius: 3px 3px 3px 3px;
            -moz-border-radius: 3px 3px 3px 3px;
            -o-border-radius: 3px 3px 3px 3px;
        }
    </style>

    <script language="javascript" type="text/javascript">
        function validate() {
            if (document.getElementById("ctl00_ContentPlaceHolder1_txtFName").value == "") {
                alert("Please Enter FirstName");
                document.getElementById("ctl00_ContentPlaceHolder1_txtFName").focus();
                return false;

            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_txtLName").value == "") {
                alert("Please Enter LastName");
                document.getElementById("ctl00_ContentPlaceHolder1_txtLName").focus();
                return false;

            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_txtAmount").value == "") {
                alert("Please Enter Amount");
                document.getElementById("ctl00_ContentPlaceHolder1_txtAmount").focus();
                return false;

            }
            var amt = document.getElementById("ctl00_ContentPlaceHolder1_txtAmount").value;
            var AAmt = Math.round(((amt * 2) / 100), 0);
            var SrvTax = ((AAmt * 14) / 100).toFixed(2);
            var OAmt = parseFloat(amt) - parseFloat(parseFloat(AAmt) + parseFloat(SrvTax));
            if (confirm("Upload amount on your UmrawatiTrip account will be Rs. " + OAmt + ""))
                return true;
            return false;

        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ((charCode >= 48 && charCode <= 57) || (charCode == 8)) {
                return true;
            }
            else {

                return false;
            }
        }
        function validatecalc() {
            if (document.getElementById("ctl00_ContentPlaceHolder1_txtAmount").value == "") {
                alert("Please Enter Amount");
                document.getElementById("ctl00_ContentPlaceHolder1_txtAmount").focus();
                return false;

            }
        }
    </script>

    <div id="Div_PaymentId" runat="server">
        <table cellspacing="10" cellpadding="0" border="0" align="center" class="tbltbl"
            width="600px">
            <tr>
                <td class="text1" width="130px">
                    First Name:
                </td>
                <td>
                    <asp:TextBox ID="txtFName" runat="server"></asp:TextBox>
                </td>
                <td class="text1">
                    Last Name:
                </td>
                <td>
                    <asp:TextBox ID="txtLName" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="text1">
                    Amount:
                </td>
                <td colspan="3">
                    <asp:TextBox ID="txtAmount" runat="server" onKeyPress="return isNumberKey(event)"
                        Width="80px" MaxLength="6"></asp:TextBox>
                    &nbsp;<asp:Button ID="btn_Calculate" runat="server" Text="Calculate Payment Fee"
                        CssClass="Text" BorderColor="Black" BorderWidth="0px" Height="30px" Width="160px"
                        OnClientClick="return validatecalc();" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="3" id="td_Transchrg" runat="server" visible="false" class="Textsmall"
                    style="font-weight: bold; color: #FF0000">
                    PLEASE NOTE:&nbsp;Additional Payment Fee of Rs.
                    <asp:Label ID="lbl_ccamt" runat="server"></asp:Label>
                    and service tax Rs.
                    <asp:Label ID="lbl_stax" runat="server"></asp:Label>
                    will be charged.
                </td>
            </tr>
            <tr>
                <td class="text1">
                    <%-- <asp:RadioButtonList ID="RBL_GatewayType" runat="server">
              <asp:ListItem Selected="True" Value=""></asp:ListItem>
              </asp:RadioButtonList>--%>
                    <img src="../../Images/payu.png" height="40px" border="0" />
                </td>
                <td align="right" colspan="3">
                    <img src="../../Images/mc_sc.png" />&nbsp;&nbsp; &nbsp;<img src="../../Images/visa_verify.png" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="1" align="left">
                    <asp:Button ID="btnPay" runat="server" Text="Continue" CssClass="button" OnClientClick="return validate();" />
                </td>
            </tr>
        </table>
    </div>
    <div id="Div_Response" runat="server" visible="false">
        <table cellspacing="10" cellpadding="0" border="0" align="center" class="tbltbl"
            width="450px">
            <tr>
                <td class="Text" style="font-weight: bold" colspan="2">
                    Credit card payment sucessfull.
                </td>
            </tr>
            <tr>
                <td class="Textsmall" width="250px">
                    Your Track Id :
                </td>
                <td id="td_trackid" runat="server" class="Textsmall" style="font-weight: bold">
                </td>
            </tr>
            <tr>
                <td class="Textsmall">
                    Your Payment Id :
                </td>
                <td id="td_PaymentId" runat="server" class="Textsmall" style="font-weight: bold">
                </td>
            </tr>
            <tr>
                <td class="Textsmall">
                    Credit amount :
                </td>
                <td id="td_creditamount" runat="server" class="Textsmall" style="font-weight: bold">
                </td>
            </tr>
            <tr>
                <td class="Textsmall">
                    Bank surcharge :
                </td>
                <td id="td_surcharge" runat="server" class="Textsmall" style="font-weight: bold">
                </td>
            </tr>
            <tr>
                <td class="Textsmall">
                    Debit amount:
                </td>
                <td id="td_Amount" runat="server" class="Textsmall" style="font-weight: bold">
                </td>
            </tr>
           
        </table>
    </div>
    <div id="Div_ResponseFail" runat="server" visible="false">
        <table cellspacing="10" cellpadding="0" border="0" align="center" class="tbltbl">
            <tr>
                <td height="200px" class="Textsmall" style="font-weight: bold">
                    Credit card payment failed.Please contact to administrator.
                </td>
            </tr>
        </table>
    </div>
    <div id="PgError" runat="server">
        <table cellspacing="10" cellpadding="0" border="0" align="center" class="tbltbl">
            <tr>
                <th class="Text">
                    Error Description
                </th>
            </tr>
            <tr class="tablebody">
                <td>
                    <table>
                        <tr>
                            <td width="215" class="Textsmall">
                                Paid Amount
                            </td>
                            <td id="td_Amt" runat="server">
                            </td>
                        </tr>
                        <tr>
                            <td class="Textsmall">
                                PaymentID
                            </td>
                            <td id="td_payment" runat="server">
                            </td>
                        </tr>
                        <tr>
                            <td class="Textsmall">
                                Track ID
                            </td>
                            <td id="td_track" runat="server">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                SessionId
                            </td>
                            <td>
                                <%=	session.sessionid %>
                            </td>
                        </tr>
                        <tr>
                            <td class="Textsmall">
                                Payment Status
                            </td>
                            <td id="td_PStatus" runat="server">
                                <%-- <font color="red"><b>
                                    <%=request("ErrorText") %> </font>--%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left">
                                <font color="red"><b>Sorry For Inconvenience Your Payment.We Are Not Able To Confirm<br />
                                    your Booking Due To Payment Failure. </b></font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
