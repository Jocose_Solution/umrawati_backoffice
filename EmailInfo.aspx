﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmailInfo.aspx.cs" Inherits="MailHtml" %>

<!DOCTYPE html>

<%--<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>--%>

<html>
<head>
    <title>Booking Details</title>
    <style type='text/css'>
        .maindiv {
            border: #20313f 1px solid;
            margin: 10px auto 10px auto;
            width: 650px;
            font-size: 12px;
            font-family: tahoma,Arial;
        }

        .text1 {
            color: #333333;
            font-weight: bold;
        }

        .pnrdtls {
            font-size: 12px;
            color: #333333;
            text-align: left;
            font-weight: bold;
        }

        .pnrdtls1 {
            font-size: 12px;
            color: #333333;
            text-align: left;
        }

        .bookdate {
            font-size: 11px;
            color: #CC6600;
            text-align: left;
        }

        .flthdr {
            font-size: 11px;
            color: #CC6600;
            text-align: left;
            font-weight: bold;
        }

        .fltdtls {
            font-size: 11px;
            color: #333333;
            text-align: left;
        }

        .text3 {
            font-size: 11px;
            padding: 5px;
            color: #333333;
            text-align: right;
        }

        .hdrtext {
            padding-left: 5px;
            font-size: 14px;
            font-weight: bold;
            color: #FFFFFF;
        }

        .hdrtd {
            background-color: #333333;
        }

        .lnk {
            color: #333333;
            text-decoration: underline;
        }

            .lnk:hover {
                color: #333333;
                text-decoration: none;
            }

        .contdtls {
            font-size: 12px;
            padding-top: 8px;
            padding-bottom: 3px;
            color: #333333;
            font-weight: bold;
        }

        .hrcss {
            color: #CC6600;
            height: 1px;
            text-align: left;
            width: 450px;
        }
    </style>

</head>
<body>
    <div class='large-12 medium-12 small-12'>
        <div class='large-12 medium-12 small-12' style='display: none'>
            <table>
                <tr>
                    <td id='td_adtcnt' class='hide'>1</td>
                    <td id='td_chdcnt' class='hide'>0</td>
                </tr>
            </table>
        </div>
        <div class='clear1'></div>
        <div class='large-8 medium-8 small-12 large-push-2 medium-push-2  bld blue center'><u>Booking Reference No. 802eeff2mTgA9ZlZ</u></div>
        <div class='clear1'></div>
        <div class='large-12 medium-12 small-12'>
            <%--<div class='large-2 medium-2 small-2 columns' style='background='http://UmrawatiTrip.co/images/nologo.png'; background-repeat: no-repeat;'></div> --%>
            <div class='large-2 medium-2 small-2 columns' style='background-repeat: no-repeat;'></div>
            <div class='large-10 medium-10 small-10 columns'>
                <div class='large-12 medium-12 small-12 bld'>Agency Information</div>
                <div class='large-12 medium-12 small-12'>Fly Wid Us</div>
                <div class='large-12 medium-12 small-12'>9999590749</div>
                <div class='large-12 medium-12 small-12'>paritoshsingh1984@gmail.com</div>
            </div>
        </div>
        <div class='clear1'></div>
        <div class='large-12 medium-12 small-12'>
            <div class='large-3 medium-3 small-6 columns  bld'>GDSPNR</div>
            <div class='large-3 medium-3 small-6 columns  '>G829079-FQ</div>
            <div class='large-3 medium-3 small-6 columns  bld'>Booking Date</div>
            <div class='large-3 medium-3 small-6 columns  '>25-04-17 1:15:34 PM</div>
            <div class='' clear1></div>
            <div class='large-3 medium-3 small-6 columns  bld'>AirlinePNR</div>
            <div class='large-3 medium-3 small-6 columns  '>G829079-FQ&nbsp; </div>
            <div class='large-3 medium-3 small-6 columns  bld'>Status</div>
            <div class='large-3 medium-3 small-6  columns '>Ticketed</div>
        </div>
        <div class='clear1'></div>
        <div class='large-12 medium-12 small-12 bld'>Traveller Information</div>
        <div class='clear1'></div>
        <div class='large-12 medium-12 small-12'>
            <div class=' large-4 medium-4 small-4   columns  bld'>Passenger Name</div>
            <div class=' large-4 medium-4 small-4   columns  bld'>Type</div>
            <div class=' large-4 medium-4 small-4   columns  bld'>Ticket No</div>
            <div class='clear1'></div>
            <div class='large-4 medium-4 small-4  columns '>Mr  dsdsd    dsdsdsd</div>
            <div class='large-4 medium-4 small-4  columns '>ADT</div>
            <div class='large-4 medium-4 small-4  columns '>543211</div>
            <div class='clear1'></div>
        </div>
        <div class='large-12 medium-12 small-12'>
            <hr />
        </div>
        <div class='clear1'></div>
        <div class='large-12 medium-12 small-12 bld'><u>Flight Information</u></div>
        <div class='clear1'></div>
        <div class='large-12 medium-12 small-12'>
            <div class='large-2 medium-2 small-3 columns  bld'>From - To</div>
            <div class='large-2 medium-2 small-3 columns  bld passenger'>&nbsp;</div>
            <div class='large-2 medium-2 small-3 columns  bld'>Depart Date</div>
            <div class='large-2 medium-2 small-3 columns  bld'>DepTime</div>
            <div class='large-2 medium-2 small-3 columns  bld'>ArrTime</div>
            <div class='large-2 medium-2 small-3 columns  bld passenger'>Aircraft</div>
        </div>
        <div class='clear1'></div>
        <div class='large-12 medium-12 small-12'>
            <div class='large-2 medium-2 small-3  columns '>New Delhi (DEL ) - Mumbai (BOM)</div>
            <div class='large-2 medium-2 small-3 columns  passenger'>
                <div class='large-12 medium-12 small-12'>
                    <img src='http://UmrawatiTrip.co/AirLogo/smG8.gif' /></div>
                <div class='large-12 medium-12 small-12'>GoAir(G8-340)</div>
            </div>
            <div class='large-2 medium-2 small-3  columns '>16 MAY, 17</div>
            <div class='large-2 medium-2 small-3  columns '>1805Hrs</div>
            <div class='large-2 medium-2 small-3  columns '>2015Hrs</div>
            <div class='large-2 medium-2 small-3  columns passenger'>320</div>
        </div>
        <div class='clear1'></div>
        <div class='large-12 medium-12 small-12'>Fare Information (Refundable)</div>
        <div class='large-12 medium-12 small-12'>
            <div class='large-1 medium-1 small-1 columns  bld'>Pax Detail</div>
            <div class='large-1 medium-1 small-1 columns  bld'>Base Fare</div>
            <div class='large-1 medium-2 small-2 columns  bld'>Fuel Surcharge</div>
            <div class='large-1 medium-1 small-1 columns  bld'>Tax</div>
            <div class='large-1 medium-1 small-1 columns  bld'>STax</div>
            <div class='large-1 medium-2 small-2 columns  bld'>Trans Fee</div>
            <div class='large-1 medium-2 small-2 columns  bld'>Trans Charge</div>
            <div class='large-1 medium-2 small-2 columns  bld'>TOTAL</div>
        </div>
        <div class='clear1'></div>
        <div class='large-12 medium-12 small-12'>
            <div class='large-1 medium-1 small-1  columns ' id='td_paxtype'>ADT</div>
            <div class='large-1 medium-1 small-1  columns '>2680</div>
            <div class='large-1 medium-2 small-2  columns '>300</div>
            <div class='large-1 medium-1 small-1  columns ' id='td_taxadt'>946</div>
            <div class='large-1 medium-1 small-1  columns '>0</div>
            <div class='large-1 medium-2 small-2  columns '>0</div>
            <div class='large-1 medium-2 small-2  columns ' id='td_tcadt'>0</div>
            <div class='large-1 medium-2 small-2  columns ' id='td_adttot'>3926</div>
        </div>
        <div class='clear1'></div>
        <div class='large-12 medium-12 small-12'>
            <div class='large-1 medium-2 small-2 large-push-8 medium-push-8 small-push-8 columns  bld blue'>GRAND TOTAL </div>
            <div class='large-1 medium-2 small-2 columns  bld' id='td_grandtot'>3926</div>
        </div>
    </div>
    <div class='clear1'></div>
    <div class='large-12 medium-12 small-12'>
        <div class='large-12 medium-12 small-12'>Kindly confirm the status of your PNR within 24 hrs of booking, as at times the same may fail on account of payment failure, internet connectivity, booking engine or due to any other reason beyond our control.</br>For Customers who book their flights well in advance of the scheduled departure date it is necessary that you re-confirm the departure time of your flight between 72 and 24 hours before the Scheduled Departure Time.</div>
        <div class='clear1'></div>
        <div class='large-12 medium-12 small-12 bld blue'><u>TERMS AND CONDITIONS :</u></div>
        <div class='clear'></div>
        <div class='large-12 medium-12 small-12'><b>1.</b> Guests are requested to carry their valid photo identification for all guests, including children</div>
        <div class='large-12 medium-12 small-12'><b>2.</b> We recommend check-in at least 2 hours prior to departure.</div>
        <div class='large-12 medium-12 small-12'><b>3.</b>  Boarding gates close 45 minutes prior to the scheduled time of departure. Please report at your departure gate at the indicated boarding time. Any passenger failing to report in time may be refused boarding privileges.</div>
        <div class='large-12 medium-12 small-12'><b>4.</b> Cancellations and Changes permitted more than two (2) hours prior to departure with payment of change fee and difference in fare if applicable only in working hours (10:00 am to 06:00 pm) except Sundays and Holidays.</div>
        <div class='large-12 medium-12 small-12'><b>5.</b>  Flight schedules are subject to change and approval by authorities.</div>
        <div class='large-12 medium-12 small-12'><b>6.</b> In case of ticket Cancellation the transaction fee will not be refunded.</div>
        <div class='large-12 medium-12 small-12'><b>7.</b> Name Changes on a confirmed booking are strictly prohibited. Please ensure that the name given at the time of booking matches as mentioned on the traveling Guests valid photo ID Proof.</div>
        <div class='large-12 medium-12 small-12'><b>8.</b>  Travel Agent does not provide compensation for travel on other airlines, meals, lodging or ground transportation.</div>
        <div class='large-12 medium-12 small-12'><b>9.</b> Bookings made under the Armed Forces quota are non cancelable and non- changeable.</div>
        <div class='large-12 medium-12 small-12'><b>10.</b> Guests are advised to check their all flight details (including their Name, Flight numbers, Date of Departure, Sectors) before leaving the Agent Counter.</div>
        <div class='large-12 medium-12 small-12'><b>11.</b> Cancellation amount will be charged as per airline rule.</div>
        <div class='large-12 medium-12 small-12'><b>12.</b> Guests requiring wheelchair assistance, stretcher, Guests traveling with infants and unaccompanied minors need to be booked in advance since the inventory for these special service requests are limited per flight.</div>
    </div>
    <div class='clear1'></div>
    <div class='large-12 medium-12 small-12 bld blue'><u>BAGGAGE INFORMATION :</u></div>
    <div class='clear1'></div>
    <div class='large-12 medium-12 small-12'>
        <div class='large-6 medium-6 small-6 columns  bld'>Airline</div>
        <div class='large-6 medium-6 small-6 columns  bld'>Weight</div>
        <div class='clear'></div>
        <div class='large-6 medium-6 small-6  columns '>Go Air(G8) (Business Class)</div>
        <div class='large-6 medium-6 small-6  columns '>35Kg + 10Kg (Baggage allowance + Hand carry)</div>
        <div class='clear'></div>
    </div>
    </div><div class='clear1'></div>
</body>
</html>
