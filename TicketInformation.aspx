﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TicketInformation.aspx.cs" Inherits="TicketHtml" %>

<%--<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>--%>
<table style='width: 100%;'>
    <tr>
        <td>
            <table style='width: 100%;'>
                <tr>
                    <td style='width: 50%; text-align: left;'>
                        <img src='http://UmrawatiTrip.co/images/logo.png' alt='Logo' style='height: 70px; width: 150px' /></td>
                    <td style='text-align: right; width: 100%; font-size: 12px; font-weight: bold;'>Electronic Ticket<br />
                    </td>
                </tr>
                <tr>
                    <td colspan='2' style='height: 2px; width: 100%; border-top: 1px solid #ccc'></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table style='width: 100%;'>
                <tr>
                    <td style='width: 100%; text-align: justify; color: #0f4da2; font-size: 11px; padding: 0px;'>This is travel itinerary and E-ticket receipt. You may need to show this receipt to enter the airport and/or to show return or onward travel to customs and immigration officials.</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table style='border: 1px solid #eee; font-family: Verdana, Geneva, sans-serif; font-size: 12px; padding: 0px !important; width: 100%;'>
                <tr>
                    <td style='text-align: left; background-color: #eee; color: #424242; font-size: 12px; font-weight: bold; padding: 5px;' colspan='6'>Ticket & Passenger Information</td>
                </tr>
                <tr>
                    <td colspan='6' style='font-size: 12px; padding: 5px; width: 100%'>
                        <table style='width: 100%;'>
                            <tr>
                                <td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>GDS PNR</td>
                                <td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>SQKMD6</td>
                                <td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Airline PNR</td>
                                <td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>QOHSSX</td>
                                <td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Class</td>
                                <td style='font-size: 13px; width: 30%; text-align: left; padding: 5px;'>Economy</td>
                            </tr>
                            <tr>
                                <td style='font-size: 11px; width: 10%; text-align: left; padding: 5px;'>Status</td>
                                <td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>Ticketed</td>
                                <td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Agency Info</td>
                                <td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>9327308914<br />
                                    chawla13101971@outlook.com</td>
                                <td style='font-size: 11px; width: 15%; text-align: left; padding: 5px;'>Date Of Issue</td>
                                <td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>26 Feb 2018</td>
                                <td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Fare Type</td>
                                <td style='font-size: 13px; width: 30%; text-align: left; padding: 5px;'>Refundable</td>
                            </tr>
                            <tr>
                                <td style='text-align: left; color: #0f4da2; font-size: 11px; font-weight: bold; padding: 5px;' colspan='6'>Passenger  Information</td>
                            </tr>
                            <tr></tr>
                            <tr>
                                <td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Customer Info</td>
                                <td style='font-size: 11px; width: 30%; text-align: left; padding: 5px;'>9845038464<br />
                                    chawla1310@gmail.com</td>
                            </tr>
                            <tr>
                                <td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Passenger Name</td>
                                <td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>Mr  P Samuel    Dhinakaran (ADT)</td>
                                <td style='font-size: 11px; width: 20%; text-align: left; padding: 5px;'>Ticket No.</td>
                                <td style='font-size: 11px; width: 35%; text-align: left; padding: 5px;'>589-5315429492</td>
                            </tr>
                        </table>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style='text-align: left; background-color: #eee; color: #424242; width: 100%; padding: 5px;' colspan='6'>
            <table style='width: 100%;'>
                <tr>
                    <td style='text-align: left; color: #424242; font-size: 12px; width: 25%; font-weight: bold;' colspan='1'>Flight Information</td>
                    <td colspan='3' style='font-size: 12px; color: #424242; font-weight: bold; width: 75%; text-align: left;'></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan='4' style='height: 5px;'>&nbsp;</td>
    </tr>
    <tr>
        <td colspan='5' style='background-color: #eee; width: 100%;'>
            <table style='width: 100%;'>
                <tr>
                    <td style='font-size: 10.5px; color: #424242; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>FLIGHT</td>
                    <td style='font-size: 10.5px; color: #424242; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>DEPART</td>
                    <td style='font-size: 10.5px; color: #424242; width: 20%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE</td>
                    <td style='font-size: 10.5px; color: #424242; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>DEPART AIRPORT/TERMINAL</td>
                    <td style='font-size: 10.5px; color: #424242; width: 25%; text-align: left; padding: 4px; font-weight: bold;'>ARRIVE AIRPORT/TERMINAL</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan='5' style='width: 100%;'>
            <table style='width: 100%;'>
                <tr>
                    <td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>9W 7001<br />
                        <br />
                        <img alt='Logo Not Found' src='http://UmrawatiTrip.co/AirLogo/sm9W.gif' /></td>
                    <td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>28 Feb 18<br />
                        <br />
                        08 : 00</td>
                    <td style='font-size: 11px; width: 20%; text-align: left; vertical-align: top;'>28 Feb 18<br />
                        <br />
                        09 : 40</td>
                    <td style='font-size: 11px; width: 25%; text-align: left; padding: 2px;'>Bangalore                                         ( BLR)<br />
                        <br />
                        Bangalore International Airport Airport - Trml: NA</td>
                    <td style='font-size: 11px; width: 25%; text-align: left; padding: 2px;'>Mumbai                                             (BOM)<br />
                        <br />
                        Chhatrapati Shivaji International Airport - Trml:2</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan='4' style='width: 100%;'>
            <table style='width: 100%;'>
                <tr>
                    <td style='font-size: 11px; width: 322%; text-align: left; font-weight: bold;'>
                        <br />
                    </td>
                    <td style='width: 32%;'></td>
                    <td style='width: 18%; font-size: 12px; text-align: left;'></td>
                    <td style='width: 18%; font-size: 11px; text-align: left; font-weight: bold;'></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr id='TR_FareInformation1'>
        <td colspan='4' style='background-color: #0f4da2; color: #fff; font-size: 12px; font-weight: bold; padding: 5px;'>Fare Information</td>
    </tr>
    <tr id='TR_FareInformation2'>
        <td colspan='8' style='background-color: #eee; width: 100%;'>
            <table style='width: 100%;'>
                <tr id='TR_FareInformation3'>
                    <td style='font-size: 12px; color: #424242; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>Pax Type</td>
                    <td style='font-size: 12px; color: #424242; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>Pax Count</td>
                    <td style='font-size: 12px; color: #424242; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Base fare</td>
                    <td style='font-size: 12px; color: #424242; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Fuel Surcharge</td>
                    <td style='font-size: 12px; color: #424242; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>Tax</td>
                    <td style='font-size: 12px; color: #424242; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>STax</td>
                    <td style='font-size: 12px; color: #424242; width: 10%; text-align: left; padding: 4px; font-weight: bold;'>Trans Fee</td>
                    <td style='font-size: 12px; color: #424242; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>Trans Charge</td>
                    <td style='font-size: 12px; color: #424242; width: 15%; text-align: left; padding: 4px; font-weight: bold;'>TOTAL</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr id='TR_FareInformation4'>
        <td colspan='8' style='width: 100%;'>
            <table style='width: 100%;'>
                <tr id='TR_FareInformation5'>
                    <td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>ADT</td>
                    <td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;' id='td_adtcnt'>1</td>
                    <td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>4375</td>
                    <td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;'>450</td>
                    <td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;' id='td_taxadt'>836</td>
                    <td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>0</td>
                    <td style='font-size: 11px; width: 10%; text-align: left; vertical-align: top;'>0</td>
                    <td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;' id='td_tcadt'>0</td>
                    <td style='font-size: 11px; width: 15%; text-align: left; vertical-align: top;' id='td_adttot'>5661</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr id='TR_FareInformation10'>
        <td colspan='4' style='background-color: #0f4da2; color: #fff; font-size: 12px; font-weight: bold; padding: 5px;'>
            <table style='width: 100%;' id='TR_FareInformation11'>
                <tr>
                    <td style='font-size: 12px; width: 20%; text-align: left; vertical-align: top;'></td>
                    <td style='font-size: 12px; width: 20%; text-align: left; vertical-align: top;'></td>
                    <td style='font-size: 12px; width: 20%; text-align: left; vertical-align: top;'></td>
                    <td style='font-size: 12px; width: 15%; text-align: left; vertical-align: top;'></td>
                    <td style='color: #fff; font-size: 12px; width: 15%; text-align: left; vertical-align: top;'>GRAND TOTAL</td>
                    <td style='color: #fff; font-size: 12px; width: 10%; text-align: left; vertical-align: top;' id='td_grandtot'>5661</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan='4'>
            <ul style='list-style-image: url(http://UmrawatiTrip.co/Images/bullet.png);'>
                <li style='font-size: 10.5px;'>Kindly confirm the status of your PNR within 24 hrs of booking, as at times the same may fail on account of payment failure, internet connectivity, booking engine or due to any other reason beyond our control.For Customers who book their flights well in advance of the scheduled departure date it is necessary that you re-confirm the departure time of your flight between 72 and 24 hours before the Scheduled Departure Time.</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td colspan='4' style='background-color: #0f4da2; color: #fff; font-size: 11px; font-weight: bold; padding: 5px;'>TERMS AND CONDITIONS :</td>
    </tr>
    <tr>
        <td colspan='4'>
            <ul style='list-style-image: url(http://UmrawatiTrip.co/Images/bullet.png);'>
                <li style='font-size: 10.5px;'>Guests are requested to carry their valid photo identification for all guests, including children.</li>
                <li style='font-size: 10.5px;'>We recommend check-in at least 2 hours prior to departure.</li>
                <li style='font-size: 10.5px;'>Boarding gates close 45 minutes prior to the scheduled time of departure. Please report at your departure gate at the indicated boarding time. Any passenger failing to report in time may be refused boarding privileges.</li>
                <li style='font-size: 10.5px;'>Cancellations and Changes permitted more than two (2) hours prior to departure with payment of change fee and difference in fare if applicable only in working hours (10:00 am to 06:00 pm) except Sundays and Holidays.</li>
                <li style='font-size: 10.5px;'>Flight schedules are subject to change and approval by authorities.<br />
                </li>
                <li style='font-size: 10.5px;'>Name Changes on a confirmed booking are strictly prohibited. Please ensure that the name given at the time of booking matches as mentioned on the traveling Guests valid photo ID Proof.<br />
                    Travel Agent does not provide compensation for travel on other airlines, meals, lodging or ground transportation.</li>
                <li style='font-size: 10.5px;'>Bookings made under the Armed Forces quota are non cancelable and non- changeable.</li>
                <li style='font-size: 10.5px;'>Guests are advised to check their all flight details (including their Name, Flight numbers, Date of Departure, Sectors) before leaving the Agent Counter.</li>
                <li style='font-size: 10.5px;'>Cancellation amount will be charged as per airline rule.</li>
                <li style='font-size: 10.5px;'>Guests requiring wheelchair assistance, stretcher, Guests traveling with infants and unaccompanied minors need to be booked in advance since the inventory for these special service requests are limited per flight.</li>
            </ul>
        </td>
    </tr>
</table>
<table style='width: 100%;'>
    <tr>
        <td colspan='4' style='background-color: #eee; color: #424242; font-size: 12px; font-weight: bold; padding: 5px;'>BAGGAGE INFORMATION :</td>
    </tr>
    <tr>
        <td colspan='2'></td>
        <td colspan='2'>15 Kg Baggage allowance</td>
    </tr>
</table>
</td></tr></table>