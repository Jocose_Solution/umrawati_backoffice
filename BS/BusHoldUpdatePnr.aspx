﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="BusHoldUpdatePnr.aspx.cs" Inherits="BS_BusHoldUpdatePnr" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
       <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
       <script language="javascript" type="text/javascript">

           function Validate() {
               if (document.getElementById("ctl00_ContentPlaceHolder1_txt_Reject").value == "") {

                   alert('Please Fill Remark');
                   document.getElementById("ctl00_ContentPlaceHolder1_txt_Reject").focus();
                   return false;


               }
               if (confirm("Are you sure you want to Reject!"))
                   return true;
               return false;
           }
    </script>
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-12">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Bus Hold Pnr Update</h3>
                    </div>
                    <div class="panel-body">



                        <div class="row" id="td_Reject" runat="server" visible="false">
                            <div class="col-md-4">
                                <div class="form-group">

                                    <label for="exampleInputEmail1">Remark</label>
                                    <asp:TextBox ID="txt_Reject" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                    <asp:Button ID="btn_Comment" runat="server" Text="Comment" OnClientClick="return Validate();"
                                        CssClass="button buttonBlue" />

                                    <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" CssClass="button buttonBlue" />

                                </div>

                            </div>
                        </div>

                        <div class="row" id="divReport" style="background-color: #fff; overflow-y: scroll; overflow-x: scroll; max-height: 500px;" runat="server" visible="true">
                            <div class="col-md-28">
                                <asp:GridView ID="GrdBusHoldReport" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                    CssClass="table" GridLines="None" PageSize="30" OnRowCommand="RowCommand">
                                    <Columns>
                                      <asp:TemplateField HeaderText="ORDERID">
                                                                <ItemTemplate>
                                                                    <a href='../BS/TicketSummary.aspx?tin=<%#Eval("TICKETNO")%>&oid=<%#Eval("ORDERID")%>'
                                                                        rel="lyteframe" rev="width: 1000px; height: 500px; overflow:hidden;" target="_blank"
                                                                        style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #668aff">
                                                                        <asp:Label ID="TKTNO" runat="server" Text='<%#Eval("ORDERID")%>'></asp:Label>
                                                                    </a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                       <%-- <asp:BoundField DataField="ORDERID" HeaderText="Order&nbsp;Id" />--%>
                                         <asp:TemplateField HeaderText="Agent_ID">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_Agent_ID" runat="server" Text='<%# Eval("AGENTID")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ORDERID" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_ORDERID" runat="server" Text='<%# Eval("ORDERID")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ref no" Visible="false">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_BOOKING_REF" runat="server" Text='<%# Eval("BOOKING_REF")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Amount">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_TA_NET_FARE" runat="server" Text='<%# Eval("TA_NET_FARE")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>  
                                        <asp:BoundField DataField="TRIPID" HeaderText="Trip&nbsp;Id" />
                                        <asp:BoundField DataField="SOURCE" HeaderText="Source" />
                                        <asp:BoundField DataField="DESTINATION" HeaderText="Destination" />
                                        <asp:BoundField DataField="BUSOPERATOR" HeaderText="Bus&nbsp;Operator" />                                      
                                        <asp:TemplateField HeaderText="SEATNO">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_SEATNO" runat="server" Text='<%# Eval("SEATNO")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                        <asp:BoundField DataField="PAXNAME" HeaderText="PaxName" />
                                        <asp:BoundField DataField="GENDER" HeaderText="Gender" />
                                        <asp:BoundField DataField="TA_NET_FARE" HeaderText="Fare" />
                                        <asp:BoundField DataField="TICKETNO" HeaderText="Ticket&nbsp;No" />  
                                        <asp:TemplateField HeaderText="Update">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="ITZ_Accept" runat="server" CommandArgument='<%#Eval("OrderId") %>'
                                                    Font-Bold="True" Font-Underline="False" OnClick="ITZ_Accept_Click">UpdatePNR</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                   
                                        <asp:TemplateField HeaderText="Accept">
                                            <ItemTemplate>                                               
                                                <asp:LinkButton ID="LB_Reject" runat="server" CommandName="Reject" CommandArgument='<%#Eval("OrderId") %>'
                                                   Font-Bold="True" Font-Underline="False" ForeColor="Red">Refund</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                 
                                         <asp:TemplateField HeaderText="Reject Remark">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtRemark" runat="server" Height="47px" TextMode="MultiLine" Width="175px"
                                                    Visible="false"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField ControlStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkSubmit" runat="server"  OnClientClick="return Validate();" Visible="false"
                                                    CommandArgument='<%# Eval("OrderId") %>'  CommandName="submit"><img src="../../Images/Submit.png" alt="Ok" /></asp:LinkButton><br />
                                               
                                                 <asp:LinkButton ID="lnkHides" runat="server" OnClick="lnkHides_Click" Visible="false"
                                                    CommandName="lnkHides" CommandArgument='<%# Eval("OrderId") %>'><img src="../../Images/Cancel.png" alt="Cancel" /></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="RowStyle" />
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <PagerStyle CssClass="PagerStyle" />
                                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                                    <HeaderStyle CssClass="HeaderStyle" />
                                    <EditRowStyle CssClass="EditRowStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

