﻿

using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
partial class  BS_Bus_RefundRequest : System.Web.UI.Page
{
    SqlTransaction ST = new SqlTransaction();
    DataSet ds = new DataSet();  
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myCon"].ConnectionString);
    protected void Page_Load(object sender, System.EventArgs e)
    {
        ((Label)Page.Master.FindControl("lblBC")).Text = "<a title='' class='tip-bottom' href='#' data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Bus </a><a class='current' href='#'>BusRefund Request in Queue </a>";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        try
        {
            if (string.IsNullOrEmpty(Session["UID"].ToString()) | Session["UID"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }

            if (IsPostBack)
            {
            }
            else
            {
                BindGrid();
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);

        }
    }
    private void BindGrid()
    
    {
        try
        {
            ds = RefundRequest("RefundRequested" ,"Requested","");             
            Acept_grdview.DataSource = ds;
            Acept_grdview.DataBind();
            ViewState["ds"] = ds;
            Acept_grdview.Columns[15].Visible = false;
           Acept_grdview.Columns[16].Visible = false;
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);

        }
    }
    public void CheckEmptyValue()
    {
        try
        {
            string PNR = String.IsNullOrEmpty(txt_PNR.Text) ? "" : txt_PNR.Text.Trim();
            ds = RefundRequest("RefundRequested", "Requested", PNR);
            Acept_grdview.DataSource = ds;
            Acept_grdview.DataBind();
            //ViewState["ds"] = ds;
            Acept_grdview.Columns[21].Visible = false;
            Acept_grdview.Columns[20].Visible = false;
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    protected void btn_result_Click(object sender, System.EventArgs e)
    {
        CheckEmptyValue();
    }
    protected void RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Accept")
            {
                try
                {
                    ViewState["Counter"] = int.Parse(e.CommandArgument.ToString());
                    UpdateBuscanceldetail(Convert.ToInt32(ViewState["Counter"]), Session["UID"].ToString(), "B_A", "InProcess", "");
                   //ST.UpdateReIssueCancleAccept(Convert.ToInt32(e.CommandArgument), Session["UID"].ToString(), "C", StatusClass.InProcess);
                    LinkButton lb = e.CommandSource as LinkButton;
                    GridViewRow gvr = lb.Parent.Parent as GridViewRow;
                    gvr.BackColor = System.Drawing.Color.GreenYellow;
                    BindGrid();
                }
                catch (Exception ex)
                {
                    clsErrorLog.LogInfo(ex);
                }

            }
            else if (e.CommandName == "Reject")
            {
                try
                {
                    //BindGrid();                 
                    ViewState["Counter"] = int.Parse(e.CommandArgument.ToString());
                    Acept_grdview.Columns[15].Visible = true;
                    Acept_grdview.Columns[16].Visible = true;
                    Acept_grdview.Columns[14].Visible = false;
                    Acept_grdview.Columns[13].Visible = false;
                    LinkButton lb = e.CommandSource as LinkButton;
                    GridViewRow gvr = lb.Parent.Parent as GridViewRow;
                    int RowIndex = gvr.RowIndex;
                    ViewState["RowIndex"] = RowIndex;
                    TextBox txtRemark = (TextBox)Acept_grdview.Rows[RowIndex].FindControl("txtRemark");
                    LinkButton lnkSubmit = (LinkButton)Acept_grdview.Rows[RowIndex].FindControl("lnkSubmit");
                    LinkButton lnkHides = (LinkButton)Acept_grdview.Rows[RowIndex].FindControl("lnkHides");
                    lnkHides.Visible = true;
                    txtRemark.Visible = true;
                    lnkSubmit.Visible = true;
                    gvr.BackColor = System.Drawing.Color.Yellow;
                }
                catch (Exception ex)
                {
                    clsErrorLog.LogInfo(ex);

                }

            }
            else if (e.CommandName == "lnkHides")
            {
                try
                {
                    ViewState["Counter"] = int.Parse(e.CommandArgument.ToString());
                    Acept_grdview.Columns[15].Visible = false;
                    Acept_grdview.Columns[16].Visible = false;
                    Acept_grdview.Columns[14].Visible = true;
                    Acept_grdview.Columns[13].Visible = true;
                    LinkButton lb = e.CommandSource as LinkButton;
                    GridViewRow gvr = lb.Parent.Parent as GridViewRow;
                    int RowIndex = gvr.RowIndex;
                    ViewState["RowIndex"] = RowIndex;
                    TextBox txtRemark = (TextBox)Acept_grdview.Rows[RowIndex].FindControl("txtRemark");
                    LinkButton lnkSubmit = (LinkButton)Acept_grdview.Rows[RowIndex].FindControl("lnkSubmit");
                    LinkButton lnkHides = (LinkButton)Acept_grdview.Rows[RowIndex].FindControl("lnkHides");
                    lnkHides.Visible = false;
                    txtRemark.Visible = false;
                    lnkSubmit.Visible = false;
                    gvr.BackColor = System.Drawing.Color.White;

                }
                catch (Exception ex)
                {
                    clsErrorLog.LogInfo(ex);

                }

            }
            else if (e.CommandName == "submit")
            {
                LinkButton lb = e.CommandSource as LinkButton;
                GridViewRow gvr = lb.Parent.Parent as GridViewRow;
                int RowIndex = gvr.RowIndex;
                ViewState["RowIndex"] = RowIndex;
                TextBox txtRemark = (TextBox)Acept_grdview.Rows[RowIndex].FindControl("txtRemark");
                if (txtRemark.Text == "")
                {
                    ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('please Provide Remark.');javascript: window.opener.location=window.opener.location.href;", true);

                }
                else {
                    try
                    {
                        int i = 0;
                        ViewState["Counter"] = int.Parse(e.CommandArgument.ToString());
                        Acept_grdview.Columns[15].Visible = false;
                        Acept_grdview.Columns[16].Visible = false;
                        Acept_grdview.Columns[14].Visible = true;                 
                        LinkButton lnkSubmit = (LinkButton)Acept_grdview.Rows[RowIndex].FindControl("lnkSubmit");
                        LinkButton lnkHides = (LinkButton)Acept_grdview.Rows[RowIndex].FindControl("lnkHides");
                        lnkHides.Visible = false;
                        txtRemark.Visible = false;
                        lnkSubmit.Visible = false;
                        gvr.BackColor = System.Drawing.Color.White;
                        i = UpdateBuscanceldetail(Convert.ToInt32(ViewState["Counter"]), Session["UID"].ToString(), "B_C", "Rejected", txtRemark.Text);
                        if (i > 0)
                        {
                            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Rejected Successfully.');javascript: window.close();window.opener.location=window.opener.location.href;", true);
                            BindGrid();
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Try Again.');javascript: window.close();window.opener.location=window.opener.location.href;", true);
                            BindGrid();

                        }

                    }
                    catch (Exception ex)
                    {
                        clsErrorLog.LogInfo(ex);

                    }
                }
                
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);

        }
    }
    protected void btnCanFee_Click(object sender, System.EventArgs e)
    {
        try
        {
            int RowIndex = Convert.ToInt32(ViewState["RowIndex"]);
            TextBox txtRemark = (TextBox)Acept_grdview.Rows[RowIndex].FindControl("txtRemark");
            Label OrderId = (Label)Acept_grdview.Rows[RowIndex].FindControl("OrderId");
            Label PnrNo = (Label)Acept_grdview.Rows[RowIndex].FindControl("GdsPNR");
            Label Pax = (Label)Acept_grdview.Rows[RowIndex].FindControl("PaxType");
            Label NetAmount = (Label)Acept_grdview.Rows[RowIndex].FindControl("lbltotalfare");        
            IntlDetails ObjIntDetails = new IntlDetails();          
            UpdateBuscanceldetail(Convert.ToInt32(ViewState["Counter"]), Session["UID"].ToString(), "B_C", "Rejected", txtRemark.Text);                    
            DataTable dt = new DataTable();
            string strMailMsgHold = null;
            strMailMsgHold = "<table>";
            strMailMsgHold = strMailMsgHold + "<tr>";
            strMailMsgHold = strMailMsgHold + "<td><h2> Refund Request Booking Summary </h2>";
            strMailMsgHold = strMailMsgHold + "</td>";
            strMailMsgHold = strMailMsgHold + "</tr>";
            strMailMsgHold = strMailMsgHold + "<tr>";
            strMailMsgHold = strMailMsgHold + "</tr>";
            strMailMsgHold = strMailMsgHold + "<tr>";
            strMailMsgHold = strMailMsgHold + "<td><b>Order ID: </b>" + Convert.ToString(OrderId.Text);
            strMailMsgHold = strMailMsgHold + "</td>";
            strMailMsgHold = strMailMsgHold + "</tr>";
            strMailMsgHold = strMailMsgHold + "<tr>";
            strMailMsgHold = strMailMsgHold + "<td><b>PnrNo: </b>";
            strMailMsgHold = strMailMsgHold + "</td>";
            strMailMsgHold = strMailMsgHold + "</tr>";
            strMailMsgHold = strMailMsgHold + "<tr>";
            strMailMsgHold = strMailMsgHold + "<td><b>Pax Name: </b>";
            strMailMsgHold = strMailMsgHold + "</td>";
            strMailMsgHold = strMailMsgHold + "</tr>";
            strMailMsgHold = strMailMsgHold + "<tr>";
            strMailMsgHold = strMailMsgHold + "<td><b>Pax Type: </b>";
            strMailMsgHold = strMailMsgHold + "</td>";
            strMailMsgHold = strMailMsgHold + "</tr>";
            strMailMsgHold = strMailMsgHold + "<tr>";
            strMailMsgHold = strMailMsgHold + "<td><b>Net Amount: </b>" ;
            strMailMsgHold = strMailMsgHold + "</td>";
            strMailMsgHold = strMailMsgHold + "</tr>";
            strMailMsgHold = strMailMsgHold + "<tr>";
            strMailMsgHold = strMailMsgHold + "<td><b>Remark: </b>" ;
            strMailMsgHold = strMailMsgHold + "</td>";
            strMailMsgHold = strMailMsgHold + "</tr>";
            strMailMsgHold = strMailMsgHold + "</table>";
            dt = Email_Credentilas(OrderId.Text.Trim().ToString(), "C_REJECTED", ViewState["Counter"].ToString());
            SqlTransactionDom STDOM = new SqlTransactionDom();
            DataTable MailDt = new DataTable();
            MailDt = STDOM.GetMailingDetails(MAILING.AIR_PNRSUMMARY.ToString(), Session["UID"].ToString()).Tables[0];
            try
            {
                if ((MailDt.Rows.Count > 0))
                {
                    for (int j = 0; j <= dt.Rows.Count - 1; j++)
                    {
                        STDOM.SendMail(dt.Rows[j][1].ToString(), "info@UmrawatiTrip.com", "", MailDt.Rows[0]["CC"].ToString(), MailDt.Rows[0]["SMTPCLIENT"].ToString(), MailDt.Rows[0]["UserId"].ToString(), MailDt.Rows[0]["Pass"].ToString(), strMailMsgHold, "Ticket Reissue Request", "");
                    }
                }

            }
            catch (Exception ex)
            {
                clsErrorLog.LogInfo(ex);
            }       
            ShowAlertMessage("Reject successfully");
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    protected void lnkHides_Click(object sender, System.EventArgs e)
    {
        try
        {
            Acept_grdview.Columns[21].Visible = false;
            Acept_grdview.Columns[20].Visible = false;
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);

        }
    }
    public static void ShowAlertMessage(string error)
    {
        try
        {
            Page page = HttpContext.Current.Handler as Page;
            if (page != null)
            {
                error = error.Replace("'", "'");
                ScriptManager.RegisterStartupScript(page, page.GetType(), "err_msg", "alert('" + error + "');", true);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);

        }
    }
    public DataSet RefundRequest(string status,string rstatus,string Pnr)
    {
        
        DataSet DS = new DataSet();
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "SP_BusRefundRequested";
                sqlcmd.Parameters.AddWithValue("@Status", status);
                sqlcmd.Parameters.AddWithValue("@RStatus", rstatus);
                sqlcmd.Parameters.AddWithValue("@Pnr", Pnr); 
                SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                da.Fill(DS);
                con.Close();
                DS.Dispose();
                con.Close();
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        return DS;

    }
    public int UpdateBuscanceldetail(int counter, string ExecutiveID, string ReqType, string Status, string remark)
    {
        int i = 0;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myCon"].ConnectionString);
        DataSet DS = new DataSet();
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "UpdateReIssueCancleAccept_bus";
                sqlcmd.Parameters.AddWithValue("@counter", counter);
                sqlcmd.Parameters.AddWithValue("@ExecutiveID", ExecutiveID);
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
                sqlcmd.Parameters.AddWithValue("@Status", Status);
                sqlcmd.Parameters.AddWithValue("@remark", remark);
                sqlcmd.Parameters.AddWithValue("@acceptby", Session["UID"].ToString());
                i = sqlcmd.ExecuteNonQuery();
                con.Close();

            }
        }

        catch (Exception ex)
        {

        }
        return i;

    }
    public DataTable Email_Credentilas(string Orderid, string Cmd_Type, string Counter)
    {
        SqlConnection con1 = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        con1.Open();
        DataTable dt = new DataTable();
        SqlCommand cmd = new SqlCommand();
        //cmd.CommandText = "USP_TICKETSTATUS"
        cmd.CommandText = "USP_TICKETSTATUS_PP";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@ORDERID", SqlDbType.VarChar).Value = Orderid;
        cmd.Parameters.Add("@CMD_TYPE", SqlDbType.VarChar).Value = Cmd_Type;
        cmd.Parameters.Add("@COUNTER", SqlDbType.VarChar).Value = Counter;
        cmd.Connection = con1;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        con1.Close();
        return dt;
    }

}

