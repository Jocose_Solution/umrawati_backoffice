﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="BusRefundRequestInproces.aspx.cs" Inherits="BS_BusRefundRequestInproces" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function Validate() {
            if (document.getElementById("ctl00_ContentPlaceHolder1_GridRefundRequest_ctl02_txtRemark").value == "") {

                alert('Remark can not be blank,Please Fill Remark');
                document.getElementById("ctl00_ContentPlaceHolder1_GridRefundRequest_ctl02_txtRemark").focus();
                return false;
            }
            if (confirm("Are you sure you want to Reject!"))
                return true;
            return false;
        }
    </script>
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow-y: scroll;
        }
    </style>


    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="container-fluid" style="padding-right: 285px">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
     
                    <div class="panel-body"> 
                        <hr />               
                        <div class="row" id="divReport" style="background-color: #fff; overflow-y: scroll; overflow-x: scroll; max-height: 500px;" runat="server" visible="True">
                            <div class="col-md-28">
                                <asp:GridView ID="GridRefundRequest" runat="server" AllowPaging="True" AllowSorting="True" OnRowCommand="RowCommand"
                                    AutoGenerateColumns="False"  CssClass="table" GridLines="None" PageSize="30">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Order Id">
                                            <ItemTemplate>
                                                <asp:Label ID="OrderID" runat="server" Text='<%#Eval("ORDERID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pax ID" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="TID" runat="server" Text='<%#Eval("Counter")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="PNR">
                                <ItemTemplate>
                                    <asp:Label ID="lblPnr" runat="server" Text='<%#Eval("PNR")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pax Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaxName" runat="server" Text='<%#Eval("PAXNAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                    
                                         <asp:TemplateField HeaderText="Seat_no">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkupdate" runat="server" Text='<%#Eval("SEATNO") %>' ForeColor="Red"
                                                    Font-Bold="true" Font-Size="11px" CommandName="UpdateRefund" CommandArgument='<%#Eval("Counter")%>'
                                                    OnClick="lnkupdate_Click"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Gender">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGender" runat="server" Text='<%#Eval("GENDER")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="AgentId">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAgentId" runat="server" Text='<%#Eval("AGENTID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="CancellationID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCancellationID" runat="server" Text='<%#Eval("CANCELLATIONID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("REFUND_STATUS")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="API_Cancellation_Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStatus1" runat="server" Text='<%#Eval("API_CANCELLATION_STATUS")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Refund Amt">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRefund_Amt" runat="server" Text='<%#Eval("REFUND_AMT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CancelCharge">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCancelCharge" runat="server" Text='<%#Eval("CancelCharge")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Refund Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRefunddate" runat="server" Text='<%#Eval("REFUNDEDDATE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PAYMENT_MODE">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPAYMENT_MODE" runat="server" Text='<%#Eval("PAYMENT_MODE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Refund Service_chrg">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRefundService_chrg" runat="server" Text='<%#Eval("REFUND_SERVICECHRG")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="" >
                                            <ItemTemplate>                                                                                           
                                                <asp:LinkButton ID="lnkreject" runat="server" ForeColor="Red" Font-Strikeout="False"
                                                 Font-Overline="False" Font-Size="11px" CommandArgument='<%#Eval("SEATNO") %>'
                                                 CommandName="Reject" Text='Reject'></asp:LinkButton>
                                            </ItemTemplate>                                           
                                        </asp:TemplateField>                                                       
                            <asp:TemplateField HeaderText="Reject Remark">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtRemark" runat="server" Height="47px" TextMode="MultiLine" Width="175px"
                                        Visible="false"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ControlStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkSubmit" runat="server"  OnClientClick="return Validate();" Visible="false"
                                        CommandArgument='<%# Eval("SEATNO") %>' CommandName="submit"><img src="../../Images/Submit.png" alt="Ok" /></asp:LinkButton><br />
                                    <asp:LinkButton ID="lnkHides" runat="server" OnClick="lnkHides_Click" Visible="false"
                                        CommandName="lnkHides" CommandArgument='<%# Eval("SEATNO") %>'><img src="../../Images/Cancel.png" alt="Cancel" /></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="RowStyle" />
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <PagerStyle />
                                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                                    <HeaderStyle CssClass="HeaderStyle" Height="50px" />
                                    <EditRowStyle CssClass="EditRowStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 

</asp:Content>

