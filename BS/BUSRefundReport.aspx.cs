﻿
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

public partial class BS_BUSRefundReport : System.Web.UI.Page
{
    private SqlTransaction ST = new SqlTransaction();
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private clsInsertSelectedFlight CllInsSelectFlt = new clsInsertSelectedFlight();
    DataSet AgencyDDLDS = new DataSet();
    DataSet grdds = new DataSet();
    DataSet fltds = new DataSet();
    private Status sttusobj = new Status();
    SqlConnection con = new SqlConnection();
    ClsCorporate clsCorp = new ClsCorporate();
    protected void Page_Load(object sender, System.EventArgs e)
    {
        ((Label)Page.Master.FindControl("lblBC")).Text = "<a title='' class='tip-bottom' href='#' data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Bus </a><a class='current' href='#'>Bus Refund Report</a>";
        try
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            if (string.IsNullOrEmpty(Session["UID"].ToString()) | Session["UID"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }       
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    public DataSet BUSDetails(string loginid, string usertype, string fromdate, string todate, string orderid, string agentid, string paymentStatus, string Source, string Destination, string BusOperator, string TicketNo, string Pnr)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myCon"].ConnectionString);
        DataSet DS = new DataSet();
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "SP_BUSCancellationReport_PP";
                sqlcmd.Parameters.AddWithValue("@usertype",usertype);
                 sqlcmd.Parameters.AddWithValue("@LoginID",loginid);
                 sqlcmd.Parameters.AddWithValue("@FormDate",fromdate);
                 sqlcmd.Parameters.AddWithValue("@ToDate",todate);
                 sqlcmd.Parameters.AddWithValue("@OrderID",orderid);
                 sqlcmd.Parameters.AddWithValue("@AgentId",agentid);
                 sqlcmd.Parameters.AddWithValue("@PaymentStatus", paymentStatus);
                 sqlcmd.Parameters.AddWithValue("@source", Source);
                 sqlcmd.Parameters.AddWithValue("@destination", Destination);
                 sqlcmd.Parameters.AddWithValue("@BUSOPERATOR", BusOperator);
                 sqlcmd.Parameters.AddWithValue("@TICKETNO", TicketNo);
                 sqlcmd.Parameters.AddWithValue("@PNR", Pnr);           
                SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                da.Fill(DS);
                con.Close();
                DS.Dispose();
                con.Close();
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        return DS;
    }
    protected void btn_export_Click(object sender, System.EventArgs e)
    {
        try
        {
            string FromDate = null;
            string ToDate = null;          
            if (String.IsNullOrEmpty(Request["From"]))
            {
                FromDate = "";
            }
            else
            {
                FromDate = Request["From"].Substring(3, 2) + "/" + Request["From"].Substring(0, 2) + "/" + Request["From"].Substring(6, 4);
                FromDate = FromDate + " " + "12:00:00 AM";
            }
            if (String.IsNullOrEmpty(Request["To"]))
            {
                ToDate = "";
            }
            else
            {
                ToDate = Request["To"].Substring(3, 2) + "/" + Request["To"].Substring(0, 2) + "/" + Request["To"].Substring(6, 4);
                ToDate = ToDate + " " + "11:59:59 PM";
            }
            string AgentID = String.IsNullOrEmpty(Request["hidtxtAgencyName"]) | Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"];
            string OrderID = String.IsNullOrEmpty(txtOrderID.Text) ? "" : txtOrderID.Text.Trim();
            string Source = String.IsNullOrEmpty(TxtSource.Text) ? "" : TxtSource.Text.Trim();
            string Destination = String.IsNullOrEmpty(TxtDestination.Text) ? "" : TxtDestination.Text.Trim();
            string orderID = String.IsNullOrEmpty(txtOrderID.Text) ? "" : txtOrderID.Text.Trim();
            string BusOperator = String.IsNullOrEmpty(txtBusOperator.Text) ? "" : txtBusOperator.Text.Trim();
            string TicketNo = String.IsNullOrEmpty(txtTicketNo.Text) ? "" : txtTicketNo.Text.Trim();
            string Pnr = String.IsNullOrEmpty(txtPnr.Text) ? "" : txtPnr.Text.Trim();
          
            if (AgentID != "")
            {              
                grdds.Clear();
                grdds = BUSDetails(Session["UID"].ToString(), Session["User_Type"].ToString(), FromDate, ToDate, OrderID, AgentID,  "",  Source,  Destination,  BusOperator,  TicketNo,  Pnr);
                STDom.ExportData(grdds);
            }
            else
            {
                grdds.Clear();
                grdds = BUSDetails(Session["UID"].ToString(), Session["User_Type"].ToString(), FromDate, ToDate, OrderID, AgentID, "", Source, Destination, BusOperator, TicketNo, Pnr);
                grdds.Tables[0].Columns.Remove(grdds.Tables[0].Columns["MerchantKey_ITZ"]);
                grdds.Tables[0].Columns.Remove(grdds.Tables[0].Columns["Pwd_ITZ"]);
                grdds.Tables[0].Columns.Remove(grdds.Tables[0].Columns["LastLogin_ITZ"]);
                grdds.Tables[0].Columns.Remove(grdds.Tables[0].Columns["ModeType_ITZ"]);
                grdds.Tables[0].Columns.Remove(grdds.Tables[0].Columns["SvcType_ITZ"]);
                grdds.Tables[0].Columns.Remove(grdds.Tables[0].Columns["District"]);
                grdds.Tables[0].Columns.Remove(grdds.Tables[0].Columns["StateCode"]);
                grdds.Tables[0].Columns.Remove(grdds.Tables[0].Columns["NAV_ID"]);
                grdds.Tables[0].Columns.Remove(grdds.Tables[0].Columns["ExmptTdsLimit"]);
                grdds.Tables[0].Columns.Remove(grdds.Tables[0].Columns["ExmptTDS"]);
                grdds.Tables[0].Columns.Remove(grdds.Tables[0].Columns["SalesExecID"]);
                grdds.Tables[0].Columns.Remove(grdds.Tables[0].Columns["Online_Tkt"]);
                grdds.Tables[0].Columns.Remove(grdds.Tables[0].Columns["Agent_status"]);
                grdds.Tables[0].Columns.Remove(grdds.Tables[0].Columns["ag_logo"]);
                grdds.Tables[0].Columns.Remove(grdds.Tables[0].Columns["Distr"]);
                grdds.Tables[0].Columns.Remove(grdds.Tables[0].Columns["City"]);
                grdds.Tables[0].Columns.Remove(grdds.Tables[0].Columns["State"]);
                grdds.Tables[0].Columns.Remove(grdds.Tables[0].Columns["Country"]);
                grdds.Tables[0].Columns.Remove(grdds.Tables[0].Columns["zipcode"]);
                STDom.ExportData(grdds);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    protected void btn_result_Click(object sender, EventArgs e)
    {
        CheckEmptyValue();
    }
    public void CheckEmptyValue()
    {
        try
        {
            string FromDate = null;
            string ToDate = null;
            if (String.IsNullOrEmpty(Request["From"]))
            {
                FromDate = "";
            }
            else
            {

                FromDate = Request["From"].Substring(3, 2) + "/" + Request["From"].Substring(0, 2) + "/" + Request["From"].Substring(6, 4);
                FromDate = FromDate + " " + "12:00:00 AM";
            }
            if (String.IsNullOrEmpty(Request["To"]))
            {
                ToDate = "";
            }
            else
            {
                ToDate = Request["To"].Substring(3, 2) + "/" + Request["To"].Substring(0, 2) + "/" + Request["To"].Substring(6, 4);
                ToDate = ToDate + " " + "11:59:59 PM";
            }
            string AgentID = String.IsNullOrEmpty(Request["hidtxtAgencyName"]) | Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"];
            string OrderID = String.IsNullOrEmpty(txtOrderID.Text) ? "" : txtOrderID.Text.Trim();
            string Source = String.IsNullOrEmpty(TxtSource.Text) ? "" : TxtSource.Text.Trim();
            string Destination = String.IsNullOrEmpty(TxtDestination.Text) ? "" : TxtDestination.Text.Trim();
            string orderID = String.IsNullOrEmpty(txtOrderID.Text) ? "" : txtOrderID.Text.Trim();
            string BusOperator = String.IsNullOrEmpty(txtBusOperator.Text) ? "" : txtBusOperator.Text.Trim();
            string TicketNo = String.IsNullOrEmpty(txtTicketNo.Text) ? "" : txtTicketNo.Text.Trim();
            string Pnr = String.IsNullOrEmpty(txtPnr.Text) ? "" : txtPnr.Text.Trim();
            if (AgentID != "")
            {
                grdds.Clear();
                grdds = BUSDetails(Session["UID"].ToString(), Session["User_Type"].ToString(), FromDate, ToDate, OrderID, AgentID, "", Source, Destination, BusOperator, TicketNo, Pnr);
                ViewState["grdds"] = grdds;
                GrdBusReport.DataSource = grdds;
                GrdBusReport.DataBind();
            }
            else
            {
                grdds.Clear();
                grdds = BUSDetails(Session["UID"].ToString(), Session["User_Type"].ToString(), FromDate, ToDate, OrderID, AgentID, "", Source, Destination, BusOperator, TicketNo, Pnr);
                ViewState["grdds"] = grdds;
                GrdBusReport.DataSource = grdds;
                GrdBusReport.DataBind();
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
}	