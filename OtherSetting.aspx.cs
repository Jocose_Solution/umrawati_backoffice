﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class OtherSetting : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ((Label)Page.Master.FindControl("lblBC")).Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Settings </a><a class='current' href='#'>Other Settings</a>";
        if (Page.Request.QueryString.Keys.Count > 0)
        {

         
            if (Page.Request.QueryString["r"] == "airfee")
                addUC("~/Reports/Other_Settings/AirlineFee.ascx");
            if (Page.Request.QueryString["r"] == "hldbcharge")
                addUC("~/Reports/Other_Settings/HoldBookingCharges.ascx");
            if (Page.Request.QueryString["r"] == "aps")
                addUC("~/Reports/Other_Settings/airproviderswitch.ascx");
            
             else if (Page.Request.QueryString["r"] == "tds")
                addUC("~/Reports/Other_Settings/Agent_Tds.ascx");
        }
        else
        {
            addUC("~/Reports/Other_Settings/Agent_Tds.ascx");
        }

    }
    private void addUC(string path)
    {
        UserControl myControl = (UserControl)Page.LoadControl(path);

        //UserControlHolder is a place holder on the aspx page where I want to load the
        //user control to.
        phModule.Controls.Add(myControl);
    }
}