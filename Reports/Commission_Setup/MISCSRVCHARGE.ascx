﻿
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MISCSRVCHARGE.ascx.vb" Inherits="Reports_Financial_WebUserControl" %>

<%@ Register Src="~/UserControl/Settings.ascx" TagPrefix="uc1" TagName="Settings" %>


    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
    <script type='text/javascript'>
        $(function () {
            $('#ctl00_ContentPlaceHolder1_drpMarkupType').change(function () {
                debugger;
                var x = $(this).val();
                // and update the hidden input's value
                $('#ctl00_ContentPlaceHolder1_txt_basic').val(0);
                $('#ctl00_ContentPlaceHolder1_txt_CYQ').val(0);
                $('#ctl00_ContentPlaceHolder1_txt_CYB').val(0);
                $('#ctl00_ContentPlaceHolder1_TXTAmount').val(0);
                if (x == 'P') {
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_basic").readOnly = false;
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_CYQ").readOnly = false;
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_CYB").readOnly = false;
                    document.getElementById("ctl00_ContentPlaceHolder1_TXTAmount").readOnly = true;
                    $('#ctl00_ContentPlaceHolder1_txt_basic').val(0);
                    $('#ctl00_ContentPlaceHolder1_txt_CYQ').val(0);
                    $('#ctl00_ContentPlaceHolder1_txt_CYB').val(0);
                    $('#ctl00_ContentPlaceHolder1_TXTAmount').val(0);
                }
                else if (x == 'F') {
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_basic").readOnly = true;
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_CYQ").readOnly = true;
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_CYB").readOnly = true;
                    document.getElementById("ctl00_ContentPlaceHolder1_TXTAmount").readOnly = false;
                    $('#ctl00_ContentPlaceHolder1_txt_basic').val(0);
                    $('#ctl00_ContentPlaceHolder1_txt_CYQ').val(0);
                    $('#ctl00_ContentPlaceHolder1_txt_CYB').val(0);
                    $('#ctl00_ContentPlaceHolder1_TXTAmount').val(0);
                }
                else {
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_basic").readOnly = true;
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_CYQ").readOnly = true;
                    document.getElementById("ctl00_ContentPlaceHolder1_txt_CYB").readOnly = true;
                    document.getElementById("ctl00_ContentPlaceHolder1_TXTAmount").readOnly = true;
                    $('#ctl00_ContentPlaceHolder1_txt_basic').val(0);
                    $('#ctl00_ContentPlaceHolder1_txt_CYQ').val(0);
                    $('#ctl00_ContentPlaceHolder1_txt_CYB').val(0);
                    $('#ctl00_ContentPlaceHolder1_TXTAmount').val(0);

                }

            });
        });
</script>

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
<asp:UpdatePanel ID="updatepnl" runat="server">

<ContentTemplate>
    <div class="row">
        <div class="col-md-12">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                <%--    <div class="panel-heading">
                        <h3 class="panel-title">Flight Setting > MISC SERVICE CHARGE</h3>
                    </div>--%>
                    <div class="panel-body">

                        <div class="row">
                             <div class="col-md-2">
                                <div class="form-group">
                                    <%--<label for="exampleInputPassword1">Trip</label>--%>
                                    <asp:DropDownList ID="ddl_TripType" runat="server" CssClass="input-text full-width" required="">
                                        <asp:ListItem value disable style="display:none">SELECT TRIP TYPE</asp:ListItem>
                                        <asp:ListItem Value="D">Domestic</asp:ListItem>
                                        <asp:ListItem Value="I">International</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                             <div class="col-md-2">
                                <div class="form-group">
                                   <%-- <label for="exampleInputPassword1">Airline</label>--%>
                                    <%--<asp:DropDownList ID="ddl_airline" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="ALL">--ALL--</asp:ListItem>
                                        <asp:ListItem Value="AI">Air India</asp:ListItem>
                                        <asp:ListItem Value="9W">Jet Airways</asp:ListItem>
                                        <asp:ListItem Value="UK">Vistara</asp:ListItem>
                                        <asp:ListItem Value="6E">Indigo</asp:ListItem>
                                        <asp:ListItem Value="G8">Goair</asp:ListItem>
                                        <asp:ListItem Value="SG">Spicejet</asp:ListItem>
                                    </asp:DropDownList>--%>

                                    <asp:DropDownList ID="ddl_airline" CssClass="input-text full-width" runat="server" data-placeholder="Choose a Airline..." required="">
                                    </asp:DropDownList>
                                </div>
                            </div>

                             <div class="col-md-2">
                                <div class="form-group">
                                    <%--<label for="exampleInputPassword1"></label>--%>
                                    <asp:DropDownList ID="DdlFareType" runat="server" CssClass="input-text full-width" required="">
                                          <asp:ListItem value disable style="display:none">FARE TYPE</asp:ListItem>
                                       <%-- <asp:ListItem Value="ALL" Text="ALL" Selected="True"></asp:ListItem>--%>
                                        <asp:ListItem Value="NRM" Text="Normal Fare"></asp:ListItem>
                                        <asp:ListItem Value="CRP" Text="Corporate Fare"></asp:ListItem>
                                        <asp:ListItem Value="CPN" Text="Coupon Fare"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                                                       
                       

                     
                            
                            <div class="col-md-2">
                                <div class="form-group">
                                   <%-- <label for="exampleInputPassword1">GroupType</label>--%>
                                    <asp:DropDownList ID="ddl_GroupType" runat="server" CssClass="input-text full-width" required="">
                         <asp:ListItem value disable style="display:none">GROUP TYPE</asp:ListItem>      
                                        <%--<asp:ListItem Value="ALL">--ALL--</asp:ListItem>--%>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                   <%-- <label for="exampleInputPassword1">Brand Id</label>--%>
                                    <input type="text" id="txtAgencyName" placeholder="BRAND ID" class=" form-control input-text full-width" name="txtAgencyName" onfocus="focusObjag(this);"
                                        onblur="blurObjag(this);" defvalue="ALL" autocomplete="off"  required=""/>
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                      <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-black-tie"></span>
                                        </span>
                                </div>
                            </div>
                       
                            <div class="col-md-2">
                                <div class="form-group">
                                   <%-- <label for="exampleInputPassword1">Markup Type</label>--%>
                                    <asp:DropDownList ID="drpMarkupType" runat="server" CssClass="input-text full-width" required="">
                                         <asp:ListItem value disable style="display:none">MARKUP TYPE</asp:ListItem>
<%--                                        <asp:ListItem Value="0" Text="--Select Markup Type--" Selected="True"></asp:ListItem>--%>
                                        <asp:ListItem Value="P" Text="PERCENTAGE"></asp:ListItem>
                                        <asp:ListItem Value="F" Text="FIXED"></asp:ListItem>                                       
                                    </asp:DropDownList>
                                </div>
                            </div>
                                   </div>                     
                       

                       

                        
                         <div class="row">
                        <div class="col-md-2">
                                <div class="form-group">
                                   <%-- <label for="exampleInputPassword1">Origin</label>--%>
                                    <asp:TextBox ID="TXTOrg" placeholder="ORIGIN" CssClass="input-text full-width" runat="server" MaxLength="3" onfocus="focusObjag(this);"
                                        autocomplete="off"  value="ALL"></asp:TextBox>
                                </div>
                            </div>
                        
                        <div class="col-md-2">
                                <div class="form-group">
                                    <%--<label for="exampleInputPassword1">Destination</label>--%>
                                    <asp:TextBox ID="TXTDest" PLACEHOLDER="DESTINATION" CssClass="input-text full-width" runat="server" MaxLength="3" onfocus="focusObjag(this);"
                                       autocomplete="off" value="ALL"></asp:TextBox>

                                </div>
                            </div>

                        <div class="col-md-2">
                                <div class="input-group">
                                   <%-- <label for="exampleInputPassword1">Markup Basic :</label>--%>
                                    <asp:TextBox ID="txt_basic" PLACEHOLDER="MARKUP BASIC" CssClass="form-control input-text full-width" runat="server" onKeyPress="return keyRestrict(event,'.0123456789');"
                                        MaxLength="5" required=""> </asp:TextBox>
                                         <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="fa  fa-inr"></span>
                                    </span>
                                   
                                    
                                </div>
                            </div>
                        
                      


                       

                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">Markup YQ:</label>--%>
                                    <asp:TextBox ID="txt_CYQ" placeholder="MARKUP YQ" CssClass="form-control input-text full-width" runat="server" onKeyPress="return isNumberKey(event)" MaxLength="5" required=""> </asp:TextBox>
                                         <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="fa  fa-inr"></span>
                                    </span>
                                   
                                   
                                </div>
                            </div>
                          
                        
                        <div class="col-md-2">
                                <div class="input-group">
                                   <%-- <label for="exampleInputPassword1">Markup(BASIC+YQ) :</label>--%>
                                    <asp:TextBox ID="txt_CYB" runat="server" PLACEHOLDER="MARKUP(BASIC+YQ)" CssClass="form-control input-text full-width" onKeyPress="return isNumberKey(event)" MaxLength="5"
                                      required=""></asp:TextBox>
                                         <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="fa  fa-inr"></span>
                                    </span>
                                    
                                     
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                   <%-- <label for="exampleInputPassword1">Amount</label>--%>
                                    <asp:TextBox ID="TXTAmount" placeholder="AMOUNT" CssClass="form-control input-text full-width" onKeyPress="return isNumberKey(event)" runat="server" MaxLength="5" required=""></asp:TextBox>
                                 <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="fa  fa-inr"></span>
                                    </span>
                                </div>
                            </div>
                            
                            <div class="col-md-2" style="float:right">
                               
                                <div class="form-group">
                                    <br />
                                  <%--  <label for="exampleInputPassword1"></label>--%>
                                    <asp:Button ID="save" runat="server" Text="Save" CssClass="btn btn-success" OnClientClick=" return validate()"
                                        OnClick="SAVE_Click" />
                                    <asp:Button ID="btnreset" runat="server" OnClick="btnreset_Click" Text="RESET" Visible="false" />
                                </div>
                            </div>
                        </div>

                        <div class="row" style="background-color: #fff; overflow: auto;">
                            <div class="large-10 medium-12 small-12 large-push-1">

                                <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>--%>
                                        <asp:GridView ID="grdemp" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover" GridLines="None" Width="100%"
                                            OnRowEditing="grdemp_RowEditing" OnRowUpdating="grdemp_RowUpdating"
                                            OnRowCancelingEdit="grdemp_RowCancelingEdit" OnRowDeleting="grdemp_RowDeleting" style="text-transform:uppercase">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbtnCounter" runat="server" Text='<%#Bind("Counter")%>' CssClass="hide"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Trip">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbtnTrip" runat="server" Text='<%#Bind("Trip")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Airline">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbtnAirline" runat="server" Text='<%#Bind("Airline")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField> 
                                                <asp:TemplateField HeaderText="FareType">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbFareType" runat="server" Text='<%#Bind("FareType")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                
                                                <asp:TemplateField HeaderText="GroupType">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbtnGroupType" runat="server" Text='<%#Bind("GroupType")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="AgentId">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbtnAgentId" runat="server" Text='<%#Bind("AgentId")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderText="Origin">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbtnOrg" runat="server" Text='<%#Bind("Org")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Destination">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbtdest" runat="server" Text='<%#Bind("Dest")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="MarkupType">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbMarkupType" runat="server" Text='<%#Bind("MarkupType")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbtnAmount" runat="server" Text='<%#Bind("Amount")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtAmount" onKeyPress="return isNumberKey(event)" runat="server"
                                                            Text='<%#Eval("Amount") %>' Width="90px" MaxLength="7"></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="MarkupOnBasic">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbCommisionOnBasic" runat="server" Text='<%#Bind("CommisionOnBasic")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtCommisionOnBasic" onKeyPress="return isNumberKey(event)" runat="server"
                                                            Text='<%#Eval("CommisionOnBasic")%>' Width="90px" MaxLength="5"></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="MarkupOnYq">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbCommissionOnYq" runat="server" Text='<%#Bind("CommissionOnYq")%>' MaxLength="5"></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtCommissionOnYq" onKeyPress="return isNumberKey(event)" runat="server"
                                                            Text='<%#Eval("CommissionOnYq")%>' Width="90px" MaxLength="5"></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="MarkupOnBasicYq">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbCommisionOnBasicYq" runat="server" Text='<%#Bind("CommisionOnBasicYq")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtCommisionOnBasicYq" onKeyPress="return isNumberKey(event)" runat="server"
                                                            Text='<%#Eval("CommisionOnBasicYq")%>' Width="90px" MaxLength="5"></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>                                               
                                                <asp:TemplateField HeaderText="CreatedDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbtncreatedDate" runat="server" Text='<%#Bind("createdDate")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                 <asp:TemplateField HeaderText="UpadtedDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbUpadtedDate" runat="server" Text='<%#Bind("UpdatedDate")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="btnEdit" ImageUrl="~/Images/icons/edit_editor_pen_pencil_write-512.png" style="width: 23px;" runat="server" CommandName="Edit" Text="Edit" formnovalidate=""/>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:ImageButton ID="btnUpdate" ImageUrl="~/Images/icons/update.png" style="width: 23px;" runat="server" CommandName="Update"  Text="Update" formnovalidate=""/>&nbsp;&nbsp
                                            <asp:ImageButton ID="btnCancel" ImageUrl="~/Images/icons/cancle.png" style="width: 23px;" runat="server" CommandName="Cancel"  Text="Cancel" formnovalidate=""/>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="btnDelete" ImageUrl="~/Images/icons/Recycle_Bin_Full.png" style="width: 23px;"  runat="server" CommandName="Delete"  Text="Delete"
                                                            OnClientClick="if(!confirm('Do you want to delete?')){ return false; };" formnovalidate=""/>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                        </asp:GridView>
                                   <%-- </ContentTemplate>
                                </asp:UpdatePanel>--%>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        function isNumberKey(event) {

            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode >= 48 && charCode <= 57 || charCode == 08 || charCode == 46) {
                return true;
            }
            else {
                alert("please enter 0 to 9 only ");
                return false;
            }
        }
        function isCharKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode >= 65 && charCode <= 122 || charCode == 32 || charCode == 08) {
                return true;
            }
            else {
                alert("please enter char type ");
                return false;
            }
        }
        function validate() {
            debugger;
            var Amount = document.getElementById('<%=TXTAmount.ClientID %>');

            //            if (Trip.value == "--Select--") {
            //                alert("Please select trip.");
            //                Airline.focus();
            //                return false;
            //            }
            //             if (Trip.value == "") {
            //                 alert("Enter Trip");
            //                 Trip.focus();
            //                 return false;
            //             }

            //            if (AgentId.value == "") {
            //                alert("Enter AgentId");
            //                AgentId.focus();
            //                return false;
            //            }


            //             if (GroupType.value == "") {
            //                 alert("Enter GroupType");
            //                 GroupType.focus();
            //                 return false;
            //             }
            //            if (Org.value == "") {
            //                alert("Enter Org");
            //                Org.focus();
            //                return false;
            //            }


            //            if (Dest.value == "") {
            //                alert("Enter Dest");
            //                Dest.focus();
            //                return false;
            //            }

            if ($('#ctl00_ContentPlaceHolder1_drpMarkupType').val() == "0") {
                alert("Select Markup Type.");
                $('#ctl00_ContentPlaceHolder1_drpMarkupType').focus()
                return false;
            }

            if ($('#ctl00_ContentPlaceHolder1_drpMarkupType').val() == "P") {
                var Basic = true;
                var YQ = true;
                var BasicYQ = true;
                if ($('#ctl00_ContentPlaceHolder1_txt_basic').val() == "" || $('#ctl00_ContentPlaceHolder1_txt_basic').val() == "0") {
                    Basic = false;
                }
                if ($('#ctl00_ContentPlaceHolder1_txt_CYQ').val() == "" || $('#ctl00_ContentPlaceHolder1_txt_CYQ').val() == "0") {
                    YQ = false;
                }
                if ($('#ctl00_ContentPlaceHolder1_txt_CYB').val() == "" || $('#ctl00_ContentPlaceHolder1_txt_CYB').val() == "0") {
                    BasicYQ = false;
                }

                if (Basic == false && YQ == false && BasicYQ == false) {
                    alert("Enter percentage amount.");
                    $('#ctl00_ContentPlaceHolder1_txt_basic').focus();
                    return false;
                }
                $('#ctl00_ContentPlaceHolder1_TXTAmount').val(0);

            }
            if ($('#ctl00_ContentPlaceHolder1_drpMarkupType').val() == "F") {
                $('#ctl00_ContentPlaceHolder1_txt_basic').val(0);
                $('#ctl00_ContentPlaceHolder1_txt_CYQ').val(0);
                $('#ctl00_ContentPlaceHolder1_txt_CYB').val(0);
                if ($('#ctl00_ContentPlaceHolder1_TXTAmount').val() == "") {
                    alert("Enter Amount");
                    $('#ctl00_ContentPlaceHolder1_TXTAmount').focus();
                    return false;
                }

                if ($('#ctl00_ContentPlaceHolder1_TXTAmount').val() == "0") {
                    alert("Enter valid amount");
                    $('#ctl00_ContentPlaceHolder1_TXTAmount').focus();
                    return false;
                }
                //if (Amount.value == "") {
                //    alert("Enter Amount");
                //    Amount.focus();
                //    return false;
                //}
            }

            if ($('#ctl00_ContentPlaceHolder1_txt_basic').val() = "") {
                $('#ctl00_ContentPlaceHolder1_txt_basic').val(0);
            }
            if ($('#ctl00_ContentPlaceHolder1_txt_CYQ').val() = "") {
                $('#ctl00_ContentPlaceHolder1_txt_CYQ').val(0);
            }
            if ($('#ctl00_ContentPlaceHolder1_txt_CYB').val() = "") {
                $('#ctl00_ContentPlaceHolder1_txt_CYB').val(0);
            }
            if ($('#ctl00_ContentPlaceHolder1_txt_CYQ').val() = "") {
                $('#ctl00_ContentPlaceHolder1_txt_CYQ').val(0);
            }
            if ($('#ctl00_ContentPlaceHolder1_TXTAmount').val() = "") {
                $('#ctl00_ContentPlaceHolder1_TXTAmount').val(0);
            }


            if (confirm("Are you sure??"))
                return true;
            return false;
        }
    </script>

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

    <script type="text/javascript">
        function focusObjag(obj) { if (obj.value == "ALL") obj.value = ""; }
        function blurObjag(obj) { if (obj.value == "") obj.value = "ALL"; }

        function focusObjorigin(obj) { if (obj.value == "ALL") obj.value = ""; }
        function blurObjorigin(obj) { if (obj.value == "") obj.value = "ALL"; }

        function focusObjdest(obj) { if (obj.value == "ALL") obj.value = ""; }
        function blurObjdest(obj) { if (obj.value == "") obj.value = "ALL"; }

    </script>


