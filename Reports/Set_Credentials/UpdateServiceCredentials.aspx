﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UpdateServiceCredentials.aspx.cs" Inherits="DetailsPort_Admin_UpdateServiceCredentials" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/bootstrap.min.css" rel="stylesheet" />
    <style type="text/css">
        .tablesss {
            margin-bottom: 10px;
        }

        .button {
            position: relative;
            display: inline-block;
            padding: 12px 24px;
            margin: .3em 0 1em 0;
            width: 100%;
            vertical-align: middle;
            color: #fff;
            font-size: 16px;
            line-height: 20px;
            border-radius: 2px;
            -webkit-font-smoothing: antialiased;
            text-align: center;
            letter-spacing: 1px;
            background: transparent;
            border: 0;
            border-bottom: 2px solid #af3e3a;
            cursor: pointer;
            -webkit-transition: all 0.15s ease;
            transition: all 0.15s ease;
        }

        .buttonBlue {
            background: #d9534f;
        }

            .buttonBlue:hover {
                background: #af3e3a;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="row">
                <div class="col-md-2">
                </div>
                <div class="col-md-10">
                    <div class="page-wrapperss">

                        <div class="panel panel-primary">
                            <div class="panel-heading" style="background: #fdb714">
                                <h3 class="panel-title">Update Searching Credential</h3>
                            </div>
                            <div class="panel-body">

                                 <div class="row">
                                    <div class="col-sm-3">
                                        <label for="exampleInputPassword1">Trip Type</label>
                                        <asp:TextBox ID="txtTripType" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                    </div>
                                      <div class="col-sm-3">
                                        <label for="exampleInputPassword1">Provider</label>
                                        <asp:TextBox ID="lblProvider" CssClass="form-control" runat="server"  ReadOnly="true"></asp:TextBox>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Airline :</label>
                                            <asp:TextBox ID="TxtAirline" CssClass="form-control" runat="server"  ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                   
                                     <div class="col-sm-3">
                                        <div class="form-group">
                                            
                                        </div>
                                    </div>
                                </div>



                                <div class="row">
                                   
                                    <div class="col-sm-3" runat="server" id="CorporateID">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">CorporateID :</label>
                                            <asp:TextBox ID="TxtCorporateID" CssClass="form-control" runat="server" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">User ID:</label>
                                            <asp:TextBox ID="TxtUserID" CssClass="form-control" runat="server" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                     <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Password :</label>
                                            <asp:TextBox ID="TxtPassword" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                   
                                    <div class="col-sm-3" runat="server" id="LoginID">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">LoginID :</label>
                                            <asp:TextBox ID="TxtLoginID" CssClass="form-control" runat="server" MaxLength="300"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-3" runat="server" id="LoginPassword">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Login Password:</label>
                                            <asp:TextBox ID="TxtLoginPwd" CssClass="form-control" runat="server" MaxLength="100"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" runat="server" id="Url">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Server Url Or IP:</label>
                                            <asp:TextBox ID="TxtServerUrlOrIP" CssClass="form-control" runat="server" MaxLength="300"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Bkg Server Url Or IP :</label>
                                            <asp:TextBox ID="TxtBkgServerUrlOrIP" CssClass="form-control" runat="server" MaxLength="300"></asp:TextBox>
                                        </div>
                                    </div>                                    
                                </div>

                                <div class="row">
                                    <div class="col-sm-3" runat="server" id="CarrierAcc" >
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">CarrierAcc(PCC):</label>
                                            <asp:TextBox ID="TxtCarrierAcc" CssClass="form-control" runat="server" MaxLength="10"></asp:TextBox>

                                        </div>
                                    </div>
                                   
                                    <div class="col-sm-3" runat="server" id="ResultFrom">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Result From:</label>
                                            <%--<asp:TextBox ID="TxtResultFrom" CssClass="form-control" runat="server" MaxLength="10"></asp:TextBox>--%>
                                        <asp:DropDownList ID="DdlResultFrom" runat="server" CssClass="form-control" Enabled="False">                                                                             
                                        <asp:ListItem Value="AP" Text="API" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="SP" Text="Scraping"></asp:ListItem>                                        
                                    </asp:DropDownList>  
                                        </div>
                                    </div>
                                   
                                     <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">CrdType:</label>
                                            <%--<asp:TextBox ID="TxtCrdType" CssClass="form-control" runat="server" MaxLength="10"></asp:TextBox>--%>
                                        <asp:DropDownList ID="DdlCrdType" runat="server" CssClass="form-control" Enabled="False">                                                 
                                        <asp:ListItem Value="NRM" Text="Normal Fare" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="CRP" Text="Corporate Fare"></asp:ListItem>
                                        <asp:ListItem Value="CPN" Text="Coupon Fare"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Status:</label>
                                            <asp:DropDownList ID="DdlStatus" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="true" Text="ACTIVE" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="false" Text="DEACTIVE"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-sm-4">
                                    </div>
                                </div>
                            </div>

                            <div class="row" align="center">
                                <asp:Button ID="BtnSubmit" runat="server" Text="Update" OnClick="BtnSubmit_Click" Style="width: 200px;" CssClass="btn btn-success" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="HdnId" runat="server" />

    </form>
</body>
</html>
