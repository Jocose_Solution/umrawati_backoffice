﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HoldBookingCharges.ascx.cs" Inherits="Reports_Other_Settings_HoldBookingCharges" %>


<script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>
    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
     <style>

        .mydatagrid th
        {
          
            background-color: black;
    color: white;
        }
    </style>
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="container-fluid" style="padding-right:35px">
            <div class="page-wrapperss" style="background-color: #ffffff2e;">
                <div class="panel panel-primary" style="background-color: #fff0;">
             
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <%--<label for="exampleInputPassword1">Trip Type</label>--%>
                                    <asp:DropDownList ID="DdlTripType" runat="server" CssClass="input-text full-width" TabIndex="1" required="">
                                        <%--<asp:ListItem  Text="SELECT TRIP TYPE" Selected="True"></asp:ListItem>--%>
                                        <asp:ListItem  value="" disabled selected style="display: none;">SELECT TRIP TYPE</asp:ListItem>
                                        <asp:ListItem Value="D" Text="DOMESTIC" ></asp:ListItem>
                                        <asp:ListItem Value="I" Text="INTERNATIONAL"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <%--<label for="exampleInputPassword1">GroupType :</label>--%>                                    
                                    <asp:DropDownList ID="ddl_ptype" CssClass="input-text full-width" runat="server" AppendDataBoundItems="true" TabIndex="2" required="">
                                        <%--<asp:ListItem  value="" disabled selected style="display: none;">SELECT GROUP TYPE</asp:ListItem>--%>
                                        
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">Brand Id :</label>--%>
                                    <input type="text" placeholder="ENTER BRAND ID" id="txtAgencyName" name="txtAgencyName"
                                        autocomplete="off" 
                                        class="form-control input-text full-width" tabindex="3" required=""/>
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />   
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                    <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-black-tie"></span> 
                                        </span>                               
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">Airline :</label>--%>                                   
                                    <input type="text" placeholder="AIRLINES" class="form-control input-text full-width" name="txtAirline" value="" id="txtAirline" tabindex="4" required=""/>
                                    <input type="hidden" id="hidtxtAirline" name="hidtxtAirline" value="" />
                                    <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-plane"></span> 
                                        </span> 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                              <div class="col-md-3">
                                <div class="form-group">
                                    <%--<label for="exampleInputPassword1">Charges :</label>--%>                                     
                                    <asp:TextBox ID="TxtCharges" placeholder="HOLD BOOKING CHARGES" runat="server" CssClass="input-text full-width" TabIndex="5"  MaxLength="5" onKeyPress="return keyRestrict(event,'.0123456789');" required=""></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                 <div class="form-group">
                                    <%--<label for="exampleInputPassword1">Status:</label>--%>
                                    <asp:DropDownList ID="DdlStatus" runat="server" CssClass="input-text full-width" TabIndex="6" required="">
                                        <%--<option value="" selected disabled>Please select</option>--%>
                                        <asp:ListItem  value="" disabled selected style="display: none;">SELECT STATUS</asp:ListItem>
                                        <asp:ListItem Value="true" Text="ACTIVE" ></asp:ListItem>
                                        <asp:ListItem Value="false" Text="DEACTIVE"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                           <div class="col-md-3" style="margin-top: 11px">
                                <div class="form-group">                                     
                                    <br />
                                    <asp:Button ID="BtnSubmit" runat="server" Text="Submit" OnClick="BtnSubmit_Click" CssClass="btn btn-success" TabIndex="7" style="margin-top: -45px;" />
                                </div>
                            </div>
                         
                        </div>
                        <div class="row">

                             
                        </div>
                        <div class="clear"></div>
                        <%--<hr />--%>
                        <div class="row">
                            <div class="col-md-14">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow-y: scroll; max-height: 500px;">
                                    <ContentTemplate>
                                        <asp:GridView ID="grd_P_IntlDiscount" runat="server" AutoGenerateColumns="false"
                                            class="table"  GridLines="None" Width="100%" PageSize="30" OnRowCancelingEdit="grd_P_IntlDiscount_RowCancelingEdit"
                                            OnRowEditing="grd_P_IntlDiscount_RowEditing" OnRowUpdating="grd_P_IntlDiscount_RowUpdating" OnRowDeleting="OnRowDeleting"  AllowPaging="true"
                                            OnPageIndexChanging="OnPageIndexChanging">
                                            <Columns>                                            
                                                <asp:TemplateField HeaderText="Trip_Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Visible="false" Text='<%#Eval("ID") %>'></asp:Label>
                                                        <asp:Label ID="lblTripTypeName" runat="server" Text='<%#Eval("TripTypeName") %>'></asp:Label>                                                        
                                                    </ItemTemplate>
                                                </asp:TemplateField> 
                                                <asp:TemplateField HeaderText="Airline">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAirlineName" runat="server" Text='<%#Eval("AirlineName") %>'></asp:Label>
                                                    </ItemTemplate>                                               
                                                </asp:TemplateField>                                               
                                                <asp:TemplateField HeaderText="Group_Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGroupType" runat="server" Text='<%#Eval("GroupType") %>'></asp:Label>                                                        
                                                    </ItemTemplate>                                                   
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Brand_Id">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAgentId" runat="server" Text='<%#Eval("AgentId") %>'></asp:Label>                                                        
                                                    </ItemTemplate>                                                                                             
                                                </asp:TemplateField>
                                                                                                
                                                 <asp:TemplateField HeaderText="Charges">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCharges" runat="server" Text='<%#Eval("Charges") %>'></asp:Label>
                                                    </ItemTemplate>
                                                      <EditItemTemplate>
                                                        <asp:TextBox ID="txt_Charges" runat="server" Text='<%#Eval("Charges") %>' Width="60px"
                                                            onKeyPress="return keyRestrict(event,'.0123456789');" MaxLength="5" BackColor="#ffff66"></asp:TextBox>                                                        
                                                    </EditItemTemplate>
                                                </asp:TemplateField>                                                                                         
                                                <asp:TemplateField HeaderText="Active">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIsActive" runat="server" Text='<%#Eval("IsActive") %>' Style="text-wrap: inherit;"></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:DropDownList ID="ddl_IsActive" runat="server" Width="150px" DataValueField='<%#Eval("IsActive")%>' SelectedValue='<%#Eval("IsActive")%>'>
                                                             <asp:ListItem Value="True" Text="ACTIVE"></asp:ListItem>
                                                             <asp:ListItem Value="False" Text="DEACTIVE"></asp:ListItem> 
                                                        </asp:DropDownList>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>                                               
                                                <asp:TemplateField HeaderText="CreatedDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreatedDate" runat="server" Text='<%#Eval("CreatedDate") %>'></asp:Label>
                                                    </ItemTemplate>                                                  
                                                </asp:TemplateField>                                                                                                                                        
                                               <%-- <asp:TemplateField HeaderText="EDIT">
                                                    <ItemTemplate>
                                                        <asp:Button ID="lnledit" runat="server" Text="Edit" CommandName="Edit" Font-Bold="true"
                                                            CssClass="newbutton_2" />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>                                                       
                                                        <asp:Button ID="lnlupdate" runat="server" Text="Update" CommandName="Update" Font-Bold="true" CssClass="newbutton_2" />
                                                        <asp:Button ID="lnlcancel" runat="server" Text="Cancel" CommandName="Cancel" Font-Bold="true"
                                                            CssClass="newbutton_2" />
                                                    </EditItemTemplate>
                                                </asp:TemplateField>--%>
                                                 <asp:TemplateField HeaderText="Update">
                                                    <ItemTemplate>                                                        
                                                        <a href='Detailsport/Admin/UpdateFlightHoldBookingSetting.aspx?ID=<%#Eval("ID")%>' rel="lyteframe" rev="width: 900px; height: 400px; overflow:hidden;"
                                                            target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                          <u>Update</u> 
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>                                                      
                                                       <asp:Button ID="btn_delete" CssClass="newbutton_2" runat="server" Text="Delete" formnovalidate="" CommandName="Delete" OnClientClick="if(!confirm('Do you want to delete?')){ return false; };" Font-Bold="true" />
                                                    </ItemTemplate>                                                   
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                            <EmptyDataTemplate>No records Found</EmptyDataTemplate>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>


                                <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                    <ProgressTemplate>
                                        <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                        </div>
                                        <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                            Please Wait....<br />
                                            <br />
                                            <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                            <br />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/search4.js") %>"></script>     --%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

    <script type="text/javascript">
        function AppliedOn() {
            //var DealType = $("#ctl00_ContentPlaceHolder1_DdlDealCodeType").val();
            //if (DealType == "PF") {
            //    $("#DivAppliedOn").show();
            //    $("#DivFareType").hide();
            //}
            //else if (DealType == "PC") {
            //    $("#DivAppliedOn").show();
            //    $("#DivFareType").show();
            //}
            //else {
            //    $("#DivAppliedOn").hide();
            //    $("#DivFareType").hide();
            //}
        }

        function CheckDeal() {
            if ($("#ctl00_ContentPlaceHolder1_TxtCharges").val() == "") {
                alert("Enter hold booking charges :.");
                $("#ctl00_ContentPlaceHolder1_TxtCharges").focus();
                return false;
            }
        }



    </script>


    <script language="javascript" type="text/javascript">
        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }
        </script>