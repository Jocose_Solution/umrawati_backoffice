﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Grid.aspx.cs" Inherits="Grid" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
  
  <style>
      #form1 {
              width: 4000px;
    margin-left: 152px;
      }
      th {
    width: 146px;
        height: 48px;
}
      #Item {
         margin-left: 59px;
      }

      #Item1 {
           margin-left: 59px;
      }
  </style>
</head>
<body>
    <div>
        <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
      <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
       
     <form id="form1" runat="server">
           
<%--Code For grid--%>
      <%--   <asp:Button ID="btnConfirm" runat="server" Text="Search" ForeColor="white" BackColor="#006699" BorderColor="#006699" BorderStyle="Double" Font-Bold="true" Width="99px" Height="27px"
   PostBackUrl="~/Insert.aspx" />--%>
       <div id="griddiv">
      <asp:GridView ID="gvPhoneBook" runat="server" AutoGenerateColumns="false"   ShowFooter="true" DataKeyNames="Counter"
         OnRowEditing="OnRowEditing"
         OnRowCancelingEdit="gvPhoneBook_RowCancelingEdit"
         OnRowUpdating="gvPhoneBook_RowUpdating"
         OnRowDeleting="gvPhoneBook_RowDeleting"
         OnRowCommand="gvPhoneBook_RowCommand"
         PageSize = "10"
         AllowPaging="true"
         PageButtonCount="4" FirstPageText="First" LastPageText="Last"
         PagerSettings-Mode="NumericFirstLast"
         OnPageIndexChanging="GridView1_PageIndexChanging"
       CellPadding="3"  style="margin-right: 79px"
       BackColor="White" BorderColor="#CCCCCC" 
       BorderStyle="None" BorderWidth="1px" >
          <%--Gird View Theme code --%>
          <FooterStyle BackColor="White" ForeColor="#000066" />
          <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
          <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
          <RowStyle ForeColor="#000066" />
          <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
          <SortedAscendingCellStyle BackColor="#F1F1F1" />
          <SortedAscendingHeaderStyle BackColor="#007DBB" />
          <SortedDescendingCellStyle BackColor="#CAC9C9" />
          <SortedDescendingHeaderStyle BackColor="#00547E" />
          <Columns>

              
               <asp:TemplateField HeaderText="Airport Code">
                  <ItemTemplate>
                    <asp:Label ID="Label2"   Text='<%# Eval("AirportCode") %>' runat="server" />
                  </ItemTemplate>

                   
                  <EditItemTemplate>
                        <asp:Textbox ID="AirportCode1"  Text='<%# Eval("AirportCode") %>' runat="server"/>
                      <asp:RequiredFieldValidator ID="reqword11" ControlToValidate="AirportCode1" ValidationGroup="LoginFrameq" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                  </EditItemTemplate>
                  
                  <FooterTemplate>
                      <asp:Textbox ID="AirportCode"  Text='<%# Eval("AirportCode") %>' runat="server" />
                  <asp:RequiredFieldValidator ID="reqword1" ControlToValidate="AirportCode" ValidationGroup="LoginFrame" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                  </FooterTemplate>
              </asp:TemplateField>

               <asp:TemplateField HeaderText="Airport Name">
                  <ItemTemplate>
                    <asp:Label ID="Label3"   Text='<%# Eval("AirportName") %>' runat="server" />
                  </ItemTemplate>
                  <EditItemTemplate>
                        <asp:Textbox ID="AirportName1"  Text='<%# Eval("AirportName") %>' runat="server" />
                      <asp:RequiredFieldValidator ID="reqword22" ControlToValidate="AirportName1" ValidationGroup="LoginFrameq" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>

                  </EditItemTemplate>
                  <FooterTemplate>
                      <asp:Textbox ID="AirportName"  Text='<%# Eval("AirportName") %>' runat="server" />
                        <asp:RequiredFieldValidator ID="reqword2" ControlToValidate="AirportName" ValidationGroup="LoginFrame" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                  </FooterTemplate>
              </asp:TemplateField>
             



               <asp:TemplateField HeaderText="City Name">
                  <ItemTemplate>
                    <asp:Label ID="Label4"   Text='<%# Eval("CityName") %>' runat="server" />
                  </ItemTemplate>
                  <EditItemTemplate>
                        <asp:Textbox ID="CityName1"  Text='<%# Eval("CityName") %>' runat="server" />
                  </EditItemTemplate>
                  <FooterTemplate>
                      <asp:Textbox ID="CityName"  Text='<%# Eval("CityName") %>' runat="server"  />
                       <asp:RequiredFieldValidator ID="reqword3" ControlToValidate="CityName" ValidationGroup="LoginFrame" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                  </FooterTemplate>
              </asp:TemplateField>

               <asp:TemplateField HeaderText="Contry Name">
                  <ItemTemplate>
                    <asp:Label ID="Label5"  Text='<%# Eval("CountryName") %>' runat="server" />
                  </ItemTemplate>
                  <EditItemTemplate>
                        <asp:Textbox ID="CountryName1"  Text='<%# Eval("CountryName") %>' runat="server" />
                      <asp:RequiredFieldValidator ID="reqword33" ControlToValidate="CountryName1" ValidationGroup="LoginFrameq" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>

                  </EditItemTemplate>
                   <FooterTemplate>
                      <asp:Textbox ID="CountryName"  Text='<%# Eval("CountryName") %>' runat="server"  />
                         <asp:RequiredFieldValidator ID="reqword4" ControlToValidate="CountryName" ValidationGroup="LoginFrame" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                  </FooterTemplate>
              </asp:TemplateField>

               <asp:TemplateField HeaderText="Country Code">
                  <ItemTemplate>
                    <asp:Label ID="Label6"   Text='<%# Eval("CountryCode") %>' runat="server" />
                  </ItemTemplate>

                  <EditItemTemplate>
                        <asp:Textbox ID="CountryCode1"  Text='<%# Eval("CountryCode") %>' runat="server" />
                      <asp:RequiredFieldValidator ID="reqword44" ControlToValidate="CountryName1" ValidationGroup="LoginFrameq" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>

                  </EditItemTemplate>

                  <FooterTemplate>
                      <asp:Textbox ID="CountryCode"  Text='<%# Eval("CountryCode") %>' runat="server"  />
                        <asp:RequiredFieldValidator ID="reqword5" ControlToValidate="CountryCode" ValidationGroup="LoginFrame" runat="server" ErrorMessage="*" ForeColor="red"></asp:RequiredFieldValidator>
                  </FooterTemplate>
              </asp:TemplateField> 
             

               <asp:TemplateField>
                  <ItemTemplate>
                    <div id="Item">
                      <asp:ImageButton  ID="ImageButton1"  ImageUrl="~/Image/Pencile.png" runat="server"  CommandName="Edit" ToolTip="Edit" Width="20px" Height="30px"/>
                      <asp:ImageButton ID="ImageButton2" ImageUrl="~/Image/delete.png" runat="server" CommandName="Delete" ToolTip="Delete" Width="20px" Height="30px"/>
                   </div>
                   </ItemTemplate>
                      
                  <EditItemTemplate>
                      <div id="Item1">
                      <asp:ImageButton ID="ImageButton3"  ImageUrl="~/Image/Update.png" runat="server"  CommandName="Update" ToolTip="Update" Width="30px" Height="20px" ValidationGroup="LoginFrameq"/>
                      <asp:ImageButton ID="ImageButton4"  ImageUrl="~/Image/Cancle.png" runat="server"  CommandName="Cancel" ToolTip="Cancel" Width="30px" Height="20px"/>
                    <div>
                 </EditItemTemplate>
                  <FooterTemplate>
                 <asp:ImageButton ID="ImageButton5"  ImageUrl="~/Image/AddNew.png" runat="server"  CommandName="AddNew" ToolTip="AddNew" Width="20px" Height="20px" ValidationGroup="LoginFrame"/>
                  </FooterTemplate>
              </asp:TemplateField>
          </Columns>
      </asp:GridView>
           </div>
    </form>
   </div>
    
</body>
</html>
