﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Grid : System.Web.UI.Page
{
    string Con = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ShowGrid();
        }
    }
    void ShowGrid()
    {
        SqlConnection conn = new SqlConnection(Con);
        SqlDataAdapter sda = new SqlDataAdapter("GetAllWorldAirportInfo", conn);
        DataTable dt = new DataTable();
        sda.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            gvPhoneBook.DataSource = dt;
            gvPhoneBook.DataBind();
            Label1.Text = "";
        }
        else
        {
            Label1.Text  = "No Records Found";
        }

    }

    protected void OnRowEditing(object sender, GridViewEditEventArgs e)
    {
        gvPhoneBook.EditIndex = e.NewEditIndex;
        ShowGrid();
    }

    protected void gvPhoneBook_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvPhoneBook.EditIndex = -1;
        ShowGrid();
    }

    protected void gvPhoneBook_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            SqlConnection conn = new SqlConnection(Con);
            SqlCommand cmd = new SqlCommand("UpdateWorldAirportInfo", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("AirportCode", (gvPhoneBook.Rows[e.RowIndex].FindControl("AirportCode1") as TextBox).Text.Trim());
            cmd.Parameters.AddWithValue("AirportName", (gvPhoneBook.Rows[e.RowIndex].FindControl("AirportName1") as TextBox).Text.Trim());
            cmd.Parameters.AddWithValue("CityName", (gvPhoneBook.Rows[e.RowIndex].FindControl("CityName1") as TextBox).Text.Trim());
            cmd.Parameters.AddWithValue("CountryName", (gvPhoneBook.Rows[e.RowIndex].FindControl("CountryName1") as TextBox).Text.Trim());
            cmd.Parameters.AddWithValue("CountryCode", (gvPhoneBook.Rows[e.RowIndex].FindControl("CountryCode1") as TextBox).Text.Trim());
            cmd.Parameters.AddWithValue("Counter", Convert.ToInt64(gvPhoneBook.DataKeys[e.RowIndex].Value.ToString()));
            conn.Open();
            cmd.ExecuteNonQuery();
            gvPhoneBook.EditIndex = -1;
            conn.Close();
            ShowGrid();
            Label1.Text = "Update Successfully...........!";
        }
        catch (Exception ex)
        {
            Label1.Text = ex.Message;
        }
    }

    protected void gvPhoneBook_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            SqlConnection conn = new SqlConnection(Con);
            SqlCommand cmd = new SqlCommand("DeleteWorldAirportInfo", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("Counter", Convert.ToInt64(gvPhoneBook.DataKeys[e.RowIndex].Value.ToString()));
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            ShowGrid();
            Label1.Text = " Delete Successfully...........!";
        }
        catch(Exception ex)
        {
            Label1.Text = ex.Message;
        }

        
}

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvPhoneBook.PageIndex = e.NewPageIndex;
        ShowGrid();
    }


    protected void gvPhoneBook_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("AddNew"))
        {
            try
            {
                SqlConnection conn = new SqlConnection(Con);
                SqlCommand cmd = new SqlCommand("InsertWorldAirportInfo", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Status", SqlDbType.Bit).Direction = ParameterDirection.Output;
                cmd.Parameters.AddWithValue("AirportCode", (gvPhoneBook.FooterRow.FindControl("AirportCode") as TextBox).Text.Trim());
                cmd.Parameters.AddWithValue("AirportName", (gvPhoneBook.FooterRow.FindControl("AirportName") as TextBox).Text.Trim());
                cmd.Parameters.AddWithValue("CityName", (gvPhoneBook.FooterRow.FindControl("CityName") as TextBox).Text.Trim());
                cmd.Parameters.AddWithValue("CountryName", (gvPhoneBook.FooterRow.FindControl("CountryName") as TextBox).Text.Trim());
                cmd.Parameters.AddWithValue("CountryCode", (gvPhoneBook.FooterRow.FindControl("CountryCode") as TextBox).Text.Trim());
                conn.Open();
                cmd.ExecuteNonQuery();
              bool Status = Convert.ToBoolean(cmd.Parameters["@Status"].Value);

              if (!Status)
              {
                  ShowGrid();
                  Label1.Text = "Already Existing.......!";
                  Label1.Attributes["style"] = "color:red";
              }
              else
              {
                  ShowGrid();
                  Label1.Text = " Data Insert Successfully...........!";
                  Label1.Attributes["style"] = "color:Green";
                 
                
              }
              conn.Close();
               
            }
            catch (Exception ex)
            {
                Label1.Text = ex.Message;
            }
        
        }
    }

    
}