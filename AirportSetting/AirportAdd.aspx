﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="AirportAdd.aspx.cs" Inherits="AirportSetting_AirportAdd" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script src="../../Scripts/Search3_cel.js"></script>
    <div class="row">

        <div class="container-fluid" style="padding-right: 35px">
            <div class="page-wrapperss">

                <div class="panel panel-primary">


                    <div class="panel-body" style="margin-right: -135px">
                            <div class="row">
                        <div class="col-md-12" style="font-size: 14px; color: #000;">
                            <div class="col-md-3">
                                <div class="form-group">
                        <asp:TextBox ID="RArrCity" CssClass="input-text full-width" runat="server" placeholder="Destination City" autocomplete="off"></asp:TextBox>
                        <asp:HiddenField ID="RhfArrCity" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator44" ControlToValidate="RArrCity"
                            ErrorMessage="Aiport Name is Required" Style="color: red" runat="server" ValidationGroup="AirportName" >
                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                             <div class="col-md-3">
                                <div class="form-group">
                        <asp:Button ID="btn_result" runat="server" CssClass="btn btn-success" Text="Search" OnClick="btn_result_Click" Width="74px" ValidationGroup="AirportName" />
                               </div>
                                      </div>
                                </div>
                        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                        <div id="griddiv">
                            <asp:GridView ID="gvPhoneBook" runat="server" AutoGenerateColumns="false" ShowFooter="true" DataKeyNames="Counter"
                                OnRowEditing="OnRowEditing"
                                OnRowCancelingEdit="gvPhoneBook_RowCancelingEdit"
                                OnRowUpdating="gvPhoneBook_RowUpdating"
                                OnRowDeleting="gvPhoneBook_RowDeleting"
                                OnRowCommand="gvPhoneBook_RowCommand"
                                PageSize="10"
                                AllowPaging="true"
                                PageButtonCount="4" FirstPageText="First" LastPageText="Last"
                                PagerSettings-Mode="NumericFirstLast"
                                OnPageIndexChanging="GridView1_PageIndexChanging"
                                CellPadding="3" Style="margin-right: 79px"
                                BackColor="White" BorderColor="#CCCCCC"
                                BorderStyle="None" BorderWidth="1px">
                                <%--Gird View Theme code --%>
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                <Columns>


                                    <asp:TemplateField HeaderText="Airport Code">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" Text='<%# Eval("AirportCode") %>' runat="server" />
                                        </ItemTemplate>


                                        <EditItemTemplate>
                                            <asp:TextBox ID="AirportCode1" Text='<%# Eval("AirportCode") %>' runat="server" />
                                            <asp:RequiredFieldValidator ID="reqword11" ControlToValidate="AirportCode1" ValidationGroup="LoginFrameq" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </EditItemTemplate>

                                        <FooterTemplate>
                                            <asp:TextBox ID="AirportCode" Text='<%# Eval("AirportCode") %>' runat="server" />
                                            <asp:RequiredFieldValidator ID="reqword1" ControlToValidate="AirportCode" ValidationGroup="LoginFrame" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Airport Name">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" Text='<%# Eval("AirportName") %>' runat="server" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="AirportName1" Text='<%# Eval("AirportName") %>' runat="server" />
                                            <asp:RequiredFieldValidator ID="reqword22" ControlToValidate="AirportName1" ValidationGroup="LoginFrameq" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>

                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="AirportName" Text='<%# Eval("AirportName") %>' runat="server" />
                                            <asp:RequiredFieldValidator ID="reqword2" ControlToValidate="AirportName" ValidationGroup="LoginFrame" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>




                                    <asp:TemplateField HeaderText="City Name">
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" Text='<%# Eval("CityName") %>' runat="server" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="CityName1" Text='<%# Eval("CityName") %>' runat="server" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="CityName" Text='<%# Eval("CityName") %>' runat="server" />
                                            <asp:RequiredFieldValidator ID="reqword3" ControlToValidate="CityName" ValidationGroup="LoginFrame" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Contry Name">
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" Text='<%# Eval("CountryName") %>' runat="server" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="CountryName1" Text='<%# Eval("CountryName") %>' runat="server" />
                                            <asp:RequiredFieldValidator ID="reqword33" ControlToValidate="CountryName1" ValidationGroup="LoginFrameq" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>

                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="CountryName" Text='<%# Eval("CountryName") %>' runat="server" />
                                            <asp:RequiredFieldValidator ID="reqword4" ControlToValidate="CountryName" ValidationGroup="LoginFrame" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Country Code">
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" Text='<%# Eval("CountryCode") %>' runat="server" />
                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <asp:TextBox ID="CountryCode1" Text='<%# Eval("CountryCode") %>' runat="server" />
                                            <asp:RequiredFieldValidator ID="reqword44" ControlToValidate="CountryName1" ValidationGroup="LoginFrameq" runat="server" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>

                                        </EditItemTemplate>

                                        <FooterTemplate>
                                            <asp:TextBox ID="CountryCode" Text='<%# Eval("CountryCode") %>' runat="server" />
                                            <asp:RequiredFieldValidator ID="reqword5" ControlToValidate="CountryCode" ValidationGroup="LoginFrame" runat="server" ErrorMessage="*" ForeColor="red"></asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <div id="Item">
                                                <asp:ImageButton ID="ImageButton1" ImageUrl="~/AirportSetting/Image/Pencile.png" runat="server" CommandName="Edit" ToolTip="Edit" Width="20px" Height="30px" />
                                                <asp:ImageButton ID="ImageButton2" ImageUrl="~/AirportSetting/Image/delete.png" runat="server" CommandName="Delete" ToolTip="Delete" Width="20px" Height="30px" />
                                            </div>
                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <div id="Item1">
                                                <asp:ImageButton ID="ImageButton3" ImageUrl="~/AirportSetting/Image/Update.png" runat="server" CommandName="Update" ToolTip="Update" Width="30px" Height="20px" ValidationGroup="LoginFrameq" />
                                                <asp:ImageButton ID="ImageButton4" ImageUrl="~/AirportSetting/Image/Cancle.png" runat="server" CommandName="Cancel" ToolTip="Cancel" Width="30px" Height="20px" />
                                                <div>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:ImageButton ID="ImageButton5" ImageUrl="~/AirportSetting/Image/AddNew.png" runat="server" CommandName="AddNew" ToolTip="AddNew" Width="20px" Height="20px" ValidationGroup="LoginFrame" />
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        </div>
</asp:Content>

