﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="FinanceSetup.aspx.cs" Inherits="CommissionSetup" %>

<%--<%@ Register Src="~/Reports/Commission_Setup/commissionmaster.ascx" TagPrefix="Search" TagName="commissionmaster" %>
<%@ Register Src="~/Reports/Commission_Setup/DealCodeMaster.ascx" TagPrefix="Search1" TagName="DealCodeMaster" %>
<%@ Register Src="~/Reports/Commission_Setup/MISCSRVCHARGE.ascx" TagPrefix="Search2" TagName="MISCSRVCHARGE" %>
<%@ Register Src="~/Reports/Commission_Setup/DealTransfer.ascx" TagPrefix="Search3" TagName="DealTransfer" %>
<%@ Register Src="~/Reports/Commission_Setup/AddCreditCard.ascx" TagPrefix="Search4" TagName="AddCreditCard" %>--%>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
     <script type="text/javascript" >
         function activetab(str) {
             debugger;
             if (str == "comm_mst") {
                 location.href = 'http://backoffice.UmrawatiTrip.com/FinanceSetup?r=' + str + '';
             }
             if (str == "Dlmst") {
                 location.href = 'http://backoffice.UmrawatiTrip.com/FinanceSetup?r=' + str + '';
             }
             if (str == "msc_chrg") {
                 location.href = 'http://backoffice.UmrawatiTrip.com/FinanceSetup?r=' + str + '';
             }
             if (str == "dl_tf") {
                 location.href = 'http://backoffice.UmrawatiTrip.com/FinanceSetup?r=' + str + '';
             }
             if (str == "Crd_info") {
                 location.href = 'http://backoffice.UmrawatiTrip.com/FinanceSetup?r=' + str + '';
             }


         };
</script>     <script type="text/javascript">
         var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>
    <div class="col-md-12 container-fluid" style="padding-right: 35px">
        <div class="page-wrapperss">

            <div class="panel panel-primary">

                <div class="panel-body">
                    <div class="container">

                        <ul class="nav nav-tabs">
                            <li id="comm_mst"><a data-toggle="tab" onclick="activetab('comm_mst')">Commission Master</a></li>
                            <li id="Dlmst"><a data-toggle="tab" onclick="activetab('Dlmst')">Deal Master</a></li>
                            <li id="msc_chrg"><a data-toggle="tab" onclick="activetab('msc_chrg')">Misc. Markup Charges</a></li>
                            <li id="dl_tf"><a data-toggle="tab" onclick="activetab('dl_tf')">Deal Transfer</a></li>
                            <li id="Crd_info"><a data-toggle="tab" onclick="activetab('Crd_info')">Credit Card Info</a></li>
                        </ul>
                         <asp:PlaceHolder ID="phModule" runat="server"></asp:PlaceHolder>

                        <%--<div class="tab-content" style="overflow: hidden;">
                            <div id="home" class="tab-pane fade in active">
                                
                                <Search:commissionmaster ID="commissionmaster1" runat="server" />
                                  
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                  
                                <Search1:DealCodeMaster runat="server" ID="DealCodeMaster1" />
                                  
                            </div>
                            <div id="menu2" class="tab-pane fade">
                                 
                                <Search2:MISCSRVCHARGE runat="server" ID="MISCSRVCHARGE1" />
                                  
                            </div>

                            <div id="menu3" class="tab-pane fade">
                                 

                                <Search3:DealTransfer runat="server" ID="DealTransfer1" />
                                   
                            </div>

                            <div id="menu4" class="tab-pane fade">
                                

                                <Search4:AddCreditCard runat="server" ID="AddCreditCard1" />
                                      
                            </div>

                        </div>--%>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script type="text/javascript">
        // wait for the DOM to be loaded 
        $(document).ready(function () {
            debugger;

          
            if (window.location.search.indexOf('Dlmst') > 0) {

                var element = document.getElementById("Dlmst");
                element.classList.add("active");
            }
            if (window.location.search.indexOf('msc_chrg') > 0) {

                var element = document.getElementById("msc_chrg");
                element.classList.add("active");
            }
            if (window.location.search.indexOf('dl_tf') > 0) {

                var element = document.getElementById("dl_tf");
                element.classList.add("active");
            }
            if (window.location.search.indexOf('Crd_info') > 0) {

                var element = document.getElementById("Crd_info");
                element.classList.add("active");
            }
            if ((window.location.search.indexOf('?') < 0) || (window.location.search.indexOf('comm_mst') > 0)) {

                var element = document.getElementById("comm_mst");
                element.classList.add("active");
            }
            //else {
            //    var element = document.getElementById("comm_mst"); element.classList.add("active");

            //}

            // bind 'myForm' and provide a simple callback function 
            //$('#myForm').ajaxForm(function () {

            //});
        });
    </script>

</asp:Content>
