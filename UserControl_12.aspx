﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="UserControl_12.aspx.cs" Inherits="Page" %>






<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />


    <script src="<%=ResolveUrl("~/Js/jquery-ui-1.8.8.custom.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Js/jquery-1.7.1.min.js") %>" type="text/javascript"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            $("#ctl00_ContentPlaceHolder1_Pagetxt").click(function () {

                $("#ctl00_ContentPlaceHolder1_Label1").hide();


            });
            $("#ctl00_ContentPlaceHolder1_Pageurltxt").click(function () {

                $("#ctl00_ContentPlaceHolder1_Label1").hide();
            });
            $("#ctl00_ContentPlaceHolder1_Root_page_Name").click(function () {

                $("#ctl00_ContentPlaceHolder1_Label1").hide();
            });
            $("#ctl00$ContentPlaceHolder1$CheckBox1").click(function () {

                $("#ctl00_ContentPlaceHolder1_Label1").hide();
            });
        });
    </script>
<%--    <style>
        .mydatagrid
        {
            width: 80%;
            border: solid 2px black;
            min-width: 80%;
        }

        .header
        {
            background-color: #000;
            font-family: Arial;
            color: White;
            height: 25px;
            text-align: center;
            font-size: 12px;
        }



        .rowws
        {
            background-color: #fff;
            font-family: Arial;
            font-size: 14px;
            color: #000;
            min-height: 25px;
            text-align: left;
        }

            .rows:hover
            {
                background-color: #5badff;
                color: #fff;
            }

        .mydatagrid a /** FOR THE PAGING ICONS  **/
        {
            background-color: Transparent;
            padding: 5px 5px 5px 5px;
            color: #fff;
            text-decoration: none;
            font-weight: bold;
        }

            .mydatagrid a:hover /** FOR THE PAGING ICONS  HOVER STYLES**/
            {
                background-color: #49cced;
                color: #fff;
            }

        .mydatagrid span /** FOR THE PAGING ICONS CURRENT PAGE INDICATOR **/
        {
            background-color: #fff;
            color: #000;
            padding: 5px 5px 5px 5px;
        }

        .pager
        {
            background-color: #5badff;
            font-family: Arial;
            color: White;
            height: 30px;
            text-align: left;
        }

        .mydatagrid td
        {
            padding: 5px;
        }

        .mydatagrid th
        {
            padding: 5px;
        }
    </style>--%>
 

    <style type="text/css">
        .page-wrapperss
        {
            background-color: #fff;
            /*margin-left: 15px;*/
        }
    </style>
    <div class="row">
        <div class="col-md-12 container-fluid" style="padding-right: 35px">
            <div class="page-wrapperss">

                <div class="panel panel-primary">

                    <div class="panel-body">

                        <div class="row">

                            <div class="col-xs-2" style="margin-left: 112px">

                                <div class="input-group">
                                    <%-- <label for="exampleInputPassword1">SURFACE NAME :</label>--%>
                                    <asp:TextBox CssClass="form-control input-text full-width" runat="server" name="Page_Name" ID="Pagetxt" placeholder="SURFACE NAME :"></asp:TextBox>
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-level-up"></span>
                                    </span>
                                    <asp:RequiredFieldValidator ID="RFVMK" runat="server" ControlToValidate="Pagetxt" ErrorMessage="*"
                                        Display="dynamic" ValidationGroup="group1"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="col-xs-2">
                                <div class="input-group">
                                    <%-- <label for="exampleInputPassword1">Surface URL :</label>--%>
                                    <asp:TextBox CssClass="form-control input-text full-width" runat="server" name="Page_url" ID="Pageurltxt" placeholder="SURFACE URL"></asp:TextBox>
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-random"></span>
                                    </span>

                                </div>
                            </div>




                            <div class="col-xs-2">
                                <div class="form-group">
                                    <%--  <label for="exampleInputEmail1">Surface_Root_Name:</label>--%>
                                    <asp:DropDownList CssClass="input-text full-width" ID="Root_page_Name" runat="server">
                                        <asp:ListItem>SURFACE_ROOT_NAME:</asp:ListItem>
                                    </asp:DropDownList>

                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="input-group">
                                
                                    <asp:CheckBox ID="CheckBox1" runat="server" CssClass="input-text full-width" />
                                        <label for="exampleInputEmail1">Is Parent Surface:</label>
                                </div>

                            </div>
                            <div class="col-xs-2">


                                <div class="form-group">

                                   <%-- <asp:Button ID="Submit" runat="server" Text="Submit" CssClass="btn btn-success" OnClick="Submit_Click" ValidationGroup="group1" />--%>

                                </div>
                            </div>
                        </div>


                       



                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputPassword1" id="Label1" runat="server"></label>
                                </div>
                            </div>

                        </div>

                        <div>&nbsp;</div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    


    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</asp:Content>

