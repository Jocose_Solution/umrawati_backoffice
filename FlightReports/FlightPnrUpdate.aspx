﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="FlightPnrUpdate.aspx.cs" Inherits="SprReports_Admin_FlightPnrUpdate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .tickectstatusdrop {
            height: 23px !important;
            padding: 1px 0 8px 8px !important;
            background-color: #FFFF66 !important;
        }

        .newbutton_2 {
            font-weight: bold!important;
            background: #af3e3a!important;
            color: #fff!important;
            border: 1px solid #fff!important;
            padding: 1px 5px 0px 5px!important;
        }
    </style>

    <script language="javascript" type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ((charCode >= 48 && charCode <= 57) || (charCode == 08 || charCode == 46 || charCode == 32)) {
                return true;
            }
            else {
                return false;
            }
        }

        function validateptk() {
            if (document.getElementById("ctl00_ContentPlaceHolder1_txt_OrderId").value == "") {
                alert("Please enter order id.");
                return false;
            }
        }

    </script>


    <script language="javascript" type="text/javascript">
        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }
    </script>

    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-12">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight Pnr & Ticket No Update</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">OrderId</label>
                                    <asp:TextBox ID="txt_OrderId" runat="server" MaxLength="25" CssClass="form-control" onKeyPress="return keyRestrict(event,'0123456789abcdefghijklmnopqrstuvwxyz');"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1"></label>
                                    <br />
                                    <asp:Button ID="btn_result" runat="server" Text="Get Details" CssClass="button buttonBlue" OnClick="btn_result_Click" OnClientClick="return validateptk();" />
                                    <%-- OnClientClick="return validateptk();"--%>
                                </div>
                            </div>
                        </div>



                        <div class="clear"></div>
                        <div class="row">


                            <div class="col-md-12" style="overflow: auto;">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow-y: scroll; overflow-x: scroll; max-height: 500px;">
                                    <ContentTemplate>


                                        <asp:GridView ID="GridFltHeader" runat="server" AutoGenerateColumns="false"
                                            CssClass="table" GridLines="None" Width="100%" PageSize="30" OnRowCancelingEdit="GridFltHeader_RowCancelingEdit"
                                            OnRowEditing="GridFltHeader_RowEditing" OnRowUpdating="GridFltHeader_RowUpdating" AllowPaging="true">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblHeaderId" runat="server" Text='<%#Eval("HeaderId") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="OrderId">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOrderId" runat="server" Text='<%#Eval("OrderId") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="AgencyId">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAgencyId" runat="server" Text='<%#Eval("AgencyId") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="AgencyName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAgencyName" runat="server" Text='<%#Eval("AgencyName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="TripType">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrdTripType" runat="server" Text='<%#Eval("TripType") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Trip">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTrip" runat="server" Text='<%#Eval("Trip") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Airline">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblVC" runat="server" Text='<%#Eval("VC") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sector">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSector" runat="server" Text='<%#Eval("sector") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Adult">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAdult" runat="server" Text='<%#Eval("Adult") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Child">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblChild" runat="server" Text='<%#Eval("Child") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Infant">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblInfant" runat="server" Text='<%#Eval("Infant") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%# !string.IsNullOrEmpty(Eval("MordifyStatus").ToString()) ? Eval("MordifyStatus").ToString() : Eval("Status").ToString() %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:DropDownList ID="ddlTicketStatus" runat="server" SelectedValue='<%# !string.IsNullOrEmpty(Eval("MordifyStatus").ToString()) ? Eval("MordifyStatus").ToString() : Eval("Status").ToString() %>' CssClass="tickectstatusdrop">
                                                            <asp:ListItem Value="Ticketed">Ticketed</asp:ListItem>
                                                            <asp:ListItem Value="Confirmed">Confirmed</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="GdsPnr">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGdsPnr" runat="server" Text='<%#Eval("GdsPnr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtGdsPnr" runat="server" Text='<%#Eval("GdsPnr") %>' Width="100px" MaxLength="25" BackColor="#ffff66" onKeyPress="return keyRestrict(event,'#-0123456789abcdefghijklmnopqrstuvwxyz');"></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="AirlinePnr">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAirlinePnr" runat="server" Text='<%#Eval("AirlinePnr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtGrdAirlinePnr" runat="server" Text='<%#Eval("AirlinePnr") %>' Width="100px" MaxLength="15" BackColor="#ffff66" onKeyPress="return keyRestrict(event,'-0123456789abcdefghijklmnopqrstuvwxyz');"></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="BookingDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreateDate" runat="server" Text='<%#Eval("CreateDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="EDIT">
                                                    <ItemTemplate>
                                                        <asp:Button ID="lnledit" runat="server" Text="Edit" CommandName="Edit" Font-Bold="true"
                                                            CssClass="newbutton_2" />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:Button ID="lnlupdate" runat="server" Text="Update" CommandName="Update" Font-Bold="true" CssClass="newbutton_2" OnClientClick="if(!confirm('Do you want to update PNR?')){ return false; };" />
                                                        <asp:Button ID="lnlcancel" runat="server" Text="Cancel" CommandName="Cancel" Font-Bold="true"
                                                            CssClass="newbutton_2" />
                                                    </EditItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                            <EmptyDataTemplate>No records Found</EmptyDataTemplate>
                                        </asp:GridView>
                                        <br />
                                        <div class="clear"></div>
                                        <asp:GridView ID="Grid1" runat="server" AutoGenerateColumns="false"
                                            CssClass="table" GridLines="None" Width="100%" PageSize="30" OnRowCancelingEdit="Grid1_RowCancelingEdit"
                                            OnRowEditing="Grid1_RowEditing" OnRowUpdating="Grid1_RowUpdating" AllowPaging="true"
                                            OnPageIndexChanging="OnPageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="PaxID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPaxId" runat="server" Text='<%#Eval("PaxId") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="OrderId">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOrderId" runat="server" Text='<%#Eval("OrderId") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="PaxName">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPaxName" runat="server" Text='<%#Eval("PaxName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="PaxType">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPaxType" runat="server" Text='<%#Eval("PaxType") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Gender">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGender" runat="server" Text='<%#Eval("Gender") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="TicketNumber">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTicketNumber" runat="server" Text='<%#Eval("TicketNumber") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtGrdTicketNumber" runat="server" Text='<%#Eval("TicketNumber") %>' Width="100px" MaxLength="20" BackColor="#ffff66" onKeyPress="return keyRestrict(event,'-0123456789abcdefghijklmnopqrstuvwxyz');"></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="EDIT">
                                                    <ItemTemplate>
                                                        <asp:Button ID="lnledit" runat="server" Text="Edit" CommandName="Edit" Font-Bold="true"
                                                            CssClass="newbutton_2" />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:Button ID="lnlupdate" runat="server" Text="Update" CommandName="Update" Font-Bold="true" CssClass="newbutton_2" OnClientClick="if(!confirm('Do you want to update ticket No?')){ return false; };" />
                                                        <asp:Button ID="lnlcancel" runat="server" Text="Cancel" CommandName="Cancel" Font-Bold="true"
                                                            CssClass="newbutton_2" />
                                                    </EditItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                            <EmptyDataTemplate>No records Found</EmptyDataTemplate>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>


                                <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                    <ProgressTemplate>
                                        <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                        </div>
                                        <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                            Please Wait....<br />
                                            <br />
                                            <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                            <br />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>








</asp:Content>

