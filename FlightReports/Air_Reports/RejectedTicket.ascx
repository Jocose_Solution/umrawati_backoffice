﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="RejectedTicket.ascx.vb" Inherits="FlightReports_Air_Reports_RejectedTicket" %>

<%@ Register Src="~/UserControl/LeftMenu.ascx" TagPrefix="uc1" TagName="LeftMenu" %>

   <%-- <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />--%>
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />

    <%-- <link href="../CSS/main2.css" rel="stylesheet" type="text/css" />--%>
    <%--<link rel="stylesheet" href="../chosen/chosen.css" />--%>
    <%-- <link href="../CSS/style.css" rel="stylesheet" type="text/css" />--%>

    <%--    <script src="../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>

    <script src="../chosen/chosen.jquery.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $(".chzn-select").chosen();
            $(".chzn-select-deselect").chosen({ allow_single_deselect: true });
        });
    </script>--%>
    <style type="text/css">
        .page-wrapperss
        {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl
        {
            overflow: auto;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            //$("#From1").datepicker();
            $('#From4').datepicker({ dateFormat: 'dd-mm-yy' }).val();
        
            $('#To4').datepicker({ dateFormat: 'dd-mm-yy' }).val();
         
        });
    </script>

    <div class="row">
        <div class="container-fluid" style="padding-right: 35px">
            <div class="page-wrapperss">

                <div class="panel panel-primary">

                    <div class="panel-body">




                        <div class="row">
                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">From Date</label>--%>
                                    <input type="text" name="From" id="From4" placeholder="FROM DATE" class="form-control input-text full-width datepicker" readonly="readonly" />
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-calendar cd"></span>
                                    </span>
                                </div>
                            </div>



                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">To Date</label>--%>
                                    <input type="text" name="To" placeholder="TO DATE" id="To4" class="form-control input-text full-width datepicker" readonly="readonly" />
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-calendar cd1"></span>
                                    </span>
                                </div>
                            </div>










                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">PNR</label>--%>
                                    <asp:TextBox ID="txt_PNR" runat="server" placeholder="PNR" class="form-control input-text full-width"></asp:TextBox>
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-list-alt"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">OrderId</label>--%>
                                    <asp:TextBox ID="txt_OrderId" runat="server" placeholder="ORDER_ID" class="form-control input-text full-width"></asp:TextBox>
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="fa fa-first-order"></span>
                                    </span>
                                </div>
                            </div>






                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">Pax Name</label>--%>
                                    <asp:TextBox ID="txt_PaxName" runat="server" placeholder="NAME" class="form-control input-text full-width"></asp:TextBox>
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-user"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2" >
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">Ticket No</label--%>
                                    <asp:TextBox ID="txt_TktNo" runat="server" placeholder="TICKET NO" class="form-control input-text full-width"></asp:TextBox>
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="fa fa-ticket"></span>

                                        <%-- <a href="~/Images/icons/flight-ticket.svg"></a>--%>
                                        <%--<img src="<%=ResolveUrl("~/Images/icons/flight-ticket.svg")
                                          %>"/>--%>
                                    </span>

                                </div>
                            </div>
                        </div>

                        <div class="row">


                            <div class="col-md-2" style="top: 5px">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"></label>
                                    <asp:DropDownList ID="ddlTrip" runat="server" CssClass="input-text full-width">
                                        <asp:ListItem  value="" disabled selected style="display: none;">SELECT TRIP TYPE</asp:ListItem>
                                        <%--<asp:ListItem Text="--Select Trip--" Value="0"></asp:ListItem>--%>
                                        <asp:ListItem Text="Domestic" Value="D"></asp:ListItem>
                                        <asp:ListItem Text="International" Value="I"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>





                            <div class="col-md-2" style="top: 10px">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">Airline</label>--%>
                                    <asp:TextBox ID="txt_AirPNR" runat="server" placeholder="Airline" class="form-control input-text full-width"></asp:TextBox>
                                    <input type="hidden" id="aircode" name="aircode" value="" />
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-plane"></span>
                                    </span>
                                </div>
                            </div>





                            <div class="col-md-2" id="td_Agency" runat="server" style="top: 10px">
                                <div class="input-group" id="Div1" runat="server">
                                    <%--<label for="exampleInputPassword1">Agency Name</label>--%>
                                    <input type="text" id="Text2" placeholder="Brand Name or ID" class="form-control input-text full-width" name="txtAgencyName" <%--onfocus="focusObj(this);"--%>
                                       <%-- onblur="blurObj(this);" defvalue="Brand Name or ID" value="Brand Name or ID"--%>  />
                                    <input type="hidden" id="Hidden1" name="hidtxtAgencyName" value="" />
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-briefcase"></span>
                                    </span>
                                </div>
                            </div>
            
                                  <div class="col-md-3" style="top:12px">
                                <div class="form-group"> 
                                                                   
                                    <asp:Button ID="btn_result" runat="server" Text="Go" Width="74px" CssClass="btn btn-success" />

                             
                                    <asp:Button ID="btn_export" runat="server" Text="Export" CssClass="btn btn-success" />
                                    </div>
                            </div>
                        </div>

                        <div class="row">
                        </div>

                        <div class="row">
                            <div class="col-md-2" id="divPaymentMode" runat="server">
                                <label for="exampleInputEmail1">PaymentMode :</label>
                                <asp:DropDownList CssClass="input-text full-width" ID="txtPaymentmode" runat="server">
                                    <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                    <asp:ListItem Text="PG" Value="pg"></asp:ListItem>
                                    <asp:ListItem Text="Wallet" Value="wallet"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-2" id="divPartnerName" runat="server">
                                <label for="exampleInputEmail1">PartnerName :</label>
                                <asp:DropDownList CssClass="input-text full-width" ID="txtPartnerName" runat="server">
                                </asp:DropDownList>
                            </div>


                        </div>











                        <div class="row">
                            <div class="col-md-9">
                                <span style="color: #FF0000"></span>
                            </div>
                        </div>

                        <div class="row" id="divReport" runat="server" visible="true">
                            <div class="col-md-12">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow: auto; max-height: 500px;">
                                    <ContentTemplate>
                                        <asp:GridView ID="ticket_grdview" runat="server" AllowPaging="True"
                                            AutoGenerateColumns="False" CssClass="table"
                                            GridLines="None" Width="100%" PageSize="30">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Order ID">
                                                    <ItemTemplate>
                                                        <%-- <asp:Label ID="OrderID" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label>--%>
                                                        <a data-toggle="modal" data-target="#myModal" href='PnrSummaryIntl.aspx?OrderId=<%#Eval("OrderId")%> &TransID=' rel="lyteframe"
                                                            rev="width: 900px; height: 500px; overflow:hidden;" target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                            <asp:Label ID="OrderID" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label></a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Pnr">
                                                    <ItemTemplate>
                                                        <asp:Label ID="GdsPNR" runat="server" Text='<%#Eval("GdsPnr")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="AgencyId">
                                                    <ItemTemplate>
                                                        <asp:Label ID="AgentID" runat="server" Text='<%#Eval("AgencyID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="UserId">
                                                    <ItemTemplate>
                                                        <asp:Label ID="AgentID" runat="server" Text='<%#Eval("UserId")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Executive ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="ExcutiveID" runat="server" Text='<%#Eval("ExecutiveId")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Rejected Remark">
                                                    <ItemTemplate>
                                                        <asp:Label ID="RejectRemark" runat="server" Text='<%#Eval("RejectedRemark")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="AirLine">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Airline" runat="server" Text='<%#Eval("VC")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Partner Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_CreateDate" runat="server" Text='<%#Eval("PartnerName")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:BoundField HeaderText="Sector" DataField="sector"></asp:BoundField>
                                                <asp:BoundField HeaderText="Trip" DataField="trip"></asp:BoundField>
                                                <asp:BoundField HeaderText="Net Fare" DataField="TotalAfterDis">
                                                    <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Convenience Fee">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_PGcharges" runat="server" Text='<%#Eval("PgCharges")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Status" DataField="Status"></asp:BoundField>
                                                <asp:BoundField HeaderText="Booking Date" DataField="CreateDate"></asp:BoundField>
                                                <asp:BoundField HeaderText="Rejected Date" DataField="RejectDate"></asp:BoundField>

                                                <asp:TemplateField HeaderText="Payment Mode">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_PaymentMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>