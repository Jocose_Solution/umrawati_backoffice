﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ReissueReport.ascx.vb" Inherits="FlightReports_Air_Reports_ReissueReport" %>


<%@ Register Src="~/UserControl/LeftMenu.ascx" TagPrefix="uc1" TagName="LeftMenu" %>

 <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />

   <%-- <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />--%>

    <%--<link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />--%>
    <style type="text/css">
        .HeaderStyle th
        {
            white-space: nowrap;
        }
    </style>
    <style type="text/css">
        .page-wrapperss
        {
            background-color: #fff;
            /*margin-left: 15px;*/
        }

        .overfl
        {
            overflow: auto;
        }
    </style>
 <%--    <script type="text/javascript">

         $(document).ready(function () {
             //$("#From1").datepicker();
             $('#From3').datepicker({ dateFormat: 'dd-mm-yy' }).val();

             $('#To3').datepicker({ dateFormat: 'dd-mm-yy' }).val();

         });


    </script>--%>

<script type="text/javascript">
    $(document).ready(function () {
        $("#From3").datepicker({ dateFormat: 'dd-mm-yy' }).val();
        $(".cd").click(function () {
            $("#From3").focus();
        }
        );
        $("#To3").datepicker({ dateFormat: 'dd-mm-yy' }).val();
        $(".cd1").click(function () {
            $("#To3").focus();
        }
        );

    });
    </script>




    <div class="row">
        <%--   <div class="col-md-11 pull-right" style="padding:30px 30px 30px 100px;">--%>
        <div class="container-fluid" style="padding-right: 35px">
            <div class="page-wrapperss">

                <div class="panel panel-primary">

                    <div class="panel-body" style="margin-right: -145px">

                        <div class="row">
                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">From Date</label>--%>
                                    <input type="text" name="From" id="From3" placeholder="FROM DATE" class="form-control input-text full-width" readonly="readonly" />
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-calendar cd" style="cursor:pointer;"></span>
                                    </span>
                                </div>
                            </div>


                            <div class="col-md-2">
                                <div class="input-group" >
                                    <%--<label for="exampleInputPassword1">To Date</label>--%>
                                    <input type="text" name="To" placeholder="TO DATE" id="To3" class="form-control input-text full-width" readonly="readonly" />
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-calendar cd1" style="cursor:pointer;"></span>
                                    </span>
                                </div>
                            </div>

                             <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">PNR</label>--%>
                                    <asp:TextBox ID="txt_PNR" runat="server" placeholder="PNR" class="form-control input-text full-width"></asp:TextBox>
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-list-alt"></span>
                                    </span>
                                </div>
                            </div>
                            
                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">OrderId</label>--%>
                                    <asp:TextBox ID="txt_OrderId" runat="server" placeholder="ORDER ID" class="form-control input-text full-width"></asp:TextBox>
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="fa fa-first-order"></span>
                                    </span>
                                </div>
                            </div>
                            
                    
                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">Pax Name</label>--%>
                                    <asp:TextBox ID="txt_PaxName" runat="server" placeholder="NAME" class="form-control input-text full-width"></asp:TextBox>
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-user"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        </div>


                     <div class="panel-body" style="margin-right: -145px">
                        <div class="row">

                            <div class="col-md-2" >
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">Ticket No</label--%>
                                    <asp:TextBox ID="txt_TktNo" runat="server" placeholder="TICKET NO" class="form-control input-text full-width"></asp:TextBox>
                                    <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-ticket"></span>
                                        </span>

                                </div>
                            </div>
                            <div id="tr_ExecID" runat="server">
                                <div class="col-md-2" >
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"></label>
                                        <asp:DropDownList ID="ddl_ExecID" CssClass="input-text full-width" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group" >
                                        <label for="exampleInputEmail1"></label><asp:DropDownList CssClass="input-text full-width" ID="ddl_Status" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group" id="td_Agency" runat="server">
                                        <label for="exampleInputEmail1"></label>
                                        <input type="text" placeholder="BRAND NAME" id="txtAgencyName" name="txtAgencyName" class="form-control input-text full-width" onfocus="focusObj(this);"
                                            onblur="blurObj(this);"
                                             autocomplete="off" value="" />
                                        <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                    </div>
                                </div>
                                <div class="col-md-2" style="top: -11px">
                                   <div class="input-group">
                                            <%--<label for="exampleInputPassword1">Airline</label>--%>
                                            <asp:TextBox ID="txt_AirPNR" runat="server" placeholder="AIRLINE" class="form-control input-text full-width"></asp:TextBox>
                                            <input type="hidden" id="aircode" name="aircode" value="" />
                                            <span class="input-group-addon" style="background-color: #49cced">
                                                <span class="glyphicon glyphicon-plane"></span>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    
                                        



                                      



                        <div class="row">



                          

                            <div class="col-md-2" >
                                <div id="Div1" class="form-group" runat="server">
                                    <label for="exampleInputEmail1" id="tdTripNonExec1" runat="server"></label>
                                    <div class="form-group" id="tdTripNonExec2" runat="server">
                                        <asp:DropDownList CssClass="input-text full-width" ID="ddlTripReissueDomIntl" runat="server">
                                            <asp:ListItem  value="" disabled selected style="display: none;">SELECT TRIP TYPE</asp:ListItem>
                                            <%--<asp:ListItem Value="">-----Select-----</asp:ListItem>--%>
                                            <asp:ListItem Value="D">Domestic</asp:ListItem>
                                            <asp:ListItem Value="I">International</asp:ListItem>
                                        </asp:DropDownList>
                                       </div>
                                    

                                </div>
                                 
                            </div>
                              <div class="col-md-2" id="partnername" runat="server">
                                <label for="exampleInputEmail1">PartnerName </label>
                                <asp:DropDownList CssClass="input-text full-width" ID="txtPartnerName" runat="server">
                                </asp:DropDownList>
                            </div> </div>
                            <div class="col-md-2" >
                            <asp:Button ID="btn_result" runat="server" Text="Go"   CssClass="btn btn-success" />
                               
                         <asp:Button ID="btn_export" runat="server" Text="Export"  CssClass="btn btn-success" />
                            </div>        
                       



                        <div class="row">
                            <div class="col-md-4" id="divPaymentMode" runat="server">
                                <label for="exampleInputEmail1">PaymentMode :</label>
                                <asp:DropDownList CssClass="input-text full-width" ID="txtPaymentmode" runat="server">
                                    <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                    <asp:ListItem Text="PG" Value="pg"></asp:ListItem>
                                    <asp:ListItem Text="Wallet" Value="wallet"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                       







                        


                            
                        </div>





                        <%-- <div class="row" id="divReport" runat="server">

                        <div class="col-md-28">--%>
                        <%-- style="overflow: scroll; height: 200px;"--%>


                        <div class="row">
                            <div class="col-md-12 overfl" id="divReport" runat="server" visible="false">
                                <asp:UpdatePanel ID="UP" runat="server" style="background-color: #fff; overflow: auto; max-height: 500px;">
                                    <ContentTemplate>

                                        <asp:GridView ID="grd_paxstatusinfo" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover"
                                            AllowPaging="True" PageSize="100" border="1" style="text-transform:uppercase;">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Customer ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbluserid" runat="server" Text='<%#Eval("UserID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Agency Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblagencyname" runat="server" Text='<%#Eval("Agency_Name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="P ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPaxID" runat="server" Text='<%#Eval("PaxID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="P Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblpaxtype" runat="server" Text='<%#Eval("pax_type") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="P Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblpaxfname" runat="server" Text='<%#Eval("pax_fname") %>'></asp:Label>
                                                        <asp:Label ID="lbllastname" runat="server" Text='<%#Eval("pax_lname") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--  <asp:TemplateField HeaderText="Pax LastName">
                                        <ItemTemplate>
                                            
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="Pnr">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblpnr" runat="server" Text='<%#Eval("pnr_locator") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Ticket Number">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbltktno" runat="server" Text='<%#Eval("Tkt_No") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Airline">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblVC" runat="server" Text='<%#Eval("VC") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sector">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblsector" runat="server" Text='<%#Eval("Sector") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Flight Number">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblflightno" runat="server" Text='<%#Eval("FlightNo") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Departure Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbldeptdate" runat="server" Text='<%#Eval("departure_date") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="T Fare">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbltotalfare" runat="server" Text='<%#Eval("TotalFare") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Fare After Discount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbltotalfareafterdiscount" runat="server" Text='<%#Eval("TotalFareAfterDiscount") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Reissue Charge">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblrissuechrge" runat="server" Text='<%#Eval("ReissueCharge") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Service Charge">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblsrvcharge" runat="server" Text='<%#Eval("ServiceCharge") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Fare Difference">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblfarediff" runat="server" Text='<%#Eval("FareDiff") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Exec ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblExecutiveId" runat="server" Text='<%#Eval("ExecutiveID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Exec Reject Remark">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRejectionComment" runat="server" Text='<%#Eval("RejectionComment") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Requested Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblsubmit" runat="server" Text='<%#Eval("SubmitDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Accepted Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbldateA" runat="server" Text='<%#Eval("AcceptDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Updated Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbldateU" runat="server" Text='<%#Eval("UpdateDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Partner Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_CreateDate" runat="server" Text='<%#Eval("PartnerName")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Payment Mode">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_PaymentMode" runat="server" Text='<%#Eval("PgMode")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Convenience Fee">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_PGCharges" runat="server" Text='<%#Eval("PgCharges")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>

                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UP">
                                    <ProgressTemplate>
                                        <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                        </div>
                                        <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                            Please Wait....<br />
                                            <br />
                                            <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                            <br />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   

                <script type="text/javascript">
                    var UrlBase = '<%=ResolveUrl("~/") %>';
                </script>


                <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

                <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

                <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>