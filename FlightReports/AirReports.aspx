﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="AirReports.aspx.cs" Inherits="FlightReports_Air_Reports" %>

<%--<%@ Register Src="~/FlightReports/Air_Reports/CancellationReport.ascx" TagPrefix="Search" TagName="CancellationReport" %>--%>
<%--<%@ Register Src="~/FlightReports/Air_Reports/HoldPnrReport.ascx" TagPrefix="Search1" TagName="HoldPnrReport" %>--%>
<%--<%@ Register Src="~/FlightReports/Air_Reports/QCTicketReport.ascx" TagPrefix="Search2" TagName="QCTicketReport" %>--%>
<%--<%@ Register Src="~/FlightReports/Air_Reports/ReissueReport.ascx" TagPrefix="Search3" TagName="ReissueReport" %>--%>
<%--<%@ Register Src="~/FlightReports/Air_Reports/RejectedTicket.ascx" TagPrefix="Search4" TagName="RejectedTicket" %>--%>
<%--<%@ Register Src="~/FlightReports/Air_Reports/RequestedRequest.ascx" TagPrefix="Search5" TagName="RequestedRequest" %>--%>
<%--<%@ Register Src="~/FlightReports/Air_Reports/TicketReport.ascx" TagPrefix="Search6" TagName="TicketReport" %>--%>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript" >
      function activaTab(str) {
          debugger;
          if (str == "can") {
              location.href = 'http://backoffice.UmrawatiTrip.com/flightreports/airreports?r=' + str + '';
          }
          if (str == "Tkt")
          {
              location.href = 'http://backoffice.UmrawatiTrip.com/flightreports/airreports?r=' + str + '';
          }
          //if (str == "trans") {
          //    location.href = 'http://backoffice.UmrawatiTrip.com/flightreports/airreports?r=' + str + '';
          //}
          if (str == "reissue") {
              location.href = 'http://backoffice.UmrawatiTrip.com/flightreports/airreports?r=' + str + '';
          }
          
          
      };
  </script>     <script type="text/javascript">
         var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>
    <div class="col-md-12 container-fluid" style="padding-right: 35px">
        <div class="page-wrapperss">

            <div class="panel panel-primary">

                <div class="panel-body">
                    <div class="container">
      

                        <ul class="nav nav-tabs" >
                          <%--  <li id="aa" ><a data-toggle="tab" onclick ="activaTab('trans')">Transaction Details</a></li>--%>
                              <li id="Tkt"><a  data-toggle="tab" onclick ="activaTab('Tkt')" >Ticket Report</a></li>
                            <%--<li id="hh" ><a data-toggle="tab" href="#menu1">Hold PNR Report</a></li>--%>
                            <li id="can" ><a data-toggle="tab" onclick ="activaTab('can')" >Cancellation Report</a></li>
                            <li id="reissue"><a data-toggle="tab"  onclick ="activaTab('reissue')" >Reissue Report</a></li>
                           <%-- <li><a data-toggle="tab" href="#menu4">Rejected Ticket</a></li>--%>
                            <%--<li><a data-toggle="tab" href="#menu5">Requested Request</a></li>--%>
                          
                        </ul>

                        <asp:PlaceHolder ID="phModule" runat="server"></asp:PlaceHolder>


                      <%--  <div class="tab-content" style="overflow: hidden;">
                            <div id="home" class="tab-pane fade in active">--%>

                             
                                <%--<Search2:QCTicketReport runat="server" ID="QCTicketReport1" />--%>
                                 
                            </div>
                    <%--        <div id="menu1" class="tab-pane fade">
                                    <Search1:HoldPnrReport runat="server" ID="HoldPnrReport1" />
                            </div>--%>
                            <%--<div id="menu2" class="tab-pane fade">--%>


                                <%--<Search:CancellationReport ID="CancellationReport1" runat="server" />--%>
                            <%--</div>

                            <div id="menu3" class="tab-pane fade">--%>

                                <%--<Search3:ReissueReport runat="server" ID="ReissueReport1" />--%>
                            <%--</div>--%>

                         <%--   <div id="menu4" class="tab-pane fade">

                                <Search4:RejectedTicket runat="server" ID="RejectedTicket1" />
                            </div>--%>

                        <%--    <div id="menu5" class="tab-pane fade">

                                <Search5:RequestedRequest runat="server" ID="RequestedRequest1" />
                            </div>--%>

                            <%--<div id="menu6" class="tab-pane fade">--%>

                                <%--<Search6:TicketReport runat="server" ID="TicketReport1" />--%>
                            <%--</div>--%>

                        </div>
                    </div>
                </div>
            </div>
     <%--   </div>

    </div>--%>
<%--    <script type="text/javascript">
        $(document).ready(function () {
            $('#ctl00_ContentPlaceHolder1_HoldPnrReport1_btn_result').click(function (e) {
                e.preventDefault()
            });

            $('ul.tabs').each(function () {
                var $active, $content, $links = $(this).find('a');
                $active = $links.first().addClass('active');
                $content = $($active.attr('href'));
                $links.not(':first').each(function () {
                    $($(this).attr('href')).hide();
                });

                $(this).on('click', 'a', function (e) {
                    $active.removeClass('active');
                    $content.hide();
                    $active = $(this);
                    $content = $($(this).attr('href'));
                    $active.addClass('active');
                    $content.show();
                    e.preventDefault();
                });
            });
        });

    </script>--%>

 <script type="text/javascript">
            // wait for the DOM to be loaded 
     $(document).ready(function () {
         debugger;
                
                if (window.location.search.indexOf('can') > 0) {
                    
                    var element = document.getElementById("can");
                    element.classList.add("active");
                }
                if (window.location.search.indexOf('reissue') > 0) {

                    var element = document.getElementById("reissue");
                    element.classList.add("active");
                }
                //if (window.location.search.indexOf('Tkt') > 0) {

                //    var element = document.getElementById("Tkt");
                //    element.classList.add("active");
                //}
                if ((window.location.search.indexOf('?') < 0) || (window.location.search.indexOf('Tkt') > 0)) {

                    var element = document.getElementById("Tkt");

                     element.classList.add("active");
                 }
                //if ((window.location.search.indexOf('?') < 0)|| (window.location.search.indexOf('trans')>0)) {

                //    var element = document.getElementById("aa");
                 
                //    element.classList.add("active");
                //}
                //else {
                //    var element = document.getElementById("aa"); element.classList.add("active");
                 
                //}

                    // bind 'myForm' and provide a simple callback function 
                    //$('#myForm').ajaxForm(function () {

                    //});
            });
    </script>






<%--    <script type="text/javascript">
        $(document).ready(function () {
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = localStorage.getItem('activeTab');
            if (activeTab) {
                $('#myTab a[href="' + activeTab + '"]').tab('show');
            }
        });
</script>



      <script type="text/javascript">
         //jQuery.noConflict();
         //$(document).ready(function () {
         //    $("#tabs").tabs();
          //});
          $("#myTab").tabs({
              load: function (event, ui) {
                  var $curr_panel = $(ui.panel);
                  $curr_panel.find(selector).datepicker()
              }
          });
         </script>--%>
</asp:Content>

