﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FlightReports_Air_Reports : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ((Label)Page.Master.FindControl("lblBC")).Text = "<a title='' class='tip-bottom tt' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Reports</a><a class='current' href='#'>Flight Reports</a>";
        
        if (Page.Request.QueryString.Keys.Count>0)
        {
            if (Page.Request.QueryString["r"] == "can")
                addUC("~/FlightReports/Air_Reports/CancellationReport.ascx");
            if (Page.Request.QueryString["r"] == "Tkt")
                addUC("~/FlightReports/Air_Reports/TicketReport.ascx");
            //if (Page.Request.QueryString["r"] == "trans")
            //    addUC("~/FlightReports/Air_Reports/QCTicketReport.ascx");
            if (Page.Request.QueryString["r"] == "reissue")
                addUC("~/FlightReports/Air_Reports/ReissueReport.ascx");
            //else if (Page.Request.QueryString["r"].IndexOf("aa") > 0)
            //    addUC("~/FlightReports/Air_Reports/QCTicketReport.ascx");
        }
        else
        {
            //addUC("~/FlightReports/Air_Reports/QCTicketReport.ascx");
            addUC("~/FlightReports/Air_Reports/TicketReport.ascx");
        }

    }
    private void addUC(string path)
    {
        UserControl myControl = (UserControl)Page.LoadControl(path);

        //UserControlHolder is a place holder on the aspx page where I want to load the
        //user control to.
        phModule.Controls.Add(myControl);
    }
}