﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="IntlHoldPNRRequest.ascx.vb" Inherits="FlightReports_Int_task_IntlHoldPNRRequest" %>

 <%-- <link href="../../css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="Styles/tables.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />--%>
      <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
     <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
     

 

     <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    
    <div class="row">
      
        <div class="col-md-2">
        </div>
        <div class="container-fluid" style="padding-right:35px">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
             
                     <div class="panel-body">

                   
                            <div id="td_Reject" runat="server" align="right" visible="false" >
                                 
                               <div class="row">
                                <div class="col-md-4">
                                     <div class="form-group">
                                <asp:TextBox ID="txt_Reject" runat="server" TextMode="MultiLine"  CssClass="form-control"  ></asp:TextBox>
                              
                                 </div>
                                    </div>
                                   </div>

                                      <div class="row">
                                  <div class="col-md-4">
                                     <div class="form-group">
                                <asp:Button ID="btn_Comment" runat="server" Text="Comment" OnClientClick="return Validate();"  CssClass="button buttonBlue" />&nbsp;&nbsp;
                                 </div>
                                    </div>
                                         <div class="col-md-4">
                                     <div class="form-group">
                                         <asp:Button ID="btn_Cancel" runat="server" Text="Cancle"  CssClass="button buttonBlue" />
                                    </div>
                                </div>
                                </div>
                                 </div>
                              
                              
        
        
    <hr />

                                 <div class="row" id="divReport" style="background-color:#fff; overflow-y:scroll; overflow-x:scroll; max-height:500px; " runat="server" visible="True">
                                <asp:GridView ID="GridHoldPNRAccept" runat="server" AllowPaging="True" AllowSorting="True"
                                    AutoGenerateColumns="False"  CssClass="table" GridLines="None">
                                    <Columns>
                                          <asp:TemplateField HeaderText="Booking">
                                                    <ItemTemplate>
                                                        <a href='../Admin/Update_BookingOrder.aspx?OrderId=<%#Eval("OrderId")%> &TransID='
                                                            target="_blank"
                                                            style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">Booking&nbsp;Details
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                        <asp:TemplateField HeaderText="OrderId">
                                            <ItemTemplate>
                                                <a id="ancher" href='../PnrSummaryIntl.aspx?OrderId=<%#Eval("OrderId")%>' target="_blank"
                                                    style="font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #004b91;
                                                    font-weight: bold;">
                                                    <asp:Label ID="lbl_OrderId" runat="server" Text='<%#Eval("OrderId") %>'></asp:Label>(View)</a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="AgentId">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_AgentID" runat="server" Text='<%#Eval("AgentID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PNR">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PNR" runat="server" Text='<%#Eval("GdsPnr") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Sector">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Sector" runat="server" Text='<%#Eval("sector") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Status" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Duration">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Duration" runat="server" Text='<%#Eval("Duration") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TripType">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TripType" runat="server" Text='<%#Eval("TripType") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Trip">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Trip" runat="server" Text='<%#Eval("Trip") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TourCode">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TourCode" runat="server" Text='<%#Eval("TourCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TotalBookingCost">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TotalBookingCost" runat="server" Text='<%#Eval("TotalBookingCost") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TotalAfterDis">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TotalAfterDis" runat="server" Text='<%#Eval("TotalAfterDis") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pax Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PgFName" runat="server" Text='<%#Eval("PgFName") %>'></asp:Label>
                                                <asp:Label ID="lbl_PgLName" runat="server" Text='<%#Eval("PgLName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PgMobile">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PgMobile" runat="server" Text='<%#Eval("PgMobile") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PgEmail">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PgEmail" runat="server" Text='<%#Eval("PgEmail") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CreateDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CreateDate" runat="server" Text='<%#Eval("CreateDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Accept">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="ITZ_Accept" runat="server" CommandName="Accept" CommandArgument='<%#Eval("OrderId") %>'
                                                    Font-Bold="True" Font-Underline="False">Accept</asp:LinkButton>
                                                ||&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:LinkButton ID="LB_Reject" runat="server" CommandName="Reject" CommandArgument='<%#Eval("OrderId") %>'
                                                    Font-Bold="True" Font-Underline="False" ForeColor="Red">Refund</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Payment Mode">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PaymentMode" runat="server" Text='<%# If(Convert.ToString(Eval("PaymentMode")) = "PG", "PG", "Wallet")%>'></asp:Label>
                                                <%--<asp:Label ID="lbl_PgMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Convenience Fee">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_PGCharges" runat="server" Text='<%#Eval("PgCharges")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                <asp:TemplateField HeaderText="Hand_Bag_Fare">
                                <ItemTemplate>
                                    <asp:Label ID="Lbl_IsBagFare" runat="server" Text='<%#Eval("IsBagFare")%>'></asp:Label>
                                </ItemTemplate>
                               </asp:TemplateField>

                                    </Columns>
                                    <RowStyle CssClass="RowStyle" />
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <PagerStyle CssClass="PagerStyle" />
                                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                                    <HeaderStyle CssClass="HeaderStyle" Height="50px" />
                                    <EditRowStyle CssClass="EditRowStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                         </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
       

                      
  


       <script language="javascript" type="text/javascript">

           function Validate() {
               if (document.getElementById("ctl00_ContentPlaceHolder1_txt_Reject").value == "") {

                   alert('Please Fill Remark');
                   document.getElementById("ctl00_ContentPlaceHolder1_txt_Reject").focus();
                   return false;


               }
               if (confirm("Are you sure you want to Reject!"))
                   return true;
               return false;
           }
    </script>

  <%-- <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-10">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"> Flight >  Hold PNR Request</h3>
                    </div>
                    <div class="panel-body">
                        
                    <div id="td_Reject" runat="server" align="right" visible="false" >
                                 
                               <div class="row">
                                <div class="col-md-4">
                                     <div class="form-group">
                                <asp:TextBox ID="txt_Reject" runat="server" TextMode="MultiLine"  CssClass="form-control"  ></asp:TextBox>
                              
                                 </div>
                                    </div>
                                   </div>

                                      <div class="row">
                                  <div class="col-md-4">
                                     <div class="form-group">
                                <asp:Button ID="btn_Comment" runat="server" Text="Comment" OnClientClick="return Validate();"  CssClass="button buttonBlue" />&nbsp;&nbsp;
                                 </div>
                                    </div>
                                         <div class="col-md-4">
                                     <div class="form-group">
                                         <asp:Button ID="btn_Cancel" runat="server" Text="Cancle"  CssClass="button buttonBlue" />
                                    </div>
                                </div>
                                </div>
                                 </div>
                        
                     <div class="row"  style="background-color:#fff; overflow-y:scroll; " runat="server">                                
                            <div class="col-md-28">

                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>     
                                     <asp:GridView ID="GridHoldPNRAccept" runat="server" AllowPaging="True" AllowSorting="True"
                                    AutoGenerateColumns="False"  CssClass="table" GridLines="None" PageSize="10">
                                    <Columns>
                                        <asp:TemplateField HeaderText="OrderId">
                                            <ItemTemplate>
                                                <a id="ancher" href='../PnrSummaryIntl.aspx?OrderId=<%#Eval("OrderId")%>' target="_blank"
                                                    style="font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #004b91;
                                                    font-weight: bold;">
                                                    <asp:Label ID="lbl_OrderId" runat="server" Text='<%#Eval("OrderId") %>'></asp:Label>(View)</a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="AgentId">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_AgentID" runat="server" Text='<%#Eval("AgentID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PNR">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PNR" runat="server" Text='<%#Eval("GdsPnr") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Sector">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Sector" runat="server" Text='<%#Eval("sector") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Status" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Duration">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Duration" runat="server" Text='<%#Eval("Duration") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TripType">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TripType" runat="server" Text='<%#Eval("TripType") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Trip">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Trip" runat="server" Text='<%#Eval("Trip") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TourCode">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TourCode" runat="server" Text='<%#Eval("TourCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TotalBookingCost">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TotalBookingCost" runat="server" Text='<%#Eval("TotalBookingCost") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TotalAfterDis">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TotalAfterDis" runat="server" Text='<%#Eval("TotalAfterDis") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pax Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PgFName" runat="server" Text='<%#Eval("PgFName") %>'></asp:Label>
                                                <asp:Label ID="lbl_PgLName" runat="server" Text='<%#Eval("PgLName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PgMobile">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PgMobile" runat="server" Text='<%#Eval("PgMobile") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PgEmail">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PgEmail" runat="server" Text='<%#Eval("PgEmail") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CreateDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CreateDate" runat="server" Text='<%#Eval("CreateDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Accept">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="ITZ_Accept" runat="server" CommandName="Accept" CommandArgument='<%#Eval("OrderId") %>'
                                                    Font-Bold="True" Font-Underline="False">Accept</asp:LinkButton>
                                                ||&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:LinkButton ID="LB_Reject" runat="server" CommandName="Reject" CommandArgument='<%#Eval("OrderId") %>'
                                                    Font-Bold="True" Font-Underline="False" ForeColor="Red">Refund</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Remarks">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_remarks" runat="server" Text='<%#Eval("Remark") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="RowStyle" />
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <PagerStyle CssClass="PagerStyle" />
                                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                                    <HeaderStyle CssClass="HeaderStyle" Height="50px" />
                                    <EditRowStyle CssClass="EditRowStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>      
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div> 
                               </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>--%>