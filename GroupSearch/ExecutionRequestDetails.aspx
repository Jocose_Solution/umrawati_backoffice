﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="ExecutionRequestDetails.aspx.cs" Inherits="GroupSearch_ExecRequestDetails" %>

<%@ Register Src="~/UserControl/LeftMenu.ascx" TagPrefix="uc1" TagName="LeftMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function loadimg() {
            $("#waitMessage").show();
        }
    </script>
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow-y: scroll;
        }
    </style>



    <div class="row">
        <div class="col-md-2"></div>

        <div class="col-md-10">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight > Group Search Report </h3>
                    </div>
                    <div class="panel-body">


                        <%-- <div class="large-12 medium-12 small-12" style="margin-top: 80px">
        <div class="large-3 medium-3 small-12 columns">
            <uc1:LeftMenu runat="server" ID="LeftMenu" />
        </div--%>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Date To:</label>
                                    <asp:TextBox ID="txt_todate" class="form-control date" runat="server"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Date From :</label>
                                    <asp:TextBox ID="txt_fromDate" runat="server" class="form-control date"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Request Id:</label>
                                    <asp:TextBox ID="txt_RequestID" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Status:</label>
                                    <asp:DropDownList ID="ddl_status" runat="server" class="form-control">
                                        <asp:ListItem Selected="True" Value="N">---All ---</asp:ListItem>
                                        <asp:ListItem>Requested</asp:ListItem>
                                        <asp:ListItem>Freezed</asp:ListItem>
                                        <asp:ListItem>Paid</asp:ListItem>
                                        <asp:ListItem>Ticketed</asp:ListItem>
                                        <asp:ListItem>Cancelled</asp:ListItem>
                                        <asp:ListItem>Rejected</asp:ListItem>
                                        <asp:ListItem>Cancellation Requested</asp:ListItem>
                                        <asp:ListItem>Refunded</asp:ListItem>
                                        <asp:ListItem>Quoted</asp:ListItem>
                                        <asp:ListItem>Accepted</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <br />
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Button ID="btn_submit" runat="server" Text="Search" CssClass="button buttonBlue" OnClick="btn_submit_Click" />
                                </div>
                            </div>
                        </div>




                        <div class="row" id="divReport" style="background-color: #fff; overflow-y: scroll; overflow-x: scroll; max-height: 500px;" runat="server">
                            <div id="DivExec" runat="server"></div>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always" ChildrenAsTriggers="true">
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="GrpBookingDetails" />
                                </Triggers>
                                <ContentTemplate>
                                    <asp:GridView ID="GrpBookingDetails" runat="server" AllowPaging="True" AllowSorting="True"
                                        AutoGenerateColumns="False" CssClass="table " GridLines="None" Width="100%"
                                        PageSize="30" OnPageIndexChanging="GrpBookingDetails_PageIndexChanging" OnRowDataBound="GrpBookingDetails_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="RequestID">
                                                <ItemTemplate>
                                                    <a href="#" onclick='openWindow("<%# Eval("RequestID") %>");'>
                                                        <asp:Label ID="lblRequestID" runat="server" Text='<%#Eval("RequestID")%>'></asp:Label></a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Booking Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBooking" runat="server" Text='<%#Eval("BookingName")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Trip">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTrip" runat="server" Text='<%#Eval("Trip")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Trip Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTripType" runat="server" Text='<%#Eval("TripType")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Passangers">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNoOfPax" runat="server" Text='<%#Eval("NoOfPax")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Expected Fare">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblExpectedPrice" runat="server" Text='<%#Eval("ExpactedPrice")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Journey">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblJourney" runat="server" Text='<%#Eval("Journey")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Journey Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblJourneydate" runat="server" Text='<%#Eval("JourneyDate")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Payment Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPaymentStatus" runat="server" Text='<%#Eval("PaymentStatus")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="" Visible="false">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="link_Refund" runat="server" CommandArgument='<%#Eval("RequestID") %>'
                                                        Font-Bold="True" Font-Underline="False" OnClick="link_Refund_Click">RefundRequest</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Booking Amount">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_BookedPrice" runat="server" Text='<%#Eval("BookedPrice")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PaymentMode">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPaymentMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PgCharges">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPgCharges" runat="server" Text='<%#Eval("PgCharges")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CancellationRemarks">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCancellationRemarks" runat="server" Text='<%#Eval("CancellationRemarks")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="link_Invoice" runat="server" CommandArgument='<%#Eval("RequestID") %>'
                                                        Font-Bold="True" Font-Underline="False" OnClick="link_Invoice_Click">View Invoice</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="RowStyle" />
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <PagerStyle CssClass="PagerStyle" />
                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                        <HeaderStyle CssClass="HeaderStyle" />
                                        <EditRowStyle CssClass="EditRowStyle" />
                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div id="waitMessage" style="display: none;">
                                <div class="" style="text-align: center; opacity: 0.9; position: fixed; z-index: 99999; top: 0px; width: 100%; height: 100%; background-color: #afafaf; font-size: 12px; font-weight: bold; padding: 20px; box-shadow: 0px 1px 5px #000; /* border: 5px solid #d1d1d1; */ /* border-radius: 10px; */">
                                    <div style="position: absolute; top: 264px; left: 45%; font-size: 18px; color: #fff;">
                                        Please wait....<br />
                                        <br />
                                        <img alt="loading" src="<%=ResolveUrl("~/images/loadingAnim.gif")%>" />
                                        <br />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript">
        $(function () {
            $(".date").datepicker({ dateFormat: 'dd/mm/yy' });
        });
    </script>
    <script type="text/javascript">
        function openWindow(Requestid) {
            window.open('GroupRequestidDetails.aspx?RequestID=' + Requestid, 'open_window', ' width=1024, height=720, left=0, top=0 status=yes,toolbar=no,scrollbars=yes');
        }
    </script>
</asp:Content>

