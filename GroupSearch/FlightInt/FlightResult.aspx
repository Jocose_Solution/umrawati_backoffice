﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false" CodeFile="FlightResult.aspx.vb" Inherits="FlightInt_FltResult" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="../css/flightIntlRes.css" type="text/css" />
    <link rel="stylesheet" href="../css/main2.css" type="text/css" />
    <script type="text/javascript" src="../js/fareinfo.js"></script>
    <script type="text/javascript" src="../js/chrome.js"></script>
    <table style="background: #fff; width: 950px; margin: auto;" align="center">
        <tr>
            <td colspan="2" style="width: 100%; border: 1px solid #eee;">
                <asp:PlaceHolder ID="mtrxPL" runat="server"></asp:PlaceHolder>
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 700px;" valign="top">
                <div style="width: 720px; margin: auto; background: #fff;">
                    <asp:Xml ID="xmlRes" runat="server" TransformSource="Multicity.xsl"></asp:Xml>
                </div>
            </td>
            <td valign="top" style="padding: 8px; width: 200px; border: 1px solid #eee;" rowspan="2">
                <div style="clear: both; color: #2a0d37; text-align: center; font-weight: bold; height: 25px;">
                    <asp:Label ID="lblTotal" runat="server"></asp:Label>
                </div>
                <div style="clear: both; background: #f2f4f4; padding-left: 10px; font-weight: bold; height: 25px; line-height: 25px;">
                    Filter By Airline
                </div>
                <div id="div2" runat="server" style="float: left; padding-left: 10px">
                    <asp:CheckBoxList ID="chkair" runat="server" CellPadding="10" CellSpacing="10" RepeatDirection="Vertical"
                        RepeatLayout="Table" TextAlign="Right" AutoPostBack="true">
                    </asp:CheckBoxList>
                </div>
            </td>
        </tr>
    </table>
    <script type="text/javascript" language="javascript">
        function __doPostBack(eventTarget, eventArgument) {
            var formObj = document.getElementById("aspnetForm");

            if (!formObj.onsubmit || (formObj.onsubmit() != false)) {

                formObj.__EVENTTARGET.value = eventTarget;

                formObj.__EVENTARGUMENT.value = eventArgument;

                formObj.submit();

            }
        }

    </script>

    <script type="text/javascript">
        function SetHiddenVariable(st) {
            var jsVar;
            if (st == 0)
            { jsVar = '0-Stop'; }
            else if (st == 1)
            { jsVar = '1-Stop'; }
            else if (st == 2)
            { jsVar = '2-Stop'; }
            else
            { jsVar = st; }
            __doPostBack('callPostBack', jsVar);
        }
        function SetHiddenVariable1(st) {
            var jsVar = st + "";
            __doPostBack('callPostBack', jsVar);
        }
    </script>

    <div id="divfareDetails" class="frdiv" style="display: none">
    </div>
</asp:Content>

