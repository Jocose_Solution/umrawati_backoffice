﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Collections.Generic

Public Class SqlTransactionDom
    Dim objDataAcess As New DataAccess(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    Dim paramHashtable As New Hashtable
    Public Function insertProxyDetails(ByVal BookingType As String, ByVal TravelType As String, ByVal ProxyFrom As String, ByVal ProxyTo As String, ByVal DepartDate As String, ByVal ReturnDate As String, ByVal DepartTime As String, ByVal ReturnTime As String, ByVal Adult As Integer, ByVal Child As Integer, ByVal Infrant As Integer, ByVal ClassT As String, ByVal Airline As String, ByVal Classes As String, ByVal PaymentMode As String, ByVal Remark As String, ByVal AgentID As String, ByVal Ag_Name As String, ByVal Status As String, ByVal Trip As String, ByVal Ptype As String, ByVal projectId As String, ByVal BookedBy As String) As Integer
        paramHashtable.Clear()
        paramHashtable.Add("@BookingType", BookingType)
        paramHashtable.Add("@TravelType", TravelType)
        paramHashtable.Add("@ProxyFrom", ProxyFrom)
        paramHashtable.Add("@ProxyTo", ProxyTo)
        paramHashtable.Add("@DepartDate", DepartDate)
        paramHashtable.Add("@ReturnDate", ReturnDate)
        paramHashtable.Add("@DepartTime", DepartTime)
        paramHashtable.Add("@ReturnTime", ReturnTime)
        paramHashtable.Add("@Adult", Adult)
        paramHashtable.Add("@Child", Child)
        paramHashtable.Add("@Infrant", Infrant)
        paramHashtable.Add("@Class", ClassT)
        paramHashtable.Add("@Airline", Airline)
        paramHashtable.Add("@Classes", Classes)
        paramHashtable.Add("@PaymentMode", PaymentMode)
        paramHashtable.Add("@Remark", Remark)
        paramHashtable.Add("@AgentID", AgentID)
        paramHashtable.Add("@Ag_Name", Ag_Name)
        paramHashtable.Add("@Status", Status)
        paramHashtable.Add("@Trip", Trip)
        paramHashtable.Add("@Ptype", Ptype) 'Added ProxyType Domestic or International
        paramHashtable.Add("@ProjectID", projectId)
        paramHashtable.Add("@BookedBy", BookedBy)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "InsertProxy", 2)
    End Function
    Public Function InsertProxyPaxDetail(ByVal ProxyID As Integer, ByVal Title As String, ByVal FirstName As String, ByVal LastName As String, ByVal Age As String, ByVal AgentID As String, ByVal PaxType As String) As Integer
        paramHashtable.Clear()
        paramHashtable.Add("@ProxyID", ProxyID)
        paramHashtable.Add("@Title", Title)
        paramHashtable.Add("@FirstName", FirstName)
        paramHashtable.Add("@LastName", LastName)
        paramHashtable.Add("@Age", Age)
        paramHashtable.Add("@AgentID", AgentID)
        paramHashtable.Add("@PaxType", PaxType)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "InsertProxyPax", 1)
    End Function

    Public Function ProxyDetails(ByVal st As String, Optional ByVal Ptype As String = "", Optional ByVal id As String = "") As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@st", st)
        paramHashtable.Add("@Ptype", Ptype)
        paramHashtable.Add("@id", id)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "ProxyDetail", 3)
    End Function

    Public Function UpdateProxyDetails(ByVal st As String, ByVal id As String, ByVal ProxyID As String, ByVal Rm As String) As Integer
        paramHashtable.Clear()
        paramHashtable.Add("@st", st)
        paramHashtable.Add("@id", id)
        paramHashtable.Add("@Proxyid", ProxyID)
        paramHashtable.Add("@Remark", Rm)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "UpdateProxy", 1)
    End Function
    Public Function AgentCommentProxy(ByVal PID As String, ByVal RM As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@Remark", RM)
        paramHashtable.Add("@Proxyid", PID)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "AgentCommentProxy", 3)
    End Function
    Public Function ProxyPaxDetails(ByVal PID As String, ByVal Type As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@ProxyID", PID)
        paramHashtable.Add("@Type", Type)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "ProxyPaxDetails", 3)
    End Function
    Public Function GetAgencyDetails(ByVal UserId As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@UserId", UserId)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "AgencyDetails", 3)
    End Function
    Public Function Servicecharge(ByVal AirCode As String, ByVal SelectType As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@AirCode", AirCode)
        paramHashtable.Add("@SelectType", SelectType)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "ProxyServiceTax", 3)
    End Function
    Public Function InsertProxyPaxInfoIntl(ByVal OrderId As String, ByVal tittle As String, ByVal fname As String, ByVal mname As String, ByVal lname As String, ByVal Paxtype As String, ByVal TktNo As String, ByVal Tri As String) As Integer
        paramHashtable.Clear()
        paramHashtable.Add("@OrderId", OrderId)
        paramHashtable.Add("@tittle", tittle)
        paramHashtable.Add("@first_name", fname)
        paramHashtable.Add("@middle_name", mname)
        paramHashtable.Add("@last_name", lname)
        paramHashtable.Add("@paxtype", Paxtype)
        paramHashtable.Add("@ticketno", TktNo)
        paramHashtable.Add("@Tri", Tri)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "InsertPaxIntl", 1)
    End Function
    Public Function UpdateProxyDate(ByVal Status As String, ByVal ProxyID As String, ByVal SrvChgOneWay As Double, ByVal SrvChgTwoWay As Double, ByVal OrderIdOneWay As String, ByVal OrderIdTwoWay As String, ByVal RBDOneWay As String, ByVal RBDTwoWay As String) As Integer
        paramHashtable.Clear()
        paramHashtable.Add("@Status", Status)
        paramHashtable.Add("@ProxyID", ProxyID)
        paramHashtable.Add("@SrvChgOneWay", SrvChgOneWay)
        paramHashtable.Add("@SrvChgTwoWay", SrvChgTwoWay)
        paramHashtable.Add("@OrderIdOneWay", OrderIdOneWay)
        paramHashtable.Add("@OrderIdTwoWay", OrderIdTwoWay)
        paramHashtable.Add("@RBDOneWay", RBDOneWay)
        paramHashtable.Add("@RBDTwoWay", RBDTwoWay)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "UpdateProxyDate", 1)
    End Function
    Public Function insertLedgerDetails(ByVal AgentID As String, ByVal AgencyName As String, ByVal InvoiceNo As String, ByVal PnrNo As String, ByVal TicketNo As String, ByVal TicketingCarrier As String, ByVal YatraAccountID As String, ByVal AccountID As String, ByVal ExecutiveID As String, ByVal IPAddress As String, ByVal Debit As Double, ByVal Credit As Double, ByVal Aval_Balance As Double, ByVal BookingType As String, ByVal Remark As String, ByVal PaxId As Integer, Optional ByVal TDS As Double = 0, Optional ByVal ProjectId As String = Nothing, Optional ByVal BookedBy As String = Nothing, Optional ByVal BillNo As String = Nothing) As Integer
        paramHashtable.Clear()
        paramHashtable.Add("@AgentID", AgentID)
        paramHashtable.Add("@AgencyName", AgencyName)
        paramHashtable.Add("@InvoiceNo", InvoiceNo)
        paramHashtable.Add("@PnrNo", PnrNo)
        paramHashtable.Add("@TicketNo", TicketNo)
        paramHashtable.Add("@TicketingCarrier", TicketingCarrier)
        paramHashtable.Add("@YatraAccountID", YatraAccountID)

        paramHashtable.Add("@AccountID", AccountID)
        paramHashtable.Add("@ExecutiveID", ExecutiveID)
        paramHashtable.Add("@IPAddress", IPAddress)

        paramHashtable.Add("@Debit", Debit)
        paramHashtable.Add("@Credit", Credit)
        paramHashtable.Add("@Aval_Balance", Aval_Balance)
        paramHashtable.Add("@BookingType", BookingType)
        paramHashtable.Add("@Remark", Remark)
        paramHashtable.Add("@PaxId", PaxId)
        paramHashtable.Add("@ProjectId", ProjectId)
        paramHashtable.Add("@BookedBy", BookedBy)
        paramHashtable.Add("@BillNo", BillNo)
        paramHashtable.Add("@TDS", TDS)

        'Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "usp_insert_LedgerDetails", 1)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "usp_insert_LedgerDetails_PP", 1)
    End Function
    Public Function GetFltFareDtlForLedger(ByVal TrackID As String, ByVal PaxType As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@Trackid", TrackID)
        paramHashtable.Add("@PaxType", PaxType)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetFltFareDtlForLedger", 3)
    End Function
    'Upload Statement
    Public Function GetUploadType() As DataSet
        paramHashtable.Clear()
        'Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetUploadType", 3)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetUploadType_PP", 3)
    End Function
    Public Function GetCategory(ByVal UType As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@UType", UType)
        'Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetCategory", 3)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetCategory_PP", 3)
    End Function
    Public Function GetAgencyByType(ByVal Type As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@Type", Type)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetAgencyByType", 3)
    End Function
    Public Function GetAllAgency(ByVal Action As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@Action", Action)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetAllAgency", 3)
    End Function
    Public Function insertUploadLedgerDetails(ByVal AgentID As String, ByVal AgencyName As String, ByVal AccountID As String, ByVal IPAddress As String, ByVal Debit As Double, ByVal Credit As Double, ByVal Aval_Balance As Double, ByVal BookingType As String, ByVal Remark As String, ByVal UploadType As String) As Integer
        paramHashtable.Clear()
        paramHashtable.Add("@AgentID", AgentID)
        paramHashtable.Add("@AgencyName", AgencyName)
        paramHashtable.Add("@AccountID", AccountID)
        paramHashtable.Add("@IPAddress", IPAddress)
        paramHashtable.Add("@Debit", Debit)
        paramHashtable.Add("@Credit", Credit)
        paramHashtable.Add("@Aval_Balance", Aval_Balance)
        paramHashtable.Add("@BookingType", BookingType)
        paramHashtable.Add("@Remark", Remark)
        paramHashtable.Add("@UploadType", UploadType)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "InsertUploadAmount", 1)
    End Function
    Public Function insertUploadDetails(ByVal AgentID As String, ByVal AgencyName As String, ByVal AccountID As String, ByVal IPAddress As String, ByVal Debit As Double, ByVal Credit As Double, ByVal Remark As String, ByVal UploadType As String, ByVal LastAval_Balance As Double, ByVal CurrentAval_Balance As Double, ByVal YtrRcptNo As String) As Integer
        paramHashtable.Clear()
        paramHashtable.Add("@agentid", AgentID)
        paramHashtable.Add("@agencyname", AgencyName)
        paramHashtable.Add("@accid", AccountID)
        paramHashtable.Add("@IPaddress", IPAddress)
        paramHashtable.Add("@debit", Debit)
        paramHashtable.Add("@credit", Credit)

        paramHashtable.Add("@remark", Remark)
        paramHashtable.Add("@Uploadtype", UploadType)
        paramHashtable.Add("@lastavlbal", LastAval_Balance)
        paramHashtable.Add("@curravlbal", CurrentAval_Balance)
        paramHashtable.Add("@YtrRcptNo", YtrRcptNo)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "InsertUploadDetails", 2)
    End Function
    Public Function GetUploadTypeByType(ByVal Type As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@Type", Type)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetUploadTypeByType", 3)
    End Function

    'Agent Panel
    Public Function BankInformation(ByVal AgentId As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@AgentId", AgentId)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "BankInformation", 3)
    End Function
    Public Function GetBranchAccount(ByVal Bank As String, ByVal Type As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@Bank", Bank)
        paramHashtable.Add("@Type", Type)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetBranchAccount", 3)
    End Function
    Public Function insertDeposite(ByVal AgencyName As String, ByVal AgencyID As String, ByVal Amount As Double, ByVal ModeOfPayment As String, ByVal BankName As String, ByVal BranchName As String, ByVal AccountNo As String, ByVal ChequeNo As String, ByVal ChequeDate As String, ByVal TransactionID As String, ByVal BankAreaCode As String, ByVal DepositCity As String, ByVal DepositeDate As String, ByVal Remark As String, ByVal Status As String, ByVal UploadType As String, ByVal DepositeOffice As String, ByVal ConcernPerson As String, ByVal RecieptNo As String, ByVal BranchCode As String, ByVal AgentBankName As String, ByVal SalesExecID As String, ByVal AgentType As String) As Integer
        paramHashtable.Clear()
        paramHashtable.Add("@AgencyName", AgencyName)
        paramHashtable.Add("@AgencyID", AgencyID)
        paramHashtable.Add("@Amount", Amount)

        paramHashtable.Add("@ModeOfPayment", ModeOfPayment)
        paramHashtable.Add("@BankName", BankName)
        paramHashtable.Add("@BranchName", BranchName)

        paramHashtable.Add("@AccountNo", AccountNo)
        paramHashtable.Add("@ChequeNo", ChequeNo)
        paramHashtable.Add("@ChequeDate", ChequeDate)

        paramHashtable.Add("@TransactionID", TransactionID)
        paramHashtable.Add("@BankAreaCode", BankAreaCode)
        paramHashtable.Add("@DepositCity", DepositCity)

        paramHashtable.Add("@DepositeDate", DepositeDate)
        paramHashtable.Add("@Remark", Remark)
        paramHashtable.Add("@Status", Status)

        paramHashtable.Add("@UploadType", UploadType)
        paramHashtable.Add("@DepositeOffice", DepositeOffice)
        paramHashtable.Add("@ConcernPerson", ConcernPerson)

        paramHashtable.Add("@RecieptNo", RecieptNo)
        paramHashtable.Add("@BranchCode", BranchCode)
        paramHashtable.Add("@AgentBankName", AgentBankName)

        paramHashtable.Add("@SalesExecID", SalesExecID)
        paramHashtable.Add("@AgentType", AgentType)


        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "InsertDepositDetails", 1)
    End Function
    Public Function GetDepositDetails(ByVal FromDate As String, ByVal ToDate As String, ByVal UserType As String, ByVal LoginID As String, ByVal PaymentMode As String, ByVal AgentID As String, ByVal Status As String, ByVal SearchType As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@FromDate", FromDate)
        paramHashtable.Add("@ToDate", ToDate)
        paramHashtable.Add("@UserType", UserType)
        paramHashtable.Add("@LoginID", LoginID)
        paramHashtable.Add("@PaymentMode", PaymentMode)
        paramHashtable.Add("@AgentID", AgentID)
        paramHashtable.Add("@Status", Status)
        paramHashtable.Add("@SearchType", SearchType)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetDepositDetailsWithdate", 3)
    End Function



    'Account Panel
    Public Function DepositProcessDetails(ByVal Status As String, ByVal Type As String, Optional ByVal AccountID As String = "") As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@Status", Status)
        paramHashtable.Add("@Type", Type)
        paramHashtable.Add("@AccountID", AccountID)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetDepositProcessDetails", 3)
    End Function
    Public Function UpdateDepositDetails(ByVal ID As String, ByVal AgentID As String, ByVal Status As String, ByVal Type As String, ByVal AccountID As String, ByVal Rmk As String) As Integer
        paramHashtable.Clear()
        paramHashtable.Add("@ID", ID)
        paramHashtable.Add("@AgentID", AgentID)
        paramHashtable.Add("@Status", Status)
        paramHashtable.Add("@Type", Type)
        paramHashtable.Add("@AccountID", AccountID)
        paramHashtable.Add("@Rmk", Rmk)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "UpdateDepositDetails", 1)
    End Function
    Public Function GetDepositDetailsByID(ByVal ID As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@ID", ID)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetDepositeDetailsByID", 3)
    End Function
    Public Function DepositStatusDetails(ByVal Status As String, Optional ByVal AccountID As String = "") As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@Status", Status)
        paramHashtable.Add("@AccountID", AccountID)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetDepositStatusDetails", 3)
    End Function
    Public Function GetTypeByID() As DataSet
        paramHashtable.Clear()
        ' paramHashtable.Add("@UID", ID)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetTypeByID", 3)
    End Function
    Public Function GetAgentType() As DataSet
        paramHashtable.Clear()
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetAgentType", 3)
    End Function
    Public Function UpdateAgentTypeSalesRef(ByVal ID As String, ByVal SalesRef As String, ByVal Type As String, ByVal AgentStatus As String, ByVal Online_Tkt As String, ByVal Address As String, ByVal City As String, ByVal State As String, ByVal Country As String, ByVal Zipcode As String, ByVal FName As String, ByVal LName As String, ByVal Mobile As String, ByVal Email As String, ByVal Fax As String, ByVal Pan As String, ByVal Title As String, ByVal AgencyName As String, ByVal pwd As String, ByVal NamePanCard As String, ByVal OTPLoginStatus As String, ByVal ExpPass As String) As Integer
        paramHashtable.Clear()
        paramHashtable.Add("@UID", ID)
        paramHashtable.Add("@SalesRef", SalesRef)
        paramHashtable.Add("@Type", Type)
        paramHashtable.Add("@AgentStatus", AgentStatus)
        paramHashtable.Add("@Online_Tkt", Online_Tkt)
        paramHashtable.Add("@Address", Address)
        paramHashtable.Add("@City", City)
        paramHashtable.Add("@State", State)
        paramHashtable.Add("@Country", Country)
        paramHashtable.Add("@Zipcode", Zipcode)
        paramHashtable.Add("@FName", FName)
        paramHashtable.Add("@LName", LName)
        paramHashtable.Add("@Mobile", Mobile)
        paramHashtable.Add("@Email", Email)
        paramHashtable.Add("@Fax", Fax)
        paramHashtable.Add("@Pan", Pan)
        paramHashtable.Add("@Title", Title)
        paramHashtable.Add("@AgencyName", AgencyName)
        paramHashtable.Add("@pwd", pwd)
        paramHashtable.Add("@NamePanCard", NamePanCard)
        paramHashtable.Add("@OTPLoginStatus", OTPLoginStatus)
        paramHashtable.Add("@ExpPass", ExpPass)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "UpdateAgentTypeSalesRef", 1)
    End Function

    Public Function GetLedgerDetails(ByVal usertype As String, ByVal LoginID As String, ByVal FormDate As String, ByVal ToDate As String, ByVal AgentId As String, ByVal BookingType As String, ByVal SearchType As String, ByVal PaymentMode As String, ByVal TransType As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@usertype", usertype)
        paramHashtable.Add("@LoginID", LoginID)
        paramHashtable.Add("@FormDate", FormDate)
        paramHashtable.Add("@ToDate", ToDate)
        paramHashtable.Add("@AgentId", AgentId)
        paramHashtable.Add("@BookingType", BookingType)
        paramHashtable.Add("@SearchType", SearchType)
        paramHashtable.Add("@PaymentMode", PaymentMode)
        paramHashtable.Add("@TransType", TransType)
        'Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetLedgerDetail", 3)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetLedgerDetail_PP", 3)
    End Function

    'InsertRegistration
    Public Function InsertRegistration(ByVal user_id As String, ByVal Title As String, ByVal Fname As String, ByVal Lname As String, ByVal Address As String, ByVal city As String, ByVal state As String, ByVal country As String, ByVal Area As String, ByVal zipcode As String, ByVal Phone As String, ByVal Mobile As String, ByVal email As String, ByVal Alt_Email As String, ByVal Fax_no As String, ByVal Agency_Name As String, ByVal Website As String, ByVal NameOnPan As String, ByVal PanNo As String, ByVal Status As String, ByVal Stax_no As String, ByVal Remark As String, ByVal Sec_Qes As String, ByVal Sec_Ans As String, ByVal PWD As String, ByVal Agent_Type As String, ByVal Distr As String, ByVal ag_logo As String, ByVal SalesExecID As String) As Integer
        Try
            paramHashtable.Clear()
            paramHashtable.Add("@user_id", user_id)
            paramHashtable.Add("@Title", Title)
            paramHashtable.Add("@Fname", Fname)

            paramHashtable.Add("@Lname", Lname)
            paramHashtable.Add("@Address", Address)
            paramHashtable.Add("@city", city)

            paramHashtable.Add("@state", state)
            paramHashtable.Add("@country", country)
            paramHashtable.Add("@Area", Area)
            paramHashtable.Add("@zipcode", zipcode)

            paramHashtable.Add("@Phone", Phone)
            paramHashtable.Add("@Mobile", Mobile)
            paramHashtable.Add("@email", email)

            paramHashtable.Add("@Alt_Email", Alt_Email)
            paramHashtable.Add("@Fax_no", Fax_no)
            paramHashtable.Add("@Agency_Name", Agency_Name)
            paramHashtable.Add("@Website", Website)
            paramHashtable.Add("@NameOnPan", NameOnPan)
            paramHashtable.Add("@PanNo", PanNo)
            paramHashtable.Add("@Status", Status)
            paramHashtable.Add("@Stax_no", Stax_no)

            paramHashtable.Add("@Remark", Remark)
            paramHashtable.Add("@Sec_Qes", Sec_Qes)
            paramHashtable.Add("@Sec_Ans", Sec_Ans)

            paramHashtable.Add("@PWD", PWD)
            paramHashtable.Add("@Agent_Type", Agent_Type)
            paramHashtable.Add("@Distr", Distr)
            paramHashtable.Add("@ag_logo", ag_logo)
            paramHashtable.Add("@SalesExecID", SalesExecID)
            Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "InsertRegistrationDetails", 1)
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
            Return 0
        End Try
    End Function
    Public Function GetDetailByEmailMobile(ByVal Email As String, ByVal Mobile As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@Email", Email)

        paramHashtable.Add("@Mobile", Mobile)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetDetailByEmailMobile", 3)
    End Function
    Public Function getmaxcount() As Double
        paramHashtable.Clear()
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetNewRegsMaxCount", 2)
    End Function
    Public Function GetSalesRef() As DataSet
        paramHashtable.Clear()
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetSalesRef", 3)
    End Function
    Public Function GetCreditNodeDetails(ByVal RefundID As String, ByVal Trip As String, ByVal Status As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@RefundID", RefundID)
        paramHashtable.Add("@Trip", Trip)
        paramHashtable.Add("@Status", Status)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetCreditNodeDetails", 3)
    End Function
    Public Function InvoiceNoRail(ByVal PNR As String) As Integer
        paramHashtable.Clear()
        paramHashtable.Add("@PNR", PNR)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "InvoiceNoRail", 2)
    End Function
    Public Function getAgencybySalesRef(ByVal SalesRef As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@SalesRef", SalesRef)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "getAgencybySalesRef", 3)
    End Function
    Public Function UpdateTktDomIntlOnLedger(ByVal OrderId As String, ByVal PaxId As Integer, ByVal TktNo As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@OrderId", OrderId)
        paramHashtable.Add("@PaxId", PaxId)
        paramHashtable.Add("@TktNo", TktNo)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "UpdateTktDomIntl", 3)
    End Function
    'Select Status and Executive
    Public Function GetStatusExecutiveID(ByVal ModuleName As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@Module", ModuleName)
        'Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetStatusExecutiveID", 3)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetStatusExecutiveID_PP", 3)
    End Function
    'Get Proxy Booking Report
    Public Function GetProxyBookingReport(ByVal usertype As String, ByVal LoginID As String, ByVal FormDate As String, ByVal ToDate As String, ByVal AgentID As String, ByVal ExecID As String, ByVal Status As String, ByVal ProxyTrip As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@usertype", usertype)
        paramHashtable.Add("@LoginID", LoginID)
        paramHashtable.Add("@FormDate", FormDate)
        paramHashtable.Add("@ToDate", ToDate)
        paramHashtable.Add("@AgentID", AgentID)
        paramHashtable.Add("@ExecID", ExecID)
        paramHashtable.Add("@Status", Status)
        paramHashtable.Add("@ProxyTrip", ProxyTrip)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetProxyBookingReport", 3)
    End Function
    'Update Agent Profile
    Public Function UpdateAgentProfile(ByVal Type As String, ByVal UID As String, ByVal pwd As String, ByVal AEmail As String, ByVal Landline As String, ByVal Fax As String, ByVal Address As String, ByVal City As String, ByVal State As String, ByVal Country As String) As Integer
        paramHashtable.Clear()
        paramHashtable.Add("@Type", Type)
        paramHashtable.Add("@UID", UID)
        paramHashtable.Add("@pwd", pwd)
        paramHashtable.Add("@AEmail", AEmail)
        paramHashtable.Add("@Landline", Landline)
        paramHashtable.Add("@Fax", Fax)
        paramHashtable.Add("@Address", Address)
        paramHashtable.Add("@City", City)
        paramHashtable.Add("@State", State)
        paramHashtable.Add("@Country", Country)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "UpdateAgentProfile", 1)
    End Function
    'Ckeck Forgot  Password
    Public Function CheckForgotPassword(ByVal UID As String, ByVal Email As String, ByVal Mobile As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@UID", UID)
        paramHashtable.Add("@Email", Email)
        paramHashtable.Add("@Mobile", Mobile)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "CheckForgotPassword", 3)
    End Function
    'Public Sub ExportData(ByVal dset As DataSet)
    '    Dim response As HttpResponse = HttpContext.Current.Response
    '    ' first let's clean up the response.object   
    '    response.Clear()
    '    response.Charset = ""
    '    ' set the response mime type for excel  
    '    'Dim filename As String = "Record.xls"
    '    Dim filename As String = "Report(" & "" & DateTime.Now.ToString("dd-MM-yyyy") & ").xls"

    '    response.ContentType = "application/vnd.ms-excel"
    '    response.AddHeader("Content-Disposition", "attachment;filename=""" & filename & """")

    '    ' create a string writer   
    '    Using sw As New StringWriter()
    '        Using htw As New HtmlTextWriter(sw)
    '            ' instantiate a datagrid   
    '            Dim dg As New DataGrid()
    '            dg.DataSource = dset.Tables(0)
    '            dg.DataBind()
    '            dg.RenderControl(htw)
    '            response.Write(sw.ToString())
    '            response.[End]()
    '        End Using
    '    End Using

    'End Sub

    Public Sub ExportData(ByVal ds As DataSet)
        'Dim response As HttpResponse = HttpContext.Current.Response
        '' first let's clean up the response.object   
        'response.Clear()
        'response.Charset = ""
        '' set the response mime type for excel  
        ''Dim filename As String = "Record.xls"
        'Dim filename As String = "Report(" & "" & DateTime.Now.ToString("dd-MM-yyyy") & ").xls"

        'response.ContentType = "application/vnd.ms-excel"
        'response.AddHeader("Content-Disposition", "attachment;filename=""" & filename & """")

        '' create a string writer   
        'Using sw As New StringWriter()
        '    Using htw As New HtmlTextWriter(sw)
        '        ' instantiate a datagrid   
        '        Dim dg As New DataGrid()
        '        dg.DataSource = dset.Tables(0)
        '        dg.DataBind()
        '        dg.RenderControl(htw)
        '        response.Write(sw.ToString())
        '        response.[End]()
        '    End Using
        'End Using
        Dim FilterList As String() = New String() {"Red", "Blue"}
        Dim Response As HttpResponse = HttpContext.Current.Response
        Response.Clear()
        Response.ClearContent()
        Response.ClearHeaders()

        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.Write("<!DOCTYPE HTML  PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">")
        Dim filename As String = ""
        If fileName <> "" Then
            Response.AddHeader("content-disposition", "attachment;filename=" & fileName & ").xls")

        Else
            Response.AddHeader("content-disposition", "attachment;filename=Report(" & "" & DateTime.Now.ToString("dd-MM-yyyyHHmmss") & ").xls")

        End If


        Response.ContentEncoding = Encoding.UTF8
        Response.Charset = ""
        'EnableViewState = False

        'Set Fonts
        Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>")
        Response.Write("<BR><BR><BR>")

        'Sets the table border, cell spacing, border color, font of the text, background,
        'foreground, font height
        Response.Write("<Table border='2' bgColor='#ffffff' borderColor='#000000' cellSpacing='0' cellPadding='0' style='font-size:10.0pt; font-family:Calibri; background:#F4F4F4;'> <TR>")

        ' Check not to increase number of records more than 65k according to excel,03
        If ds.Tables(0).Rows.Count <= 65536 Then
            ' Get DataTable Column's Header
            For Each column As DataColumn In ds.Tables(0).Columns
                'Write in new column
                Response.Write("<Td align='center'    style='padding-top: 7px;font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; background-color: #004b91; color: #FFFFFF;'>")

                'Get column headers  and make it as bold in excel columns
                Response.Write("<B >")
                Response.Write(column)
                Response.Write("</B>")
                Response.Write("</Td>")

            Next

            Response.Write("</TR>")

            ' Get DataTable Column's Row
            For Each dtRow As DataRow In ds.Tables(0).Rows
                'Write in new row
                Response.Write("<TR>")

                For i As Integer = 0 To ds.Tables(0).Columns.Count - 1
                    Response.Write("<Td>")
                    Response.Write(dtRow(i).ToString())
                    Response.Write("</Td>")
                Next

                Response.Write("</TR>")
            Next
        End If

        Response.Write("</Table>")
        Response.Write("</font>")

        Response.Flush()
        Response.[End]()
    End Sub
    Public Sub ExportData(ByVal ds As DataSet, Optional ByVal fileName As String = "")
        'Dim response As HttpResponse = HttpContext.Current.Response
        '' first let's clean up the response.object   
        'response.Clear()
        'response.Charset = ""
        '' set the response mime type for excel  
        ''Dim filename As String = "Record.xls"
        'Dim filename As String = "Report(" & "" & DateTime.Now.ToString("dd-MM-yyyy") & ").xls"

        'response.ContentType = "application/vnd.ms-excel"
        'response.AddHeader("Content-Disposition", "attachment;filename=""" & filename & """")

        '' create a string writer   
        'Using sw As New StringWriter()
        '    Using htw As New HtmlTextWriter(sw)
        '        ' instantiate a datagrid   
        '        Dim dg As New DataGrid()
        '        dg.DataSource = dset.Tables(0)
        '        dg.DataBind()
        '        dg.RenderControl(htw)
        '        response.Write(sw.ToString())
        '        response.[End]()
        '    End Using
        'End Using
        Dim FilterList As String() = New String() {"Red", "Blue"}
        Dim Response As HttpResponse = HttpContext.Current.Response
        Response.Clear()
        Response.ClearContent()
        Response.ClearHeaders()

        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.Write("<!DOCTYPE HTML  PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">")

        If fileName <> "" Then
            Response.AddHeader("content-disposition", "attachment;filename=" & fileName & ").xls")

        Else
            Response.AddHeader("content-disposition", "attachment;filename=Report(" & "" & DateTime.Now.ToString("dd-MM-yyyyHHmmss") & ").xls")

        End If


        Response.ContentEncoding = Encoding.UTF8
        Response.Charset = ""
        'EnableViewState = False

        'Set Fonts
        Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>")
        Response.Write("<BR><BR><BR>")

        'Sets the table border, cell spacing, border color, font of the text, background,
        'foreground, font height
        Response.Write("<Table border='2' bgColor='#ffffff' borderColor='#000000' cellSpacing='0' cellPadding='0' style='font-size:10.0pt; font-family:Calibri; background:#F4F4F4;'> <TR>")

        ' Check not to increase number of records more than 65k according to excel,03
        If ds.Tables(0).Rows.Count <= 65536 Then
            ' Get DataTable Column's Header
            For Each column As DataColumn In ds.Tables(0).Columns
                'Write in new column
                Response.Write("<Td align='center'    style='padding-top: 7px;font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; background-color: #004b91; color: #FFFFFF;'>")

                'Get column headers  and make it as bold in excel columns
                Response.Write("<B >")
                Response.Write(column)
                Response.Write("</B>")
                Response.Write("</Td>")

            Next

            Response.Write("</TR>")

            ' Get DataTable Column's Row
            For Each dtRow As DataRow In ds.Tables(0).Rows
                'Write in new row
                Response.Write("<TR>")

                For i As Integer = 0 To ds.Tables(0).Columns.Count - 1
                    Response.Write("<Td>")
                    Response.Write(dtRow(i).ToString())
                    Response.Write("</Td>")
                Next

                Response.Write("</TR>")
            Next
        End If

        Response.Write("</Table>")
        Response.Write("</font>")

        Response.Flush()
        Response.[End]()
    End Sub
    'AirlineStatusTrueFalse
    Public Function AirlineEnableTrueFalse(ByVal org As String, ByVal Dest As String, ByVal FlightNo As String, ByVal Airline As String, ByVal Trip As String) As String
        paramHashtable.Clear()
        paramHashtable.Add("@Org", org)
        paramHashtable.Add("@Dest", Dest)
        paramHashtable.Add("@FlightNo", FlightNo)
        paramHashtable.Add("@Airline", Airline)
        paramHashtable.Add("@Trip", Trip)
        Dim i As String = objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "SpBlockBookingAirlineWise", 2)
        Return i
    End Function
    'For Ledger Details BookingType
    Public Function GetLedgerBookingType() As DataSet
        paramHashtable.Clear()
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GET_LEDGER_BOOKINGTYPE", 3)
    End Function
    'Public Function GetAgencyDetailsDynamic(ByVal UserId As String, ByVal AgentType As String, ByVal SalesPersonID As String, ByVal FromDate As String, ByVal ToDate As String, ByVal DistrId As String, ByVal UserType As String) As DataSet
    '    paramHashtable.Clear()
    '    paramHashtable.Add("@UserId", UserId)
    '    paramHashtable.Add("@AgentType", AgentType)
    '    paramHashtable.Add("@SalesExecID", SalesPersonID)
    '    paramHashtable.Add("@FromDate", FromDate)
    '    paramHashtable.Add("@ToDate", ToDate)
    '    paramHashtable.Add("@DistrId", DistrId)
    '    paramHashtable.Add("@UserType", UserType)

    '    Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "AgencyDetailsDynamic", 3)
    'End Function
    Public Function GetAgencyDetailsDynamic(ByVal UserId As String, ByVal AgentType As String, ByVal SalesPersonID As String, ByVal FromDate As String, ByVal ToDate As String, ByVal DistrId As String, ByVal UserType As String, ByVal DiSearch As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@UserId", UserId)
        paramHashtable.Add("@AgentType", AgentType)
        paramHashtable.Add("@SalesExecID", SalesPersonID)
        paramHashtable.Add("@FromDate", FromDate)
        paramHashtable.Add("@ToDate", ToDate)
        paramHashtable.Add("@DistrId", DistrId)
        paramHashtable.Add("@UserType", UserType)
        paramHashtable.Add("@DiSearch", DiSearch)

        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "AgencyDetailsDynamic", 3)
    End Function
    Public Function GetAllGroupType() As DataSet
        paramHashtable.Clear()
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "Sp_GetAllGroupType", 3)
    End Function

    'baggage allowance of domestic airlines
    'Public Function GetBaggageInformation(ByVal Trip As String, ByVal airCode As String) As DataSet
    '    paramHashtable.Clear()
    '    paramHashtable.Add("@Trip", Trip)
    '    paramHashtable.Add("@AIRLINE", airCode)
    '    Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "SP_GETBAGGAGE_INFO", 3)
    'End Function

    'baggage allowance of domestic airlines
    Public Function GetBaggageInformation(ByVal Trip As String, ByVal airCode As String, ByVal IsBagFare As Boolean) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@Trip", Trip)
        paramHashtable.Add("@AIRLINE", airCode)
        paramHashtable.Add("@IsBagFare", IsBagFare)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "SP_GETBAGGAGE_INFO", 3)
    End Function
    Public Function GetFlightDetailsByAgentId(ByVal AgentID As String, ByVal date1 As String) As List(Of FltDetails)


        Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString.ToString())
        Dim i As Integer = 0
        Dim DateFarmate() As String
        DateFarmate = date1.Split("-")
        If (Convert.ToInt16(DateFarmate(1)) = 0) Then
            date1 = 1
            date1 = DateFarmate(0) + "-" + (Convert.ToInt16(DateFarmate(1)) + 1).ToString() + "-" + DateFarmate(2)
            date1 = Convert.ToDateTime(date1).ToString("ddMMyy")
            '   var validdate = validDate.split('-');
            '  if (parseInt(validdate[1]) == 0) {

            '      validDate = validdate[0] + "-" + (parseInt(validdate[1]) + 1).toString() + "-" + validdate[2];
            '}


        Else
            date1 = Convert.ToDateTime(date1).AddMonths(1).ToString("ddMMyy")
        End If
        Dim fltDList As New List(Of FltDetails)
        Try

            Dim cmd As New SqlCommand("Sp_GetFlightDetailsForTravelCal", con)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AgentID", AgentID.Trim())
            'cmd.Parameters.AddWithValue("@date", Convert.ToDateTime(date1).AddMonths(1).ToString("ddMMyy"))
            cmd.Parameters.AddWithValue("@date", date1)
            con.Open()
            Dim dr As SqlDataReader = cmd.ExecuteReader()


            While dr.Read()
                fltDList.Add(New FltDetails() With {.DepDate = dr("DepDate").ToString().Trim(), .Pnr = dr("GdsPnr").ToString().Trim(), .DepTime = dr("DepTime").ToString().Trim(), .Sector = dr("sector").ToString().Trim(), .OrderID = dr("OrderId").ToString().Trim()})

            End While


            con.Close()
        Catch ex As SqlException
            'throw ex;
            ' ex.ToString();

        Finally
            con.Dispose()
        End Try


        Return fltDList




    End Function



    Public Function GetFlightDetailsByDateForCal(ByVal AgentID As String, ByVal date1 As String) As List(Of FltDetails)


        Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString.ToString())
        Dim i As Integer = 0
        Dim fltDList As New List(Of FltDetails)
        Try

            Dim cmd As New SqlCommand("Sp_GetFlightDetailsByDateForCal", con)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AgentID", AgentID.Trim())
            cmd.Parameters.AddWithValue("@date", Convert.ToDateTime(date1).ToString("ddMMyy"))
            con.Open()
            Dim dr As SqlDataReader = cmd.ExecuteReader()


            While dr.Read()
                fltDList.Add(New FltDetails() With {.DepDate = dr("DepDate").ToString().Trim(), .Pnr = dr("GdsPnr").ToString().Trim(), .DepTime = dr("DepTime").ToString().Trim(), .Sector = dr("sector").ToString().Trim(), .OrderID = dr("OrderId").ToString().Trim()})

            End While


            con.Close()
        Catch ex As SqlException
            'throw ex;
            ' ex.ToString();

        Finally
            con.Dispose()
        End Try


        Return fltDList




    End Function
    'MAILING
    Public Function SendMail(ByVal toEMail As String, ByVal from As String, ByVal bcc As String, ByVal cc As String, ByVal smtpClient As String, ByVal userID As String, ByVal pass As String, ByVal body As String, ByVal subject As String, ByVal AttachmentFile As String) As Integer

        Dim objMail As New System.Net.Mail.SmtpClient
        Dim msgMail As New System.Net.Mail.MailMessage
        msgMail.To.Clear()
        msgMail.To.Add(New System.Net.Mail.MailAddress(toEMail))
        msgMail.From = New System.Net.Mail.MailAddress(from)
        If bcc <> "" Then
            msgMail.Bcc.Add(New System.Net.Mail.MailAddress(bcc))
        End If
        If cc <> "" Then
            msgMail.CC.Add(New System.Net.Mail.MailAddress(cc))
        End If
        If AttachmentFile <> "" Then
            msgMail.Attachments.Add(New System.Net.Mail.Attachment(AttachmentFile))
        End If

        msgMail.Subject = subject
        msgMail.IsBodyHtml = True
        msgMail.Body = body


        Try
            objMail.Credentials = New System.Net.NetworkCredential(userID, pass)
            objMail.DeliveryMethod = Net.Mail.SmtpDeliveryMethod.Network
            objMail.Host = smtpClient

            objMail.EnableSsl = True
            objMail.Port = 587


            objMail.Send(msgMail)
            Return 1

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
            Return 0

        End Try
    End Function
    Public Function GetMailingDetails(ByVal department As String, ByVal UserID As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@Department", department)
        paramHashtable.Add("@UserID", UserID)
        'Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "SP_GETMAILINGCREDENTIAL_ITZ", 3)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "SP_GETMAILINGCREDENTIAL_ITZ_PP", 3)
    End Function
    Public Function GetCompanyAddress(ByVal AddressType As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@TYPE", AddressType)
        'Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "SP_GETADDRESS", 3)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "SP_GETADDRESS_PP", 3)
    End Function
    'Ledger and Credit Limit With Transaction
    Public Function Ledgerandcreditlimit_Transaction(ByVal AGENTID As String, ByVal TOTALAFETRDIS As Double, ByVal TRACKID As String, ByVal VC As String, ByVal GDSPNR As String, ByVal AGENCYNAME As String, ByVal IP As String, ByVal ProjectId As String, ByVal BookedBy As String, ByVal BillNo As String, ByVal AvailBal As Double, ByVal EasyID As String) As Integer
        paramHashtable.Clear()
        paramHashtable.Add("@AGENTID", AGENTID)
        paramHashtable.Add("@TOTALAFETRDIS", TOTALAFETRDIS)
        paramHashtable.Add("@TRACKID", TRACKID)
        paramHashtable.Add("@VC", VC)
        paramHashtable.Add("@GDSPNR", GDSPNR)
        paramHashtable.Add("@AGENCYNAME", AGENCYNAME)
        paramHashtable.Add("@IP", IP)
        paramHashtable.Add("@ProjectId", ProjectId)
        paramHashtable.Add("@BookedBy", BookedBy)
        paramHashtable.Add("@BillNo", BillNo)
        paramHashtable.Add("@AVAILBAL", AvailBal)
        paramHashtable.Add("@EasyID", EasyID)

        Dim i As Integer = objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "SP_INSERT_LEDGERNEWREGES_TRANSACTION", 2)
        ''Dim i As Integer = objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "USP_INSERT_LEDGERNEWREGES_TRANSACTION_NEW", 2)
        Return i
        'End Ledger and Credit Limit With Transaction
    End Function
    ''Public Function Ledgerandcreditlimit_Transaction(ByVal AGENTID As String, ByVal TOTALAFETRDIS As Double, ByVal TRACKID As String, ByVal VC As String, ByVal GDSPNR As String, ByVal AGENCYNAME As String, ByVal IP As String, ByVal ProjectId As String, ByVal BookedBy As String, ByVal BillNo As String) As Integer
    ''    paramHashtable.Clear()
    ''    paramHashtable.Add("@AGENTID", AGENTID)
    ''    paramHashtable.Add("@TOTALAFETRDIS", TOTALAFETRDIS)
    ''    paramHashtable.Add("@TRACKID", TRACKID)
    ''    paramHashtable.Add("@VC", VC)
    ''    paramHashtable.Add("@GDSPNR", GDSPNR)
    ''    paramHashtable.Add("@AGENCYNAME", AGENCYNAME)
    ''    paramHashtable.Add("@IP", IP)
    ''    paramHashtable.Add("@ProjectId", ProjectId)
    ''    paramHashtable.Add("@BookedBy", BookedBy)
    ''    paramHashtable.Add("@BillNo", BillNo)
    ''    Dim i As Integer = objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "SP_INSERT_LEDGERNEWREGES_TRANSACTION", 2)
    ''    Return i
    ''    'End Ledger and Credit Limit With Transaction
    ''End Function

    'Update Ledger by paxId
    Public Function UpdateLedger_PaxId(ByVal PAXID As Integer, ByVal TICKETNO As String, ByVal PNR As String) As Integer
        paramHashtable.Clear()
        paramHashtable.Add("@PAXID", PAXID)
        paramHashtable.Add("@TICKETNO", TICKETNO)
        paramHashtable.Add("@PNR", PNR)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "SP_UPDATE_LEDGERBYPAXID", 1)
    End Function
    'End Update Ledger by paxId

    'Get Module Details 
    Public Function GetModuleAccessDetails(ByVal UID As String, ByVal MODULENAME As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@USERID", UID)
        paramHashtable.Add("@MODULE", MODULENAME)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "SP_GETMODULEACCESS_DETAILS", 3)
    End Function
    'New FLayout
    Public Function SelectHeaderDetail_HOLD(ByVal OrderId As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@OrderId", OrderId)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "SP_GETHRDDETAILS_HOLD", 3)
    End Function
    Public Function GetAirportName(ByVal AirCode As String) As String
        paramHashtable.Clear()
        paramHashtable.Add("@AIRCODE", AirCode)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "SP_GET_AIRPORTNAME", 2)
    End Function
    Public Function CustFltDetails_Dom(ByVal OBDS As DataSet, ByVal IBDS As DataSet, ByVal FT As String) As String
        Dim FlightDtlsTotalInfo As String = ""
        Dim DepTerminal As String
        Dim ArrTerminal As String
        Dim RBD As String
        'Dim IBRBD As String

        'Dim OBCABIN As String
        Dim CABIN As String
        Dim FlightType = ""
        If FT = "InBound" Then
            FlightType = "OutBound"
        End If

        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<table  width='100%' border='0' cellspacing='0' cellpadding='0'>"
        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr ><td colspan='2' style='font-size:16px; line-height:35px; border-bottom:2px solid #d1d1d1;color:#004b91' >" & FlightType & " Flight Details</td><td align='left' style='font-size:14px; line-height:35px; border-bottom:2px solid #d1d1d1;color:#004b91; '></td><tr>" '" & OBDS.Tables(0).Rows(0)("AdtFareType") & "
        For i As Integer = 0 To OBDS.Tables(0).Rows.Count - 1
            DepTerminal = ""
            ArrTerminal = ""
            RBD = ""
            CABIN = ""
            If OBDS.Tables(0).Rows(i)("RBD").ToString().Trim() <> "" Then
                RBD = "Class (" & OBDS.Tables(0).Rows(i)("RBD") & ")"
            End If
            If OBDS.Tables(0).Rows(i)("AdtCabin").ToString().Trim() <> "" Then
                CABIN = "Cabin (" & CabinName(OBDS.Tables(0).Rows(i)("AdtCabin")) & ")"
            End If
            'Dim AirportName As String =STDom.GetAirportName(OBDS.Tables(0).Rows(i)("DepartureLocation")
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td colspan='2' ><img alt='' src='../Airlogo/sm" & OBDS.Tables(0).Rows(i)("MarketingCarrier") & ".gif'/> " & OBDS.Tables(0).Rows(i)("AirlineName") & "</td><td align='left' style='font-size:14px; line-height:35px;color:#004b91; '>" & OBDS.Tables(0).Rows(i)("AdtFareType") & "</td>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"

            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>(" & OBDS.Tables(0).Rows(i)("MarketingCarrier") & "-" & OBDS.Tables(0).Rows(i)("FlightIdentification") & ")" & "</td>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='font-size:14px; font-weight:bold;'>" & OBDS.Tables(0).Rows(i)("Departure_Date") & " (" & MultiValueFunction_Dom(OBDS.Tables(0), "Depall", i) & ")</td>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='font-size:14px; font-weight:bold;'>" & OBDS.Tables(0).Rows(i)("Arrival_Date") & " (" & MultiValueFunction_Dom(OBDS.Tables(0), "Arrall", i) & ")</td>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"

            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td >" & RBD & " </td>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td >" & OBDS.Tables(0).Rows(i)("DepartureCityName") & "(" & OBDS.Tables(0).Rows(i)("DepartureLocation") & ")</td>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td >" & OBDS.Tables(0).Rows(i)("ArrivalCityName") & "(" & OBDS.Tables(0).Rows(i)("ArrivalLocation") & ")</td>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"

            If OBDS.Tables(0).Rows(i)("DepartureTerminal").ToString().Trim() <> "" Then
                DepTerminal = "Terminal - " & OBDS.Tables(0).Rows(i)("DepartureTerminal").ToString().Trim()
            End If
            If OBDS.Tables(0).Rows(i)("ArrivalTerminal").ToString().Trim() <> "" Then
                ArrTerminal = "Terminal - " & OBDS.Tables(0).Rows(i)("ArrivalTerminal").ToString().Trim()
            End If

            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td >" & CABIN & "</td>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>" & GetAirportName(OBDS.Tables(0).Rows(i)("DepartureLocation")) & " " & DepTerminal & "</td>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>" & GetAirportName(OBDS.Tables(0).Rows(i)("ArrivalLocation")) & " " & ArrTerminal & "</td>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"
        Next
        If FT = "InBound" Then

            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr><td style='padding-top: 20px'> </td></tr>"
            ' FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr><td colspan='4' style='font-size:18px; line-height:35px; border-bottom:2px solid #d1d1d1;'>Outbound Flight Details<td><tr>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr ><td colspan='2' style='font-size:16px; line-height:35px; border-bottom:2px solid #d1d1d1;color:#004b91' >InBound Flight Details</td><td align='left' style='font-size:14px; line-height:35px; border-bottom:2px solid #d1d1d1;color:#004b91; '></td><tr>" '" & IBDS.Tables(0).Rows(0)("AdtFareType") & "
            For i As Integer = 0 To IBDS.Tables(0).Rows.Count - 1
                RBD = ""
                CABIN = ""
                If IBDS.Tables(0).Rows(i)("RBD").ToString().Trim() <> "" Then
                    RBD = "Class (" & IBDS.Tables(0).Rows(i)("RBD") & ")"
                End If
                If IBDS.Tables(0).Rows(i)("AdtCabin").ToString().Trim() <> "" Then
                    CABIN = "Cabin (" & CabinName(IBDS.Tables(0).Rows(i)("AdtCabin")) & ")"
                End If
                'FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
                'FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td><img alt='' src='../Airlogo/sm" & IBDS.Tables(0).Rows(i)("MarketingCarrier") & ".gif'/> " & IBDS.Tables(0).Rows(i)("AirlineName") & "(" & IBDS.Tables(0).Rows(i)("MarketingCarrier") & "-" & IBDS.Tables(0).Rows(i)("FlightIdentification") & ")" & "</td>"
                'FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='font-size:14px; font-weight:bold;'>" & IBDS.Tables(0).Rows(i)("DepartureLocation") & "(" & IBDS.Tables(0).Rows(i)("DepartureCityName") & ")</td>"
                'FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='font-size:14px; font-weight:bold;'>" & IBDS.Tables(0).Rows(i)("ArrivalLocation") & "(" & IBDS.Tables(0).Rows(i)("ArrivalCityName") & ")</td>"
                'FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"

                FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
                FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td colspan='2'><img alt='' src='../Airlogo/sm" & IBDS.Tables(0).Rows(i)("MarketingCarrier") & ".gif'/> " & IBDS.Tables(0).Rows(i)("AirlineName") & "</td><td align='left' style='font-size:14px; line-height:35px;color:#004b91; '>" & IBDS.Tables(0).Rows(i)("AdtFareType") & "</td>"
                FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"

                FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
                FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>(" & IBDS.Tables(0).Rows(i)("MarketingCarrier") & "-" & IBDS.Tables(0).Rows(i)("FlightIdentification") & ")" & "</td>"
                FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='font-size:14px; font-weight:bold;'>" & IBDS.Tables(0).Rows(i)("Departure_Date") & " (" & MultiValueFunction_Dom(IBDS.Tables(0), "Depall", i) & ")</td>"
                FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='font-size:14px; font-weight:bold;'>" & IBDS.Tables(0).Rows(i)("Arrival_Date") & " (" & MultiValueFunction_Dom(IBDS.Tables(0), "Arrall", i) & ")</td>"
                FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"


                FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
                FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td >" & RBD & " </td>"
                FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td >" & IBDS.Tables(0).Rows(i)("DepartureCityName") & "(" & IBDS.Tables(0).Rows(i)("DepartureLocation") & ")</td>"
                FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td >" & IBDS.Tables(0).Rows(i)("ArrivalCityName") & "(" & IBDS.Tables(0).Rows(i)("ArrivalLocation") & ")</td>"
                FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"

                'FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
                'FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td >Class(" & IBDS.Tables(0).Rows(i)("RBD") & ") </td>"
                'FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>" & IBDS.Tables(0).Rows(i)("Departure_Date") & " (" & MultiValueFunction_Dom(IBDS.Tables(0), "Depall", i) & ")</td>"
                'FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>" & IBDS.Tables(0).Rows(i)("Arrival_Date") & " (" & MultiValueFunction_Dom(IBDS.Tables(0), "Arrall", i) & ")</td>"
                'FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"
                DepTerminal = ""
                ArrTerminal = ""
                If IBDS.Tables(0).Rows(i)("DepartureTerminal").ToString().Trim() <> "" Then
                    DepTerminal = "Terminal - " & IBDS.Tables(0).Rows(i)("DepartureTerminal").ToString().Trim()
                End If
                If IBDS.Tables(0).Rows(i)("ArrivalTerminal").ToString().Trim() <> "" Then
                    ArrTerminal = "Terminal - " & IBDS.Tables(0).Rows(i)("ArrivalTerminal").ToString().Trim()
                End If
                FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
                FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>" & CABIN & "</td>"
                FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>" & GetAirportName(IBDS.Tables(0).Rows(i)("DepartureLocation")) & " " & DepTerminal & "</td>"
                FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>" & GetAirportName(IBDS.Tables(0).Rows(i)("ArrivalLocation")) & " " & ArrTerminal & "</td>"
                FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"
            Next
        End If
        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</table>"


        Return FlightDtlsTotalInfo
    End Function
    Public Function CustFltDetails_Intl(ByVal OBDS As DataSet) As String
        Dim FlightDtlsTotalInfo As String = ""
        'Dim FlightType = ""
        'If FT = "InBound" Then
        '    FlightType = "OutBound"
        'End If
        Dim DepTerminal As String
        Dim ArrTerminal As String
        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<table  width='100%' border='0' cellspacing='0' cellpadding='0'>"
        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr ><td colspan='2' style='font-size:16px; line-height:35px; border-bottom:2px solid #d1d1d1;color:#004b91' >Flight Details</td><td align='left' style='font-size:14px; line-height:35px; border-bottom:2px solid #d1d1d1;color:#004b91; '></td><tr>" '" & OBDS.Tables(0).Rows(0)("AdtFareType") & "
        For i As Integer = 0 To OBDS.Tables(0).Rows.Count - 1

            Dim RBD As String = ""
            Dim CABIN As String = ""
            If OBDS.Tables(0).Rows(i)("RBD").ToString().Trim() <> "" Then
                RBD = "Class (" & OBDS.Tables(0).Rows(i)("RBD") & ")"
            End If
            If OBDS.Tables(0).Rows(i)("AdtCabin").ToString().Trim() <> "" Then
                CABIN = "Cabin (" & CabinName(OBDS.Tables(0).Rows(i)("AdtCabin")) & ")"

            End If

            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
            If OBDS.Tables(0).Rows(i)("depdatelcc").ToString.Trim() <> "" Then
                FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td colspan='2'><img alt='' src='../Airlogo/sm" & OBDS.Tables(0).Rows(i)("MarketingCarrier") & ".gif'/> " & OBDS.Tables(0).Rows(i)("AirlineName") & "</td><td align='left' style='font-size:14px; line-height:35px;color:#004b91; '>" & OBDS.Tables(0).Rows(i)("AdtFareType") & "</td>"
            Else
                FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td colspan='2'><img alt='' src='../Airlogo/sm" & OBDS.Tables(0).Rows(i)("MarketingCarrier") & ".gif'/> " & OBDS.Tables(0).Rows(i)("AirlineName") & "</td><td align='left' style='font-size:14px; line-height:35px;color:#004b91; '></td>"

            End If
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"



            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>(" & OBDS.Tables(0).Rows(i)("MarketingCarrier") & "-" & OBDS.Tables(0).Rows(i)("FlightIdentification") & ")" & "</td>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='font-size:14px; font-weight:bold;'>" & OBDS.Tables(0).Rows(i)("Departure_Date") & " (" & MultiValueFunction_Intl(OBDS.Tables(0), "Depall", i) & ")</td>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='font-size:14px; font-weight:bold;'>" & OBDS.Tables(0).Rows(i)("Arrival_Date") & " (" & MultiValueFunction_Intl(OBDS.Tables(0), "Arrall", i) & ")</td>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"

            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td >" & RBD & "</td>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td >" & OBDS.Tables(0).Rows(i)("DepartureLocation") & "(" & OBDS.Tables(0).Rows(i)("DepartureCityName") & ")</td>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td >" & OBDS.Tables(0).Rows(i)("ArrivalLocation") & "(" & OBDS.Tables(0).Rows(i)("ArrivalCityName") & ")</td>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"
            DepTerminal = ""
            ArrTerminal = ""
            If OBDS.Tables(0).Rows(i)("DepartureTerminal").ToString().Trim() <> "" Then
                DepTerminal = "Terminal - " & OBDS.Tables(0).Rows(i)("DepartureTerminal").ToString().Trim()
            End If
            If OBDS.Tables(0).Rows(i)("ArrivalTerminal").ToString().Trim() <> "" Then
                ArrTerminal = "Terminal - " & OBDS.Tables(0).Rows(i)("ArrivalTerminal").ToString().Trim()
            End If

            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>" & CABIN & "</td>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>" & GetAirportName(OBDS.Tables(0).Rows(i)("DepartureLocation")) & " " & DepTerminal & "</td>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>" & GetAirportName(OBDS.Tables(0).Rows(i)("ArrivalLocation")) & " " & ArrTerminal & "</td>"
            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"
        Next
        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</table>"


        Return FlightDtlsTotalInfo
    End Function
    Private Function MultiValueFunction_Dom(ByVal dt As DataTable, ByVal Type As String, Optional ByVal Position As Int32 = 0, Optional ByVal dtrow As String = "") As String
        Dim OutputString As String = ""
        If (Type = "Logo") Then
            OutputString = "../Airlogo/sm" & dt.Rows(0)("MarketingCarrier") & ".gif"
        ElseIf Type = "Airline" Then
            OutputString = dt.Rows(0)("AirlineName") & "(" & dt.Rows(0)("MarketingCarrier") & "-" & dt.Rows(0)("FlightIdentification") & ")"
        ElseIf Type = "Dep" Then
            If (dt.Rows(0)("DepartureTime").ToString().Contains(":") = True) Then
                OutputString = dt.Rows(0)("DepartureTime").ToString()

            Else
                OutputString = dt.Rows(0)("DepartureTime").ToString().Substring(0, 2) & ":" & dt.Rows(0)("DepartureTime").ToString().Substring(2, 2)

            End If
        ElseIf Type = "Deprow" Then
            If (dtrow.Contains(":") = True) Then
                OutputString = dtrow

            Else
                OutputString = dtrow.Substring(0, 2) & ":" & dtrow.Substring(2, 2)

            End If

        ElseIf Type = "Arrrow" Then
            If (dtrow.Contains(":") = True) Then
                OutputString = dtrow

            Else
                OutputString = dtrow.Substring(0, 2) & ":" & dtrow.Substring(2, 2)

            End If
        ElseIf Type = "Arr" Then
            If (dt.Rows(0)("ArrivalTime").ToString().Contains(":") = True) Then
                OutputString = dt.Rows(dt.Rows.Count - 1)("ArrivalTime").ToString()

            Else
                OutputString = dt.Rows(dt.Rows.Count - 1)("ArrivalTime").ToString().Substring(0, 2) & ":" & dt.Rows(dt.Rows.Count - 1)("ArrivalTime").ToString().Substring(2, 2)

            End If
        ElseIf Type = "Depall" Then
            If (dt.Rows(Position)("DepartureTime").ToString().Contains(":") = True) Then
                OutputString = dt.Rows(Position)("DepartureTime").ToString()

            Else
                OutputString = dt.Rows(Position)("DepartureTime").ToString().Substring(0, 2) & ":" & dt.Rows(Position)("DepartureTime").ToString().Substring(2, 2)

            End If
        ElseIf Type = "Arrall" Then
            If (dt.Rows(Position)("ArrivalTime").ToString().Contains(":") = True) Then
                OutputString = dt.Rows(Position)("ArrivalTime").ToString()

            Else
                OutputString = dt.Rows(Position)("ArrivalTime").ToString().Substring(0, 2) & ":" & dt.Rows(Position)("ArrivalTime").ToString().Substring(2, 2)

            End If
        End If
        Return OutputString
    End Function
    Private Function MultiValueFunction_Intl(ByVal dt As DataTable, ByVal Type As String, Optional ByVal Position As Int32 = 0, Optional ByVal dtrow As String = "") As String
        Dim OutputString As String = ""
        If (Type = "Logo") Then
            OutputString = "../Airlogo/sm" & dt.Rows(0)("MarketingCarrier") & ".gif"
        ElseIf Type = "Airline" Then
            OutputString = dt.Rows(0)("AirlineName") & "(" & dt.Rows(0)("MarketingCarrier") & "-" & dt.Rows(0)("FlightIdentification") & ")"
        ElseIf Type = "Dep" Then
            If (dt.Rows(0)("DepartureTime").ToString().Contains(":") = True) Then
                OutputString = dt.Rows(0)("DepartureTime").ToString()

            Else
                OutputString = dt.Rows(0)("DepartureTime").ToString().Substring(0, 2) & ":" & dt.Rows(0)("DepartureTime").ToString().Substring(2, 2)

            End If
        ElseIf Type = "Deprow" Then
            If (dtrow.Contains(":") = True) Then
                OutputString = dtrow

            Else
                OutputString = dtrow.Substring(0, 2) & ":" & dtrow.Substring(2, 2)

            End If

        ElseIf Type = "Arrrow" Then
            If (dtrow.Contains(":") = True) Then
                OutputString = dtrow

            Else
                OutputString = dtrow.Substring(0, 2) & ":" & dtrow.Substring(2, 2)

            End If
        ElseIf Type = "Arr" Then
            If (dt.Rows(0)("ArrivalTime").ToString().Contains(":") = True) Then
                OutputString = dt.Rows(dt.Rows.Count - 1)("ArrivalTime").ToString()

            Else
                OutputString = dt.Rows(dt.Rows.Count - 1)("ArrivalTime").ToString().Substring(0, 2) & ":" & dt.Rows(dt.Rows.Count - 1)("ArrivalTime").ToString().Substring(2, 2)

            End If
        ElseIf Type = "Depall" Then
            If (dt.Rows(Position)("DepartureTime").ToString().Contains(":") = True) Then
                OutputString = dt.Rows(Position)("DepartureTime").ToString()

            Else
                OutputString = dt.Rows(Position)("DepartureTime").ToString().Substring(0, 2) & ":" & dt.Rows(Position)("DepartureTime").ToString().Substring(2, 2)

            End If
        ElseIf Type = "Arrall" Then
            If (dt.Rows(Position)("ArrivalTime").ToString().Contains(":") = True) Then
                OutputString = dt.Rows(Position)("ArrivalTime").ToString()

            Else
                OutputString = dt.Rows(Position)("ArrivalTime").ToString().Substring(0, 2) & ":" & dt.Rows(Position)("ArrivalTime").ToString().Substring(2, 2)

            End If
        End If
        Return OutputString
    End Function
    Private Function CabinName(ByVal Cabin As String) As String
        Dim C As String = ""
        If Cabin.ToUpper().Trim() = "E" Or Cabin.ToUpper().Trim() = "Y" Then
            C = "ECONOMY"
        ElseIf Cabin.ToUpper().Trim() = "F" Then
            C = "FIRST"
        ElseIf Cabin.ToUpper().Trim() = "B" Then
            C = "BUSINESS"
        ElseIf Cabin.ToUpper().Trim() = "W" Then
            C = "Premium Economy"
        Else
            C = Cabin.ToUpper().Trim()
        End If
        Return C
    End Function

    'Ledger Entry for Hotel and other module
    Public Function LedgerEntry_Common(ByVal TRACKID As String, ByVal DebitAmount As Double, ByVal CreditAmount As Double, ByVal AvailBal As Double, ByVal VC As String, ByVal GDSPNR As String, ByVal TicketNo As String, _
                                                     ByVal BookingType As String, ByVal AGENTID As String, ByVal AGENCYNAME As String, ByVal BookedBy As String, ByVal IP As String, ByVal ProjectId As String, _
                                                     ByVal BillNo As String, ByVal EasyID As String, ByVal Remark As String, ByVal TRIP As String, ByVal PaxID As Integer) As Integer
        paramHashtable.Clear()
        paramHashtable.Add("@TRACKID", TRACKID)
        paramHashtable.Add("@DebitAmount", DebitAmount)
        paramHashtable.Add("@CreditAmount", CreditAmount)
        paramHashtable.Add("@Provider", VC)
        paramHashtable.Add("@PNRNO", GDSPNR)
        paramHashtable.Add("@TicketNo", TicketNo)
        paramHashtable.Add("@BookingType", BookingType)
        paramHashtable.Add("@AGENTID", AGENTID)
        paramHashtable.Add("@AGENCYNAME", AGENCYNAME)
        paramHashtable.Add("@IP", IP)
        paramHashtable.Add("@ProjectId", ProjectId)
        paramHashtable.Add("@BookedBy", BookedBy)
        paramHashtable.Add("@BillNo", BillNo)
        paramHashtable.Add("@AVAILBAL", AvailBal)
        paramHashtable.Add("@EasyID", EasyID)
        paramHashtable.Add("@Remark", Remark)
        paramHashtable.Add("@TRIP", TRIP)
        paramHashtable.Add("@PaxID", PaxID)
        'Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "SP_INSERT_LEDGER_CommonModule", 1)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "SP_INSERT_LEDGER_CommonModule_PP", 1)
    End Function
    'Update Ledger by paxId

    Public Function GetTicketingProvider(ByVal orderID As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@OrderID", orderID)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "usp_Get_TicketingProvider", 3)
    End Function

    Public Function CheckAgentUserId(ByVal UserId As String, ByVal Action As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@UserId", UserId)
        paramHashtable.Add("@Action", Action)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "CheckAgentUserId", 3)
    End Function

  Public Function GetLedgerDetailsNew(ByVal usertype As String, ByVal LoginID As String, ByVal FormDate As String, ByVal ToDate As String, ByVal AgentId As String, ByVal BookingType As String, ByVal SearchType As String, ByVal PaymentMode As String) As DataSet
        paramHashtable.Clear()
        paramHashtable.Add("@usertype", usertype)
        paramHashtable.Add("@LoginID", LoginID)
        paramHashtable.Add("@FormDate", FormDate)
        paramHashtable.Add("@ToDate", ToDate)
        paramHashtable.Add("@AgentId", AgentId)
        paramHashtable.Add("@BookingType", BookingType)
        paramHashtable.Add("@SearchType", SearchType)
        paramHashtable.Add("@PaymentMode", PaymentMode)
        'Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetLedgerDetail", 3)
        Return objDataAcess.ExecuteData(Of Object)(paramHashtable, True, "GetLedgerDetail_PPNEW", 3)
    End Function

End Class



